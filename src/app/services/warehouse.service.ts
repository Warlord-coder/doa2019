﻿import {Injectable} from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {environment} from '../../environments/environment';
import {first} from 'rxjs/operators';
import { Observable, Subject } from 'rxjs';

@Injectable({providedIn: 'root'})
export class WarehouseService {
  constructor(private http: HttpClient) {
  }

  getAllLab(isAlert) {
    let url = `${environment.apiUrl}whs/labs/list`;
    if (isAlert) {
      url = `${url}?isAlert=${isAlert}`;
    }
    return this.http.get<any[]>(url);
  }

  getList() {
    const url = `${environment.apiUrl}whs/list`;
    return this.http.get<any[]>(url);
  }
  getAll() {
    const url = `${environment.apiUrl}whs/getAll`;
    return this.http.get<any[]>(url);
  }
  getFilterList(filter) {
    return this.http.post(`${environment.apiUrl}whs/filterList`, filter);
  }
  
  getAlertFilterList(filter) {
    return this.http.post(`${environment.apiUrl}whs/alertFilterList`, filter);
  }
  alertFilterNotDraftList(filter) {
    return this.http.post(`${environment.apiUrl}whs/alertFilterNotDraftList`, filter);
  }
  alertFilterDraftList(filter) {
    return this.http.post(`${environment.apiUrl}whs/alertFilterDraftList`, filter);
  }
  getM2FilterList(filter) {
    return this.http.post(`${environment.apiUrl}whs/m2FilterList`, filter);
  }

  getListAll() {
    const url = `${environment.apiUrl}whs/listAll`;
    return this.http.get<any[]>(url);
  }
  getAllAlertPlant(){
    return this.http.post(`${environment.apiUrl}whs/listAllAlertPLant`, {});
  }
  getSpecificLab(id, status = '', submitTo = null, isAlert = null) {
    let url = `${environment.apiUrl}whs/labs/${id}`;
    if (status) {
      url = `${url}?status=${status}`;
    }
    if (submitTo) {
      url = `${url}${status ? '&' : '?'}submitTo=${submitTo}`;
    }
    if (isAlert) {
      url = `${url}${status ? '&' : '?'}isAlert=${isAlert}`;
    }


    return this.http.get<any[]>(url);
  }
  getStoreDate(id){
    return this.http.post(`${environment.apiUrl}whs/storeDate`, {id:id});
  }

  getSpecific(id: string, isAlert = null , withdraw = null) {
    let url = `${environment.apiUrl}whs/${id}`;
    if (isAlert) {
      url = `${url}?isAlert=${isAlert}`;
    }
    if (withdraw) {
      url = `${url}?withdraw=${isAlert}`;
    }

    return this.http.get<any[]>(url);
  }
  get_id_by_reg_gene_id(reg_gene_id){
    return this.http.post(`${environment.apiUrl}whs/getIdByRegGeneId`, {id:reg_gene_id});
  }
  createOrUpdate(role: any, id) {

    //let formData = new FormData();
    let formData = new FormData();
    Object.keys(role).forEach(key => {
      if (Array.isArray(role[key])) {
        role[key].forEach(value => {
          formData.append(key, value);
        });
      } else {
        if(key == "attribute") {
          formData.append(key, JSON.stringify(role[key]));
        } else {
          formData.append(key, role[key]);
        }
        
      }
    });	
    if (!id || id === 'create') {
      return this.http.post(`${environment.apiUrl}whs/create`, formData);
    }

    return this.http.put(`${environment.apiUrl}whs/${id}`, formData);
  }

  delete(id: number) {
    return this.http.delete(`${environment.apiUrl}whs/${id}`);
  }

  submitToModule(id, data) {
    return this.http.put(`${environment.apiUrl}whs/module/${id}`, data);
  }
  getAlertList() {	
    const url = `${environment.apiUrl}whs/alert-lists`;	
    return this.http.post(url, {});	
  }	
  getAlertListM4() {	
    const url = `${environment.apiUrl}whs/alert-listsM4`;	
    return this.http.post(url, {});	
  }	
  getLabAlertM9(status) {	
    return this.http.post<any[]>(`${environment.apiUrl}lab-alert-forms/list-alertM9`,{status:status});	
  }
  gsNoSearch(keyword) {
    return this.http.post(`${environment.apiUrl}whs/gsNoSearch`, {keyword: keyword});
  }
  regNoSearch(keyword) {
    return this.http.post(`${environment.apiUrl}whs/regNoSearch`, {keyword: keyword});
  }
  exportDoa(body: object) {
    return this.http.post<any[]>(`${environment.apiUrl}whs/exportDoa`, body);
  }
  public stockWatch = new Subject<JSON>();
  public emitStockData(val) {
    this.stockWatch.next(val);
  }
}
