import {Injectable} from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {BehaviorSubject, Observable} from 'rxjs';
import {map} from 'rxjs/operators';
import {environment} from '../../environments/environment';

@Injectable({providedIn: 'root'})
export class AuthService {
  private currentUserSubject: BehaviorSubject<any>;
  public currentUser: Observable<any>;

  constructor(private http: HttpClient) {
    this.currentUserSubject = new BehaviorSubject<any>(JSON.parse(localStorage.getItem('currentUser')));
    this.currentUser = this.currentUserSubject.asObservable();
  }

  public get currentUserValue(): any {
    return this.currentUserSubject.value;
  }

  login(email: string, password: string) {
    return this.http.post<any>(`${environment.apiUrl}users/login`, {email, password})
      .pipe(map(response => {
        const {data: user} = response;
        const {authToken, role, isSuperAdmin} = user;
        if (authToken) {
          localStorage.setItem('authorization', authToken);
          localStorage.setItem('role', role);
          localStorage.setItem('superAdmin', isSuperAdmin);
        }
        this.currentUserSubject.next(user);
        return user;
      }));
  }

  me() {
    return this.http.get<any>(`${environment.apiUrl}users/me`)
      .pipe(map(response => {
        const {data: user} = response;
        this.currentUserSubject.next(user);
        return user;
      }));
  }

  logout() {
    localStorage.removeItem('currentUser');
    this.currentUserSubject.next(null);
  }
}
