﻿import {Injectable} from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {environment} from '../../environments/environment';

@Injectable({providedIn: 'root'})
export class LabService {
  constructor(private http: HttpClient) {
  }

  getAll(isAlert = null) {
    let url = `${environment.apiUrl}labs/list`;
    if (isAlert) {
      url = `${url}?isAlert=${isAlert}`;
    }

    return this.http.get<any[]>(url);
  }
  getAllM8(isAlert = null) {
    let url = `${environment.apiUrl}labs/listM8`;
    if (isAlert) {
      url = `${url}?isAlert=${isAlert}`;
    }

    return this.http.get<any[]>(url);
  }

  getSpecific(id: string) {
    return this.http.get<any[]>(`${environment.apiUrl}labs/${id}`);
  }

  getAllPendingAlertPlants() {
    return this.http.get<any[]>(`${environment.apiUrl}labs/list/alert-plants`);
  }

  createOrUpdate(role: any, id) {
    if (!id || id === 'create') {
      return this.http.post(`${environment.apiUrl}labs/create`, role);
    }

    return this.http.put(`${environment.apiUrl}labs/${id}`, role);
  }

  delete(id: number) {
    return this.http.delete(`${environment.apiUrl}labs/${id}`);
  }


  submit(labId, plantIds = []) {
    return this.http.post(`${environment.apiUrl}labs/submit/${labId}`, {plantIds});
  }

  submitAlertPlant(labId, plantIds = []) {
    return this.http.post(`${environment.apiUrl}labs/submit-alert-plantM9/${labId}`, {plantIds});
  }
  getAlertM9() {
    return this.http.post(`${environment.apiUrl}labs/list/alertM9`,{});
  }


  submitToModule9(plantIds = []) {
    return this.http.post(`${environment.apiUrl}labs/submit-m9`, {plantIds});
  }
}
