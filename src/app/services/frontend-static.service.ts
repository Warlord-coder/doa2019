﻿import {Injectable} from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {environment} from '../../environments/environment';

@Injectable({providedIn: 'root'})
export class FrontendStaticService {
  constructor(private http: HttpClient) {
  }

  getAll(isAlert = null, type) {
    let url = `${environment.apiUrl}${type}/list`;
    if (isAlert) {
      url = `${url}?isAlert=${isAlert}`;
    }

    return this.http.get<any[]>(url);
  }

  getSpecific(id: string, type) {
    return this.http.get<any[]>(`${environment.apiUrl}${type}/${id}`);
  }

  getAllPendingAlertPlants() {
    return this.http.get<any[]>(`${environment.apiUrl}labs/list/alert-plants`);
  }

  createOrUpdate(role: any, id, type = '', isFormData = true) {
    const formData = isFormData ? new FormData() : role;
    if (isFormData) {
      Object.keys(role).forEach(key => {
        if (Array.isArray(role[key])) {
          role[key].forEach(value => {
            formData.append(key, value);
          });
        } else {
          formData.append(key, role[key]);
        }
      });
    }
    if (!id || id === 'create' || id === 'register') {
      return this.http.post(`${environment.apiUrl}${type}/${id}`, formData);
    }

    return this.http.put(`${environment.apiUrl}${type}/${id}`, formData);
  }

  delete(id: number, type) {
    return this.http.delete(`${environment.apiUrl}${type}/${id}`);
  }


  submit(labId, plantIds = []) {
    return this.http.post(`${environment.apiUrl}labs/submit/${labId}`, {plantIds});
  }

  submitAlertPlant(labId, plantIds = []) {
    return this.http.post(`${environment.apiUrl}labs/submit-alert-plant/${labId}`, {plantIds});
  }


  submitToModule9(plantIds = []) {
    return this.http.post(`${environment.apiUrl}labs/submit-m9`, {plantIds});
  }
}
