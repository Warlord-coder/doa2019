﻿import {Injectable} from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {environment} from '../../environments/environment';

@Injectable({providedIn: 'root'})
export class PlantGroupsService {
  constructor(private http: HttpClient) {
  }

  getAll(hideDeletedEntries = false) {
    let url = `${environment.apiUrl}plant-groups/list`;
    if (hideDeletedEntries) {
      url = `${url}?hideDeletedEntries=true`;
    }
    return this.http.get<any[]>(url);
  }

  getSpecific(id: string) {
    return this.http.get<any[]>(`${environment.apiUrl}plant-groups/${id}`);
  }

  createOrUpdate(role: any, id) {
    if (id === 'create') {
      return this.http.post(`${environment.apiUrl}plant-groups/create`, role);
    }

    return this.http.put(`${environment.apiUrl}plant-groups/${id}`, role);
  }

  delete(id: number) {
    return this.http.delete(`${environment.apiUrl}plant-groups/${id}`);
  }
}
