﻿import {Injectable} from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {environment} from '../../environments/environment';

@Injectable({providedIn: 'root'})
export class CommonUnitService {
  constructor(private http: HttpClient) {
  }

  getAll(hideDeletedEntries = false) {
    let url = `${environment.apiUrl}common-unit/list`;
    if (hideDeletedEntries) {
      url = `${url}?hideDeletedEntries=true`;
    }
    return this.http.get<any[]>(url);
  }

  getSpecific(id: string) {
    return this.http.get<any[]>(`${environment.apiUrl}common-unit/${id}`);
  }

  createOrUpdate(role: any, id) {
    if (id === 'create') {
      return this.http.post(`${environment.apiUrl}common-unit/create`, role);
    }

    return this.http.put(`${environment.apiUrl}common-unit/${id}`, role);
  }

  delete(id: number) {
    return this.http.delete(`${environment.apiUrl}common-unit/${id}`);
  }
}
