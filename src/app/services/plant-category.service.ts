﻿import {Injectable} from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {environment} from '../../environments/environment';
import {map} from 'rxjs/operators';

@Injectable({providedIn: 'root'})
export class PlantCategoryService {
  constructor(private http: HttpClient) {
  }

  getAll(hideDeletedEntries = false) {
    let url = `${environment.apiUrl}plant-categories/list`;
    if (hideDeletedEntries) {
      url = `${url}?hideDeletedEntries=true`;
    }
    return this.http.get<any[]>(url);
  }

  getSpecific(id: string) {
    return this.http.get<any[]>(`${environment.apiUrl}plant-categories/${id}`);
  }

  createOrUpdate(role: any, id) {
    if (id === 'create') {
      return this.http.post(`${environment.apiUrl}plant-categories/create`, role);
    }

    return this.http.put(`${environment.apiUrl}plant-categories/${id}`, role);
  }

  delete(id: string) {
    return this.http.delete(`${environment.apiUrl}plant-categories/${id}`);
  }

  search(keyword: string) {
    return this.http.post(`${environment.apiUrl}plant-categories/search`, {keyword: keyword});
  }
}
