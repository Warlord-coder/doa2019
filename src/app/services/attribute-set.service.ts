import {Injectable} from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {environment} from '../../environments/environment';

@Injectable({providedIn: 'root'})
export class AttributeSetService {
  constructor(private http: HttpClient) {
  }

  getAll() {
    return this.http.get<any[]>(`${environment.apiUrl}attribute-sets/list`);
  }

  getSpecific(id: string) {
    return this.http.get<any[]>(`${environment.apiUrl}attribute-sets/${id}`);
  }
  
  getFilterList(filter = {}){
    let url = `${environment.apiUrl}attribute-sets/filterlist`;
    return this.http.post(url, filter);
  }

  createOrUpdate(role: any, id) {
    if (id === 'create') {
      return this.http.post(`${environment.apiUrl}attribute-sets/${id}`, role);
    }

    return this.http.put(`${environment.apiUrl}attribute-sets/${id}`, role);
  }

  delete(id: number) {
    return this.http.delete(`${environment.apiUrl}common-unit/${id}`);
  }
}
