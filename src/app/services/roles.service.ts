﻿import {Injectable} from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {environment} from '../../environments/environment';
import {map} from 'rxjs/operators';

@Injectable({providedIn: 'root'})
export class RoleService {
  constructor(private http: HttpClient) {
  }

  getAll() {
    return this.http.get<any[]>(`${environment.apiUrl}roles/list`);
  }

  getSpecific(id: string) {
    return this.http.get<any[]>(`${environment.apiUrl}roles/${id}`);
  }

  createOrUpdate(role: any, id) {
    if (!id || id === 'create') {
      return this.http.post(`${environment.apiUrl}roles/create`, role);
    }

    return this.http.put(`${environment.apiUrl}roles/${id}`, role);
  }

  delete(id: number) {
    return this.http.delete(`${environment.apiUrl}roles/${id}`);
  }
}
