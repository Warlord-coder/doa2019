﻿import {Injectable} from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {environment} from '../../environments/environment';

@Injectable({providedIn: 'root'})
export class PlantRegisterService {
  constructor(private http: HttpClient) {
  }

  getAll(volume, listing = null, status = null, formtype = null, labId = null) {
    let url = `${environment.apiUrl}plant-register/list/${volume}`;
    if (status) {
      url = `${url}?status=${status}`;
    }

    if (listing) {
      url = `${url}${status ? '&' : '?'}listing=${listing}`;
    }
    if (formtype) {
      url = `${url}${url.includes('?') ? '&' : '?'}form=${formtype}`;
    }
    if (labId) {
      url = `${url}${url.includes('?') ? '&' : '?'}labId=${labId}`;
    }
    return this.http.get<any[]>(url);
  }

  getRegSenderAutoComplete() {
    return this.http.get<any[]>(`${environment.apiUrl}plant-register/list/sender-autocomplete`);
  }

  getSpecific(id: string, form = null) {
    let url = `${environment.apiUrl}plant-register/${id}`;
    if (form) {
      url = `${url}?form=${form}`;
    }
    return this.http.get<any[]>(url);
  }

  createOrUpdate(role: any, id) {
    const formData = new FormData();
    Object.keys(role).forEach(key => {
      if (Array.isArray(role[key])) {
        role[key].forEach(value => {
          formData.append(key, value);
        });
      } else {
        formData.append(key, role[key]);
      }
    });

    if (!id || id === 'create') {
      return this.http.post(`${environment.apiUrl}plant-register/create`, formData);
    }

    return this.http.put(`${environment.apiUrl}plant-register/${id}`, formData);
  }

  delete(id: number) {
    return this.http.delete(`${environment.apiUrl}plant-register/${id}`);
  }

  submitToModule8(plantIds = []) {
    return this.http.post(`${environment.apiUrl}plant-register/submit-m8`, {
      plantIds
    });
  }
  getDepartmentTypes() {
    return this.http.get(
      `${environment.apiUrl}plant-register/getDepartmentTypes`
    );
  }
  filter(search) {
    return this.http.post(`${environment.apiUrl}plant-register/filter`, {filter: search});
  }
  filterLabStatus(search) {
    return this.http.post(`${environment.apiUrl}plant-register/filterLabStatus`, {filter: search});
  }
}
