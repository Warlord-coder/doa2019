﻿import {Injectable} from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {environment} from '../../environments/environment';

@Injectable({providedIn: 'root'})
export class PlantTypeService {
  constructor(private http: HttpClient) {
  }

  getAll(hideDeletedEntries = false) {
    let url = `${environment.apiUrl}plant-types/list`;
    if (hideDeletedEntries) {
      url = `${url}?hideDeletedEntries=true`;
    }
    return this.http.get<any[]>(url);
  }

  getFilterList(filter = {}){
    let url = `${environment.apiUrl}plant-types/filterlist`;
    return this.http.post(url, filter);
  }

  getSpecific(id: string) {
    return this.http.get<any[]>(`${environment.apiUrl}plant-types/${id}`);
  }

  createOrUpdate(role: any, id) {
    if (!id || id === 'create') {
      return this.http.post(`${environment.apiUrl}plant-types/create`, role);
    }

    return this.http.put(`${environment.apiUrl}plant-types/${id}`, role);
  }

  delete(id: number) {
    return this.http.delete(`${environment.apiUrl}plant-types/${id}`);
  }

  search(keyword: string, category_id: string) {
    return this.http.post(`${environment.apiUrl}plant-types/search`, {keyword: keyword, category_id: category_id});
  }
}
