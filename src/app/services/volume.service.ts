﻿import {Injectable} from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {environment} from '../../environments/environment';
import {numericIndexGetter} from '@swimlane/ngx-datatable/release/utils';

@Injectable({providedIn: 'root'})
export class VolumeService {
  constructor(private http: HttpClient) {
  }

  getAll(id, filter = null) {
    let url = `${environment.apiUrl}volumes/list/${id}`;
    if (filter) {
      url = `${url}?filter=${filter}`;
    }

    return this.http.get<any[]>(url);
  }

  getSpecific(id: string) {
    return this.http.get<any[]>(`${environment.apiUrl}volumes/${id}`);
  }

  createOrUpdate(data: any, id) {
    if (!id || id === 'create') {
      return this.http.post(`${environment.apiUrl}volumes/create`, data);
    }

    return this.http.put(`${environment.apiUrl}volumes/${id}`, data);
  }

  delete(id: number) {
    return this.http.delete(`${environment.apiUrl}volumes/${id}`);
  }
}
