﻿import {Injectable} from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {environment} from '../../environments/environment';

@Injectable({providedIn: 'root'})
export class UserService {
  constructor(private http: HttpClient) {
  }

  getAll() {
    return this.http.get<any[]>(`${environment.apiUrl}users/list`);
  }

  createOrUpdate(user: any, id) {
    const formData = new FormData();
    Object.keys(user).forEach(key => {
      formData.append(key, user[key]);
    });

    if (!id || id === 'create') {
      return this.http.post(`${environment.apiUrl}users/register`, formData);
    }

    return this.http.put(`${environment.apiUrl}users/${id}`, formData);
  }

  getSpecific(id: string) {
    return this.http.get(`${environment.apiUrl}users/${id}`);
  }

  delete(id: string) {
    return this.http.delete(`${environment.apiUrl}users/${id}`);
  }
}
