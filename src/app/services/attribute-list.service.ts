import {Injectable} from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {environment} from '../../environments/environment';

@Injectable({providedIn: 'root'})
export class AttributeListService {
  constructor(private http: HttpClient) {
  }

  getAll() {
    return this.http.get<any[]>(`${environment.apiUrl}attribute-lists/list`);
  }

  getFilterList(filter = {}){
    let url = `${environment.apiUrl}attribute-lists/filterlist`;
    return this.http.post(url, filter);
  }

  getSpecific(id: string) {
    return this.http.get<any[]>(`${environment.apiUrl}attribute-lists/${id}`);
  }

  createOrUpdate(role: any, id) {
    if (!id || id === 'create') {
      return this.http.post(`${environment.apiUrl}attribute-lists/create`, role);
    }

    return this.http.put(`${environment.apiUrl}attribute-lists/${id}`, role);
  }

  delete(id: number) {
    return this.http.delete(`${environment.apiUrl}attribute-lists/${id}`);
  }
}
