﻿import {Injectable} from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {environment} from '../../environments/environment';

@Injectable({providedIn: 'root'})
export class PlantService {
  constructor(private http: HttpClient) {
  }

  getAll() {
    return this.http.get<any[]>(`${environment.apiUrl}plants/list`);
  }

  getSpecific(id: string) {
    return this.http.get<any[]>(`${environment.apiUrl}plants/${id}`);
  }

  createOrUpdate(role: any, id) {
    if (!id || id === 'create') {
      return this.http.post(`${environment.apiUrl}plants/create`, role);
    }

    return this.http.put(`${environment.apiUrl}plants/${id}`, role);
  }

  delete(id: number) {
    return this.http.delete(`${environment.apiUrl}plants/${id}`);
  }
}
