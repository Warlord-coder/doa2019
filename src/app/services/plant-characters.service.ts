﻿import {Injectable} from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {environment} from '../../environments/environment';

@Injectable({providedIn: 'root'})
export class PlantCharacterService {
  constructor(private http: HttpClient) {
  }

  getAll(hideDeletedEntries = false) {
    let url = `${environment.apiUrl}plant-characters/list`;
    if (hideDeletedEntries) {
      url = `${url}?hideDeletedEntries=true`;
    }
    return this.http.get<any[]>(url);
  }

  getFilterList(filter = {}){
    let url = `${environment.apiUrl}plant-characters/filterlist`;
    return this.http.post(url, filter);
  }

  getSpecific(id: string) {
    return this.http.get<any[]>(`${environment.apiUrl}plant-characters/${id}`);
  }

  createOrUpdate(role: any, id) {
    if (id === 'create') {
      return this.http.post(`${environment.apiUrl}plant-characters/create`, role);
    }

    return this.http.put(`${environment.apiUrl}plant-characters/${id}`, role);
  }

  delete(id: number) {
    return this.http.delete(`${environment.apiUrl}plant-characters/${id}`);
  }
}
