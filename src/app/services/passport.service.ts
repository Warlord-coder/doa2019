﻿import {Injectable} from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {environment} from '../../environments/environment';

@Injectable({providedIn: 'root'})
export class PassportService {
  constructor(private http: HttpClient) {
  }

  getAll(status = 'draft') {
    return this.http.get<any[]>(`${environment.apiUrl}passports/list?status=${status}`);
  }

  getSpecific(id: string) {
    return this.http.get<any[]>(`${environment.apiUrl}passports/${id}`);
  }

  createOrUpdate(data: any, id) {
    const formData = new FormData();
    Object.keys(data).forEach(key => {
      formData.append(key, data[key]);
    });

    if (!id || id === 'create') {
      return this.http.post(`${environment.apiUrl}passports/create`, formData);
    }

    return this.http.put(`${environment.apiUrl}passports/${id}`, formData);
  }

  delete(id: number) {
    return this.http.delete(`${environment.apiUrl}passports/${id}`);
  }
  exportExcel(startDt, endDt) {
    return this.http.get<any[]>(`${environment.apiUrl}passports/export-excel?startDt=${startDt}&endDt=${endDt}`);
  }
}
