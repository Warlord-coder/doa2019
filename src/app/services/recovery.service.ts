﻿import {Injectable} from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {environment} from '../../environments/environment';

@Injectable({providedIn: 'root'})
export class RecoveryService {
  constructor(private http: HttpClient) {
  }

  getAlertRecoveryM9() {
    return this.http.post<any[]>(`${environment.apiUrl}whs/alert-lists-recovery`,{});
  }

  getAll() {
    return this.http.get<any[]>(`${environment.apiUrl}recovers/list`);
  }

  getAllPending() {
    return this.http.get<any[]>(`${environment.apiUrl}recovers/list-pending`);
  }
  getAllPendingM4() {
    return this.http.get<any[]>(`${environment.apiUrl}recovers/list-M4pending`);
  }
  getRecoveryByStatus(status) {
    return this.http.post<any[]>(`${environment.apiUrl}recovers/list-m4?status=${status}`,{status:status});
  }


  getPlants(id: any, status = 'draft') {
    return this.http.get<any[]>(`${environment.apiUrl}recovers/plants/${id}?status=${status}`);
  }

  getSpecific(id: string) {
    return this.http.get<any[]>(`${environment.apiUrl}recovers/${id}`);
  }

  submitToRecovery(recoverId: string, plantIds = [],status:string = null) {	
    return this.http.post<any[]>(`${environment.apiUrl}recovers/${recoverId}`, {plantIds,status:status});	
  }
  submitToRecoveryPending(recoverId: string, plantIds = []) {
    return this.http.post<any[]>(`${environment.apiUrl}recovers/submitToPending/${recoverId}`, {plantIds});
  }
  
  submitToRegisterModule(volumeId: string, plantIds = []) {
    return this.http.post<any[]>(`${environment.apiUrl}recovers/submit-register-module/${volumeId}`, {plantIds});
  }

  listCompleteRecoveryRegister() {
    return this.http.get<any[]>(`${environment.apiUrl}recovers/list-complete-recovery-register`);
  }
  getRecoveryAlertM9(id) {
    return this.http.get<any[]>(`${environment.apiUrl}recovers/list-alertM9/${id}`,{});
  }

  changeRecoveryStatus(status, plants) {
    return this.http.put<any[]>(`${environment.apiUrl}recovers/plants/change-status`, {plants, status});
  }

  markRecoveryStatusComplete(recoverId: string, plantIds = []) {
    return this.http.post<any[]>(`${environment.apiUrl}recovers/mark-complete/${recoverId}`, {plantIds});
  }

  updateStatusM9(id: string, updatedInfo = {}) {
    return this.http.put<any[]>(`${environment.apiUrl}recovers/setStock/${id}`, updatedInfo);
  }

  createOrUpdate(data: any, id) {
    const formData = new FormData();
    Object.keys(data).forEach(key => {
      formData.append(key, data[key]);
    });

    if (!id || id === 'create') {
      return this.http.post(`${environment.apiUrl}recovers/create`, formData);
    }

    return this.http.put(`${environment.apiUrl}recovers/${id}`, formData);
  }

  delete(id: number) {
    return this.http.delete(`${environment.apiUrl}recovers/${id}`);
  }

  getByPlace(filter) {
    let url = `${environment.apiUrl}recovers/getByPlace`;
    return this.http.post(url, filter);
  }
  getByCategory(filter) {
    let url = `${environment.apiUrl}recovers/getByCategory`;
    return this.http.post(url, filter);
  }
}
