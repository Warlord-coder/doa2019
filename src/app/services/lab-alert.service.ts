﻿import {Injectable} from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {environment} from '../../environments/environment';

@Injectable({providedIn: 'root'})
export class LabAlertFormService {
  constructor(private http: HttpClient) {
  }

  getAll(labId, status = 'draft') {
    return this.http.get<any[]>(`${environment.apiUrl}lab-alert-forms/list/${labId}?status=${status}`);
  }

  getAllAlert() {
    return this.http.get<any[]>(`${environment.apiUrl}lab-alert-forms/list-alert`);
  }

  getTotalAlert() {
    return this.http.get<any[]>(`${environment.apiUrl}lab-alert-forms/getAll`);
  }

  getAllAlertM9() {
    return this.http.post<any[]>(`${environment.apiUrl}whs/alert-lists`,{});
  }
  
  getLabAlertM9(id) {
    return this.http.get<any[]>(`${environment.apiUrl}lab-alert-forms/list-alertM9/${id}`,{});
  }
  updateAlertM9(id: string, updatedInfo = {}) {
    return this.http.put<any[]>(`${environment.apiUrl}lab-alert-forms/setStock/${id}`, updatedInfo);
  }
  
  getSpecific(id) {
    return this.http.get<any[]>(`${environment.apiUrl}lab-alert-forms/${id}`);
  }
  getSpecificGrowth(id) {
    return this.http.post<any[]>(`${environment.apiUrl}lab-alert-forms/getGrowth`,{id:id});
  }

  update(data, id) {
    return this.http.put<any[]>(`${environment.apiUrl}lab-alert-forms/${id}`, data);
  }

  submitAlertPlant(labId, plantIds = []) {
    return this.http.post(`${environment.apiUrl}labs/submit-alert-plant/${labId}`, {plantIds});
  }


  submitToModule9(plantIds = []) {
    return this.http.post(`${environment.apiUrl}lab-alert-forms/submit-m9`, {plantIds});
  }
  submitToModule9Complete(plantIds = []) {
    return this.http.post(`${environment.apiUrl}lab-alert-forms/submit-m9-complete`, {plantIds});
  }

  getSpecificId(id) {	
    return this.http.get<any[]>(`${environment.apiUrl}lab-alert-forms/id/${id}`);	
  }	

}
