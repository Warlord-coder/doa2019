﻿import {Injectable} from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {environment} from '../../environments/environment';

@Injectable({providedIn: 'root'})
export class WithdrawService {
  constructor(private http: HttpClient) {
  }

  getUnassignedListAll() {
    return this.http.get<any[]>(`${environment.apiUrl}withdraw/unassigned-list`);
  }

  getAll(module = '') {
    let url = `${environment.apiUrl}withdraw/list`;
    if (module) {
      url = `${url}?module=${module}`;
    }
    return this.http.get<any[]>(url);
  }

  getAllWithdrawPlants() {
    let url = `${environment.apiUrl}withdraw/withdrawplantlist`;
    return this.http.get<any[]>(url);
  }

  getrequestAll(module = '') {
    let url = `${environment.apiUrl}withdraw/requestlist`;
    if (module) {
      url = `${url}?module=${module}`;
    }
    return this.http.get<any[]>(url);
  }
  getAllPlants(id, status = 'draft') {
    return this.http.get<any[]>(`${environment.apiUrl}withdraw/plants/list/${id}?status=${status}`);
  }

  getAllPlantsByStatus(status = 'draft') {
    return this.http.post<any[]>(`${environment.apiUrl}withdraw/plants/list-status`,{status:status});
  }

  completePlants(data, id) {
    const formData = new FormData();
    for (const key in data) {
      if (Array.isArray(data[key])) {
        if (key !== 'file') {
          data[key].forEach(value => formData.append(key + '[]', typeof value == 'object' ? JSON.stringify(value) : value));
        } else {
          data[key].forEach(value => formData.append(key, value));
        }
      } else {
        formData.append(key, data[key]);
      }
    }

    if (!id || id === 'create') {
      return this.http.post(`${environment.apiUrl}withdraw/plants/complete/create`, formData);
    }

    return this.http.put(`${environment.apiUrl}withdraw/plants/complete/${id}`, formData);
  }

  getAllPending() {
    return this.http.get<any[]>(`${environment.apiUrl}withdraw/unassigned-list`);
  }

  getPlants(id: any, status = 'draft') {
    return this.http.get<any[]>(`${environment.apiUrl}withdraw/plants/${id}?status=${status}`);
  }

  getSpecific(id: string) {
    return this.http.get<any[]>(`${environment.apiUrl}withdraw/${id}`);
  }

  getSpecificPlant(id: string) {
    return this.http.get<any[]>(`${environment.apiUrl}withdraw/plants/${id}`);
  }

  submitToWithDraw(recoverId: string, plantIds = []) {
    return this.http.post<any[]>(`${environment.apiUrl}withdraw/${recoverId}`, {plantIds});
  }

  updatePlant(id: string, updatedInfo = {}) {
    return this.http.put<any[]>(`${environment.apiUrl}withdraw/plants/${id}`, updatedInfo);
  }

  changeWithDrawStatus(status, plants) {
    return this.http.put<any[]>(`${environment.apiUrl}withdraw/plants/change-status`, {plants, status});
  }

  createOrUpdate(data: any, id) {
    const formData = new FormData();
    Object.keys(data).forEach(key => {
      formData.append(key, data[key]);
    });

    if (!id || id === 'create') {
      return this.http.post(`${environment.apiUrl}withdraw/create`, formData);
    }

    return this.http.put(`${environment.apiUrl}withdraw/${id}`, formData);
  }

  delete(id: number) {
    return this.http.delete(`${environment.apiUrl}withdraw/${id}`);
  }
  getByDepartment(data) {
    return this.http.post(`${environment.apiUrl}withdraw/getByDepartment`, data);
  }
  getByPlantType(data) {
    return this.http.post(`${environment.apiUrl}withdraw/getByPlantType`, data);
  }
  
}
