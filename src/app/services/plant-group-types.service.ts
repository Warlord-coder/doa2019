﻿import {Injectable} from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {environment} from '../../environments/environment';

@Injectable({providedIn: 'root'})
export class PlantGroupTypesService {
  constructor(private http: HttpClient) {
  }

  getAll(hideDeletedEntries = false) {
    let url = `${environment.apiUrl}plant-group-types/list`;
    if (hideDeletedEntries) {
      url = `${url}?hideDeletedEntries=true`;
    }
    return this.http.get<any[]>(url);
  }

  getSpecific(id: string) {
    return this.http.get<any[]>(`${environment.apiUrl}plant-group-types/${id}`);
  }

  createOrUpdate(role: any, id) {
    if (id === 'create') {
      return this.http.post(`${environment.apiUrl}plant-group-types/create`, role);
    }

    return this.http.put(`${environment.apiUrl}plant-group-types/${id}`, role);
  }

  delete(id: number) {
    return this.http.delete(`${environment.apiUrl}plant-group-types/${id}`);
  }
}
