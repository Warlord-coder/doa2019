﻿import {Injectable} from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {environment} from '../../environments/environment';

@Injectable({providedIn: 'root'})
export class CommonProvinceService {
  constructor(private http: HttpClient) {
  }

  getAll(hideDeletedEntries = false) {
    let url = `${environment.apiUrl}common-province/list`;
    if (hideDeletedEntries) {
      url = `${url}?hideDeletedEntries=true`;
    }
    return this.http.get<any[]>(url);
  }

  getSpecific(id: string) {
    return this.http.get<any[]>(`${environment.apiUrl}common-province/${id}`);
  }

  createOrUpdate(role: any, id) {
    if (id === 'create') {
      return this.http.post(`${environment.apiUrl}common-province/create`, role);
    }

    return this.http.put(`${environment.apiUrl}common-province/${id}`, role);
  }

  delete(id: number) {
    return this.http.delete(`${environment.apiUrl}common-province/${id}`);
  }
  search(keyword: string) {
    return this.http.post(`${environment.apiUrl}common-province/search`, {keyword: keyword});
  }
}
