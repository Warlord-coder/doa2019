import {Injectable} from '@angular/core';
import {HttpRequest, HttpHandler, HttpEvent, HttpInterceptor} from '@angular/common/http';
import {Observable, throwError} from 'rxjs';
import {catchError, tap} from 'rxjs/operators';

import {AuthService} from '../../services';

@Injectable()
export class HttpClientInterceptor implements HttpInterceptor {
  constructor(private authenticationService: AuthService) {
  }

  intercept(request: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> {
    return next.handle(request)
      .pipe(catchError(err => {
        if ((err.status === 401 || err.status === 403) && localStorage.getItem('authorization')) {
          const authToken = localStorage.getItem('authorization')
          localStorage.removeItem('authorization')
          this.authenticationService.logout();
          if (authToken) {
            location.reload(true);
          }
        }

        const error = err.error.message || err.statusText;
        return throwError(error);
      }));
  }
}
