import {FormGroup} from '@angular/forms';
import {TreeviewItem} from 'ngx-treeview';

export const MustMatch = (controlName: string, matchingControlName: string) => {
  return (formGroup: FormGroup) => {
    const control = formGroup.controls[controlName];
    const matchingControl = formGroup.controls[matchingControlName];

    if (matchingControl.errors && !matchingControl.errors.mustMatch) {
      return;
    }

    if (control.value !== matchingControl.value) {
      matchingControl.setErrors({mustMatch: true});
    } else {
      matchingControl.setErrors(null);
    }
  };
};

export const getRoleResources = (checkedValues = [], isDisabled = false) => {
  const itCategory = new TreeviewItem({
    text: 'ALL', value: 1, children: [
      {
        text: 'Team Member', value: 2, checked: checkedValues.includes(2), children: [
          {text: 'View Team Member', value: 3, checked: checkedValues.includes(3)},
          {text: 'Add Team Member', value: 4, checked: checkedValues.includes(4)},
          {text: 'Edit Team Member', value: 5, checked: checkedValues.includes(5)},
          {text: 'Delete Team Member', value: 6, checked: checkedValues.includes(6)}
      ]},
      {
        text: 'Role Settings', value: 7, checked: checkedValues.includes(7), children: [
          {text: 'View Role', value: 8, checked: checkedValues.includes(8)},
          {text: 'Add Role', value: 9, checked: checkedValues.includes(9)},
          {text: 'Edit Role', value: 10, checked: checkedValues.includes(10)},
          {text: 'Delete Role', value: 11, checked: checkedValues.includes(11)}]
      },
      {
        text: 'Plant', value: 22, checked: checkedValues.includes(22), children: [
          {text: 'View Plant', value: 23, checked: checkedValues.includes(23)},
          {text: 'Add Plant', value: 24, checked: checkedValues.includes(24)},
          {text: 'Edit Plant', value: 25, checked: checkedValues.includes(25)},
          {text: 'Delete Plant', value: 26, checked: checkedValues.includes(26)}]
      },
      {
        text: 'Scientific Name', value: 12, checked: checkedValues.includes(12), children: [
          {text: 'View Menu', value: 13, checked: checkedValues.includes(13)},
          {text: 'Add Plant Character', value: 14, checked: checkedValues.includes(14)},
          {text: 'Edit Plant Character', value: 15, checked: checkedValues.includes(15)},
          {text: 'Delete Plant Character', value: 16, checked: checkedValues.includes(16)}]
      },
      {
        text: 'Plant Group', value: 17, checked: checkedValues.includes(17), children: [
          {text: 'View Plant Group', value: 18, checked: checkedValues.includes(18)},
          {text: 'Add Plant Group', value: 19, checked: checkedValues.includes(19)},
          {text: 'Edit Plant Group', value: 20, checked: checkedValues.includes(20)},
          {text: 'Delete Plant Group', value: 21, checked: checkedValues.includes(21)}]
      },
      {
        text: 'Plant Group Type', value: 27, checked: checkedValues.includes(27), children: [
          {text: 'View Plant Group Type', value: 28, checked: checkedValues.includes(28)},
          {text: 'Add Plant Group Type', value: 29, checked: checkedValues.includes(29)},
          {text: 'Edit Plant Group Type', value: 30, checked: checkedValues.includes(30)},
          {text: 'Delete Plant Group Type', value: 31, checked: checkedValues.includes(31)}]
      },
      {
        text: 'Plant Type', value: 32, checked: checkedValues.includes(32), children: [
          {text: 'View Plant Type', value: 33, checked: checkedValues.includes(33)},
          {text: 'Add Plant Type', value: 34, checked: checkedValues.includes(34)},
          {text: 'Edit Plant Type', value: 35, checked: checkedValues.includes(35)},
          {text: 'Delete Plant Type', value: 36, checked: checkedValues.includes(36)}]
      },
      {
        text: 'Province', value: 37, checked: checkedValues.includes(37), children: [
          {text: 'View Province', value: 38, checked: checkedValues.includes(38)},
          {text: 'Add Province', value: 39, checked: checkedValues.includes(39)},
          {text: 'Edit Province', value: 40, checked: checkedValues.includes(40)},
          {text: 'Delete Province', value: 41, checked: checkedValues.includes(41)}]
      },
      {
        text: 'Unit', value: 42, checked: checkedValues.includes(42), children: [
          {text: 'View Unit', value: 43, checked: checkedValues.includes(43)},
          {text: 'Add Unit', value: 44, checked: checkedValues.includes(44)},
          {text: 'Edit Unit', value: 45, checked: checkedValues.includes(45)},
          {text: 'Delete Unit', value: 46, checked: checkedValues.includes(46)}]
      },
      {
        text: 'Country', value: 47, checked: checkedValues.includes(47), children: [
          {text: 'View Country', value: 48, checked: checkedValues.includes(48)},
          {text: 'Add Country', value: 49, checked: checkedValues.includes(49)},
          {text: 'Edit Country', value: 50, checked: checkedValues.includes(50)},
          {text: 'Delete Country', value: 51, checked: checkedValues.includes(51)}]
      },
      {
        text: 'Aumper', value: 52, checked: checkedValues.includes(52), children: [
          {text: 'View Aumper', value: 53, checked: checkedValues.includes(53)},
          {text: 'Add Aumper', value: 54, checked: checkedValues.includes(54)},
          {text: 'Edit Aumper', value: 55, checked: checkedValues.includes(55)},
          {text: 'Delete Aumper', value: 56, checked: checkedValues.includes(56)}]
      },
      {
        text: 'Condition', value: 57, checked: checkedValues.includes(57), children: [
          {text: 'View Condition', value: 58, checked: checkedValues.includes(58)},
          {text: 'Add Condition', value: 59, checked: checkedValues.includes(59)},
          {text: 'Edit Condition', value: 60, checked: checkedValues.includes(60)},
          {text: 'Delete Condition', value: 61, checked: checkedValues.includes(61)}]
      },
      {
        text: 'Register - M3 Passport', value: 179, checked: checkedValues.includes(179), children: [
          {text: 'View Menu', value: 180, checked: checkedValues.includes(180)},
          {text: 'Add', value: 181, checked: checkedValues.includes(181)},
          {text: 'Edit', value: 182, checked: checkedValues.includes(182)}]
      },
      {
        text: 'Register - M4 Coompleted Plants', value: 183, checked: checkedValues.includes(183), children: [
          {text: 'View Menu', value: 184, checked: checkedValues.includes(184)}]
      },
      {
        text: 'M3 - Yearly Report', value: 62, checked: checkedValues.includes(62), children: [
          {text: 'View Menu', value: 63, checked: checkedValues.includes(63)}]
      },
      {
        text: 'M3 - Monthly Report', value: 64, checked: checkedValues.includes(64), children: [
          {text: 'View Menu', value: 65, checked: checkedValues.includes(65)}]
      },
      {
        text: 'M8 - Lab Status', value: 66, checked: checkedValues.includes(66), children: [
          {text: 'View Menu', value: 67, checked: checkedValues.includes(67)}]
      },
      {
        text: 'M8 - Lab Plant', value: 68, checked: checkedValues.includes(68), children: [
          {text: 'View Menu', value: 69, checked: checkedValues.includes(69)}]
      },
      {
        text: 'M9 - Register Report', value: 70, checked: checkedValues.includes(70), children: [
          {text: 'View Menu', value: 71, checked: checkedValues.includes(71)}]
      },
      {
        text: 'M9 - Plant Status', value: 72, checked: checkedValues.includes(72), children: [
          {text: 'View Menu', value: 73, checked: checkedValues.includes(73)}]
      },
      {
        text: 'M5 - Withdraw Report', value: 76, checked: checkedValues.includes(76), children: [
          {text: 'View Menu', value: 77, checked: checkedValues.includes(77)}]
      },
      {
        text: 'M5 - Distribution Report', value: 74, checked: checkedValues.includes(74), children: [
          {text: 'View Menu', value: 75, checked: checkedValues.includes(75)}]
      },
      {
        text: 'M4 - Report', value: 78, checked: checkedValues.includes(78), children: [
          {text: 'View Menu', value: 79, checked: checkedValues.includes(79)}]
      },
      {
        text: 'M9 - All Status', value: 80, checked: checkedValues.includes(80), children: [
          {text: 'View Menu', value: 81, checked: checkedValues.includes(81)}]
      },
      {
        text: 'Lab - M8 - Completed Plants', value: 82, checked: checkedValues.includes(82), children: [
          {text: 'View Menu', value: 83, checked: checkedValues.includes(83)}]
      },
      {
        text: 'Lab - M8 - Lab List/Create', value: 84, checked: checkedValues.includes(84), children: [
          {text: 'View Menu', value: 85, checked: checkedValues.includes(85)},
          {text: 'Add', value: 86, checked: checkedValues.includes(86)},
          {text: 'Edit', value: 87, checked: checkedValues.includes(87)}]
      },
      {
        text: 'Lab - M8 - Alert List', value: 88, checked: checkedValues.includes(88), children: [
          {text: 'View Menu', value: 89, checked: checkedValues.includes(89)},
          {text: 'Add', value: 90, checked: checkedValues.includes(90)},
          {text: 'Edit', value: 91, checked: checkedValues.includes(91)}]
      },
      {
        text: 'Lab - M8 - Lab Alert List', value: 92, checked: checkedValues.includes(92), children: [
          {text: 'View Menu', value: 93, checked: checkedValues.includes(93)},
          {text: 'Add', value: 94, checked: checkedValues.includes(94)},
          {text: 'Edit', value: 95, checked: checkedValues.includes(95)}]
      },
      {
        text: 'WH - M9 - Set Distribution', value: 96, checked: checkedValues.includes(96), children: [
          {text: 'View Menu', value: 97, checked: checkedValues.includes(97)},
          {text: 'Edit', value: 98, checked: checkedValues.includes(98)}]
      },
      {
        text: 'WH - M9 - WH List - Lab no', value: 99, checked: checkedValues.includes(99), children: [
          {text: 'View Menu', value: 100, checked: checkedValues.includes(100)},
          {text: 'Edit', value: 101, checked: checkedValues.includes(101)}]
      },
      {
        text: 'WH - M9 - WH List - Lab All Plants', value: 102, checked: checkedValues.includes(102), children: [
          {text: 'View Menu', value: 103, checked: checkedValues.includes(103)}]
      },
      {
        text: 'WH - M9 - WH List - All Plant Alert', value: 187, checked: checkedValues.includes(187), children: [
          {text: 'View Menu', value: 188, checked: checkedValues.includes(188)}
        ]
      },
      {
        text: 'WH - M9 - WH List - Lab Alert', value: 104, checked: checkedValues.includes(104), children: [
          {text: 'View Menu', value: 105, checked: checkedValues.includes(105)},
          {text: 'Edit', value: 106, checked: checkedValues.includes(106)}]
      },
      {
        text: 'WH - M9 - WH Withdraw Req', value: 107, checked: checkedValues.includes(107), children: [
          {text: 'View Menu', value: 108, checked: checkedValues.includes(108)},
          {text: 'Edit', value: 109, checked: checkedValues.includes(109)}]
      },
      {
        text: 'WH - M9 - WH Distribution Req', value: 110, checked: checkedValues.includes(110), children: [
          {text: 'View Menu', value: 111, checked: checkedValues.includes(111)},
          {text: 'Edit', value: 112, checked: checkedValues.includes(112)}]
      },
      {
        text: 'WH - M9 - WH Labs Alert Req', value: 113, checked: checkedValues.includes(113), children: [
          {text: 'View Menu', value: 114, checked: checkedValues.includes(114)},
          {text: 'Edit', value: 115, checked: checkedValues.includes(115)}]
      },
      {
        text: 'WH - M9 - WH Recovery Alert Req', value: 116, checked: checkedValues.includes(116), children: [
          {text: 'View Menu', value: 117, checked: checkedValues.includes(117)},
          {text: 'Edit', value: 118, checked: checkedValues.includes(118)}]
      },
      {
        text: 'Service - M5 - Withdraw Alert List', value: 119, checked: checkedValues.includes(119), children: [
          {text: 'View Menu', value: 120, checked: checkedValues.includes(120)}]
      },
      {
        text: 'Service - M5 - Withdraw', value: 121, checked: checkedValues.includes(121), children: [
          {text: 'View Menu', value: 122, checked: checkedValues.includes(122)},
          {text: 'Add', value: 123, checked: checkedValues.includes(123)},
          {text: 'Edit', value: 124, checked: checkedValues.includes(124)},]
      },
      {
        text: 'Service - M5 - Distribution Alert List', value: 125, checked: checkedValues.includes(125), children: [
          {text: 'View Menu', value: 126, checked: checkedValues.includes(126)}]
      },
      {
        text: 'Service - M5 - Distribution', value: 127, checked: checkedValues.includes(127), children: [
          {text: 'View Menu', value: 128, checked: checkedValues.includes(128)},
          {text: 'Add', value: 129, checked: checkedValues.includes(129)},
          {text: 'Edit', value: 130, checked: checkedValues.includes(130)}]
      },
      {
        text: 'Manage Data - M2 - Attribute', value: 131, checked: checkedValues.includes(131), children: [
          {text: 'View Menu', value: 132, checked: checkedValues.includes(132)},
          {text: 'Add', value: 133, checked: checkedValues.includes(133)},
          {text: 'Edit', value: 134, checked: checkedValues.includes(134)}]
      },
      {
        text: 'Manage Data - M2 - Attribute List', value: 135, checked: checkedValues.includes(135), children: [
          {text: 'View Menu', value: 136, checked: checkedValues.includes(136)},
          {text: 'Add', value: 137, checked: checkedValues.includes(137)},
          {text: 'Edit', value: 138, checked: checkedValues.includes(138)},]
      },
      {
        text: 'Manage Data - M2 - DOA Listing', value: 139, checked: checkedValues.includes(139), children: [
          {text: 'View Menu', value: 140, checked: checkedValues.includes(140)},
          {text: 'Edit', value: 141, checked: checkedValues.includes(141)},]
      },
      {
        text: 'Manage Data - M2 - Export DOA', value: 185, checked: checkedValues.includes(185), children: [
          {text: 'View Menu', value: 186, checked: checkedValues.includes(186)}
        ]
      },
      {
        text: 'Recovery - M4 - Multiplication and Regeneration List', value: 142, checked: checkedValues.includes(142), children: [
          {text: 'View Menu', value: 143, checked: checkedValues.includes(143)},
          {text: 'Add', value: 144, checked: checkedValues.includes(144)},
          {text: 'Edit', value: 145, checked: checkedValues.includes(145)}]
      },
      {
        text: 'Recovery - M4 - Alert List', value: 146, checked: checkedValues.includes(146), children: [
          {text: 'View Menu', value: 147, checked: checkedValues.includes(147)}]
      },
      {
        text: 'Frontend - Event-news', value: 148, checked: checkedValues.includes(148), children: [
          {text: 'View Menu', value: 149, checked: checkedValues.includes(149)},
          {text: 'Add', value: 150, checked: checkedValues.includes(150)},
          {text: 'Edit', value: 151, checked: checkedValues.includes(151)},
          {text: 'Delete', value: 152, checked: checkedValues.includes(152)}]
      },
      {
        text: 'Frontend - Knowledge', value: 153, checked: checkedValues.includes(153), children: [
          {text: 'View Menu', value: 154, checked: checkedValues.includes(154)},
          {text: 'Add', value: 155, checked: checkedValues.includes(155)},
          {text: 'Edit', value: 156, checked: checkedValues.includes(156)},
          {text: 'Delete', value: 157, checked: checkedValues.includes(157)}]
      },
      {
        text: 'Frontend - Banner', value: 158, checked: checkedValues.includes(158), children: [
          {text: 'View Menu', value: 159, checked: checkedValues.includes(159)},
          {text: 'Add', value: 160, checked: checkedValues.includes(160)},
          {text: 'Edit', value: 161, checked: checkedValues.includes(161)},
          {text: 'Delete', value: 162, checked: checkedValues.includes(162)}]
      },
      {
        text: 'Frontend - Intro', value: 168, checked: checkedValues.includes(168), children: [
          {text: 'View Menu', value: 169, checked: checkedValues.includes(169)},
          {text: 'Add', value: 170, checked: checkedValues.includes(170)},
          {text: 'Edit', value: 171, checked: checkedValues.includes(171)},
          {text: 'Delete', value: 172, checked: checkedValues.includes(172)}]
      },
      {
        text: 'Frontend - Accessment', value: 173, checked: checkedValues.includes(173), children: [
          {text: 'View Menu', value: 174, checked: checkedValues.includes(174)}]
      },
      {
        text: 'Frontend - Users', value: 175, checked: checkedValues.includes(175), children: [
          {text: 'View Menu', value: 176, checked: checkedValues.includes(176)},
          {text: 'Add', value: 177, checked: checkedValues.includes(177)},
          {text: 'Edit', value: 178, checked: checkedValues.includes(178)}]
      },
    ], disabled: isDisabled, checked: checkedValues.includes(1)
  });
  return [itCategory];
};

export const FORM_REQUIRED_KEYS = {
  REGISTER_FORM: [
    'plant_gene_name',
    'reg_gene_category',
    'department_type', 'source_province',
    'source_district', 'source_address',
    'reg_seed_collect_person', 'reg_seed_owner', 'reg_seed_sender',
    'reg_sent_lab_date'
  ],
  LAB_FORM: ['lab_test_plant_amount', 'lab_test_moisture_method',
    'lab_test_moisture_amount', 'lab_test_moisture_person', 'lab_test_growth_percent',
    'lab_test_growth_method', 'lab_test_growth_amount', 'lab_test_growth_person',
    'lab_test_strong_percent', 'lab_test_strong_method', 'lab_test_strong_amount',
    'lab_test_strong_person', 'lab_test_alive_percent', 'lab_test_alive_method',
    'lab_test_alive_amount', 'lab_test_alive_person', 'lab_seed_rest',
    'lab_admin_name'],
  RECOVER_FORM: [
    'recovery_sent_date',
    'recovery_amount',
    // 'recovery_amount_dropdown',
    'recovery_place',
    'recovery_province',
    'recovery_district',
    'recovery_sub_district',
    'recovery_address',
    // 'recovery_lat',
    // 'recovery_long',
    'recovery_rec_recover_date',
    'recovery_rec_recover_collect_date',
    'recovery_rec_recover_collect_person',
    'recovery_rec_date',
    'recovery_rec_no',
    'recovery_rec_co_person',
    // 'recovery_rec_method',
    'recovery_rec_gene_check',
    // 'recovery_test_growth_percent',
    'recovery_seed_amount'
  ],
  REGISTER_FORM_4: [
    'reg_seed_collect_person',
    'reg_seed_sender',
    // 'reg_character_for_record',
  ],
  ALERT_LAB_FORM: ['lab_test_check_seed_date', 'lab_test_check_seed_name',
    'wh_batch_moisture_percent', 'lab_test_moisture_method',
    'lab_test_moisture_amount', 'lab_test_moisture_person', 'lab_test_growth_percent',
    'lab_test_growth_method', 'lab_test_growth_amount', 'lab_test_growth_person',
    'lab_test_strong_percent', 'lab_test_strong_method', 'lab_test_strong_amount',
    'lab_test_strong_person', 'lab_test_alive_percent', 'lab_test_alive_method',
    'lab_test_alive_amount', 'lab_test_alive_person', 'lab_seed_rest', 'lab_seed_rest_fix_method',
    'lab_seed_amount', 'lab_10seed_weight',
    'lab_admin_name', 'alert_test_amount']
};


export const isFormSubmissionError = (record: any = {}, keys) => {
  for (const key of keys) {
    if (!record.hasOwnProperty(key) || !record[key] || (Array.isArray(record[key]) && record[key].length === 0 )) {
      return record;
    }
  }

  return false;
};
