import {Directive, ElementRef, HostListener} from '@angular/core';

@Directive({
  selector: '[OnlyNumber]'
})
export class OnlyNumberDirective {

  // Allow decimal numbers. The \. is only allowed once to occur
  private regex: RegExp = new RegExp(/^[0-9]+(\.[0-9]*){0,1}$/g);

  // Allow key codes for special events. Reflect :
  // Backspace, tab, end, home
  private specialKeys: Array<string> = ['Backspace', 'Tab', 'End', 'Home', 'ArrowRight', 'ArrowLeft'];

  constructor(private el: ElementRef) {
  }

  @HostListener('keydown', ['$event'])
  onKeyDown(event: KeyboardEvent) {
    // Allow Backspace, tab, end, and home keys
    if (this.specialKeys.indexOf(event.key) !== -1) {
      return;
    }

    // Do not use event.keycode this is deprecated.
    // See: https://developer.mozilla.org/en-US/docs/Web/API/KeyboardEvent/keyCode
    const current: string = this.el.nativeElement.value;
    // We need this because the current value on the DOM element
    // is not yet updated with the value from this event
    const next: string = current.concat(event.key);
    if (next && !String(next).match(this.regex)) {
      event.preventDefault();
    }

    try {
      if (this.el.nativeElement.max) {
        const maxValue = parseFloat(this.el.nativeElement.max);
        const fieldValue = parseFloat(`${this.el.nativeElement.value}${event.key}`);
        if (maxValue !== NaN && fieldValue !== NaN && fieldValue > maxValue) {
          event.preventDefault();
        } else if (event.key === '.') {
          this.el.nativeElement.value = `${this.el.nativeElement.value}${event.key}0`;
          event.preventDefault();
        }
      }
    } catch (e) {
      console.error(e);
    }
  }
}
