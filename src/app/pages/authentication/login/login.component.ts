import {Component, OnInit} from '@angular/core';
import {FormBuilder, FormGroup, Validators} from '@angular/forms';
import {Router} from '@angular/router';
import {first} from 'rxjs/operators';

import {AuthService, AlertService} from '../../../services';

@Component({
  selector: 'app-login-page',
  templateUrl: './login.component.html',
})
export class NgbdLoginPageComponent implements OnInit {
  loginForm: FormGroup;
  submitted = false;
  loading = false;
  returnUrl = '/';

  constructor(private formBuilder: FormBuilder,
              private router: Router,
              private authenticationService: AuthService,
              private alertService: AlertService) {
    if (this.authenticationService.currentUserValue) {
      this.router.navigate([this.returnUrl]);
    }
  }

  ngOnInit() {
    this.loginForm = this.formBuilder.group({
      email: ['', [Validators.required, Validators.email]],
      password: ['', [Validators.required, Validators.minLength(6)]]
    });
  }

  get f() {
    return this.loginForm.controls;
  }

  onSubmit() {
    this.submitted = true;
    this.alertService.reset();

    if (this.loginForm.invalid) {
      return;
    }

    const {email, password} = this.loginForm.value;


    this.loading = true;
    this.authenticationService.login(email, password)
      .pipe(first())
      .subscribe(
        data => {
          this.loading = false;
          this.router.navigate([this.returnUrl]);
        },
        error => {
          this.loading = false;
          this.alertService.error(error);
        });
  }

  onReset() {
    this.submitted = false;
    this.loginForm.reset();
  }
}
