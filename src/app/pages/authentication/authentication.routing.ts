import {Routes} from '@angular/router';

import {NgbdLoginPageComponent} from './login/login.component';
import {NgbdRegisterPageComponent} from './register/register.component';

export const AuthenticationRoutes: Routes = [
  {
    path: '',
    children: [
      {
        path: '',
        redirectTo: 'login'
      },
      {
        path: 'login',
        component: NgbdLoginPageComponent,
        data: {
          title: 'Login',
          urls: []
        }
      },
      // {
      //   path: 'register',
      //   component: NgbdRegisterPageComponent,
      //   data: {
      //     title: 'Register',
      //     urls: []
      //   }
      // },
      {
        path: '**',
        redirectTo: 'login'
      },
    ]
  }
];
