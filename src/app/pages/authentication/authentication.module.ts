import {NgModule} from '@angular/core';
import {RouterModule} from '@angular/router';
import {CommonModule} from '@angular/common';
import {FormsModule, ReactiveFormsModule} from '@angular/forms';
import {NgxLoadingModule} from 'ngx-loading';
import {NgbModule} from '@ng-bootstrap/ng-bootstrap';

import {AuthenticationRoutes} from './authentication.routing';
import {NgbdLoginPageComponent} from './login/login.component';
import {NgbdRegisterPageComponent} from './register/register.component';
import {ComponentsModule} from '../../component/component.module';

@NgModule({
  imports: [
    NgxLoadingModule.forRoot({}),
    CommonModule,
    RouterModule.forChild(AuthenticationRoutes),
    FormsModule,
    ReactiveFormsModule,
    NgbModule,
    ComponentsModule
  ],
  declarations: [
    NgbdLoginPageComponent,
    NgbdRegisterPageComponent
  ]
})
export class AuthenticationModule {}
