import {Component, OnInit} from '@angular/core';
import {ActivatedRoute, Router} from '@angular/router';
import {Location} from '@angular/common';
import {FormArray, FormBuilder, FormGroup, Validators} from '@angular/forms';
import {
  AlertService,
  CommonProvinceService,
  CommonConditionService,
  PassportService,
  PlantCategoryService, PlantCharacterService,
  PlantGroupsService,
  PlantGroupTypesService, PlantTypeService,
  VolumeService, WarehouseService
} from '../../../../services';
import {identity, pickBy} from 'lodash';
import {getRoleResources} from '../../../../utils';
import {TreeviewItem} from 'ngx-treeview';
import {first} from 'rxjs/operators';
import {PlantService} from '../../../../services/plant.service';
import {CommonCountryService} from '../../../../services/common-country.service';
import {CommonAumperService} from '../../../../services/common-aumper.service';
import {environment} from '../../../../../environments/environment';
import {NgbModal,ModalDismissReasons} from '@ng-bootstrap/ng-bootstrap';

@Component({
  selector: 'app-add-update-plant-new-page',
  templateUrl: './add-update.component.html',
})
export class NgbdAddUpdateNewPlantAlertComponent implements OnInit {
  plantGroupTypeForm: FormGroup;
  submitted = false;
  loading = false;
  file = null;
  id = null;
  categoryList = [];
  groupTypeList = [];
  groupList = [];
  provinceList = [];
  aumperList = [];
  countryList = [];
  volumeInfo = null;
  passportInfo = null;
  currentAlertlab = null;
  gs_no = null;
  apiError = false;
  plantsListing = [];
  plantTypes = [];
  reg_character_attached_file = null;
  regCharacterAttachedFile = null;
  seedAttachedFile = null;
  molecularAttachedFile = null;
  reg_seed_no = null;
  reg_gene_id = null;
  submitTo = null;
  labInfo: any = {};
  formData: any = {};
  selectedPlant = null;
  selectedCheckbox = {
    room5: [],
    room10: []
  };
  textRoom = {'room5':[],'room10':[]};
  volumeId = null;
  roleResources: TreeviewItem[];
  config = {
    class: 'autocomplete', max: 5, placeholder: 'Type to search',
    sourceField: ['plantTypeName_sci_en']
  };
  conditionDataYear = [];
  conditionDataPercent = [];
  conditionDataWeight = [];
  conditionData = {}
  conditionIndex = null;
  conditionKey = '';
  conditionYear = '';
  conditionWeight = '';
  conditionPercent = '';
  reg_seed_sent_noBtn = false;
  store_date;
  constructor(private router: Router,
              private formBuilder: FormBuilder,
              private activeRoute: ActivatedRoute,
              private plantCategoryService: PlantCategoryService,
              private plantGroupTypesService: PlantGroupTypesService,
              private plantGroupsService: PlantGroupsService,
              private plantCharacterService: PlantCharacterService,
              private plantTypeService: PlantTypeService,
              private volumeService: VolumeService,
              private conditionService: CommonConditionService,
              private passportService: PassportService,
              private wareHouseService: WarehouseService,
              private countryService: CommonCountryService,
              private provinceService: CommonProvinceService,
              private aumperService: CommonAumperService,
              private modalService: NgbModal,
              private _location: Location,
              private alertService: AlertService) {
    this.onGetListing('plantCharacterService', 'plantsListing');
    this.onGetListing('plantCategoryService', 'categoryList');
    this.onGetListing('plantGroupTypesService', 'groupTypeList');
    this.onGetListing('plantGroupsService', 'groupList');
    this.onGetListing('plantTypeService', 'plantTypes');
    this.onGetListing('countryService', 'countryList');
    this.onGetListing('aumperService', 'aumperList');
    this.onGetListing('provinceService', 'provinceList');
    this.onGetListingConditon('conditionService', 'conditionData');
  }

  ngOnInit() {
    this.roleResources = getRoleResources();
    this.plantGroupTypeForm = this.formBuilder.group({
      plant_gene_name: ['', Validators.required],
      reg_seed_sent_no: ['', Validators.required],
      reg_gene_type: [''],
      reg_gene_category: ['', Validators.required],
      reg_seed_collect_date: [''],
      selfing_input: [''],
      selfing_dropdown: [''],
      oop_input: [''],
      oop_dropdown: [''],
      other_input: [''],
      other_dropdown: [''],
      department_type: [''],
      department_sub_type: [''],
      department_name: [''],
      reg_plant_sci_name: [''],
      reg_plant_alter_name: [[]],
      source_country: [''],
      source_country_1: [''],
      source_province: ['', Validators.required],
      source_district: ['', Validators.required],
      source_address: ['', Validators.required],
      lat: [''],
      long: [''],
      source_by_department: [''],
      reg_seed_collect_person: ['', Validators.required],
      reg_seed_owner: ['', Validators.required],
      reg_seed_sender: ['', Validators.required],
      reg_character_brief: [''],
      reg_character_for_record: [''],
      reg_sent_lab_date: ['', Validators.required],
      reg_remark: ['', Validators.required],
      room5: this.formBuilder.array([]),
      room10: this.formBuilder.array([]),
      wh_sample_ref_seed_check_box: [],
      wh_sample_ref_molecular_checkbox: [],
      wh_create_info_checkbox: []
    });
    this.activeRoute.params.subscribe(params => {
      const {id} = params;
      this.id = id;
      if (id && id !== 'create') {
        this.onGetLabAlertFormInfo(id);
      }
    });
  }

  get f() {
    if (!this.apiError && this.submitted && !this.plantGroupTypeForm.invalid) {
      this.alertService.error(null);
    }

    return this.plantGroupTypeForm.controls;
  }

  onSubmit() {
    this.submitted = false;
    if (this.plantGroupTypeForm.invalid) {
      // this.alertService.error('Some fields are required. Please fill those fields');
      // return;
    }
    this.plantGroupTypeForm.value['reg_plant_sci_name'] = this.findPlantSciNameToId(this.plantGroupTypeForm.value['reg_plant_sci_name'])
    let {room5, room10, ...other} = this.plantGroupTypeForm.value;

    room5 = room5.filter(({batch_no}, index) => this.selectedCheckbox.room5.includes(index));
    room10 = room10.filter(({batch_no}, index) => this.selectedCheckbox.room10.includes(index));
    // room5 = room5.filter(({batch_id}, index) => this.selectedCheckbox.room5.includes(index));
    // room10 = room10.filter(({batch_id}, index) => this.selectedCheckbox.room10.includes(index));
    for (let index = 0; index < room5.length; index++) {
      room5[index].notification_period = {
        year:room5[index].notification_year,
        percent:room5[index].notification_percent,
        weight:room5[index].notification_weight
      }
      
    }
    for (let index = 0; index < room10.length; index++) {
      room10[index].notification_period = {
        year:room10[index].notification_year,
        percent:room10[index].notification_percent,
        weight:room10[index].notification_weight
      }
      
    }
    this.wareHouseService.createOrUpdate(pickBy({
      ...other,
      room5: room5 ? JSON.stringify(room5) : '',
      room10: room10 ? JSON.stringify(room10) : '',
      // file: this.regCharacterAttachedFile,
      // seedAttachedFile: this.seedAttachedFile,
      // molecularAttachedFile: this.molecularAttachedFile
    }, identity), this.id)
      .pipe(first())
      .subscribe(
        () => {
          this.loading = false;
          this.alertService.success('Data Saved Successfully');
          this.selectedCheckbox = {
            room5: [],
            room10: []
          };
          this.plantGroupTypeForm = this.formBuilder.group({
            plant_gene_name: ['', Validators.required],
            reg_seed_sent_no: ['', Validators.required],
            reg_gene_type: [''],
            reg_gene_category: ['', Validators.required],
            reg_seed_collect_date: [''],
            selfing_input: [''],
            selfing_dropdown: [''],
            oop_input: [''],
            oop_dropdown: [''],
            other_input: [''],
            other_dropdown: [''],
            department_type: [''],
            department_sub_type: [''],
            department_name: [''],
            reg_plant_sci_name: [''],
            reg_plant_alter_name: [[]],
            source_country: [''],
            source_country_1: [''],
            source_province: ['', Validators.required],
            source_district: ['', Validators.required],
            source_address: ['', Validators.required],
            lat: [''],
            long: [''],
            source_by_department: [''],
            reg_seed_collect_person: ['', Validators.required],
            reg_seed_owner: ['', Validators.required],
            reg_seed_sender: ['', Validators.required],
            reg_character_brief: [''],
            reg_character_for_record: [''],
            reg_sent_lab_date: ['', Validators.required],
            reg_remark: ['', Validators.required],
            room5: this.formBuilder.array([]),
            room10: this.formBuilder.array([]),
            wh_sample_ref_seed_check_box: [],
            wh_sample_ref_molecular_checkbox: [],
            wh_create_info_checkbox: []
          });
          this.onGetLabAlertFormInfo(this.id);
          setTimeout(() => {
            this.alertService.reset();
          }, 1500);
        },
        error => {
          this.loading = false;
          this.apiError = true;
          this.alertService.error(error);
        });
  }

  onSelectedChange($event) {
    console.log('Select', $event);
  }

  onFilterChange($event) {
    console.log('Filter Change', $event);
  }

  onGetListing(serviceName, key) {
    this.loading = true;
    this[serviceName].getAll()
      .pipe(first())
      .subscribe(
        ({data = []}: any) => {
          this.loading = false;
          this[key] = data;
        },
        error => {
          this.loading = false;
          this.alertService.error(error);
        });
  }

  onGetLabAlertFormInfo(id) {
    this.loading = true;

    this.wareHouseService.getSpecific(id, true)
      .pipe(first())
      .subscribe(
        ({data = []}: any) => {
          const {
            reg_gene_id, reg_seed_no, gs_no,
            reg_plant_sci_name, reg_character_attached_file,
            volume, m8_lab_id, room5 = [], room10 = [], labId = {}
          } = data;
          this.formData = data;
          this.labInfo = m8_lab_id;
          this.currentAlertlab = labId;
          this.getStoreDate(labId);
          this.onGetVolumeInfo(volume);
          Object.keys(data).forEach(key => {
            if (this.plantGroupTypeForm.get(key) && !['room5', 'room10'].includes(key)) {
              this.plantGroupTypeForm.get(key).setValue(data[key]);
            }
          });
          this.reg_gene_id = reg_gene_id;
          this.gs_no = gs_no;
          this.reg_seed_no = reg_seed_no;
          this.reg_character_attached_file = reg_character_attached_file;
          this.findPlantSciName(data);

          this.selectedCheckbox.room5 = [
            room5[0] ? 0 : false,
            room5[1] ? 1 : false,
            room5[2] ? 2 : false,
            room5[3] ? 3 : false,
            room5[4] ? 4 : false];
          this.addItem(room5[0] || data, 'room5', 1, !room5[0]);
          this.addItem(room5[1] || {}, 'room5', 2, !room5[1]);
          this.addItem(room5[2] || {}, 'room5', 3, !room5[2]);
          this.addItem(room5[3] || {}, 'room5', 4, !room5[3]);
          this.addItem(room5[4] || {}, 'room5', 5, !room5[4]);

          this.displayCondition(0,'room5')
          this.displayCondition(1,'room5')
          this.displayCondition(2,'room5')
          this.displayCondition(3,'room5')
          this.displayCondition(4,'room5')

          this.selectedCheckbox.room10 = [
            room10[0] ? 0 : false,
            room10[1] ? 1 : false,
            room10[2] ? 2 : false,
            room10[3] ? 3 : false,
            room10[4] ? 4 : false];

          this.addItem(room10[0] || data, 'room10', 1, !room10[0]);
          this.addItem(room10[1] || {}, 'room10', 2, !room10[1]);
          this.addItem(room10[2] || {}, 'room10', 3, !room10[2]);
          this.addItem(room10[3] || {}, 'room10', 4, !room10[3]);
          this.addItem(room10[4] || {}, 'room10', 5, !room10[4]);

          this.displayCondition(0,'room10')
          this.displayCondition(1,'room10')
          this.displayCondition(2,'room10')
          this.displayCondition(3,'room10')
          this.displayCondition(4,'room10')

          this.loading = false;
        },
        error => {
          this.loading = false;
          this.alertService.error(error);
        });
  }

  onGetVolumeInfo(id) {
    this.loading = true;
    this.volumeService.getSpecific(id)
      .pipe(first())
      .subscribe(
        ({data = []}: any) => {
          this.loading = false;
          const {passport_no} = data;
          this.volumeInfo = data;
          this.onGetPassportInfo(passport_no);
        },
        error => {
          this.loading = false;
          this.alertService.error(error);
        });
  }
  getStoreDate(id) {
    this.loading = true;
    this.wareHouseService.getStoreDate(id)
      .pipe(first())
      .subscribe(
        ({data = []}: any) => {
          this.loading = false;
          try {
            this.store_date = data[0].warehouse.room5[0].wh_batch_store_date;
            this.plantGroupTypeForm.get('room5')['controls'][0].controls['wh_batch_store_date'].setValue(this.store_date);
            this.plantGroupTypeForm.get('room10')['controls'][0].controls['wh_batch_store_date'].setValue(this.store_date);
          } catch (error) {
            
          }
        },
        error => {
          this.loading = false;
          this.alertService.error(error);
        });
  }

  onGetPassportInfo(id) {
    this.loading = true;
    this.passportService.getSpecific(id)
      .pipe(first())
      .subscribe(
        ({data = []}: any) => {
          this.loading = false;
          const {passport_no} = data;
          this.passportInfo = data;
          if(data['reg_seed_sent_no'] != null){
            this.reg_seed_sent_noBtn = true;
            this.plantGroupTypeForm.controls['reg_seed_sent_no'].setValue(data['reg_seed_sent_no']);
            this.plantGroupTypeForm.get('reg_seed_sent_no').setValue(data['reg_seed_sent_no']);
          }
        },
        error => {
          this.loading = false;
          this.alertService.error(error);
        });
  }

  getAttachment(basePath, name) {
    if (name) {
      return `${environment.endpoint}/${basePath}/${name}`;
    }

    return null;
  }

  onFileUpload({srcElement: {files = []} = {}}, key = 'regCharacterAttachedFile') {
    this[key] = files[0];
  }


  onSelect(plant) {
    this.selectedPlant = plant;
    this.plantGroupTypeForm.get('reg_plant_sci_name').setValue(plant.genus + ' ' + plant.species);
  }

  createOptionValue(data: any, index = 1, disabled = true): FormGroup {
    const {
      lab_10seed_weight = '', lab_test_growth_percent = '',
      lab_test_growth_date = '', wh_batch_moisture_percent = '',
      lab_test_moisture_date = '', wh_batch_store_date = '',
      notification_period = '', wh_batch_weight = '',
      wh_batch_store_location = '', wh_batch_remark = '',
      notification_year = '',notification_percent = '',notification_weight = ''
    } = data || {};
    return this.formBuilder.group({
      batch_no: [index],
      // batch_id: [index],
      lab_10seed_weight: [{value: lab_10seed_weight, disabled}, Validators.required],
      lab_test_growth_percent: [{value: lab_test_growth_percent, disabled}, Validators.required],
      lab_test_growth_date: [{value: lab_test_growth_date, disabled}, Validators.required],
      wh_batch_moisture_percent: [{value: wh_batch_moisture_percent, disabled}, Validators.required],
      lab_test_moisture_date: [{value: lab_test_moisture_date, disabled}, Validators.required],
      wh_batch_store_date: [{value: wh_batch_store_date, disabled}, Validators.required],
      notification_period: [{value: notification_period, disabled}, Validators.required],
      notification_year:[{value: notification_year, disabled}],
      notification_percent:[{value: notification_percent, disabled}],
      notification_weight:[{value: notification_weight, disabled}],
      wh_batch_weight: [{value: wh_batch_weight, disabled}, Validators.required],
      wh_batch_store_location: [{value: wh_batch_store_location, disabled}, Validators.required],
      wh_batch_remark: [{value: wh_batch_remark, disabled}, Validators.required]
    });
  }

  addItem(data = {}, key, index, disabled = true): void {
    (this.plantGroupTypeForm.get(key) as FormArray).push(this.createOptionValue(data || {}, index, disabled));
  }

  onCancel() {
    this._location.back();
    //const {_id = ''} = this.labInfo || {};
    //this.router.navigate([`admin/warehouse/alert-pending-complete/listing/${this.currentAlertlab._id}`]);
  }

  onSubmitModule() {
    this.loading = true;
    this.submitted = true;
   
    if (this.plantGroupTypeForm.invalid) {
      this.alertService.error('Some fields are required. Please fill those fields');
      this.loading = false;
      return;
    }
    if (!this.f.selfing_input && !this.f.oop_input && !this.f.other_input) {
      this.alertService.error('Please Select one from Stock (Selfing, OOP, Other');
      this.loading = false;
      return;
    }

    this.wareHouseService.submitToModule(this.id, {submitTo: this.submitTo,symbol:'',gs_no:this.gs_no})
      .pipe(first())
      .subscribe(
        ({data = []}: any) => {
          this.loading = false;
          const {_id = ''} = this.labInfo || {};
          this.router.navigate([`admin/warehouse/alert-pending-complete/listing/${this.currentAlertlab}`]);
        },
        error => {
          this.loading = false;
          this.alertService.error(error);
        });
  }

  onChangeItemSelect(key, index) {
    if (this.selectedCheckbox[key].includes(index)) {
      // @ts-ignore
      this.plantGroupTypeForm.get(key).controls[index].disable();

      return this.selectedCheckbox[key] = this.selectedCheckbox[key].filter(val => val !== index);
    }

    // @ts-ignore
    this.plantGroupTypeForm.get(key).controls[index].enable();
    this.selectedCheckbox[key] = [...this.selectedCheckbox[key], index];
  }

  findPlantSciName({reg_plant_sci_name}) {
    if (!reg_plant_sci_name) {
      return;
    }
    for (const plant of  this.plantsListing) {
      if (reg_plant_sci_name === plant._id) {
        this.selectedPlant = plant;
        this.plantGroupTypeForm.get('reg_plant_sci_name').setValue(plant.genus + ' ' + plant.species);
      }
    }
    this.findPlantSciNameToId('');
  }
  findPlantSciNameToId(text) {
    if (!text) {
      return '';
    }
    for (const plant of  this.plantsListing) {
      if (text == plant.genus+' '+plant.species) {
         return plant._id;
      }
    }
    return text

  }
  onGetListingConditon(serviceName, key) {
    // this.loading = true;
    this[serviceName].getAll()
      .pipe(first())
      .subscribe(
        ({data = []}: any) => {
          // this.loading = false;
          for (let index = 0; index < data.length; index++) {
            const element = data[index];
            if(data[index].type == 'germination_percent' && data[index].conditionAt == 'module_8'){
              this.conditionDataPercent.push(data[index]);
            }else if(data[index].type == 'weight' && data[index].conditionAt == 'module_8'){
              this.conditionDataWeight.push(data[index]);
            }else if(data[index].type == 'year' && data[index].conditionAt == 'module_8'){
              this.conditionDataYear.push(data[index]);
            }
          }
        },
        error => {
          this.loading = false;
          this.alertService.error(error);
        });
  }

  openModal(content,event,key,index){
    try {
      let d = this.plantGroupTypeForm.get(key).value[index].notification_period;
      this.conditionYear = (d.year!=null?d.year:'');
      this.conditionPercent = (d.percent!=null?d.percent:'');
      this.conditionWeight = (d.weight!=null?d.weight:'');
      this.conditionKey = key;
      this.conditionIndex = index;
      event.srcElement.blur();
      event.preventDefault();
      this.modalService.open(content,  {centered: true}).result.then((result) => {
        //this.closeResult = `Closed with: ${result}`;
      }, (reason) => {
        //this.closeResult = `Dismissed ${this.getDismissReason(reason)}`;
      });
    } catch (error) {
      
    }
    
  }

  addConditionNoti(content){
    this.conditionData[this.conditionIndex] = {
      year:this.conditionYear,
      percent:this.conditionPercent,
      weight:this.conditionWeight
    }
    this.plantGroupTypeForm.get(this.conditionKey).value[this.conditionIndex].notification_period = this.conditionData[this.conditionIndex];
    this.modalService.dismissAll(content);
    this.displayCondition(this.conditionIndex,'room5')
  }
  
  displayCondition(index,room){
  
    try {
      let d = this.plantGroupTypeForm.get(room).value[index].notification_period;
      let txt = '';
      if(d!=null){
        if(d.year != null &&  d.year != ''){
          txt += d.year+' ';
        }
        if(d.percent != null &&  d.percent != ''){
          txt += ','+d.percent+' ';
        }
        if(d.weight != null &&  d.weight != ''){
          txt += ','+d.weight+' ';
        }
        if(txt.length != 0){
          this.textRoom[room][index] = txt
        }else{
          this.textRoom[room][index] = '-'
        }
      }
      
      
    } catch (error) {
      this.textRoom[room][index] = '-'
    }
    
  }

}
