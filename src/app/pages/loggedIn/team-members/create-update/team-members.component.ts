import {Component, OnInit} from '@angular/core';
import {TreeviewConfig, TreeviewItem} from 'ngx-treeview';
import {getRoleResources, MustMatch} from '../../../../utils';
import {FormBuilder, FormGroup, Validators} from '@angular/forms';
import {ActivatedRoute, Router} from '@angular/router';
import {AlertService, RoleService, UserService} from '../../../../services';
import {first} from 'rxjs/operators';
import {environment} from '../../../../../environments/environment';

@Component({
  selector: 'app-member-page',
  templateUrl: './team-members.component.html',
})
export class NgbdCreateUpdateTeamMemberComponent implements OnInit {
  roleResources: TreeviewItem[];
  profileForm: FormGroup;
  submitted = false;
  id = null;
  apiError = false;
  roles: any = [];
  loading = true;
  file = null;
  profileImg = null;

  options: any = {
    cropEnabled: false,
    autoUpload: true,
    resizeOnLoad: false,
    thumbnailHeight: 320,
    thumbnailWidth: 320,
    uploadUrl: 'https://fancy-image-uploader-demo.azurewebsites.net/api/demo/upload',
    allowedImageTypes: ['image/png', 'image/jpeg']
  };
  config = TreeviewConfig.create({
    hasAllCheckBox: false,
    hasFilter: false,
    hasCollapseExpand: false,
    decoupleChildFromParent: false,
    maxHeight: 800
  });

  onUpload(data) {
    this.profileImg = null;
    const {file} = data;
    this.file = file;
  }


  constructor(private formBuilder: FormBuilder,
              private userService: UserService,
              private roleService: RoleService,
              private alertService: AlertService,
              public router: Router,
              private activeRoute: ActivatedRoute) {

  }

  ngOnInit(): void {
    this.activeRoute.params.subscribe(params => {
      this.id = params.id;
      if (params && params.id && params.id !== 'create') {
        this.onGetSpecificUserInformation(this.id);
        setTimeout(() => {
          if (this.id !== 'create' && this.profileForm) {
            this.profileForm.get('password').setValidators([]);
            this.profileForm.get('confirmPassword').setValidators([]);
            this.profileForm.get('password').updateValueAndValidity();
            this.profileForm.get('confirmPassword').updateValueAndValidity();
          }
        }, 500);
      }
    });

    this.roleResources = getRoleResources([], true);
    this.profileForm = this.formBuilder.group({
      gender: ['', Validators.required],
      firstName: ['', Validators.required],
      lastName: ['', Validators.required],
      email: ['', [Validators.required, Validators.email]],
      password: ['', [Validators.required, Validators.minLength(6)]],
      confirmPassword: ['', Validators.required],
      status: ['', Validators.required],
      username: ['', Validators.required],
      role: ['', Validators.required],
    }, {
      validator: MustMatch('password', 'confirmPassword')
    });
    this.onGetRolesListing();
  }

  get f() {
    if (!this.apiError && this.submitted && !this.profileForm.invalid) {
      this.alertService.error(null);
    }

    return this.profileForm.controls;
  }

  onSubmit() {
    this.submitted = true;
    this.apiError = false;

    if (this.profileForm.invalid) {
      this.alertService.error('Some fields are required. Please fill those fields');
      return;
    }

    this.alertService.reset();
    this.loading = true;

    this.userService.createOrUpdate({...this.profileForm.value, file: this.file}, this.id)
      .pipe(first())
      .subscribe(
        ({data = {}}: any) => {
          this.router.navigate(['/admin/manage/teams/listing']);
          this.loading = false;
        },
        error => {
          this.loading = false;
          this.apiError = true;
          this.alertService.error(error);
        });
  }

  onGetRolesListing() {
    this.loading = true;
    this.roleService.getAll()
      .pipe(first())
      .subscribe(
        ({data = []}: any) => {
          this.roles = data;
          this.loading = false;
        },
        error => {
          this.loading = false;
          this.alertService.error(error);
        });
  }

  onGetSpecificUserInformation(id: string) {
    this.loading = true;
    this.userService.getSpecific(id)
      .pipe(first())
      .subscribe(
        ({data = {}}: any) => {
          const {name: {firstName = '', lastName = ''} = {}, gender, email, userName = '', status = '', role = '', profileImg = ''} = data;
          this.profileImg = profileImg;
          this.profileForm.get('gender').setValue(gender);
          this.profileForm.get('firstName').setValue(firstName);
          this.profileForm.get('lastName').setValue(lastName);
          this.profileForm.get('email').setValue(email);
          this.profileForm.get('username').setValue(userName);
          this.profileForm.get('status').setValue(status);
          this.profileForm.get('role').setValue(role);
          this.onGetRolesInformation(role);
          this.loading = false;
        },
        error => {
          this.loading = false;
          this.apiError = true;
          this.alertService.error(error);
        });
  }

  onChangeRole({target: {value = ''} = {}}) {
    if (value) {
      this.onGetRolesInformation(value);
    }
  }

  onGetRolesInformation(id: string) {
    this.roleService.getSpecific(id)
      .pipe(first())
      .subscribe(
        ({data = {}}: any) => {
          const {rolesInfo} = data;
          this.roleResources = getRoleResources(rolesInfo, true);
        },
        error => {
          this.alertService.error(error);
        });
  }

  getUserImage(imageUrl) {
    if (imageUrl) {
      return `${environment.endpoint}/profile-pictures/${imageUrl}`;
    }

    return 'https://banner2.kisspng.com/20180402/ojw/kisspng-united-states-avatar-organization-information-user-avatar-5ac20804a62b58.8673620215226654766806.jpg';
  }

  onCancel() {
    this.router.navigate(['/admin/manage/teams/listing']);
  }
}
