import {Component, OnInit} from '@angular/core';
import {ActivatedRoute, Router} from '@angular/router';
import {FormBuilder, FormGroup, Validators} from '@angular/forms';
import {AlertService, FrontendStaticService, UserService} from '../../../../../services';
import {getRoleResources} from '../../../../../utils';
import {TreeviewItem} from 'ngx-treeview';
import {first} from 'rxjs/operators';
import {environment} from '../../../../../../environments/environment';
import {AngularEditorConfig} from "@kolkov/angular-editor";

@Component({
  selector: 'app-add-update-plant-Intro-page',
  templateUrl: './add-update.component.html',
})
export class NgbdAddUpdateIntroComponent implements OnInit {
  plantCategoryForm: FormGroup;
  submitted = false;
  roleResources: TreeviewItem[];
  apiError = false;
  id = null;
  imageFileDat = null;
  imageFile = null;
  loading = false;
  editorConfig: AngularEditorConfig = {
    editable: true,
    spellcheck: true,
    height: 'auto',
    minHeight: '0',
    maxHeight: 'auto',
    width: 'auto',
    minWidth: '0',
    translate: 'yes',
    enableToolbar: true,
    showToolbar: true,
    placeholder: 'Enter text here...',
    defaultParagraphSeparator: '',
    defaultFontName: '',
    defaultFontSize: '',
    fonts: [
      {class: 'arial', name: 'Arial'},
      {class: 'times-new-roman', name: 'Times New Roman'},
      {class: 'calibri', name: 'Calibri'},
      {class: 'comic-sans-ms', name: 'Comic Sans MS'}
    ],
    customClasses: [
      {
        name: 'quote',
        class: 'quote',
      },
      {
        name: 'redText',
        class: 'redText'
      },
      {
        name: 'titleText',
        class: 'titleText',
        tag: 'h1',
      },
    ],
    sanitize: true,
    toolbarPosition: 'top',
  };


  constructor(private router: Router,
              private formBuilder: FormBuilder,
              private userService: UserService,
              private activeRoute: ActivatedRoute,
              private frontendService: FrontendStaticService,
              private alertService: AlertService) {
  }

  ngOnInit() {
    this.roleResources = getRoleResources();
    this.plantCategoryForm = this.formBuilder.group({
      status: ['', Validators.required],
      nameIntro: ['', Validators.required],
      linkUrl: ['', Validators.required],
      topic: ['', Validators.required],
      description: ['', Validators.required],
    });
    this.activeRoute.params.subscribe(params => {
      const {id} = params;
      this.id = id;
      if (id && id !== 'create') {
        this.onGetBannerInformation(id);
      }
    });
  }

  get f() {
    if (!this.apiError && this.submitted && !this.plantCategoryForm.invalid) {
      this.alertService.error(null);
    }

    return this.plantCategoryForm.controls;
  }

  onSubmit() {
    this.submitted = true;
    this.apiError = false;
    if (this.plantCategoryForm.invalid) {
      this.alertService.error('Some fields are required. Please fill those fields');
      return;
    }


    this.alertService.reset();
    this.loading = true;

    this.frontendService.createOrUpdate({
      ...this.plantCategoryForm.value,
      file: this.imageFile
    }, this.id, 'intro')
      .pipe(first())
      .subscribe(
        () => {
          this.loading = false;
          this.router.navigate(['/admin/frontend/intro/listing']);
        },
        error => {
          this.loading = false;
          this.apiError = true;
          this.alertService.error(error);
        });
  }

  onFilterChange($event) {
    console.log('Filter Change', $event);
  }

  onGetBannerInformation(id: string) {
    this.loading = true;
    this.frontendService.getSpecific(id, 'intro')
      .pipe(first())
      .subscribe(
        ({data = {}}: any) => {
          this.imageFileDat = data.imageFile;
          Object.keys(data).forEach(key => {
            if (this.plantCategoryForm.get(key)) {
              this.plantCategoryForm.get(key).setValue(data[key]);
            }
          });
          this.loading = false;
        },
        error => {
          this.loading = false;
          this.apiError = true;
          this.alertService.error(error);
        });
  }

  onCancel() {
    this.router.navigate(['/admin/frontend/intro/listing']);
  }

  onFileUpload({srcElement: {files = []} = {}}, key) {
    this[key] = files[0];
  }

  getAttachment(basePath, name) {
    if (name) {
      return `${environment.endpoint}/${basePath}/${name}`;
    }

    return null;
  }
}
