import {Component, OnInit} from '@angular/core';
import {ActivatedRoute, Router} from '@angular/router';
import {FormBuilder, FormGroup, Validators} from '@angular/forms';
import {AlertService, FrontendStaticService, UserService} from '../../../../../services';
import {getRoleResources} from '../../../../../utils';
import {TreeviewConfig, TreeviewItem} from 'ngx-treeview';
import {first} from 'rxjs/operators';
import {AngularEditorConfig} from '@kolkov/angular-editor';
import {environment} from '../../../../../../environments/environment';

@Component({
  selector: 'app-add-update-plant-Setting-page',
  templateUrl: './add-update.component.html',
})
export class NgbdAddUpdateSettingComponent implements OnInit {
  plantCategoryForm: FormGroup;
  submitted = false;
  roleResources: TreeviewItem[];
  apiError = false;
  id = null;
  imageFile = null;
  imageFileDat = null;
  loading = false;

  constructor(private router: Router,
              private formBuilder: FormBuilder,
              private userService: UserService,
              private activeRoute: ActivatedRoute,
              private frontendService: FrontendStaticService,
              private alertService: AlertService) {
  }

  ngOnInit() {
    this.roleResources = getRoleResources();
    this.plantCategoryForm = this.formBuilder.group({
      status: ['', Validators.required],
      topic: ['', Validators.required],
      link: ['', Validators.required],
    });
    this.activeRoute.params.subscribe(params => {
      const {id} = params;
      this.id = id;
      if (id && id !== 'create') {
        this.onGetSettingInformation(id);
      }
    });
  }

  get f() {
    if (!this.apiError && this.submitted && !this.plantCategoryForm.invalid) {
      this.alertService.error(null);
    }

    return this.plantCategoryForm.controls;
  }

  onSubmit() {
    this.submitted = true;
    this.apiError = false;
    if (this.plantCategoryForm.invalid) {
      this.alertService.error('Some fields are required. Please fill those fields');
      return;
    }


    this.alertService.reset();
    this.loading = true;

    this.frontendService.createOrUpdate({...this.plantCategoryForm.value, file: this.imageFile}, this.id, 'banner')
      .pipe(first())
      .subscribe(
        () => {
          this.loading = false;
          this.router.navigate(['/admin/frontend/banner/listing']);
        },
        error => {
          this.loading = false;
          this.apiError = true;
          this.alertService.error(error);
        });
  }

  onFilterChange($event) {
    console.log('Filter Change', $event);
  }

  onGetSettingInformation(id: string) {
    this.loading = true;
    this.frontendService.getSpecific(id, 'banner')
      .pipe(first())
      .subscribe(
        ({data = {}}: any) => {
          this.imageFileDat = data.imageFile;
          Object.keys(data).forEach(key => {
            if (this.plantCategoryForm.get(key)) {
              this.plantCategoryForm.get(key).setValue(data[key]);
            }
          });
          this.loading = false;
        },
        error => {
          this.loading = false;
          this.apiError = true;
          this.alertService.error(error);
        });
  }

  onCancel() {
    this.router.navigate(['/admin/frontend/banner/listing']);
  }

  onFileUpload({srcElement: {files = []} = {}}, key) {
    this[key] = files[0];
  }

  getAttachment(basePath, name) {
    if (name) {
      return `${environment.endpoint}/${basePath}/${name}`;
    }

    return null;
  }
}
