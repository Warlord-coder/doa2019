import {Component} from '@angular/core';
import {DataTableParams, DataTableResource} from 'ngx-datatable-bootstrap4';
import {NgbModal} from '@ng-bootstrap/ng-bootstrap';
import {Router, ActivatedRoute} from '@angular/router';
import {first} from 'rxjs/operators';
import {toLower} from 'lodash';
import * as moment from 'moment';

import {AlertService, FrontendStaticService} from '../../../../../services';
import {environment} from '../../../../../../environments/environment';

@Component({
  selector: 'app-listing-frontend-user-page',
  templateUrl: './listing.component.html',
})
export class NgbdListingFrontendUserComponent {
  resources = new DataTableResource<any>([]);
  items: any[] = [];
  tempItems: any[] = [];
  count = 0;
  loading = false;
  momentObj: any;
  deletedId = null;
  roles = [];
  constructor(private modalService: NgbModal,
              private frontendService: FrontendStaticService,
              private alertService: AlertService,
              private router: Router,
              private route: ActivatedRoute) {
    this.onGetListing();
    this.momentObj = moment;
  }

  ngOnInit(): void {
    this.route
      .data
      .subscribe(data => {
        this.roles = data['roles'];
      });
  }
  
  reloadData(params: DataTableParams) {
    this.resources.query(params).then(vals => {
      this.tempItems = vals;
      this.items = vals;
    });
  }

  onEdit({_id}) {
    this.router.navigate([`/admin/frontend/users/${_id}`]);
  }

  onSearch({target: {value: searchName = ''} = {}}) {
    this.items = this.tempItems.filter(({name}) => name && toLower(name).includes(toLower(searchName)));
  }

  onGetListing() {
    this.loading = true;
    this.frontendService.getAll(false, 'seed-users')
      .pipe(first())
      .subscribe(
        ({data = []}: any) => {
          this.loading = false;
          this.resources = new DataTableResource<any>(data);
          this.resources.count().then(count => this.count = count);
          this.reloadData({});
        },
        error => {
          this.loading = false;
          this.alertService.error(error);
        });
  }
  checkRole(index){
    if(localStorage.getItem('superAdmin') === "true") return true;
    if(localStorage.getItem('rolesId').indexOf(this.roles[index]) > -1) return true;
    return false;
  }
}
