import {Component, OnInit} from '@angular/core';
import {ActivatedRoute, Router} from '@angular/router';
import {FormBuilder, FormGroup, Validators} from '@angular/forms';
import {AlertService, FrontendStaticService, UserService} from '../../../../../services';
import {getRoleResources, MustMatch} from '../../../../../utils';
import {TreeviewItem} from 'ngx-treeview';
import {first} from 'rxjs/operators';
import {environment} from '../../../../../../environments/environment';

@Component({
  selector: 'app-add-update-user-frontend-page',
  templateUrl: './add-update.component.html',
})
export class NgbdAddUpdateUserFrontendComponent implements OnInit {
  registerForm: FormGroup;
  submitted = false;
  roleResources: TreeviewItem[];
  apiError = false;
  id = null;
  loading = false;

  constructor(private router: Router,
              private formBuilder: FormBuilder,
              private userService: UserService,
              private activeRoute: ActivatedRoute,
              private frontendService: FrontendStaticService,
              private alertService: AlertService) {
  }

  ngOnInit() {
    this.roleResources = getRoleResources();
    this.registerForm = this.formBuilder.group({
      name: ['', [Validators.required]],
      gender: ['', [Validators.required]],
      age: ['', [Validators.required]],
      career: ['', [Validators.required]],
      workPlace: ['', [Validators.required]],
      currentAddress: ['', [Validators.required]],
      telephoneNo: ['', [Validators.required]],
      email: ['', [Validators.required, Validators.email]],
      password: ['', [Validators.required, Validators.minLength(6)]],
      confirmPassword: ['', Validators.required],
    }, {
      validator: MustMatch('password', 'confirmPassword')
    });
    this.activeRoute.params.subscribe(params => {
      const {id} = params;
      if (id && id !== 'create') {
        this.onGetUserInformation(id);
      }
      this.id = id === 'create' ? 'register' : id;
    });
  }

  get f() {
    if (!this.apiError && this.submitted && !this.registerForm.invalid) {
      this.alertService.error(null);
    }

    return this.registerForm.controls;
  }

  onSubmit() {
    this.submitted = true;
    this.apiError = false;
    if (this.registerForm.invalid) {
      this.alertService.error('Some fields are required. Please fill those fields');
      return;
    }


    this.alertService.reset();
    this.loading = true;

    this.frontendService.createOrUpdate({...this.registerForm.value}, this.id, 'seed-users', false)
      .pipe(first())
      .subscribe(
        () => {
          this.loading = false;
          this.router.navigate(['/admin/frontend/users/listing']);
        },
        error => {
          this.loading = false;
          this.apiError = true;
          this.alertService.error(error);
        });
  }

  onFilterChange($event) {
    console.log('Filter Change', $event);
  }

  onGetUserInformation(id: string) {
    this.loading = true;
    this.frontendService.getSpecific(id, 'seed-users')
      .pipe(first())
      .subscribe(
        ({data = {}}: any) => {
          Object.keys(data).forEach(key => {
            if (this.registerForm.get(key)) {
              this.registerForm.get(key).setValue(data[key]);
            }
          });
          this.loading = false;
        },
        error => {
          this.loading = false;
          this.apiError = true;
          this.alertService.error(error);
        });
  }

  onCancel() {
    this.router.navigate(['/admin/frontend/users/listing']);
  }

  onFileUpload({srcElement: {files = []} = {}}, key) {
    this[key] = files[0];
  }

  getAttachment(basePath, name) {
    if (name) {
      return `${environment.endpoint}/${basePath}/${name}`;
    }

    return null;
  }
}
