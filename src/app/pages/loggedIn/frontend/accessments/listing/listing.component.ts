import {Component} from '@angular/core';
import {DataTableParams, DataTableResource} from 'ngx-datatable-bootstrap4';
import {NgbModal} from '@ng-bootstrap/ng-bootstrap';
import {Router, ActivatedRoute} from '@angular/router';
import {first} from 'rxjs/operators';
import * as moment from 'moment';
import {FormBuilder, Validators, FormGroup} from '@angular/forms';

import {AlertService, FrontendStaticService} from '../../../../../services';
import {Environment} from '@angular/compiler-cli/src/ngtsc/typecheck/src/environment';
import {environment} from '../../../../../../environments/environment';

@Component({
  selector: 'app-listing-assessment-page',
  templateUrl: './listing.component.html',
})
export class NgbdListingAccessmentCategoryComponent {
  resources = new DataTableResource<any>([]);
  accessmentFormSearch: FormGroup;
  items: any[] = [];
  tempItems: any[] = [];
  count = 0;
  loading = false;
  momentObj: any;
  viewItem = null;
  roles = [];

  constructor(private modalService: NgbModal,
              private formBuilder: FormBuilder,
              private frontendService: FrontendStaticService,
              private alertService: AlertService,
              private router: Router,
              private route: ActivatedRoute) {
    this.onGetListing();
    this.momentObj = moment;
    this.accessmentFormSearch = this.formBuilder.group({
      fromDate: [''],
      toDate: [''],
      rating: [''],
    });
  }

  ngOnInit(): void {
    this.route
      .data
      .subscribe(data => {
        this.roles = data['roles'];
      });
  }

  reloadData(params: DataTableParams) {
    this.resources.query(params).then(vals => {
      this.tempItems = vals;
      this.items = vals;
    });
  }

  onEdit({_id}) {
    this.router.navigate([`/admin/frontend/news/${_id}`]);
  }

  onView(item: any, viewContent) {
    this.viewItem = item;
    this.modalService.open(viewContent, {centered: true});
  }

  onGetListing() {
    this.loading = true;
    this.frontendService.getAll(false, 'accessement')
      .pipe(first())
      .subscribe(
        ({data = []}: any) => {
          this.loading = false;
          this.resources = new DataTableResource<any>(data);
          this.resources.count().then(count => this.count = count);
          this.reloadData({});
        },
        error => {
          this.loading = false;
          this.alertService.error(error);
        });
  }

  onSearch() {
    const {fromDate, toDate, rating} = this.accessmentFormSearch.value;
    this.items = this.tempItems.filter((item) => {
      if (rating && item.rating !== rating) {
        return false;
      }

      if (toDate && !moment(toDate).isSameOrBefore(item.createdAt)) {
        return false;
      }

      if (fromDate && !moment(fromDate).isSameOrBefore(item.createdAt)) {
        return false;
      }

      return true;
    });
  }

  onExportExcel() {
    const {fromDate, toDate, rating} = this.accessmentFormSearch.value;
    window.open(`${environment.apiUrl}accessement/list?excelImport=true&token=Bearer ${localStorage.getItem('authorization')}&rating=${rating}&toDate=${toDate}&fromDate=${fromDate}`);
  }
  checkRole(index){
    if(localStorage.getItem('superAdmin') === "true") return true;
    if(localStorage.getItem('rolesId').indexOf(this.roles[index]) > -1) return true;
    return false;
  }
}
