import {Component, OnInit} from '@angular/core';
import {ActivatedRoute, Router} from '@angular/router';
import {FormBuilder, FormGroup, Validators} from '@angular/forms';
import {AlertService} from '../../../../services';
import {getRoleResources} from '../../../../utils';
import {TreeviewConfig, TreeviewItem} from 'ngx-treeview';
import {first} from 'rxjs/operators';
import {CommonAumperService} from '../../../../services/common-aumper.service';

@Component({
  selector: 'app-add-update-common-country-page',
  templateUrl: './add-update.component.html',
})
export class NgbdAddUpdateAumperCountryComponent implements OnInit {
  AumperCountryForm: FormGroup;
  submitted = false;
  roleResources: TreeviewItem[];
  loading = false;
  apiError = false;
  id = null;
  config = TreeviewConfig.create({
    hasAllCheckBox: true,
    hasFilter: true,
    hasCollapseExpand: true,
    decoupleChildFromParent: false,
    maxHeight: 800
  });


  constructor(private router: Router,
              private formBuilder: FormBuilder,
              private activeRoute: ActivatedRoute,
              private commonAumperService: CommonAumperService,
              private alertService: AlertService) {
  }

  ngOnInit() {
    this.roleResources = getRoleResources();
    this.AumperCountryForm = this.formBuilder.group({
      status: ['', Validators.required],
      aumperName_en: ['', Validators.required],
      aumperName_th: ['', Validators.required]
    });
    this.activeRoute.params.subscribe(params => {
      const {id} = params;
      this.id = id;
      if (id && id !== 'create') {
        this.onGetCommonAumperInformation(id);
      }
    });
  }

  get f() {
    if (!this.apiError && this.submitted && !this.AumperCountryForm.invalid) {
      this.alertService.error(null);
    }

    return this.AumperCountryForm.controls;
  }

  onSubmit() {
    this.submitted = true;
    if (this.AumperCountryForm.invalid) {
      this.alertService.error('Some fields are required. Please fill those fields');
    }

    this.loading = true;
    this.apiError = false;
    this.commonAumperService.createOrUpdate({...this.AumperCountryForm.value}, this.id)
      .pipe(first())
      .subscribe(
        () => {
          this.loading = false;
          this.router.navigate(['admin/common/aumper/listing']);
        },
        error => {
          this.loading = false;
          this.apiError = true;
          this.alertService.error(error);
        });
  }

  onSelectedChange($event) {
    console.log('Select', $event);
  }

  onGetCommonAumperInformation(id: string) {
    this.loading = true;
    this.commonAumperService.getSpecific(id)
      .pipe(first())
      .subscribe(
        ({data = {}}: any) => {
          const {status, aumperName_en, aumperName_th} = data;
          this.AumperCountryForm.get('status').setValue(status);
          this.AumperCountryForm.get('aumperName_en').setValue(aumperName_en);
          this.AumperCountryForm.get('aumperName_th').setValue(aumperName_th);
          this.loading = false;
        },
        error => {
          this.loading = false;
          this.apiError = true;
          this.alertService.error(error);
        });
  }

  onCancel() {
    this.router.navigate(['admin/common/aumper/listing']);
  }
}
