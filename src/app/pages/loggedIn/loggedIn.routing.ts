import {Routes} from '@angular/router';
import {NgbdCreateUpdateTeamMemberComponent} from './team-members/create-update/team-members.component';
import {NgbdListingTeamMemberComponent} from './team-members/listing/listing.component';
import {NgbdAdminManageComponent} from './admin-manage/admin-mange.component';
import {NgbdAddRoleComponent} from './role-resource/add-role/add-role.component';
import {NgbdListingRoleComponent} from './role-resource/listing/listing.component';
import {NgbdListingPlantCategoryComponent} from './plant-category/listing/listing.component';
import {NgbdAddUpdatePlantCategoryComponent} from './plant-category/add-update/add-update.component';
import {NgbdListingPlantCharacterComponent} from './plant-character/listing/listing.component';
import {NgbdAddUpdatePlantCharacterComponent} from './plant-character/add-update/add-update.component';
import {NgbdAddUpdatePlantGroupComponent} from './plant-group/add-update/add-update.component';
import {NgbdListingPlantGroupComponent} from './plant-group/listing/listing.component';
import {NgbdListingPlantGroupTypeComponent} from './plant-group-type/listing/listing.component';
import {NgbdAddUpdatePlantGroupTypeComponent} from './plant-group-type/add-update/add-update.component';
import {NgbdListingPlantTypeComponent} from './plant-type/listing/listing.component';
import {NgbdAddUpdatePlantTypeComponent} from './plant-type/add-update/add-update.component';
import {NgbdAddUpdateCommonProvinceListComponent} from './common-province-list/add-update/add-update.component';
import {NgbdCommonProvinceListComponent} from './common-province-list/listing/listing.component';
import {NgbdCommonUnitListComponent} from './common-unit-list/listing/listing.component';
import {NgbdAddUpdateCommonUnitListComponent} from './common-unit-list/add-update/add-update.component';
import {NgbdCommonConditionComponent} from './common-condition/listing/listing.component';
import {NgbdAddUpdateCommonConditionComponent} from './common-condition/add-update/add-update.component';
import {NgbdCommonAmuperListComponent} from './common-aumper/listing/listing.component';
import {NgbdAddUpdateAumperCountryComponent} from './common-aumper/add-update/add-update.component';
import {NgbdCommonCountryListComponent} from './common-country/listing/listing.component';
import {NgbdAlertListComponent} from './withdraw/alert-list/alertList.component';
import {NgbdAlertListDisComponent} from './distribution/alert-list/alertList.component';
import {NgbdAddUpdateCommonCountryComponent} from './common-country/add-update/add-update.component';
import {NgbdRegisterPassportListComponent} from './register-passport/listing/listing.component';
import {NgbdPassportYearReportComponent} from './report/year/year-report.component';
import {NgbdPlantMonthReportComponent} from './report/month/month-report.component';
import {NgbdLabStatusReportComponent} from './report/labstatus/labstatus-report.component';
import {NgbdLabPlantReportComponent} from './report/labplant/labplant-report.component';
import {NgbdFormTypeReportComponent} from './report/formtype/formtype-report.component';
import {NgbdReportTypeReportComponent} from './report/reporttype/reporttype-report.component';
import {NgbdDistributionReportComponent} from './report/distribution/distribution-report.component';
import {NgbdWithdrawReportComponent} from './report/withdraw/withdraw-report.component';
import {NgbdRecoveryReportComponent} from './report/recovery/recovery-report.component';
import {NgbdAllstatusReportComponent} from './report/allstatus/allstatus-report.component';
import {NgbdRegisterVolsListComponent} from './register-vols/listing/listing.component';
import {NgbdListingRegisterPlant12Component} from './register-plant-1-2/listing/listing.component';
import {NgbdAttributeSetListComponent} from './attribute-set/listing/listing.component';
import {NgbdAddUpdateAttributeSetComponent} from './attribute-set/add-update/add-update.component';
import {NgbdAttributeGroupSetListComponent} from './attribute-set-list/listing/listing.component';
import {NgbdAddUpdateAttributeSetListComponent} from './attribute-set-list/add-update/add-update.component';
import {NgbdAttributePlantDOAListComponent} from './plant-doa/listing/listing.component';
import {NgbdAddUpdatePlantDOAComponent} from './plant-doa/add-update/add-update.component';
import {NgbdListingPlantComponent} from './plant/listing/listing.component';
import {NgbdAddUpdatePlantComponent} from './plant/add-update/add-update.component';
import {NgbdAddUpdatePlantRegisterComponent} from './register-plant-1-2/add-update/add-update.component';
import {NgbdListingLabComponent} from './lab-manage/listing/listing.component';
import {NgbdCreateLabComponent} from './lab-manage/listing-create/listing.component';
import {NgbdPlantsLabComponent} from './lab-manage/listing-pending/listing.component';
import {NgbdAddUpdatePlantLabComponent} from './lab-manage/add-update/add-update.component';
import {NgbdPlantsCompletedComponent} from './lab-manage/plant-completed/listing.component';
import {NgbdWHLabListingComponent} from './warehouse/lab-no-listing/listing.component';
import {NgbdWHLabAlertListingComponent} from './warehouse/lab-no-listing-alert/listing.component';
import {NgbdWHLabPlantComponent} from './warehouse/lab-plant/lab-plant.component';
import {NgbdWHPendingCompleteListingComponent} from './warehouse/listing-pending-complete/listing.component';
import {NgbdAddUpdateNewPlantComponent} from './new-plant/add-update/add-update.component';
import {NgbdRegisterRecoveryListComponent} from './recovery/listing/listing.component';
import {NgbdRegisterRecoveryListPendingComponent} from './recovery/listing-pending/listing.component';
import {NgbdRegisterRecoveryListPendingCompleteComponent} from './recovery/recover-listing/listing.component';
import {NgbdSendReceiveComponent} from './recovery/send-receive/add-update.component';
import {NgbdRegisterPlantUnassignedComponent} from './plant-unassign-list/listing.component';
import {NgbdListingRegisterPlant3Component} from './register-plant-3/listing/listing.component';
import {NgbdAddUpdatePlantRegister4Component} from './register-plant-4/add-update.component';
import {NgbdAddUpdatePlantRegister3Component} from './register-plant-3/add-update/add-update.component';
import {NgbdLabAlertListPendingComponent} from './lab-alert/listing-pending/listing.component';
import {NgbdRegisterAlertFormListPendingCompleteComponent} from './lab-alert/alert-form-listing/listing.component';
import {NgbdListingLabAlertComponent} from './lab-alert/listing/listing.component';
import {NgbdAddUpdateLabAlertLabComponent} from './lab-alert/add-update/add-update.component';
import {NgbdWHAlertPendingCompleteListingComponent} from './warehouse/alert-listing-pending-complete/listing.component';
import {NgbdAddUpdateNewPlantAlertComponent} from './form-new-plant-alert/add-update/add-update.component';
import {AuthGuard} from '../../utils/helpers';
import {NgbdAddUpdateFormKeepComponent} from './form-keep/add-update/add-update.component';
import {NgbdWithDrawListComponent} from './withdraw/listing/listing.component';
import {NgbdRegisterWithDrawListPendingComponent} from './withdraw/listing-pending/listing.component';
import {NgbdRegisterWithDrawListPlantsComponent} from './withdraw/listing-withdraw-plants/listing.component';
import {NgbdAddUpdateFormBankOfPlantComponent} from './withdraw/form-bank-of-plant/add-update/add-update.component';
import {NgbdWithWareHouseDrawListComponent} from './warehouse/withdraw-listing/listing.component';
import {NgbdRegisterWithDrawListPlantsWarehouseComponent} from './warehouse/listing-withdraw-plants/listing.component';
import {NgbdWithDistributionListComponent} from './distribution/listing/listing.component';
import {NgbdRegisterDistributionListPendingComponent} from './distribution/listing-pending/listing.component';
import {NgbdRegisterDistributionPlantsComponent} from './distribution/listing-withdraw-plants/listing.component';
import {NgbdAddUpdateDistributionFormBankOfPlantComponent} from './distribution/form-bank-of-plant/add-update/add-update.component';
import {NgbdWithWareHouseDistributionListComponent} from './warehouse/distribution-listing/listing.component';
import {NgbdRegisterDistributionListPlantsWarehouseComponent} from './warehouse/listing-distribution-plants/listing.component';
import {NgbdWHDistributionStatusComponent} from './warehouse/distrubtion-status/listing.component';
import {NgbdListingEventNewsCategoryComponent} from './frontend/event-news/listing/listing.component';
import {NgbdAddUpdateEventNewsComponent} from './frontend/event-news/add-update/add-update.component';
import {NgbdListingKnowledgeNewsCategoryComponent} from './frontend/knowledge/listing/listing.component';
import {NgbdAddUpdateKnowledgeComponent} from './frontend/knowledge/add-update/add-update.component';
import {NgbdListingBannerCategoryComponent} from './frontend/banner/listing/listing.component';
import {NgbdAddUpdateBannerComponent} from './frontend/banner/add-update/add-update.component';
import {NgbdAddUpdateIntroComponent} from './frontend/intro/add-update/add-update.component';
import {NgbdListingIntroCategoryComponent} from './frontend/intro/listing/listing.component';
import {NgbdListingAccessmentCategoryComponent} from './frontend/accessments/listing/listing.component';
import {NgbdListingFrontendUserComponent} from './frontend/users/listing/listing.component';
import { NgbdAddUpdateUserFrontendComponent } from './frontend/users/add-update/add-update.component';
import {NgbdAddUpdateWithdrawFormKeepComponent} from './withdraw/form-keep/add-update/add-update.component';
import {NgbdListingAlertComponent} from './lab-alert/alert-list/alert-list.component';	
import {NgbdListingRecoveryAlertComponent} from './recovery/alert-list/alert-list.component';
import {NgbdWithWareHouseDistributionAlertListComponent} from './warehouse/distribution-alert-listing/listing.component';
import {NgbdRegisterAlertDistributionListPlantsWarehouseComponent} from './warehouse/listing-alert-distribution-plants/listing.component';
import {NgbdWithWareHouseRecoveryAlertListComponent} from './warehouse/recovery-alert-listing/listing.component';	
import {NgbdRegisterAlertRecoveryListPlantsWarehouseComponent} from './warehouse/listing-alert-recovery-plants/listing.component';
import {NgbdM4CompletedListComponent} from './m4/listing/listing.component';
import {NgbdPlantAlertComponent} from './warehouse/plant-alert/plant-alert.component';
import { NgbdExportDoaComponent } from './export-doa/export-doa.component';
export const LoggedInRoutes: Routes = [
  {
    path: 'admin/manage',
    children: [{
      path: '',
      component: NgbdAdminManageComponent,
      canActivate: [AuthGuard],
      data: {
        title: 'Mange Team and Role Resources',
        urls: [4, 5, 6, 7]
      }
    }, {
      path: 'teams/listing',
      component: NgbdListingTeamMemberComponent,
      canActivate: [AuthGuard],
      data: {
        title: 'Team Listing',
        urls: [],
        roles: [8, 9, 10, 11],
        breadcrumbs: [
          {link: '/admin/manage', title: 'Admin Manage'},
          {link: '/admin/manage/teams/listing', title: 'Team Listing'}
        ]
      }
    }, {
      path: 'teams/:id',
      component: NgbdCreateUpdateTeamMemberComponent,
      canActivate: [AuthGuard],
      data: {
        title: 'Create or update team',
        urls: [],
        breadcrumbs: [
          {link: '/admin/manage', title: 'Admin Manage'},
          {link: '/admin/manage/teams/listing', title: 'Team Listing'},
          {link: '', title: 'Create or update team'}
        ]
      }
    }, {
      path: 'roles/listing',
      component: NgbdListingRoleComponent,
      canActivate: [AuthGuard],
      data: {
        title: 'Listing of role',
        urls: [],
        roles: [8, 9, 10, 11],
        breadcrumbs: [
          {link: '/admin/manage', title: 'Admin Manage'},
          {link: '/admin/manage/roles/listing', title: 'Listing of role'}
        ]
      }
    }, {
      path: 'roles/:id',
      component: NgbdAddRoleComponent,
      canActivate: [AuthGuard],
      data: {
        title: 'Create or update role',
        urls: [],
        breadcrumbs: [
          {link: '/admin/manage', title: 'Manage'},
          {link: '/admin/manage/roles/listing', title: 'Listing of role'},
          {link: '', title: 'Create or update role'}
        ]
      }
    }]
  },
  {
    path: 'admin/master',
    children: [{
      path: 'category/listing',
      component: NgbdListingPlantCategoryComponent,
      canActivate: [AuthGuard],
      data: {
        title: 'Plant Category Listing',
        urls: [],
        roles: [23, 24, 25, 26],
        breadcrumbs: [
          {link: '/admin/master', title: 'Admin Master'},
          {link: '/admin/master/category/listing', title: 'Category Listing'}
        ]
      }
    }, {
      path: 'category/:id',
      component: NgbdAddUpdatePlantCategoryComponent,
      canActivate: [AuthGuard],
      data: {
        title: 'Create or update category',
        urls: [],
        breadcrumbs: [
          {link: '/admin/master', title: 'Admin Master'},
          {link: '/admin/master/category/listing', title: 'Category Listing'},
          {link: '', title: 'Create or update category'}
        ]
      }
    }, {
      path: 'character/listing',
      component: NgbdListingPlantCharacterComponent,
      canActivate: [AuthGuard],
      data: {
        title: 'Plant Species Listing',
        urls: [],
        roles: [13, 14, 15, 16],
        breadcrumbs: [
          {link: '/admin/master', title: 'Admin Master'},
          {link: '/admin/master/character/listing', title: 'Plant Species Listing'}
        ]
      }
    }, {
      path: 'character/:id',
      component: NgbdAddUpdatePlantCharacterComponent,
      canActivate: [AuthGuard],
      data: {
        title: 'Create or update Species',
        urls: [],
        breadcrumbs: [
          {link: '/admin/master', title: 'Admin Master'},
          {link: '/admin/master/character/listing', title: 'Plant Species Listing'},
          {link: '', title: 'Create or update Species'}
        ]
      }
    }, {
      path: 'group/listing',
      component: NgbdListingPlantGroupComponent,
      canActivate: [AuthGuard],
      data: {
        title: 'Plant Group Listing',
        urls: [],
        roles: [18, 19, 20, 21],
        breadcrumbs: [
          {link: '/admin/master', title: 'Admin Master'},
          {link: '/admin/master/group/listing', title: 'Plant Group Listing'}
        ]
      }
    }, {
      path: 'group/:id',
      component: NgbdAddUpdatePlantGroupComponent,
      canActivate: [AuthGuard],
      data: {
        title: 'Create or update Group',
        urls: [],
        breadcrumbs: [
          {link: '/admin/master', title: 'Admin Master'},
          {link: '/admin/master/group/listing', title: 'Plant Group Listing'},
          {link: '', title: 'Create or update Group'}
        ]
      }
    }, {
      path: 'group-type/listing',
      component: NgbdListingPlantGroupTypeComponent,
      canActivate: [AuthGuard],
      data: {
        title: 'Plant Group Type Listing',
        urls: [],
        roles: [28, 29, 30, 31],
        breadcrumbs: [
          {link: '/admin/master', title: 'Admin Master'},
          {link: '/admin/master/group-type/listing', title: 'Plant Group Type Listing'}
        ]
      }
    }, {
      path: 'group-type/:id',
      component: NgbdAddUpdatePlantGroupTypeComponent,
      canActivate: [AuthGuard],
      data: {
        title: 'Create or update Group Type',
        urls: [],
        breadcrumbs: [
          {link: '/admin/master', title: 'Admin Master'},
          {link: '/admin/master/group-type/listing', title: 'Plant Group Type Listing'},
          {link: '', title: 'Create or update Group Type'}
        ]
      }
    }, {
      path: 'plant-type/listing',
      component: NgbdListingPlantTypeComponent,
      canActivate: [AuthGuard],
      data: {
        title: 'Plant List',
        urls: [],
        roles: [33, 34, 35, 36],
        breadcrumbs: [
          {link: '/admin/master', title: 'Admin Master'},
          {link: '/admin/master/plant-type/listing', title: 'Plant List'}
        ]
      }
    }, {
      path: 'plant-type/:id',
      component: NgbdAddUpdatePlantTypeComponent,
      canActivate: [AuthGuard],
      data: {
        title: 'Create or update Plant list',
        urls: [],
        breadcrumbs: [
          {link: '/admin/master', title: 'Admin Master'},
          {link: '/admin/master/plant-type/listing', title: 'Plant List'},
          {link: '', title: 'Create or update Plant list'}
        ]
      }
    }]
  },
  {
    path: 'admin/common',
    children: [{
      path: 'province-list/listing',
      component: NgbdCommonProvinceListComponent,
      canActivate: [AuthGuard],
      data: {
        title: 'Province Listing',
        urls: [],
        roles: [38, 39, 40, 41],
        breadcrumbs: [
          {link: '/admin/common', title: 'Admin Common'},
          {link: '/admin/common/province-list/listing', title: 'Province Listing'}
        ]
      }
    }, {
      path: 'province-list/:id',
      component: NgbdAddUpdateCommonProvinceListComponent,
      canActivate: [AuthGuard],
      data: {
        title: 'Create or update Province',
        urls: [],
        breadcrumbs: [
          {link: '/admin/common', title: 'Admin Common'},
          {link: '/admin/common/province-list/listing', title: 'Province Listing'},
          {link: '', title: 'Create or update Province'}
        ]
      }
    }, {
      path: 'unit/listing',
      component: NgbdCommonUnitListComponent,
      canActivate: [AuthGuard],
      data: {
        title: 'Unit Listing',
        urls: [],
        roles: [43, 44, 45, 46],
        breadcrumbs: [
          {link: '/admin/common', title: 'Admin Common'},
          {link: '/admin/common/unit/listing', title: 'Unit Listing'}
        ]
      }
    }, {
      path: 'unit/:id',
      component: NgbdAddUpdateCommonUnitListComponent,
      canActivate: [AuthGuard],
      data: {
        title: 'Create or update Unit',
        urls: [],
        breadcrumbs: [
          {link: '/admin/common', title: 'Admin Common'},
          {link: '/admin/common/unit/listing', title: 'Unit Listing'},
          {link: '', title: 'Create or update Unit'}
        ]
      }
    }, {
      path: 'country/listing',
      component: NgbdCommonCountryListComponent,
      canActivate: [AuthGuard],
      data: {
        title: 'Country Listing',
        urls: [],
        roles: [48, 49, 50, 51],
        breadcrumbs: [
          {link: '/admin/common', title: 'Admin Common'},
          {link: '/admin/common/country/listing', title: 'Country Listing'}
        ]
      }
    }, {
      path: 'country/:id',
      component: NgbdAddUpdateCommonCountryComponent,
      canActivate: [AuthGuard],
      data: {
        title: 'Create or update Country',
        urls: [],
        breadcrumbs: [
          {link: '/admin/common', title: 'Admin Common'},
          {link: '/admin/common/country/listing', title: 'Country Listing'},
          {link: '', title: 'Create or update Country'}
        ]
      }
    }, {
      path: 'condition/listing',
      component: NgbdCommonConditionComponent,
      canActivate: [AuthGuard],
      data: {
        title: 'Condition Listing',
        urls: [],
        roles: [58, 59, 60, 61],
        breadcrumbs: [
          {link: '/admin/common', title: 'Admin Common'},
          {link: '/admin/common/condition/listing', title: 'Condition Listing'}
        ]
      }
    }, {
      path: 'condition/:id',
      component: NgbdAddUpdateCommonConditionComponent,
      canActivate: [AuthGuard],
      data: {
        title: 'Create or update Condition',
        urls: [],
        breadcrumbs: [
          {link: '/admin/common', title: 'Admin Common'},
          {link: '/admin/common/condition/listing', title: 'Condition Listing'},
          {link: '', title: 'Create or update condition'}
        ]
      }
    }, {
      path: 'aumper/listing',
      component: NgbdCommonAmuperListComponent,
      canActivate: [AuthGuard],
      data: {
        title: 'Aumper Listing',
        urls: [],
        roles: [53, 54, 55, 56],
        breadcrumbs: [
          {link: '/admin/common', title: 'Admin Common'},
          {link: '/admin/common/aumper/listing', title: 'Aumper Listing'}
        ]
      }
    }, {
      path: 'aumper/:id',
      component: NgbdAddUpdateAumperCountryComponent,
      canActivate: [AuthGuard],
      data: {
        title: 'Create or update Aumper',
        urls: [],
        breadcrumbs: [
          {link: '/admin/common', title: 'Admin Common'},
          {link: '/admin/common/aumper/listing', title: 'Aumper Listing'},
          {link: '', title: 'Create or update Aumper'}
        ]
      }
    }]
  }, {
    path: 'admin/register',
    children: [{
      path: 'passport/listing',
      component: NgbdRegisterPassportListComponent,
      canActivate: [AuthGuard],
      data: {
        title: 'Passport List',
        urls: [],
        roles: [180, 181, 182],
        breadcrumbs: [
          {link: '/admin/register', title: 'Admin Register'},
          {link: '/admin/register/passport/listing', title: 'Passport List'}
        ]
      }
    }, {
      path: 'volume/:passortId',
      component: NgbdRegisterVolsListComponent,
      canActivate: [AuthGuard],
      data: {
        title: 'No Listing',
        urls: [],
        breadcrumbs: [
          {link: '/admin/register', title: 'Admin Register'},
          {link: '/admin/register/passport/listing', title: 'Passport List'},
          {link: '', title: 'No Listing'}
        ]
      }
    }, {
      path: 'unassigned-plant/:volume/listing',
      component: NgbdRegisterPlantUnassignedComponent,
      canActivate: [AuthGuard],
      data: {
        title: 'Unassigned Plant Listing (Recovery)',
        urls: [],
        breadcrumbs: [
          {link: '/admin/register', title: 'Admin Register'},
          {link: '/admin/register/passport/listing', title: 'Passport List'},
          {link: '', title: 'No Listing', checkId: true},
          {link: '', title: 'Unassigned Plant Listing (Recovery)'}
        ]
      }
    }, {
      path: 'register-3/:volume/listing',
      component: NgbdListingRegisterPlant3Component,
      canActivate: [AuthGuard],
      data: {
        title: 'Plant List',
        urls: [],
        breadcrumbs: [
          {link: '/admin/register', title: 'Admin Register'},
          {link: '/admin/register/passport/listing', title: 'Passport List'},
          {link: '', title: 'No Listing', checkId: true},
          {link: '', title: 'Plant List'}
        ]
      }
    }, {
      path: 'plant/:volume/listing',
      component: NgbdListingRegisterPlant12Component,
      canActivate: [AuthGuard],
      data: {
        title: 'Plant List',
        urls: [],
        breadcrumbs: [
          {link: '/admin/register', title: 'Admin Register'},
          {link: '/admin/register/passport/listing', title: 'Passport List'},
          {link: '', title: 'No Listing', checkId: true},
          {link: '', title: 'Plant List'}
        ]
      }
    }, {
      path: 'plant/:volume/:id',
      component: NgbdAddUpdatePlantRegisterComponent,
      canActivate: [AuthGuard],
      data: {
        title: 'Register Plant',
        urls: [],
        breadcrumbs: [
          {link: '/admin/register', title: 'Admin Register'},
          {link: '/admin/register/passport/listing', title: 'Passport List'},
          {link: '', title: 'No Listing', checkId: true},
          {link: '', title: 'Plant List', checkId1: true},
          {link: '', title: 'Register Plant'}
        ]
      }
    }, {
      path: 'plant-recover/:volume/:id',
      component: NgbdAddUpdatePlantRegister4Component,
      canActivate: [AuthGuard],
      data: {
        title: 'Create Register - พืชจากการฟื้นฟู',
        urls: [],
        breadcrumbs: [
          {link: '/admin/register', title: 'Admin Register'},
          {link: '/admin/register/passport/listing', title: 'Passport List'},
          {link: '', title: 'No Listing', checkId: true},
          {link: '', title: 'Plant List', checkId1: true},
          {link: '', title: 'Create Register - พืชจากการฟื้นฟู'}

        ]
      }
    }, {
      path: 'register-3/:volume/:id',
      component: NgbdAddUpdatePlantRegister3Component,
      canActivate: [AuthGuard],
      data: {
        title: 'Create deposit list -พืชฝากเก็บรักษา',
        urls: [],
        breadcrumbs: [
          {link: '/admin/register', title: 'Admin Register'},
          {link: '/admin/register/passport/listing', title: 'Passport List'},
          {link: '', title: 'No Listing', checkId: true},
          {link: '', title: 'Plant List', checkId1: true},
          {link: '', title: 'Create deposit list -พืชฝากเก็บรักษา'}

        ]
      }
    },
    {
      path: 'm4/listing',
      component: NgbdM4CompletedListComponent,
      canActivate: [AuthGuard],
      data: {
        title: 'M4 Completed Plant',
        urls: [],
        roles: [184],
        breadcrumbs: [
          {link: '/admin/register', title: 'Admin Register'},
          {link: '/admin/register/m4/listing', title: 'M4 Completed Plant'}
        ]
      }
    }
  ]
  }, {
    path: 'admin/manage-data',
    children: [{
      path: 'attribute/listing',
      component: NgbdAttributeSetListComponent,
      canActivate: [AuthGuard],
      data: {
        title: 'Attribute Set of Each Character',
        canActivate: [AuthGuard],
        urls: [],
        roles: [132, 133, 134],
        breadcrumbs: [
          {link: '/admin/manage-data', title: 'Admin Manage'},
          {link: '', title: 'Attribute Set of Each Character'},
        ]
      }
    }, {
      path: 'attribute/:id',
      component: NgbdAddUpdateAttributeSetComponent,
      canActivate: [AuthGuard],
      data: {
        title: 'Add or Update attribute Set',
        urls: [],
        breadcrumbs: [
          {link: '/admin/manage-data', title: 'Admin Manage'},
          {link: '/admin/manage-data/attribute/listing', title: 'Attribute Set of Each Character'},
          {link: '', title: 'Add or Update attribute Set'}
        ]
      }
    }, {
      path: 'attribute-list/listing',
      component: NgbdAttributeGroupSetListComponent,
      canActivate: [AuthGuard],
      data: {
        title: 'Attribute Set List',
        urls: [],
        roles: [136, 137, 138],
        breadcrumbs: [
          {link: '/admin/manage-data', title: 'Admin Manage'},
          {link: '/admin/manage-data/attribute-list/listing', title: 'Attribute Set List'},
        ]
      }
    }, {
      path: 'plant-doa/listing',
      component: NgbdAttributePlantDOAListComponent,
      canActivate: [AuthGuard],
      data: {
        title: 'Plant DOA Listing',
        urls: [],
        roles: [140, -1, 141],
        breadcrumbs: [
          {link: '/admin/manage-data', title: 'Admin Manage'},
          {link: '', title: 'Plant DOA Listing'},
        ]
      }
    }, {
      path: 'attribute-list/:id',
      component: NgbdAddUpdateAttributeSetListComponent,
      canActivate: [AuthGuard],
      data: {
        title: 'Create OR Update the Attribute Set of Each Character',
        urls: [],
        breadcrumbs: [
          {link: '/admin/manage-data', title: 'Admin Manage'},
          {link: '/admin/manage-data/attribute-list/listing', title: 'Attribute Set List'},
          {link: '', title: 'Create or Update'}
        ]
      }
    }, {
      path: 'plant-doa/:id',
      component: NgbdAddUpdatePlantDOAComponent,
      canActivate: [AuthGuard],
      data: {
        title: 'Manage - Data',
        urls: [],
        breadcrumbs: [
          {link: '/admin/manage-data', title: 'Admin Manage'},
          {link: '/admin/manage-data/plant-doa/listing', title: 'Plant DOA Listing'},
          {link: '', title: 'Create or Update'}
        ]
      }
    }, {
      path: 'export-doa',
      component: NgbdExportDoaComponent,
      canActivate: [AuthGuard],
      data: {
        title: 'Export Doa',
        canActivate: [AuthGuard],
        urls: [],
        roles: [183],
        breadcrumbs: [
          {link: '/admin/manage-data', title: 'Admin Manage'},
          {link: '', title: 'Export DOA'},
        ]
      }
    }]
  }, {
    path: 'admin/labs',
    children: [{
      path: 'plants-completed',
      component: NgbdPlantsCompletedComponent,
      canActivate: [AuthGuard],
      data: {
        title: 'Completed Plants',
        urls: [],
        roles: [83],
        breadcrumbs: [
          {link: '/admin/labs', title: 'Admin Labs'},
          {link: '/admin/labs/plants-completed', title: 'Completed Plants'}
        ]
      }
    }, {
      path: 'listing',
      component: NgbdListingLabComponent,
      canActivate: [AuthGuard],
      data: {
        title: 'Lab List',
        urls: [],
        roles: [85, 86, 87],
        breadcrumbs: [
          {link: '/admin/labs', title: 'Admin Labs'},
          {link: '/admin/labs/listing', title: 'Lab List'}
        ]
      }
    }, {
      path: 'alert-lab-listing',
      component: NgbdListingLabAlertComponent,
      canActivate: [AuthGuard],
      data: {
        title: 'Lab Alert Listing',
        urls: [],
        roles: [93, 94, 95]
      }
    }, {
      path: 'alert-list',	
      component: NgbdListingAlertComponent,	
      canActivate: [AuthGuard],	
      data: {	
        title: 'Lab Alert Listing',	
        urls: [],
        roles: [89, 90, 91],
        breadcrumbs: [
          {link: '/admin/labs', title: 'Admin Labs'},
          {link: '/admin/labs/alert-list', title: 'Alert List'}
        ]
      }	
    }, {
      path: 'pending-alert-plants/:labId',
      component: NgbdLabAlertListPendingComponent,
      canActivate: [AuthGuard],
      data: {
        title: 'Alert Lab Create',
        urls: []
      }
    }, {
      path: 'alert-plants/:labId',
      component: NgbdRegisterAlertFormListPendingCompleteComponent,
      canActivate: [AuthGuard],
      data: {
        title: 'Alert Plants Listing',
        urls: [],
        breadcrumbs: [
          {link: '/admin/labs', title: 'Admin Labs'},
          {link: '/admin/labs/alert-list', title: 'Alert List'},
          {link: '', title: 'ทดสอบจากการแจ้งเตือน 5-10 ปี'}
        ]
      }
    }, {
      path: 'alert-plants/details/:id',
      component: NgbdAddUpdateLabAlertLabComponent,
      canActivate: [AuthGuard],
      data: {
        title: 'ทดสอบจากการแจ้งเตือน 5-10 ปี  ',
        urls: [],
        breadcrumbs: [
          {link: '/admin/labs', title: 'Admin Labs'},
          {link: '/admin/labs/alert-list', title: 'Alert List'},
          {link: '', title: 'ทดสอบจากการแจ้งเตือน 5-10 ปี'}
        ]
      }
    }, {
      path: ':volume/:labId',
      component: NgbdCreateLabComponent,
      canActivate: [AuthGuard],
      data: {
        title: 'Lab Create',
        urls: []
      }
    }, {
      path: 'plants/:volume/:labId',
      component: NgbdPlantsLabComponent,
      canActivate: [AuthGuard],
      data: {
        title: 'Pending/Completed Lab Plant List',
        urls: [],
        breadcrumbs: [
          {link: '/admin/labs', title: 'Admin Labs'},
          {link: '/admin/labs/listing', title: 'Lab List'},
          {link: '', title: 'พืชใหม่ / ฝากเก็บ / จากระบบฟื้นฟู'}
        ]
      }
    }, {
      path: 'plants/:id/:volume/:labId',
      component: NgbdAddUpdatePlantLabComponent,
      canActivate: [AuthGuard],
      data: {
        title: 'พืชใหม่ / ฝากเก็บ / จากระบบฟื้นฟู',
        urls: [],
        breadcrumbs: [
          {link: '/admin/labs', title: 'Admin Labs'},
          {link: '/admin/labs/listing', title: 'Lab List'},
          {link: '', title: 'พืชใหม่ / ฝากเก็บ / จากระบบฟื้นฟู'}
        ]
      }
    }],

  }, {
    path: 'admin/warehouse',
    children: [{
      path: 'labs/listing',
      component: NgbdWHLabListingComponent,
      canActivate: [AuthGuard],
      data: {
        title: 'Seed stored in warehouse',
        urls: [],
        roles: [100, -1, 101],
        breadcrumbs: [
          {link: 'admin/warehouse', title: 'Warehouse'},
          {link: 'admin/warehouse/labs/listing', title: 'Seed stored in warehouse'},
        ]
      }
    }, 
    {
      path: 'labs/plant',
      component: NgbdWHLabPlantComponent,
      canActivate: [AuthGuard],
      data: {
        title: 'Warehouse Plants',
        urls: [],
        roles: [103],
        breadcrumbs: [
          {link: 'admin/warehouse', title: 'Warehouse'},
          {link: 'admin/warehouse/labs/plant', title: 'Warehouse Plants'},
        ]
        
      }
    },
    {
      path: 'labs/plantalert',
      component: NgbdPlantAlertComponent,
      canActivate: [AuthGuard],
      data: {
        title: 'Warehouse Plants alert',
        urls: [],
        roles: [103],
        breadcrumbs: [
          {link: 'admin/warehouse', title: 'Warehouse'},
          {link: '', title: 'Warehouse Plants alert'},
        ]
      }
    },
    {
      path: 'distribution/listing',
      component: NgbdWHDistributionStatusComponent,
      canActivate: [AuthGuard],
      data: {
        title: 'Set Plant for Service (Distribution)',
        urls: [],
        roles: [97, -1, 98],
        breadcrumbs: [
          {link: 'admin/warehouse', title: 'Warehouse'},
          {link: 'admin/warehouse/distribution/listing', title: 'Set Plant for Service (Distribution)'},
        ]
      }
    }, {
      path: 'labs/listing/:id',
      component: NgbdWHLabAlertListingComponent,
      canActivate: [AuthGuard],
      data: {
        title: 'Seed stored in warehouse',
        urls: [],
        roles: [105, -1, 106],
        breadcrumbs: [
          {link: 'admin/warehouse', title: 'Warehouse'},
          {link: 'admin/warehouse/labs/listing', title: 'Seed stored in warehouse'},
          {link: '', title: 'Add or Update'}
        ]
      }
    }, {
      path: 'alert-pending-complete/listing/:id',
      component: NgbdWHAlertPendingCompleteListingComponent,
      canActivate: [AuthGuard],
      data: {
        title: 'Seed stored in warehouse',
        urls: []
      }
    }, {
      path: 'pending-complete/listing/:id',
      component: NgbdWHPendingCompleteListingComponent,
      canActivate: [AuthGuard],
      data: {
        title: 'Seed stored in warehouse',
        urls: []
      }
    }, {
      path: 'form-new-plant/:id',
      component: NgbdAddUpdateNewPlantComponent,
      canActivate: [AuthGuard],
      data: {
        title: 'Form New Plant',
        urls: []
      }
    }, {
      path: 'stock/alert-listing',	
      component: NgbdWithWareHouseDistributionAlertListComponent,	
      canActivate: [AuthGuard],	
      data: {	
        title: 'WH Reuesr from Labs Alert list',	
        urls: [],
        roles: [114, -1, 115],
        breadcrumbs: [
          {link: '/admin/warehouse', title: 'Warehouse'},
          {link: '/admin/warehouse/stock/alert-listing', title: 'WH Reuesr from Labs Alert list'},
        ]
      }	
    },{
      path: 'stock/withdraw',
      component: NgbdWithWareHouseDrawListComponent,
      canActivate: [AuthGuard],
      data: {
        title: 'WH Request from Service (Withdraw) List',
        urls: [],
        roles: [108, -1, 109]
      }
    }, {
      path: 'stock/distribution',
      component: NgbdWithWareHouseDistributionListComponent,
      canActivate: [AuthGuard],
      data: {
        title: 'WH Request from Service (Withdraw) List',
        urls: [],
        roles: [111, -1, 112]
      }
    }, {
      path: 'stock/withdraw/:id',
      component: NgbdRegisterWithDrawListPlantsWarehouseComponent,
      canActivate: [AuthGuard],
      data: {
        title: 'WH Request from Service(Withdraw)',
        urls: []
      }
    }, {
      path: 'stock/distribution/:id',
      component: NgbdRegisterDistributionListPlantsWarehouseComponent,
      canActivate: [AuthGuard],
      data: {
        title: 'WH Request from Service(Distribution)',
        urls: []
      }
    }, {
      path: 'stock/alert-listing/:id',	
      component: NgbdRegisterAlertDistributionListPlantsWarehouseComponent,	
      canActivate: [AuthGuard],	
      data: {	
        title: 'WH Request from Labs (M8)',	
        urls: []	,
        breadcrumbs: [
          {link: '/admin/warehouse', title: 'Warehouse'},
          {link: '/admin/warehouse/stock/alert-listing', title: 'WH Reuesr from Labs Alert list'},
          {link: '', title: 'WH Request from Labs (M8)'}
        ]
      }	
    },  {	
      path: 'stock/recovery-listing',	
      component: NgbdWithWareHouseRecoveryAlertListComponent,	
      canActivate: [AuthGuard],	
      data: {	
        title: 'WH Request from Service (Recovery) Alert List',	
        urls: [],
        roles: [117, -1, 118],
        breadcrumbs: [
          {link: '/admin/warehouse', title: 'Warehouse'},
          {link: '/admin/warehouse/stock/recovery-listing', title: 'WH Request from Service (Recovery) Alert List'}
        ]
      }	
    }, {	
      path: 'stock/recovery-alert/:id',	
      component: NgbdRegisterAlertRecoveryListPlantsWarehouseComponent,	
      canActivate: [AuthGuard],	
      data: {	
        title: 'WH Request from Service(Recovery)',	
        urls: [],
        breadcrumbs: [
          {link: '/admin/warehouse', title: 'Warehouse'},
          {link: '/admin/warehouse/stock/recovery-listing', title: 'WH Request from Service (Recovery) Alert List'},
          {link: '', title: 'WH Request from Service(Recovery'}
        ]
      }	
    }, {
      path: 'form-keep/:id',
      component: NgbdAddUpdateFormKeepComponent,
      canActivate: [AuthGuard],
      data: {
        title: 'Form Keep',
        urls: []
      }
    }, {
      path: 'form-new-alert-plant/:id',
      component: NgbdAddUpdateNewPlantAlertComponent,
      canActivate: [AuthGuard],
      data: {
        title: 'Form Alert Plant',
        urls: []
      }
    }]
  }, {
    path: 'admin/recovery',
    children: [{
      path: 'alert-list',
      component: NgbdListingRecoveryAlertComponent,
      canActivate: [AuthGuard],
      data: {
        title: 'Alert List',
        urls: [],
        roles: [147],
        breadcrumbs: [
          {link: '/admin/recovery', title: 'Admin Recovery'},
          {link: '/admin/recovery/alert-list', title: 'Alert List'}
        ]
      }
    },{
      path: 'listing',
      component: NgbdRegisterRecoveryListComponent,
      canActivate: [AuthGuard],
      data: {
        title: 'Multiplication and Regeneration List',
        urls: [],
        roles: [143, 144, 145],
        breadcrumbs: [
          {link: '/admin/recovery', title: 'Admin Recovery'},
          {link: '/admin/recovery/listing', title: 'Multiplication and Regeneration List'}
        ]
      }
    }, {
      path: 'listing-pending/:id',
      component: NgbdRegisterRecoveryListPendingComponent,
      canActivate: [AuthGuard],
      data: {
        title: 'Multiplication and Regeneration List',
        urls: [],
        breadcrumbs: [
          {link: '/admin/recovery', title: 'Admin Recovery'},
          {link: '/admin/recovery/listing', title: 'Multiplication and Regeneration List'},
          {link: '', title: 'Add or Update'}
        ]
      }
    }, {
      path: 'listing-plants/:id',
      component: NgbdRegisterRecoveryListPendingCompleteComponent,
      canActivate: [AuthGuard],
      data: {
        title: 'Active/Completed Multiplication and Regeneration Plant',
        urls: [],
        breadcrumbs: [
          {link: '/admin/recovery', title: 'Admin Recovery'},
          {link: '/admin/recovery/listing', title: 'Multiplication and Regeneration List'},
          {link: '', title: 'Add or Update'}
        ]
      }
    }, {
      path: 'send-receive/:recoveryId/:id',
      component: NgbdSendReceiveComponent,
      canActivate: [AuthGuard],
      data: {
        title: 'Active/Complete Recovery Plant List',
        urls: []
      }
    }]
  }, {
    path: 'admin/withdraw',
    children: [{
      path: 'withdrawAlert',
      component: NgbdAlertListComponent,
      canActivate: [AuthGuard],
      data: {
        title: `Plant Alert`,
        urls: [],
        roles: [120],
        breadcrumbs: [
          {link: '/admin/withdraw', title: 'Admin Withdraw'},
          {link: '/admin/withdraw/withdrawAlert', title: 'Plant Alert'}
        ]
      }
    },{
      path: 'listing',
      component: NgbdWithDrawListComponent,
      canActivate: [AuthGuard],
      data: {
        title: `Withdraw - W No (ถอนเฉพาะเจ้าของเท่านั้น)`,
        urls: [],
        roles: [122, 123, 124],
        breadcrumbs: [
          {link: '/admin/withdraw', title: 'Admin Withdraw'},
          {link: '/admin/withdraw/listing', title: 'Withdraw - W No (ถอนเฉพาะเจ้าของเท่านั้น)'}
        ]
      }
    }, {
      path: 'pending/:id',
      component: NgbdRegisterWithDrawListPendingComponent,
      canActivate: [AuthGuard],
      data: {
        title: `Create With Draw`,
        urls: []
      }
    }, {
      path: 'listing-plants/:id',
      component: NgbdRegisterWithDrawListPlantsComponent,
      canActivate: [AuthGuard],
      data: {
        title: `With Draw Listing`,
        urls: []
      }
    }, {
      path: 'form-bank/:id',
      component: NgbdAddUpdateFormBankOfPlantComponent,
      canActivate: [AuthGuard],
      data: {
        title: `With Draw Info`,
        urls: []
      }
    }, {
      path: 'form-keep/:id',
      component: NgbdAddUpdateWithdrawFormKeepComponent,
      canActivate: [AuthGuard],
      data: {
        title: `With Draw Info`,
        urls: []
      }
    }]
  }, {
    path: 'admin/distribution',
    children: [
      {
        path: 'listing',
        component: NgbdWithDistributionListComponent,
        canActivate: [AuthGuard],
        data: {
          title: `Distribution - Di No. List`,
          urls: [],
          roles: [128, 129, 130],
          breadcrumbs: [
            {link: '/admin/distribution', title: 'Admin Distribution'},
            {link: '/admin/distribution/listing', title: 'Distribution - Di No. List'}
          ]
        }
      }, {
        path: 'pending/:id',
        component: NgbdRegisterDistributionListPendingComponent,
        canActivate: [AuthGuard],
        data: {
          title: `Create Distribution`,
          urls: []
        }
      }]},
      {
      path: 'admin/distribution',
      children: [{
        path: 'distributionAlert',
        component: NgbdAlertListDisComponent,
        canActivate: [AuthGuard],
        data: {
          title: `Plant Alert`,
          urls: [],
          roles: [126],
          breadcrumbs: [
            {link: '/admin/distribution', title: 'Admin Distribution'},
            {link: '/admin/distribution/distributionAlert', title: 'Plant Alert'}
          ]
        }
      }, {
      path: 'listing-plants/:id',
      component: NgbdRegisterDistributionPlantsComponent,
      canActivate: [AuthGuard],
      data: {
        title: `Distribution List`,
        urls: [],
        breadcrumbs: [
          {link: '/admin/distribution', title: 'Admin Distribution'},
          {link: '/admin/distribution/listing', title: 'Distribution - Di No. List'},
          {link: '', title: 'Distribution List'}
        ]
      }
    }, {
      path: 'form-bank/:id/:distId',
      component: NgbdAddUpdateDistributionFormBankOfPlantComponent,
      canActivate: [AuthGuard],
      data: {
        title: `Distribution Info`,
        urls: []
      }
    }]
  }, {
    path: 'admin/frontend',
    children: [{
      path: 'news/listing',
      component: NgbdListingEventNewsCategoryComponent,
      canActivate: [AuthGuard],
      data: {
        title: `รายการกิจกรรมและประชาสัมพันธ์ / event`,
        urls: [],
        roles: [149, 150, 151, 152],
        breadcrumbs: [
          {link: '/admin/frontend', title: 'Admin Frontend'},
          {link: '/admin/frontend/news/listing', title: 'รายการกิจกรรมและประชาสัมพันธ์ / event'}
        ]
      }
    }, {
      path: 'news/:id',
      component: NgbdAddUpdateEventNewsComponent,
      canActivate: [AuthGuard],
      data: {
        title: `รายการองค์ความรู้ / News > Create`,
        urls: [],
        breadcrumbs: [
          {link: '/admin/frontend', title: 'Admin Frontend'},
          {link: '/admin/frontend/news/listing', title: 'รายการกิจกรรมและประชาสัมพันธ์ / event'},
          {link: '', title: 'รายการองค์ความรู้ / News > Create'}
        ]
      }
    }, {
      path: 'knowledge/listing',
      component: NgbdListingKnowledgeNewsCategoryComponent,
      canActivate: [AuthGuard],
      data: {
        title: `รายการองค์ความรู้ / Knowledge`,
        urls: [],
        roles: [154, 155, 156, 157],
        breadcrumbs: [
          {link: '/admin/frontend', title: 'Admin Frontend'},
          {link: '/admin/frontend/knowledge/listing', title: 'รายการองค์ความรู้ / Knowledge'}
        ]
      }
    }, {
      path: 'knowledge/:id',
      component: NgbdAddUpdateKnowledgeComponent,
      canActivate: [AuthGuard],
      data: {
        title: `รายการองค์ความรู้ / Knowledge > Create`,
        urls: [],
        breadcrumbs: [
          {link: '/admin/frontend', title: 'Admin Frontend'},
          {link: '/admin/frontend/knowledge/listing', title: 'รายการองค์ความรู้ / Knowledge'},
          {link: '', title: 'รายการองค์ความรู้ / Knowledge > Create'}
        ]
      }
    }, {
      path: 'banner/listing',
      component: NgbdListingBannerCategoryComponent,
      canActivate: [AuthGuard],
      data: {
        title: `รายชื่อแบนเนอร์`,
        urls: [],
        roles: [159, 160, 161, 162],
        breadcrumbs: [
          {link: '/admin/frontend', title: 'Admin Frontend'},
          {link: '/admin/frontend/banner/listing', title: 'รายชื่อแบนเนอร์ / banner'}
        ]
      }
    }, {
      path: 'banner/:id',
      component: NgbdAddUpdateBannerComponent,
      canActivate: [AuthGuard],
      data: {
        title: `สร้างแบนเนอร์`,
        urls: [],
        breadcrumbs: [
          {link: '/admin/frontend', title: 'Admin Frontend'},
          {link: '/admin/frontend/banner/listing', title: 'รายชื่อแบนเนอร์ / banner'},
          {link: '', title: 'Add or Update'}
        ]
      }
    }, {
      path: 'intro/listing',
      component: NgbdListingIntroCategoryComponent,
      canActivate: [AuthGuard],
      data: {
        title: `รายการแนะนำ`,
        urls: [],
        roles: [169, 170, 171, 172],
        breadcrumbs: [
          {link: '/admin/frontend', title: 'Admin Frontend'},
          {link: '/admin/frontend/intro/listing', title: 'รายการแนะนำ / intro'}
        ]
      }
    }, {
      path: 'intro/:id',
      component: NgbdAddUpdateIntroComponent,
      canActivate: [AuthGuard],
      data: {
        title: `แนะนำสร้าง`,
        urls: [],
        breadcrumbs: [
          {link: '/admin/frontend', title: 'Admin Frontend'},
          {link: '/admin/frontend/intro/listing', title: 'รายการแนะนำ / intro'},
          {link: '', title: 'Add or Update'}
        ]
      }
    }, {
      path: 'assessment/listing',
      component: NgbdListingAccessmentCategoryComponent,
      canActivate: [AuthGuard],
      data: {
        title: 'Assessment',
        urls: [],
        roles: [174],
        breadcrumbs: [
          {link: '/admin/frontend', title: 'Admin Frontend'},
          {link: '/admin/frontend/assessment/listing', title: 'Assessment'}
        ]
      }
    }, {
      path: 'users/listing',
      component: NgbdListingFrontendUserComponent,
      canActivate: [AuthGuard],
      data: {
        title: 'Frontend Users',
        urls: [],
        roles: [176, 177, 178],
        breadcrumbs: [
          {link: '/admin/frontend', title: 'Admin Frontend'},
          {link: '/admin/frontend/users/listing', title: 'Assessment'}
        ]
      }
    }, {
      path: 'users/:id',
      component: NgbdAddUpdateUserFrontendComponent,
      canActivate: [AuthGuard],
      data: {
        title: 'Frontend Users',
        urls: [],
        breadcrumbs: [
          {link: '/admin/frontend', title: 'Admin Frontend'},
          {link: '/admin/frontend/users/listing', title: 'Assessment'},
          {link: '', title: 'Add or Update'}
        ]
      }
    }]
  }, 
  {
    path: 'admin/report',
    children: [{
      path: 'passport/year',
      component: NgbdPassportYearReportComponent,
      canActivate: [AuthGuard],
      data: {
        title: `M3 - Yearly Report M3 – Annual Report (รายงานลงทะเบียนประจำปี)`,
        urls: [],
        roles: [63],
        breadcrumbs: [
          {link: '/admin/report', title: 'Admin Report'},
          {link: '/admin/report/passport/year', title: 'M3 - Yearly Report M3 – Annual Report (รายงานลงทะเบียนประจำปี)'}
        ]
      }
    }, {
      path: 'plant/month',
      component: NgbdPlantMonthReportComponent,
      canActivate: [AuthGuard],
      data: {
        title: `M3 - Monthly Report (รายงานลงทะเบียนประจำเดือน)`,
        urls: [],
        roles: [65],
        breadcrumbs: [
          {link: '/admin/report', title: 'Admin Report'},
          {link: '/admin/report/plant/month', title: 'M3 - Monthly Report (รายงานลงทะเบียนประจำเดือน)'}
        ]
      }
    },
    {
      path: 'lab/status',
      component: NgbdLabStatusReportComponent,
      canActivate: [AuthGuard],
      data: {
        title: `M8 - Lab Status (รายงานสถานะของปฏิบัติการ)`,
        urls: [],
        roles: [67],
        breadcrumbs: [
          {link: '/admin/report', title: 'Admin Report'},
          {link: '/admin/report/lab/status', title: 'M8 - Lab Status (รายงานสถานะของปฏิบัติการ)'}
        ]
      }
    },
    {
      path: 'lab/plant',
      component: NgbdLabPlantReportComponent,
      canActivate: [AuthGuard],
      data: {
        title: `M8 - Lab Plant (รายงานผลปฏิบัติการรายพืช)`,
        urls: [],
        roles: [69],
        breadcrumbs: [
          {link: '/admin/report', title: 'Admin Report'},
          {link: '/admin/report/lab/plant', title: 'M8 - Lab Plant (รายงานผลปฏิบัติการรายพืช)'}
        ]
      }
    },
    {
      path: 'formtype/warehouse',
      component: NgbdFormTypeReportComponent,
      canActivate: [AuthGuard],
      data: {
        title: `M9 - Register Report (รายงานภาพรวมพืชจัดเก็บ) `,
        urls: [],
        roles: [71],
        breadcrumbs: [
          {link: '/admin/report', title: 'Admin Report'},
          {link: '/admin/report/formtype/warehouse', title: 'M9 - Register Report (รายงานภาพรวมพืชจัดเก็บ) '}
        ]
      }
    },
    {
      path: 'reporttype/warehouse',
      component: NgbdReportTypeReportComponent,
      canActivate: [AuthGuard],
      data: {
        title: `M9 - Plant Status (รายงานสรุปพืชจัดเก็บ)`,
        urls: [],
        roles: [73],
        breadcrumbs: [
          {link: '/admin/report', title: 'Admin Report'},
          {link: '/admin/report/reporttype/warehouse', title: 'M9 - Plant Status (รายงานสรุปพืชจัดเก็บ)'}
        ]
      }
    },
    {
      path: 'distribution',
      component: NgbdDistributionReportComponent,
      canActivate: [AuthGuard],
      data: {
        title: `M5 - Distribution Report (รายงานการให้บริการเมล็ดพันธุ์)`,
        urls: [],
        roles: [75],
        breadcrumbs: [
          {link: '/admin/report', title: 'Admin Report'},
          {link: '/admin/report/distribution', title: 'M5 - Distribution Report (รายงานการให้บริการเมล็ดพันธุ์)'}
        ]
      }
    },
    {
      path: 'withdraw',
      component: NgbdWithdrawReportComponent,
      canActivate: [AuthGuard],
      data: {
        title: `M5 - Withdraw Report (รายงานการฝาก/ถอนเมล็ดพันธุ์)`,
        urls: [],
        roles: [77],
        breadcrumbs: [
          {link: '/admin/report', title: 'Admin Report'},
          {link: '/admin/report/withdraw', title: 'M5 - Withdraw Report (รายงานการฝาก/ถอนเมล็ดพันธุ์)'}
        ]
      }
    },
    {
      path: 'recovery',
      component: NgbdRecoveryReportComponent,
      canActivate: [AuthGuard],
      data: {
        title: `M4 - Report (รายงานพื้นฟู)`,
        urls: [],
        roles: [79],
        breadcrumbs: [
          {link: '/admin/report', title: 'Admin Report'},
          {link: '/admin/report/recovery', title: 'M4 - Report (รายงานพื้นฟู)'}
        ]
      }
    },
    {
      path: 'allstatus',
      component: NgbdAllstatusReportComponent,
      canActivate: [AuthGuard],
      data: {
        title: `M9 - All Status (รายงานพืชจัดเก็บรายปี)`,
        urls: [],
        roles: [81],
        breadcrumbs: [
          {link: '/admin/report', title: 'Admin Report'},
          {link: '/admin/report/allstatus', title: 'M9 - All Status (รายงานพืชจัดเก็บรายปี)'}
        ]
      }
    }
  ]
  },
  {
    path: '**',
    redirectTo: '/admin/manage'
  },
];
