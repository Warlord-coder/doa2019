import {Component, OnInit} from '@angular/core';
import {ActivatedRoute, Router} from '@angular/router';
import {FormArray, FormBuilder, FormGroup, Validators} from '@angular/forms';
import {AlertService, PlantCategoryService, PlantCharacterService} from '../../../../services';
import {getRoleResources} from '../../../../utils';
import {TreeviewConfig, TreeviewItem} from 'ngx-treeview';
import {first} from 'rxjs/operators';

@Component({
  selector: 'app-add-update-plant-character-page',
  templateUrl: './add-update.component.html',
})
export class NgbdAddUpdatePlantCharacterComponent implements OnInit {
  plantCharacterForm: FormGroup;
  submitted = false;
  loading = false;
  apiError = false;
  categoryList = [];
  id = null;
  roleResources: TreeviewItem[];
  config = TreeviewConfig.create({
    hasAllCheckBox: true,
    hasFilter: true,
    hasCollapseExpand: true,
    decoupleChildFromParent: false,
    maxHeight: 800
  });


  constructor(private router: Router,
              private formBuilder: FormBuilder,
              private activeRoute: ActivatedRoute,
              private plantCharacterService: PlantCharacterService,
              private plantCategoryService: PlantCategoryService,
              private alertService: AlertService) {
    this.onGetPlantCategoryListing();
  }

  ngOnInit() {
    this.roleResources = getRoleResources();
    this.plantCharacterForm = this.formBuilder.group({
      status: ['', Validators.required],
      plant_category: ['', Validators.required],
      genus: [''],
      species: ['', Validators.required],
      reg_plant_common_name: ['', Validators.required],
      reg_plant_alter_name: this.formBuilder.array([])
    });
    this.activeRoute.params.subscribe(params => {
      const {id} = params;
      this.id = id;
      if (id && id !== 'create') {
        this.onGetPlantCharacterInformation(id);
      } else {
        this.addItem('');
      }
    });
  }

  addItem(data): void {
    (this.plantCharacterForm.get('reg_plant_alter_name') as FormArray).push(
      this.formBuilder.group({
        name: [data, Validators.required]
      })
    );
  }

  get f() {
    if (!this.apiError && this.submitted && !this.plantCharacterForm.invalid) {
      this.alertService.error(null);
    }

    return this.plantCharacterForm.controls;
  }

  onSubmit() {
    this.submitted = true;
    this.apiError = false;
    if (this.plantCharacterForm.invalid) {
      this.alertService.error('Some fields are required. Please fill those fields');
      return;
    }

    this.alertService.reset();
    this.loading = true;
    const data = this.plantCharacterForm.value;
    data.reg_plant_alter_name = data.reg_plant_alter_name.map(({name}) => name);

    this.plantCharacterService.createOrUpdate(data, this.id)
      .pipe(first())
      .subscribe(
        () => {
          this.loading = false;
          this.router.navigate(['/admin/master/character/listing']);
        },
        error => {
          this.loading = false;
          this.apiError = true;
          this.alertService.error(error);
        });
  }

  onGetPlantCharacterInformation(id: string) {
    this.loading = true;
    this.plantCharacterService.getSpecific(id)
      .pipe(first())
      .subscribe(
        ({data = {}}: any) => {
          const {reg_plant_alter_name = []} = data;
          Object.keys(data).forEach(key => {
            if (this.plantCharacterForm.get(key) && key !== 'reg_plant_alter_name') {
              this.plantCharacterForm.get(key).setValue(data[key]);
            }
          });

          reg_plant_alter_name.forEach(dataVal => this.addItem(dataVal));
          this.loading = false;
        },
        error => {
          this.loading = false;
          this.apiError = true;
          this.alertService.error(error);
        });
  }

  onGetPlantCategoryListing() {
    this.loading = true;
    this.plantCategoryService.getAll()
      .pipe(first())
      .subscribe(
        ({data = {}}: any) => {
          this.categoryList = data;
          this.loading = false;
        },
        error => {
          this.loading = false;
          this.apiError = true;
          this.alertService.error(error);
        });
  }

  onFilterChange($event) {
    console.log('Filter Change', $event);
  }

  onCancel() {
    this.router.navigate(['/admin/master/character/listing']);
  }

  deleteIndex(index) {
    (this.plantCharacterForm.get('reg_plant_alter_name') as FormArray).removeAt(index);
  }
}
