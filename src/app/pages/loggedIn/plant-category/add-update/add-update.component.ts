import {Component, OnInit} from '@angular/core';
import {ActivatedRoute, Router} from '@angular/router';
import {FormBuilder, FormGroup, Validators} from '@angular/forms';
import {AlertService, PlantCategoryService, UserService} from '../../../../services';
import {getRoleResources} from '../../../../utils';
import {TreeviewConfig, TreeviewItem} from 'ngx-treeview';
import {first} from 'rxjs/operators';

@Component({
  selector: 'app-add-update-plant-category-page',
  templateUrl: './add-update.component.html',
})
export class NgbdAddUpdatePlantCategoryComponent implements OnInit {
  plantCategoryForm: FormGroup;
  submitted = false;
  roleResources: TreeviewItem[];
  apiError = false;
  id = null;
  loading = false;
  config = TreeviewConfig.create({
    hasAllCheckBox: true,
    hasFilter: true,
    hasCollapseExpand: true,
    decoupleChildFromParent: false,
    maxHeight: 800
  });


  constructor(private router: Router,
              private formBuilder: FormBuilder,
              private userService: UserService,
              private activeRoute: ActivatedRoute,
              private plantCategoryService: PlantCategoryService,
              private alertService: AlertService) {
  }

  ngOnInit() {
    this.roleResources = getRoleResources();
    this.plantCategoryForm = this.formBuilder.group({
      status: ['', Validators.required],
      symbol: ['', Validators.required],
      categoryName_en: ['', Validators.required],
      categoryName_th: ['', Validators.required]
    });
    this.activeRoute.params.subscribe(params => {
      const {id} = params;
      this.id = id;
      if (id && id !== 'create') {
        this.onGetPlantCategoryInformation(id);
      }
    });
  }

  get f() {
    if (!this.apiError && this.submitted && !this.plantCategoryForm.invalid) {
      this.alertService.error(null);
    }

    return this.plantCategoryForm.controls;
  }

  onSubmit() {
    this.submitted = true;
    this.apiError = false;
    if (this.plantCategoryForm.invalid) {
      this.alertService.error('Some fields are required. Please fill those fields');
      return;
    }


    this.alertService.reset();
    this.loading = true;

    this.plantCategoryService.createOrUpdate({...this.plantCategoryForm.value}, this.id)
      .pipe(first())
      .subscribe(
        () => {
          this.loading = false;
          this.router.navigate(['/admin/master/category/listing']);
        },
        error => {
          this.loading = false;
          this.apiError = true;
          this.alertService.error(error);
        });
  }

  onFilterChange($event) {
    console.log('Filter Change', $event);
  }

  onGetPlantCategoryInformation(id: string) {
    this.loading = true;
    this.plantCategoryService.getSpecific(id)
      .pipe(first())
      .subscribe(
        ({data = {}}: any) => {
          const {status, categoryName_en, categoryName_th, symbol} = data;
          this.plantCategoryForm.get('status').setValue(status);
          this.plantCategoryForm.get('categoryName_en').setValue(categoryName_en);
          this.plantCategoryForm.get('categoryName_th').setValue(categoryName_th);
          this.plantCategoryForm.get('symbol').setValue(symbol);
          this.loading = false;
        },
        error => {
          this.loading = false;
          this.apiError = true;
          this.alertService.error(error);
        });
  }

  onCancel() {
    this.router.navigate(['/admin/master/category/listing']);
  }
}
