import { Component, OnInit } from '@angular/core';
import { DataTableParams, DataTableResource } from 'ngx-datatable-bootstrap4';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { Router, ActivatedRoute } from '@angular/router';
import { toLower } from 'lodash';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { AlertService, PlantRegisterService, WithdrawService } from '../../../../services';
import { first } from 'rxjs/operators';
import * as moment from 'moment';

@Component({
  selector: 'app-alertList-common-country-page',
  templateUrl: './alertList.component.html',
})
export class NgbdAlertListComponent implements OnInit {
  resources = new DataTableResource<any>([]);
  items: any[] = [];
  count = 0;
  resources1 = new DataTableResource<any>([]);
  items1: any[] = [];
  count1 = 0;
  resources2 = new DataTableResource<any>([]);
  items2: any[] = [];
  count2 = 0;
  registerPassportForm: FormGroup;
  tempItems: any[] = [];
  tempItems1: any[] = [];
  tempItems2: any[] = [];
  seedSenderList = [];
  submitted = false;
  momentObj: any;
  file = null;
  loading = false;
  apiError = false;
  roles = [];

  constructor(private modalService: NgbModal,
    private formBuilder: FormBuilder,
    private alertService: AlertService,
    private plantRegisterService: PlantRegisterService,
    private withDrawService: WithdrawService,
    private router: Router,
    private route: ActivatedRoute) {
    this.momentObj = moment;
  }

  ngOnInit() {
    this.route
      .data
      .subscribe(data => {
        this.roles = data['roles'];
      });
    this.registerPassportForm = this.formBuilder.group({
      dateOfReceiving: [''],
      serviceRequestLetter: [''],
      agenciesRequestSeedGermination: [''],
      agenciesSubRequestSeedGermination: [''],
      agenciesSubRequestSeedGerminationInput: [''],
      numberOfRequestor: [''],
      requestorAddress: [''],
      telephoneNumber: [''],
      email: [''],
      objective: [''],
      owner: ['']
    });
    this.withDrawService.getAllPlantsByStatus('pending')
      .pipe(first())
      .subscribe(
        ({ data = [] }: any) => {
          this.loading = false;
          let pe = [];
          let ap = [];
          for (let index = 0; index < data.length; index++) {
            if(data[index].obj.withDrawStatus === 'pending' && data[index].obj.admin_amount){
              ap.push(data[index]);
            }else{
              pe.push(data[index]);
            }
          }
          this.resources1 = new DataTableResource<any>(pe);
          this.resources1.count().then(count => this.count1 = count);
          this.reloadData1({});
          this.resources2 = new DataTableResource<any>(ap);
          this.resources2.count().then(count => this.count2 = count);
          this.reloadData2({});
        },
        error => {
          this.loading = false;
          this.alertService.error(error);
        });
  }

  get f() {
    if (!this.apiError && this.submitted && !this.registerPassportForm.invalid) {
      this.alertService.error(null);
    }

    return this.registerPassportForm.controls;
  }
  reloadData(params: DataTableParams) {
    this.resources.query(params).then(vals => {
      this.items = vals;
      this.tempItems = this.items1;
    });
  }
  reloadData1(params: DataTableParams) {
    this.resources1.query(params).then(vals => {
      this.items1 = vals;
      this.tempItems1 = this.items1;
    });
  }
  reloadData2(params: DataTableParams) {
    this.resources2.query(params).then(vals => {
      this.items2 = vals;
      this.tempItems2 = this.items2;
    });
  }

  onSearchType({target: {value: searchRole = ''} = {}}) {
    this.items = this.tempItems.filter(({plant_gene_name}) => {
      try {
        return toLower(plant_gene_name.plantCategory.categoryName_th).includes(toLower(searchRole))
      } catch (error) {
        return false;
      }
    });
    this.items1 = this.tempItems1.filter(({wh}) => {
      try {
        return toLower(wh.plant_gene_name.plantCategory.categoryName_th).includes(toLower(searchRole))
      } catch (error) {
        return false;
      }
      
    });
    this.items2 = this.tempItems2.filter(({wh}) => {
      try {
        return toLower(wh.plant_gene_name.plantCategory.categoryName_th).includes(toLower(searchRole))
      } catch (error) {
        return false;
      }
    });
    
  }
  onSearchName({target: {value: searchRole = ''} = {}}) {
    this.items = this.tempItems.filter(({plant_gene_name}) => {
      try {
        return toLower(plant_gene_name.plantTypeName_th).includes(toLower(searchRole))
      } catch (error) {
        return false;
      }
    });
    this.items1 = this.tempItems1.filter(({wh}) => {
      return toLower(wh.plant_gene_name.plantTypeName_th).includes(toLower(searchRole))
    });
    this.items2 = this.tempItems2.filter(({wh}) => {
      return toLower(wh.plant_gene_name.plantTypeName_th).includes(toLower(searchRole))
    });
  }
  onSearcGane({target: {value: searchRole = ''} = {}}) {
    this.items = this.tempItems.filter(({reg_gene_id}) => {
      try {
        return toLower(reg_gene_id).includes(toLower(searchRole))
      } catch (error) {
        return false;
      }
    });
    this.items1 = this.tempItems1.filter(({wh}) => {
      try {
        return toLower(wh.reg_gene_id).includes(toLower(searchRole))
      } catch (error) {
        return false;
      }
      
    });
    this.items2 = this.tempItems2.filter(({wh}) => {
      try {
        return toLower(wh.reg_gene_id).includes(toLower(searchRole))
      } catch (error) {
        return false;
      }
    });
  }
  onSearchSci({target: {value: searchRole = ''} = {}}) {
    this.items = this.tempItems.filter(({plant_gene_name}) => {
      try {
        return toLower(plant_gene_name.plantSpecie.genus).includes(toLower(searchRole))
      } catch (error) {
        return false;
      }
    });
    this.items1 = this.tempItems1.filter(({wh}) => {
      try {
        return toLower(wh.plant_gene_name.plantSpecie.genus).includes(toLower(searchRole))
      } catch (error) {
        return false;
      }
      
    });
    this.items2 = this.tempItems2.filter(({wh}) => {
      try {
        return toLower(wh.plant_gene_name.plantSpecie.genus).includes(toLower(searchRole))
      } catch (error) {
        return false;
      }
    });
  }

  onDetails({ obj }) {
    this.router.navigate([`/admin/withdraw/listing-plants/${obj.m5_assigned._id}`]);
  }

  checkRole(index){
    if(localStorage.getItem('superAdmin') === "true") return true;
    if(localStorage.getItem('rolesId').indexOf(this.roles[index]) > -1) return true;
    return false;
  }
}
