import {Component, OnInit} from '@angular/core';
import {ActivatedRoute, Router} from '@angular/router';
import {FormArray, FormBuilder, FormGroup, Validators} from '@angular/forms';
import {
  AlertService,
  CommonProvinceService,
  PassportService,
  PlantCategoryService, PlantCharacterService,
  PlantGroupsService,
  PlantGroupTypesService, PlantTypeService,
  VolumeService, WarehouseService , WithdrawService
} from '../../../../../services';
import {identity, pickBy} from 'lodash';
import {getRoleResources} from '../../../../../utils';
import {TreeviewItem} from 'ngx-treeview';
import {first} from 'rxjs/operators';
import {PlantService} from '../../../../../services/plant.service';
import {CommonCountryService} from '../../../../../services/common-country.service';
import {CommonAumperService} from '../../../../../services/common-aumper.service';
import {environment} from '../../../../../../environments/environment';

@Component({
  selector: 'app-add-update-plant-new-page',
  templateUrl: './add-update.component.html',
})
export class NgbdAddUpdateWithdrawFormKeepComponent implements OnInit {
  plantGroupTypeForm: FormGroup;
  submitted = false;
  loading = false;
  file = null;
  id = null;
  _id = null;
  categoryList = [];
  groupTypeList = [];
  groupList = [];
  provinceList = [];
  aumperList = [];
  countryList = [];
  volumeInfo = null;
  passportInfo = null;
  gs_no = null;
  apiError = false;
  plantsListing = [];
  reg_character_attached_file = null;
  formData: any = {};
  regCharacterAttachedFile = null;
  wh_sample_ref_molecular_file = null;
  wh_sample_ref_seed_file = null;
  seedAttachedFile = null;
  molecularAttachedFile = null;
  reg_seed_no = null;
  reg_gene_id = null;
  submitTo = null;
  disabled = true;
  labInfo: any = {};
  plantTypes = [];
  selectedPlant = null;
  selectedCheckbox = {
    room5: [],
    room10: []
  };
  volumeId = null;
  withDrawInfo = []
  roleResources: TreeviewItem[];
  config = {
    class: 'autocomplete', max: 5, placeholder: 'Type to search',
    sourceField: ['plantTypeName_sci_en']
  };

  constructor(private router: Router,
              private formBuilder: FormBuilder,
              private activeRoute: ActivatedRoute,
              private plantCategoryService: PlantCategoryService,
              private plantGroupTypesService: PlantGroupTypesService,
              private plantGroupsService: PlantGroupsService,
              private plantCharacterService: PlantCharacterService,
              private volumeService: VolumeService,
              private passportService: PassportService,
              private wareHouseService: WarehouseService,
              private withDrawService: WithdrawService,
              private plantTypeService: PlantTypeService,
              private countryService: CommonCountryService,
              private provinceService: CommonProvinceService,
              private aumperService: CommonAumperService,
              private alertService: AlertService) {
    this.onGetListing('plantCharacterService', 'plantsListing');
    this.onGetListing('plantCategoryService', 'categoryList');
    this.onGetListing('plantGroupTypesService', 'groupTypeList');
    this.onGetListing('plantGroupsService', 'groupList');
    this.onGetListing('countryService', 'countryList');
    this.onGetListing('aumperService', 'aumperList');
    this.onGetListing('provinceService', 'provinceList');
    this.onGetListing('plantTypeService', 'plantTypes');
  }

  ngOnInit() {
    this.roleResources = getRoleResources();
    this.plantGroupTypeForm = this.formBuilder.group({
      plant_gene_name: ['', Validators.required],
      reg_gene_type: [''],
      reg_plant_alter_name: [[]],
      reg_plant_sci_name: [''],
      reg_gene_category: ['', Validators.required],
      reg_seed_collect_date: [''],
      selfing_input: [''],
      selfing_dropdown: [''],
      oop_input: [''],
      oop_dropdown: [''],
      other_input: [''],
      other_dropdown: [''],
      department_type: [''],
      department_sub_type: [''],
      department_name: [''],
      source_country: [''],
      source_country_1: ['', Validators.required],
      source_province: ['', Validators.required],
      source_district: ['', Validators.required],
      source_address: ['', Validators.required],
      lat: [''],
      long: [''],
      source_by_department: [''],
      source_sea_level: ['', Validators.required],
      reg_seed_collect_person: ['', Validators.required],
      reg_seed_owner: ['', Validators.required],
      reg_seed_sender: ['', Validators.required],
      reg_character_brief: ['', Validators.required],
      reg_character_for_record: [''],
      reg_sent_lab_date: ['', Validators.required],
      reg_remark: ['', Validators.required],
      breed_owner: ['แจ้งกลับแล้ว'],
      room5: this.formBuilder.array([]),
      room10: this.formBuilder.array([]),
      anteArray: this.formBuilder.array([]),
      room130: this.formBuilder.array([]),
      wh_sample_ref_seed_check_box: [false],
      wh_sample_ref_molecular_checkbox: [false],
      wh_create_info_checkbox: [false],
      roomRadio: ['room5'],
    });
    this.activeRoute.params.subscribe(params => {
      const {id} = params;
      this.id = id.split("#")[0];
      this._id = id.split("#")[1];
      this.onGetWarehousePlantInfo(this.id);
      this.onGetSpecificRecovery()
    });
  }

  get f() {
    if (!this.apiError && this.submitted && !this.plantGroupTypeForm.invalid) {
      this.alertService.error(null);
    }

    return this.plantGroupTypeForm.controls;
  }


  onGetSpecificRecovery() {
    this.loading = true;
    this.withDrawService.getSpecific(this._id)
      .pipe(first())
      .subscribe(
        ({data = []}: any) => {
          this.withDrawInfo = data;  
          this.loading = false;
        },
        error => {
          this.loading = false;
          this.alertService.error(error);
        });
  }

  onGetListing(serviceName, key) {
    // this.loading = true;
    this[serviceName].getAll()
      .pipe(first())
      .subscribe(
        ({data = []}: any) => {
          // this.loading = false;
          this[key] = data;
        },
        error => {
          this.loading = false;
          this.alertService.error(error);
        });
  }

  onGetWarehousePlantInfo(id, reset = true) {
    this.loading = true;

    this.wareHouseService.getSpecific(id)
      .pipe(first())
      .subscribe(
        ({data = []}: any) => {
          const {
            reg_gene_id, reg_seed_no, gs_no,
            reg_plant_sci_name, reg_character_attached_file,
            wh_sample_ref_molecular_file, wh_sample_ref_seed_file,
            volume, m8_lab_id, room5 = [], room10 = [],
            anteArray = [], room130 = [],
            wh_status, submitTo
          } = data;      
          this.disabled = wh_status === 'complete';
          this.labInfo = m8_lab_id;
          this.submitTo = submitTo;
          this.onGetVolumeInfo(volume);
          Object.keys(data).forEach(key => {
            if (this.plantGroupTypeForm.get(key) && !['room5', 'room10', 'anteArray', 'room130'].includes(key)) {
              if (this.disabled) {
                this.plantGroupTypeForm.get(key).disable({onlySelf: true});
                this.plantGroupTypeForm.get(key).setValue(data[key]);
              }
              this.plantGroupTypeForm.get(key).setValue(data[key]);
            }
          });
          this.reg_gene_id = reg_gene_id;
          this.gs_no = gs_no;
          this.formData = data;
          this.reg_seed_no = reg_seed_no;
          this.reg_character_attached_file = reg_character_attached_file;
          this.wh_sample_ref_molecular_file = wh_sample_ref_molecular_file;
          this.wh_sample_ref_seed_file = wh_sample_ref_seed_file;
          this.molecularAttachedFile = this.seedAttachedFile = null;
          this.findPlantSciName(data);
          if (reset) {
            this.selectedCheckbox.room5 = [
              room5[0] ? 0 : false,
              room5[1] ? 1 : false,
              room5[2] ? 2 : false,
              room5[3] ? 3 : false,
              room5[4] ? 4 : false];
            this.addItem(room5[0] || data, 'room5', 1, !room5[0]);
            this.addItem(room5[1] || {}, 'room5', 2, !room5[1]);
            this.addItem(room5[2] || {}, 'room5', 3, !room5[2]);
            this.addItem(room5[3] || {}, 'room5', 4, !room5[3]);
            this.addItem(room5[4] || {}, 'room5', 5, !room5[4]);


            this.selectedCheckbox.room10 = [
              room10[0] ? 0 : false,
              room10[1] ? 1 : false,
              room10[2] ? 2 : false,
              room10[3] ? 3 : false,
              room10[4] ? 4 : false];

            this.addItem(room10[0] || data, 'room10', 1, !room10[0]);
            this.addItem(room10[1] || {}, 'room10', 2, !room10[1]);
            this.addItem(room10[2] || {}, 'room10', 3, !room10[2]);
            this.addItem(room10[3] || {}, 'room10', 4, !room10[3]);
            this.addItem(room10[4] || {}, 'room10', 5, !room10[4]);

            anteArray && anteArray.length && this.createOptionAnteValue('anteArray', anteArray[0]);
            room130 && room130.length && this.createOptionAnteValue('room130', room130[0]);
          }
          this.loading = false;
        },
        error => {
          this.loading = false;
          this.alertService.error(error);
        });
  }

  findPlantSciName({reg_plant_sci_name}) {
    if (!reg_plant_sci_name) {
      return;
    }
    for (const plant of  this.plantsListing) {
      if (reg_plant_sci_name === plant._id) {
        this.selectedPlant = plant;
        this.plantGroupTypeForm.get('reg_plant_sci_name').setValue(plant.genus + ' ' + plant.species);
      }
    }
  }

  onClear() {
    this.selectedPlant = null;
  }

  onGetVolumeInfo(id) {
    this.loading = true;
    this.volumeService.getSpecific(id)
      .pipe(first())
      .subscribe(
        ({data = []}: any) => {
          this.loading = false;
          const {passport_no} = data;
          this.volumeInfo = data;
          this.onGetPassportInfo(passport_no);
        },
        error => {
          this.loading = false;
          this.alertService.error(error);
        });
  }

  onGetPassportInfo(id) {
    this.loading = true;
    this.passportService.getSpecific(id)
      .pipe(first())
      .subscribe(
        ({data = []}: any) => {
          this.loading = false;
          const {passport_no} = data;
          this.passportInfo = data;
        },
        error => {
          this.loading = false;
          this.alertService.error(error);
        });
  }

  getAttachment(basePath, name) {
    if (name) {
      return `${environment.endpoint}/${basePath}/${name}`;
    }

    return null;
  }

  findItemPlantListing = (dbVal) => {
    for (const item of this.plantsListing) {
      if (item._id === dbVal) {
        this.selectedPlant = item;
        $('#plantSciName').val(item.plantTypeName_sci_en);
        return;
      }
    }
  };

  createOptionValue(data: any, index = 1, disabled = true): FormGroup {
    const {
      lab_10seed_weight = '', lab_test_growth_percent = '',
      lab_test_growth_date = '', lab_test_moisture_person = '',
      lab_test_moisture_date = '', wh_batch_store_date = '',
      notification_period = '', wh_batch_weight = '',
      wh_batch_store_location = '', wh_batch_remark = '', batch_id =''
    } = data || {};

    return this.formBuilder.group({
      batch_no: [index],
      // batch_id: [index],
      batch_id: [batch_id],
      lab_10seed_weight: [{value: lab_10seed_weight, disabled}, Validators.required],
      lab_test_growth_percent: [{value: lab_test_growth_percent, disabled}, Validators.required],
      lab_test_growth_date: [{value: lab_test_growth_date, disabled}, Validators.required],
      lab_test_moisture_person: [{value: lab_test_moisture_person, disabled}, Validators.required],
      lab_test_moisture_date: [{value: lab_test_moisture_date, disabled}, Validators.required],
      wh_batch_store_date: [{value: wh_batch_store_date, disabled}, Validators.required],
      notification_period: [{value: notification_period, disabled}, Validators.required],
      wh_batch_weight: [{value: wh_batch_weight, disabled}, Validators.required],
      wh_batch_store_location: [{value: wh_batch_store_location, disabled}, Validators.required],
      wh_batch_remark: [{value: wh_batch_remark, disabled}, Validators.required]
    });
  }

  addItem(data = {}, key, index, disabled = true): void {
    (this.plantGroupTypeForm.get(key) as FormArray).push(this.createOptionValue(data || {}, index, disabled));
  }

  createOptionAnteValue(key, data: any, disabled = false): FormGroup {
    const {
      reg_seed_sender = '', lab_test_growth_percent = '', lab_10seed_weight = '',
      lab_test_growth_date = '', lab_test_moisture_person = '',
      lab_test_moisture_date = '', wh_batch_store_date = '',
      storage = '', storage_weight = '',
      number_of_item_stored = '', wh_batch_remark = ''
    } = data || {};

    // @ts-ignore
    return (this.plantGroupTypeForm.get(key) as FormArray).push(this.formBuilder.group({
      reg_seed_sender: [{value: reg_seed_sender, disabled}, Validators.required],
      lab_10seed_weight: [{value: lab_10seed_weight, disabled}, Validators.required],
      lab_test_growth_percent: [{value: lab_test_growth_percent, disabled}, Validators.required],
      lab_test_growth_date: [{value: lab_test_growth_date, disabled}, Validators.required],
      lab_test_moisture_person: [{value: lab_test_moisture_person, disabled}, Validators.required],
      lab_test_moisture_date: [{value: lab_test_moisture_date, disabled}, Validators.required],
      wh_batch_store_date: [{value: wh_batch_store_date, disabled}, Validators.required],
      storage: [{value: storage, disabled}, Validators.required],
      number_of_item_stored: [{value: number_of_item_stored, disabled}, Validators.required],
      storage_weight: [{value: storage_weight, disabled}, Validators.required],
      wh_batch_remark: [{value: wh_batch_remark, disabled}, Validators.required]
    }));
  }

  onCancel() {
    this.router.navigate([`admin/withdraw/pending/${this._id}`]);
  }
}
