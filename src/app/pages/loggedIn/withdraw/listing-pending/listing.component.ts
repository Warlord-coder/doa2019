import {Component, OnInit} from '@angular/core';
import {DataTableParams, DataTableResource} from 'ngx-datatable-bootstrap4';
import {NgbModal} from '@ng-bootstrap/ng-bootstrap';
import {ActivatedRoute, Router} from '@angular/router';
import {toLower} from 'lodash';
import {FormBuilder, FormGroup, Validators} from '@angular/forms';
// @ts-ignore
import {AlertService, WithdrawService} from '../../../../services';
import {first} from 'rxjs/operators';
import * as moment from 'moment';

@Component({
  selector: 'app-listing-common-country-page',
  templateUrl: './listing.component.html',
})
export class NgbdRegisterWithDrawListPendingComponent implements OnInit {
  resources = new DataTableResource<any>([]);
  items: any[] = [];
  count = 0;
  registerPassportForm: FormGroup;
  tempItems: any[] = [];
  submitted = false;
  id = null;
  momentObj: any;
  file = null;
  withDrawInfo = null;
  loading = false;
  selectedPlantsForSubmit: any = [];
  apiError = false;

  constructor(private modalService: NgbModal,
              private formBuilder: FormBuilder,
              private activeRoute: ActivatedRoute,
              private alertService: AlertService,
              private withDrawService: WithdrawService,
              private router: Router) {
    this.momentObj = moment;
  }

  ngOnInit() {
    this.registerPassportForm = this.formBuilder.group({
      revival_plant: ['', Validators.required],
      date_of_submission: ['', Validators.required],
      place: ['', [Validators.required]]
    });
    this.activeRoute.params.subscribe(params => {
      const {id} = params;
      this.id = id;
      this.onGetSpecificRecovery();
    });
    this.onGetRecoveryListing();
  }

  get f() {
    if (!this.apiError && this.submitted && !this.registerPassportForm.invalid) {
      this.alertService.error(null);
    }

    return this.registerPassportForm.controls;
  }

  reloadData(params: DataTableParams) {
    this.resources.query(params).then(vals => {
      this.items = vals;
      this.tempItems = this.items;
    });
  }

  onSearch({ target: { value: searchRole = '' } = {} }) {
    this.items = this.tempItems.filter(({ plant_gene_name}) => plant_gene_name && toLower(plant_gene_name.plantCategory.categoryName_th).includes(toLower(searchRole)));
  }
  onSearch1({ target: { value: searchRole = '' } = {} }) {
    this.items = this.tempItems.filter(({ plant_gene_name: { plantTypeName_en = '' } = {} }) => plantTypeName_en && toLower(plantTypeName_en).includes(toLower(searchRole)));
  }
  onSearch2({ target: { value: searchRole = '' } = {} }) {
    this.items = this.tempItems.filter(({ reg_plant_sci_name: { genus = '' } = {} }) => genus && toLower(genus).includes(toLower(searchRole)));
  }
  onSearch3({ target: { value: searchRole = '' } = {} }) {
    this.items = this.tempItems.filter(({ reg_gene_id  }) => reg_gene_id && toLower(reg_gene_id).includes(toLower(searchRole)));
  }
  onSearch4({ target: { value: searchRole = '' } = {} }) {
    this.items = this.tempItems.filter(({ source_province: { provinceName_en = '' } = {} }) => provinceName_en && toLower(provinceName_en).includes(toLower(searchRole)));
  }

  onGetRecoveryListing() {
    this.loading = true;
    this.withDrawService.getSpecific(this.id)
      .pipe(first())
      .subscribe(
        ({data = []}: any) => {
          this.withDrawInfo = data;   
          this.withDrawService.getAllPending()
            .pipe(first())
            .subscribe(
              ({data = []}: any) => {
                let temp = []
                data.forEach(element => {
                  const key = element.reg_seed_owner + " " + element.reg_gene_id;
                  if(key == this.withDrawInfo.owner) 
                    temp.push(element)
                });
                this.resources = new DataTableResource<any>(temp);
                this.resources.count().then(count => this.count = count);
                this.reloadData({});
                this.loading = false;
              },
              error => {
                this.loading = false;
                this.alertService.error(error);
              });   
          this.loading = false;
        },
        error => {
          this.loading = false;
          this.alertService.error(error);
        });
  }

  onGetSpecificRecovery() {
    this.loading = true;
    this.withDrawService.getSpecific(this.id)
      .pipe(first())
      .subscribe(
        ({data = []}: any) => {
          this.withDrawInfo = data;      
          this.loading = false;
        },
        error => {
          this.loading = false;
          this.alertService.error(error);
        });
  }

  onChangeItemSelect({_id}) {
    if (this.selectedPlantsForSubmit.includes(_id)) {
      this.selectedPlantsForSubmit = this.selectedPlantsForSubmit.filter(id => id !== _id);
      return;
    }

    this.selectedPlantsForSubmit = [...this.selectedPlantsForSubmit, _id];
  }

  onSubmitPlants() {
    if (!this.selectedPlantsForSubmit || !this.selectedPlantsForSubmit.length) {
      return;
    }
    this.loading = true;
    this.withDrawService.submitToWithDraw(this.id, this.selectedPlantsForSubmit)
      .pipe(first())
      .subscribe(
        ({data = {}}: any) => {
          this.selectedPlantsForSubmit = [];
          this.router.navigate([`/admin/withdraw/listing`]);
        },
        error => {
          this.loading = false;
          this.alertService.error(error);
        });
  }

  getStockInfo(item) {
    const {
      selfing_input, selfing_dropdown,
      oop_input, oop_dropdown,
      other_input, other_dropdown
    } = item;
    const stock = {
      isSelfing: selfing_input && selfing_dropdown,
      isOOP: oop_input && oop_dropdown,
      isOther: other_input && other_dropdown,
    };
    if (stock.isSelfing) {
      return `Self ${selfing_input} ${selfing_dropdown}`;
    }
    if (stock.isOOP) {
      return `OP ${oop_input} ${oop_dropdown}`;
    }
    if (stock.isOther) {
      return `Other ${other_input} ${other_dropdown}`;

    }
  }
  onEdit({_id, volume , id = this.id}: any) {
    const {form_type = ''} = volume || {}
    if (volume && form_type === 'Register 3') {
      this.router.navigate([`/admin/withdraw/form-keep/${_id}#${id}`]);
      return;
    }
    this.router.navigate([`/admin/withdraw/listing`]);
  }

}
