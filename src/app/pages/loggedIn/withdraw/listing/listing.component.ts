import {Component, OnInit} from '@angular/core';
import {DataTableParams, DataTableResource} from 'ngx-datatable-bootstrap4';
import {NgbModal} from '@ng-bootstrap/ng-bootstrap';
import {Router, ActivatedRoute} from '@angular/router';
import {toLower, max} from 'lodash';
import {FormBuilder, FormGroup, Validators} from '@angular/forms';
import {AlertService, PlantRegisterService, WithdrawService, WarehouseService} from '../../../../services';
import {first, elementAt} from 'rxjs/operators';
import * as moment from 'moment';

@Component({
  selector: 'app-listing-common-country-page',
  templateUrl: './listing.component.html',
})
export class NgbdWithDrawListComponent implements OnInit {
  resources = new DataTableResource<any>([]);
  items: any[] = [];
  count = 0;
  registerPassportForm: FormGroup;
  tempItems: any[] = [];
  seedSenderList = [];
  submitted = false;
  momentObj: any;
  file = null;
  loading = false;
  apiError = false;
  seed_owner = false;
  roles = [];
  plantsListing = [];
  constructor(private modalService: NgbModal,
              private formBuilder: FormBuilder,
              private alertService: AlertService,
              private plantRegisterService: PlantRegisterService,
              private withDrawService: WithdrawService,
              private wareHouseService: WarehouseService,
              private router: Router,
              private route: ActivatedRoute) {
    this.onGetPlantListing();
    this.onGetWithdrawPlantListing();
    this.onGetRecoveryListing();
    this.momentObj = moment;
  }

  ngOnInit() {
    this.route
      .data
      .subscribe(data => {
        this.roles = data['roles'];
      });
    this.registerPassportForm = this.formBuilder.group({
      dateOfReceiving: [''],
      serviceRequestLetter: [''],
      agenciesRequestSeedGermination: [''],
      agenciesSubRequestSeedGermination: [''],
      agenciesSubRequestSeedGerminationInput: [''],
      numberOfRequestor: [''],
      requestorAddress: [''],
      telephoneNumber: [''],
      email: [''],
      objective: [''],
      owner: ['']
    });
  }

  get f() {
    if (!this.apiError && this.submitted && !this.registerPassportForm.invalid) {
      this.alertService.error(null);
    }

    return this.registerPassportForm.controls;
  }

  reloadData(params: DataTableParams) {
    this.resources.query(params).then(vals => {
      this.items = vals;
      this.tempItems = this.items;
    });
  }

  onDetails({_id}) {
    this.router.navigate([`/admin/withdraw/listing-plants/${_id}`]);
  }

  onDelete(item: object, deleteContent) {
    this.modalService.open(deleteContent, {centered: true});
  }

  onSearch({target: {value: searchRole = ''} = {}}) {
    this.items = this.tempItems.filter(({w_no}) => w_no && toLower(w_no).includes(toLower(searchRole)));
  }

  onCreate(createContent) {
    this.registerPassportForm.reset();
    this.file = null;
    this.modalService.open(createContent, {centered: true});
  }

  onFileUpload({srcElement: {files = []} = {}}) {
    this.file = files[0];
  }

  onSubmit() {
    this.submitted = true;
    
    if (this.registerPassportForm.invalid ) {
        this.alertService.error('Some fields are required. Please fill those fields');
      return;
    }
    
    if (!this.seed_owner ) {
      this.alertService.error('à¹€à¸ˆà¹‰à¸²à¸‚à¸­à¸‡à¹€à¸Šà¸·à¹‰à¸­à¸žà¸±à¸™à¸˜à¸¸à¹Œ (à¸ªà¹ˆà¸§à¸™à¸™à¸µà¹‰à¸ˆà¸°à¹ƒà¸Šà¹‰à¸ªà¸³à¸«à¸£à¸±à¸šà¸à¸²à¸£à¸”à¸¶à¸‡à¸‚à¹‰à¸­à¸¡à¸¹à¸¥) is required. Please fill');
      return;
    }

    this.loading = true;
    this.apiError = false;

    this.alertService.reset();
    this.loading = true;

    this.modalService.dismissAll();
    this.withDrawService.createOrUpdate({...this.registerPassportForm.value, file: this.file}, null)
      .pipe(first())
      .subscribe(
        ({data = {}}: any) => {
          this.loading = false;
          const {_id} = data;
          this.router.navigate([`/admin/withdraw/pending/${_id}`]);
        },
        error => {
          this.loading = false;
          this.apiError = true;
          this.alertService.error(error);
        });
  }

  onGetRecoveryListing() {
    this.loading = true;
    this.withDrawService.getAll()
      .pipe(first())
      .subscribe(
        ({data = []}: any) => {
          let values = [];
          for(var i = 0; i < data.length; i++) {
            let withdraw_id = data[i]['_id'];
            let plantsListing = this.plantsListing;
            values = [];
            for(var j = 0; j < plantsListing.length; j++) {
              if(plantsListing[j]['m5_assigned'] == withdraw_id) values.push(plantsListing[j]['updatedAt']);
            }
            data[i].m8lab_plant_updatedAt = max(values);
          }
          this.resources = new DataTableResource<any>(data);
          this.resources.count().then(count => this.count = count);
          this.reloadData({});
          this.loading = false;
        },
        error => {
          this.loading = false;
          this.alertService.error(error);
        });
  }

  onGetWithdrawPlantListing() {
    this.loading = true;
    this.withDrawService.getAllWithdrawPlants()
      .pipe(first())
      .subscribe(
        ({data = []}: any) => {
          this.plantsListing = data;
        },
        error => {
          this.loading = false;
          this.alertService.error(error);
        });
  }

  onGetPlantListing() {
    this.wareHouseService.getSpecificLab('all')
      .pipe(first())
      .subscribe(
        ({data = []}: any) => {
          var tem = [];
          var i = 0 ;
          data.forEach(ele => {
            if(ele.submitTo != undefined)
              if(ele.submitTo == "m5")
              { 
                let stock_weight = null
                if(ele.room5.length)
                {
                  ele.room5.forEach(element => {
                    if(element)
                    stock_weight=  element.wh_batch_weight;
                  });
                }
                if(ele.room10.length)
                {
                  ele.room10.forEach(element => {
                    if(element)
                      stock_weight=element.wh_batch_weight;
                  });
                }
                if(ele.room130.length)
                {
                  ele.room130.forEach(element => {
                    if(element)
                      stock_weight= element.storage_weight;
                  });
                }
                if(ele.anteArray.length)
                {
                  ele.anteArray.forEach(element => {
                    if(element)
                      stock_weight= element.storage_weight;
                  });
                }           
                if(stock_weight > 0){
                  // if(ele.m5_assigned == undefined)
                  {
                    var isseed = false ; 
                    tem.forEach(eleseed => {
                      if(ele.reg_gene_id == eleseed || ele.reg_gene_id.substring(0, 1) == 'R')
                        isseed = true;
                    });
                    if(!isseed){
                      this.seedSenderList[i] = { 'reg_seed_owner':ele.reg_seed_owner + " " + ele.reg_gene_id};
                      tem.push(ele.reg_gene_id)
                      i ++;
                    }
                  }
                }
              }
          });            
        },
        error => {
          this.loading = false;
        });
  }

  selectSeedSender({reg_seed_owner}) {
    if (reg_seed_owner) {
      this.registerPassportForm.get('owner').setValue(reg_seed_owner);
      this.seed_owner = true;
    }
  }

  getStatus(item) {
    const {pending, draft, total, approve} = item;
    if ( pending || draft || total === 0 ) {
      return 'Draft';
    }

    if (approve) {
      return ' Approve';
    }

    return 'Completed';
  }
  checkRole(index){
    if(localStorage.getItem('superAdmin') === "true") return true;
    if(localStorage.getItem('rolesId').indexOf(this.roles[index]) > -1) return true;
    return false;
  }
}