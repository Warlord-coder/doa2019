import {Component, OnInit} from '@angular/core';
import {ActivatedRoute, Router} from '@angular/router';
import {FormBuilder, FormGroup, Validators} from '@angular/forms';
import {AlertService, PlantCategoryService, PlantCharacterService, PlantGroupsService, PlantTypeService} from '../../../../services';
import {getRoleResources} from '../../../../utils';
import {TreeviewConfig, TreeviewItem} from 'ngx-treeview';
import {first} from 'rxjs/operators';
import {DataTableResource} from 'ngx-datatable-bootstrap4';

@Component({
  selector: 'app-add-update-plant-group-type-page',
  templateUrl: './add-update.component.html',
})
export class NgbdAddUpdatePlantTypeComponent implements OnInit {
  PlantTypeForm: FormGroup;
  submitted = false;
  roleResources: TreeviewItem[];
  loading = false;
  apiError = false;
  plantCategories = [];
  plantCharacters = [];
  filterCharacters = []
  id = null;
  config = TreeviewConfig.create({
    hasAllCheckBox: true,
    hasFilter: true,
    hasCollapseExpand: true,
    decoupleChildFromParent: false,
    maxHeight: 800
  });


  constructor(private router: Router,
              private formBuilder: FormBuilder,
              private activeRoute: ActivatedRoute,
              private plantTypeService: PlantTypeService,
              private plantCategoryService: PlantCategoryService,
              private plantCharacterService: PlantCharacterService,
              private alertService: AlertService) {
    this.getPlantCategories();
    this.getPlantCharacters();
  }

  ngOnInit() {
    this.roleResources = getRoleResources();
    this.PlantTypeForm = this.formBuilder.group({
      status: ['', Validators.required],
      plantCategory: ['', Validators.required],
      plantSpecie: ['', Validators.required],
      plantTypeName_en: ['', Validators.required],
      plantTypeName_th: ['', Validators.required]
    });
  }

  get f() {
    if (!this.apiError && this.submitted && !this.PlantTypeForm.invalid) {
      this.alertService.error(null);
    }

    return this.PlantTypeForm.controls;
  }

  onSubmit() {
    this.submitted = true;
    if (this.PlantTypeForm.invalid) {
      this.alertService.error('Some fields are required. Please fill those fields');
      return;
    }

    this.alertService.reset();
    this.loading = true;
    this.apiError = false;

    this.plantTypeService.createOrUpdate({...this.PlantTypeForm.value}, this.id)
      .pipe(first())
      .subscribe(
        () => {
          this.loading = false;
          this.router.navigate(['/admin/master/plant-type/listing']);
        },
        error => {
          this.loading = false;
          this.apiError = true;
          this.alertService.error(error);
        });
  }

  onSelectedChange($event) {
    console.log('Select', $event);
  }

  onGetPlantGroupInformation(id: string) {
    this.loading = true;
    this.plantTypeService.getSpecific(id)
      .pipe(first())
      .subscribe(
        ({data = {}}: any) => {
          Object.keys(data).forEach(key => {
            if (this.PlantTypeForm.get(key)) {
              this.PlantTypeForm.get(key).setValue(data[key]);
            }
          });
          this.loading = false;
          for (const category  of this.plantCharacters) {
            if (category.plant_category === data.plantCategory) {
              this.filterCharacters.push(category);
            }
          }
        },
        error => {
          this.loading = false;
          this.apiError = true;
          this.alertService.error(error);
        });
  }

  getPlantCategories() {
    this.plantCategoryService.getAll()
      .pipe(first())
      .subscribe(
        ({data = []}: any) => {
          this.loading = false;
          this.plantCategories = data;
        },
        error => {
          this.loading = false;
          this.alertService.error(error);
        });
  }

  getPlantCharacters() {
    this.plantCharacterService.getAll()
      .pipe(first())
      .subscribe(
        ({data = []}: any) => {
          this.loading = false;
          this.plantCharacters = data;
          this.activeRoute.params.subscribe(params => {
            const {id} = params;
            this.id = id;
            if (id && id !== 'create') {
              this.onGetPlantGroupInformation(id);
            }
          });
        },
        error => {
          this.loading = false;
          this.alertService.error(error);
        });
  }

  onCancel() {
    this.router.navigate(['/admin/master/plant-type/listing']);
  }

  onSelected({target: {value = ''} = {}}) {
    this.filterCharacters = []
    this.PlantTypeForm.get('plantSpecie').setValue('');
    for (const category  of this.plantCharacters) {
      if (category.plant_category === value) {
        this.filterCharacters.push(category);
      }
    }
  }
}
