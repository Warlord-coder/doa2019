import {Component, OnInit} from '@angular/core';
import {ActivatedRoute, Router} from '@angular/router';
import {FormBuilder, FormGroup, Validators} from '@angular/forms';
import {pickBy, identity} from 'lodash';
import {
  AlertService,
  CommonProvinceService, PassportService,
  PlantCategoryService,
  PlantGroupsService,
  PlantGroupTypesService,
  CommonCountryService, CommonAumperService, PlantService,
  PlantRegisterService, VolumeService, PlantTypeService, PlantCharacterService
} from '../../../../services';
import {getRoleResources} from '../../../../utils';
import {TreeviewConfig, TreeviewItem} from 'ngx-treeview';
import {first} from 'rxjs/operators';
import {omit} from 'lodash';
import {environment} from '../../../../../environments/environment';

@Component({
  selector: 'app-add-update-plant-register-page',
  templateUrl: './add-update.component.html',
})
export class NgbdAddUpdatePlantRegister3Component implements OnInit {
  plantGroupTypeForm: FormGroup;
  submitted = false;
  loading = false;
  file = null;
  id = null;
  categoryList = [];
  groupTypeList = [];
  groupList = [];
  provinceList = [];
  aumperList = [];
  plantTypes = [];
  countryList = [];
  volumeInfo = null;
  passportInfo = null;
  initialPlantGeneName = null;
  apiError = false;
  plantsListing = [];
  reg_character_attached_file = null;
  reg_agreement_attach_file = null;
  regCharacterAttachedFile = null;
  regAgreementAttachFile = null;
  reg_seed_no = null;
  reg_gene_id = null;
  selectedPlant = null;
  volumeId = null;
  disabled = false;
  regNames = [];
  roleResources: TreeviewItem[];

  constructor(private router: Router,
              private formBuilder: FormBuilder,
              private activeRoute: ActivatedRoute,
              private plantCategoryService: PlantCategoryService,
              private plantGroupTypesService: PlantGroupTypesService,
              private plantGroupsService: PlantGroupsService,
              private plantCharacterService: PlantCharacterService,
              private plantService: PlantService,
              private volumeService: VolumeService,
              private passportService: PassportService,
              private plantTypeService: PlantTypeService,
              private plantRegisterService: PlantRegisterService,
              private countryService: CommonCountryService,
              private provinceService: CommonProvinceService,
              private aumperService: CommonAumperService,
              private alertService: AlertService) {
    this.onGetListing('plantCharacterService', 'plantsListing');
    this.onGetListing('plantTypeService', 'plantTypes');
    this.onGetListing('plantCategoryService', 'categoryList');
    this.onGetListing('plantGroupTypesService', 'groupTypeList');
    this.onGetListing('plantGroupsService', 'groupList');
    this.onGetListing('countryService', 'countryList');
    this.onGetListing('aumperService', 'aumperList');
    this.onGetListing('provinceService', 'provinceList');
  }

  ngOnInit() {
    this.roleResources = getRoleResources();
    this.plantGroupTypeForm = this.formBuilder.group({
      plant_gene_name: ['', Validators.required],
      reg_gene_type: [''],
      reg_plant_alter_name: [[], Validators.required],
      reg_gene_category: ['', Validators.required],
      reg_seed_collect_date: [''],
      selfing_input: ['', Validators.required],
      selfing_dropdown: ['', Validators.required],
      oop_input: ['', Validators.required],
      reg_plant_sci_name: [''],
      oop_dropdown: ['', Validators.required],
      other_input: ['', Validators.required],
      other_dropdown: ['', Validators.required],
      department_type: ['', Validators.required],
      department_sub_type: [''],
      reg_agreement_field: [''],
      department_name: [''],
      source_country: [''],
      source_country_1: [''],
      source_province: ['', Validators.required],
      source_district: ['', Validators.required],
      source_address: ['', Validators.required],
      lat: [''],
      long: [''],
      source_sea_level: [''],
      source_by_department: [''],
      reg_seed_collect_person: ['', Validators.required],
      reg_seed_owner: ['', Validators.required],
      reg_seed_sender: ['', Validators.required],
      reg_character_brief: ['', Validators.required],
      reg_character_for_record: [''],
      reg_sent_lab_date: ['', Validators.required],
      reg_remark: ['', Validators.required],
    });
    this.activeRoute.params.subscribe(params => {
      const {id, volume} = params;
      this.id = id;
      this.volumeId = volume;
      this.onGetVolumeInfo(this.volumeId);
      if (id && id !== 'create') {
        this.onGetPlantRegisterInfo(id);
      }
      setTimeout(() => {
        this.findRegGeneName();
      }, 500);
    });

    this.activeRoute.queryParams.subscribe(queryParams => {
      const {copy} = queryParams;
      if (copy && this.id === 'create') {
        this.onGetPlantRegisterInfo(copy, ['reg_plant_sci_name', 'reg_plant_common_name',
          'reg_plant_alter_name', 'reg_gene_id', 'reg_seed_no', 'plant_category','selfing_input','oop_input','other_input']);
      }
    });

  }

  get f() {
    if (!this.apiError && this.submitted && !this.plantGroupTypeForm.invalid) {
      this.alertService.error(null);
    }

    return this.plantGroupTypeForm.controls;
  }

  onSubmit() {
    // this.submitted = true;
    if (this.plantGroupTypeForm.invalid) {
      // this.alertService.error('Some fields are required. Please fill those fields');
      // return;
    }

    if (!this.selectedPlant) {
      // this.alertService.error('Please Select Reg Plant Sci Name');
      // return;
    }

    const {reg_plant_alter_name = [], plant_gene_name} = this.plantGroupTypeForm.value;
    this.plantRegisterService.createOrUpdate(pickBy({
      ...this.plantGroupTypeForm.value,
      passport: this.passportInfo._id,
      volume: this.volumeId,
      reg_plant_alter_name: reg_plant_alter_name ? JSON.stringify(reg_plant_alter_name) : [],
      plant_gene_name_selection: !this.initialPlantGeneName && plant_gene_name && typeof plant_gene_name === 'string',
      plant_gene_name: plant_gene_name && typeof plant_gene_name === 'string' && !this.initialPlantGeneName ? plant_gene_name : (this.initialPlantGeneName ? this.initialPlantGeneName._id : plant_gene_name._id),
      plant_category: this.selectedPlant ? this.selectedPlant.plant_category : null,
      reg_plant_sci_name: this.selectedPlant ? this.selectedPlant._id : null,
      file: [this.regCharacterAttachedFile, this.regAgreementAttachFile],
      isRegCharacterAttachedFileUpload: Boolean(this.regCharacterAttachedFile),
      isRegAgreementAttachFileUpload: Boolean(this.regAgreementAttachFile),
      reg_character_attached_file: this.reg_character_attached_file,
      reg_agreement_attach_file: this.reg_agreement_attach_file,
      is_generate_d_gene: true
    }, identity), this.id)
      .pipe(first())
      .subscribe(
        () => {
          this.loading = false;
          this.router.navigate([`/admin/register/register-3/${this.volumeId}/listing`]);
        },
        error => {
          this.loading = false;
          this.apiError = true;
          this.alertService.error(error);
        });
  }

  onSelectedChange($event) {
    console.log('Select', $event);
  }

  onFilterChange($event) {
    console.log('Filter Change', $event);
  }

  onGetListing(serviceName, key) {
    this.loading = true;
    this[serviceName].getAll()
      .pipe(first())
      .subscribe(
        ({data = []}: any) => {
          this.loading = false;
          this[key] = data;
        },
        error => {
          this.loading = false;
          this.alertService.error(error);
        });
  }

  onGetPlantRegisterInfo(id, skipFields = []) {
    this.loading = true;

    this.plantRegisterService.getSpecific(id)
      .pipe(first())
      .subscribe(
        ({data = []}: any) => {
          data = omit(data, skipFields);
          const {
            reg_gene_id, reg_seed_no, reg_plant_sci_name, reg_character_attached_file,
            reg_agreement_attach_file,
            m3_status = ''
          } = data;
          this.disabled = m3_status === 'complete';
          Object.keys(data).forEach(key => {
            if (this.plantGroupTypeForm.get(key)) {
              if (!skipFields.includes(key)) {
                this.plantGroupTypeForm.get(key).setValue(data[key]);
              }
              if (this.disabled) {
                this.plantGroupTypeForm.get(key).disable({onlySelf: true});
              }
            }
          });
          this.reg_gene_id = reg_gene_id;
          this.reg_seed_no = reg_seed_no;
          this.reg_character_attached_file = reg_character_attached_file;
          this.reg_agreement_attach_file = reg_agreement_attach_file;
          this.loading = false;
          setTimeout(() => {
            this.findPlantSciName(data);
            this.findPlantTypeGeneName(data);
          }, 500);
        },
        error => {
          this.loading = false;
          this.alertService.error(error);
        });
  }

  onGetVolumeInfo(id) {
    this.loading = true;
    this.volumeService.getSpecific(id)
      .pipe(first())
      .subscribe(
        ({data = []}: any) => {
          this.loading = false;
          const {passport_no} = data;
          this.volumeInfo = data;
          this.onGetPassportInfo(passport_no);
        },
        error => {
          this.loading = false;
          this.alertService.error(error);
        });
  }

  onGetPassportInfo(id) {
    this.loading = true;
    this.passportService.getSpecific(id)
      .pipe(first())
      .subscribe(
        ({data = []}: any) => {
          this.loading = false;
          const {passport_no} = data;
          this.passportInfo = data;
        },
        error => {
          this.loading = false;
          this.alertService.error(error);
        });
  }

  getAttachment(basePath, name) {
    if (name) {
      return `${environment.endpoint}/${basePath}/${name}`;
    }

    return null;
  }

  findPlantSciName({reg_plant_sci_name}) {
    if (!reg_plant_sci_name) {
      return;
    }
    for (const plant of  this.plantsListing) {
      if (reg_plant_sci_name === plant._id) {
        this.selectedPlant = plant;
        this.plantGroupTypeForm.get('reg_plant_sci_name').setValue(plant.genus + ' ' + plant.species);
      }
    }

  }

  onFileUpload({srcElement: {files = []} = {}}, key) {
    this[key] = files[0];
  }

  onSelect(plant) {
    this.selectedPlant = plant;
    this.plantGroupTypeForm.get('reg_plant_sci_name').setValue(plant.genus + ' ' + plant.species);    
    var i = 0 ; 
    this.regNames = [] ;
    for (const planttype of  this.plantTypes) {
      if (plant.plant_category == planttype.plantCategory)
      this.regNames[i] = planttype.plantTypeName_en;
      i ++ ;
    }
    for (const planttype of  this.plantsListing) {
      if (plant.plant_category == planttype.plant_category)
        this.regNames[i] = planttype.genus + ' ' + planttype.species;
      i ++ ;
    }
  }

  findRegGeneName() {
    var i = 0 ; 
    for (const plant of  this.plantTypes) {
      this.regNames[i] = plant.plantTypeName_en;
      i ++ ;
    }
    for (const plant of  this.plantsListing) {
      this.regNames[i] = plant.genus + ' ' + plant.species;
      i ++ ;
    }
  }

  findPlantTypeGeneName({plant_gene_name}) {
    if (!plant_gene_name) {
      return;
    }
    for (const plant of  this.plantTypes) {
      if (plant_gene_name === plant._id) {
        this.initialPlantGeneName = plant;
        this.plantGroupTypeForm.get('plant_gene_name').setValue(plant.plantTypeName_en);
      }
    }
  }

  onClear() {
    this.selectedPlant = null;
  }

  onCancel() {
    this.router.navigate([`/admin/register/register-3/${this.volumeId}/listing`]);
  }

  onBlur() {
  }

  onChangeRegGeneName() {
    this.initialPlantGeneName = null;
  }
}
