import {Component, OnInit} from '@angular/core';
import {DataTableParams, DataTableResource} from 'ngx-datatable-bootstrap4';
import {NgbModal} from '@ng-bootstrap/ng-bootstrap';
import {ActivatedRoute, Router} from '@angular/router';
import {first} from 'rxjs/operators';
import {
  AlertService,
  AttributeListService,
  PassportService,
  PlantRegisterService,
  VolumeService
} from '../../../../services';
import * as moment from 'moment';
import {toLower} from 'lodash';
import {FORM_REQUIRED_KEYS, isFormSubmissionError} from '../../../../utils';

@Component({
  selector: 'app-listing-plant-group-page',
  templateUrl: './listing.component.html',
})
export class NgbdListingRegisterPlant3Component implements OnInit {
  roleResources = new DataTableResource<any>([]);
  items: any[] = [];
  tempItems: any[] = [];
  count = 0;
  volumeId = null;
  loading = false;
  volumeInfo = null;
  momentObj: any;
  passportInfo = null;
  currentStatus = 'draft';

  selectedPlantsForSubmit: any = [];

  constructor(private modalService: NgbModal,
              private activeRoute: ActivatedRoute,
              private passportService: PassportService,
              private plantRegisterService: PlantRegisterService,
              private attributeListService: AttributeListService,
              private volumeService: VolumeService,
              private alertService: AlertService,
              private router: Router) {
    this.momentObj = moment;
  }

  ngOnInit(): void {
    this.activeRoute.params.subscribe(params => {
      const {volume} = params;
      this.volumeId = volume;
      this.onGetVolumeInfo(this.volumeId);
    });
  }

  reloadData(params: DataTableParams) {
    console.log(params);
    this.roleResources.query(params).then(vals => {
      this.items = vals;
      this.tempItems = this.items;
    });
  }

  onEdit({_id}) {
    localStorage.setItem('lastUrl1', `/admin/register/register-3/${this.volumeId}/listing`);
    this.router.navigate([`/admin/register/register-3/${this.volumeId}/${_id}`]);
  }

  onCopy({_id}) {
    this.router.navigate([`/admin/register/register-3/${this.volumeId}/create`], {
      queryParams: {copy: _id}
    });
  }

  onDelete(item: object, deleteContent) {
    this.modalService.open(deleteContent, {centered: true});
  }

  onSearch({target: {value: searchRole = ''} = {}}) {
    this.items = this.tempItems.filter(({passport}) => {
      const {passport_no = null} = passport || {};
      return passport_no && toLower(passport_no).includes(toLower(searchRole));
    });
  }

  onGetVolumeInfo(id) {
    this.loading = true;
    this.volumeService.getSpecific(id)
      .pipe(first())
      .subscribe(
        ({data = []}: any) => {
          this.loading = false;
          const {passport_no} = data;
          this.volumeInfo = data;
          this.onGetListing('draft');
          this.onGetPassportInfo(passport_no);
        },
        error => {
          this.loading = false;
          this.alertService.error(error);
        });
  }

  onGetPassportInfo(id) {
    this.loading = true;
    this.passportService.getSpecific(id)
      .pipe(first())
      .subscribe(
        ({data = []}: any) => {
          this.loading = false;
          const {passport_no} = data;
          this.passportInfo = data;
        },
        error => {
          this.loading = false;
          this.alertService.error(error);
        });
  }

  onGetListing(status) {
    this.currentStatus = status;
    this.loading = true;
    this.plantRegisterService.getAll(this.volumeId, 'plant_listing', status, 'Register 3')
      .pipe(first())
      .subscribe(
        ({data = []}: any) => {
          this.loading = false;
          this.roleResources = new DataTableResource<any>(data);
          this.roleResources.count().then(count => this.count = count);
          this.reloadData({});
        },
        error => {
          this.loading = false;
          this.alertService.error(error);
        });
  }

  onChangeItemSelect({_id}) {
    if (this.selectedPlantsForSubmit.includes(_id)) {
      this.selectedPlantsForSubmit = this.selectedPlantsForSubmit.filter(id => id !== _id);
      return;
    }

    this.selectedPlantsForSubmit = [...this.selectedPlantsForSubmit, _id];
  }

  onSubmitPlants() {
    if (!this.selectedPlantsForSubmit || !this.selectedPlantsForSubmit.length) {
      return;
    }
    const {form_type} = this.volumeInfo;
    this.loading = true;
    for (const item of this.items) {
      const {_id}: any = item;
      if (this.selectedPlantsForSubmit.includes(_id)) {
        const record = isFormSubmissionError(item,
          form_type === 'Register 4' ? FORM_REQUIRED_KEYS.REGISTER_FORM_4 : FORM_REQUIRED_KEYS.REGISTER_FORM);
        if (!item.selfing_input && !item.oop_input && !item.other_input) {
          this.alertService.error('Please Select one from Stock (Selfing, OOP, Other)');
          this.loading = false;
          return;
        }
        if (item.selfing_input && !item.selfing_dropdown) {
          this.alertService.error('Please Select Stock Type for Selfing');
          this.loading = false;
          return;
        }
        if (item.oop_input && !item.oop_dropdown) {
          this.alertService.error('Please Select Stock Type for OP');
          this.loading = false;
          return;
        }
        if (item.other_input && !item.other_dropdown) {
          this.alertService.error('Please Select Stock Type for Other');
          this.loading = false;
          return;
        }
        if (record) {
          this.loading = false;
          return this.alertService.error(`Validation Failed for record Reg Seed No: ${record.reg_seed_no}`);
        }
      }
    }

    this.plantRegisterService.submitToModule8(this.selectedPlantsForSubmit)
      .pipe(first())
      .subscribe(
        ({data = {}}: any) => {
          this.onGetListing('draft');
          this.selectedPlantsForSubmit = [];
        },
        error => {
          this.loading = false;
          this.alertService.error(error);
        });
  }

  isRegisterForm12() {
    if (this.volumeInfo && this.volumeInfo.form_type === 'Register 4') {
      return false;
    }
    return true;
  }

  onLockRegisterForm() {
    this.loading = true;
    this.volumeService.createOrUpdate({isRegisterFormLock: true}, this.volumeId)
      .pipe(first())
      .subscribe(
        ({data = []}: any) => {
          this.loading = false;
          this.volumeInfo = data;
        },
        error => {
          this.loading = false;
          this.alertService.error(error);
        });
  }

  getPlantStockType(item) {
    const {
      selfing_input, selfing_dropdown,
      oop_input, oop_dropdown,
      other_input, other_dropdown
    } = item;
    const stock = {
      isSelfing: selfing_input && selfing_dropdown,
      isOOP: oop_input && oop_dropdown,
      isOther: other_input && other_dropdown,
    };
    if (stock.isSelfing) {
      return `Self ${selfing_input} ${selfing_dropdown}`;
    }
    if (stock.isOOP) {
      return `OP ${oop_input} ${oop_dropdown}`;
    }
    if (stock.isOther) {
      return `Other ${other_input} ${other_dropdown}`;

    }
  }

  addPlantsToForm4() {
    this.router.navigate([`/admin/register/unassigned-plant/${this.volumeId}/listing`]);
  }
}
