import { Component, OnInit } from '@angular/core';
import {FormBuilder, FormGroup, Validators, FormArray, FormControl, ValidationErrors} from '@angular/forms';
import {PlantCategoryService, PlantTypeService, WarehouseService, CommonProvinceService, CommonCountryService, PlantService} from '../../../services';
import {first} from 'rxjs/operators';
import * as moment from "moment";
import { Workbook } from "exceljs";
import * as fs from "file-saver";
import {uniqBy, toLower} from 'lodash';
@Component({
  selector: 'app-export-doa',
  templateUrl: './export-doa.component.html',
  styleUrls: ['./export-doa.component.scss']
})
export class NgbdExportDoaComponent implements OnInit {

  exportDoaForm: FormGroup;

  plantCategoriesListing = [];
  plantTypesListing = [];
  allPlantTypesListing = [];
  gsNoListing = [];
  allGsNoListing = [];
  regNoListing = [];
  allRegNoListing = [];
  provinceListing = [];
  allProvinceListing = [];
  countryListing = [];
  allCountryListing = [];
  warehouseListing = [];
  attribute_headers = [];
  selectedPlantCategory;
  submitted = false;
  error = false;
  loading = false;
  constructor(
    private formBuilder: FormBuilder,
    private plantCategoryService: PlantCategoryService,
    private plantTypeService: PlantTypeService,
    private warehouseService: WarehouseService,
    private provinceService: CommonProvinceService,
    private countryService: CommonCountryService,
  ) {
    this.getAutoCompleteInfo();
  }

  ngOnInit() {
    this.loading = true;
    this.warehouseService.getListAll()
    .pipe(first())
    .subscribe(
      ({data}: any) => {
        this.warehouseListing = data;
        this.loading = false;
      });
    this.exportDoaForm = this.formBuilder.group({
      plantCategory: ['', [Validators.required]],
      plantType: [''],
      gs_no: [''],
      reg_gene_id: [''],
      country: [''],
      province: ['']
    });
  }

  getAutoCompleteInfo() {
    this.getAutoCategory('');
  }

  onChangeSearch(keyword: string, type: string) {
    if(type === "category") {
      this.getAutoCategory(keyword);
    } else if (type == "type") {
      this.plantTypesListing = this.allPlantTypesListing.filter(plantType => {
        if(toLower(plantType['plantTypeName_en']).includes(toLower(keyword)))
          return true;
      });
    } else if(type == "gs_no") {
      this.gsNoListing = this.allGsNoListing.filter(item => {
        if(toLower(item['gs_no']).includes(toLower(keyword)))
          return true;
      });
    } else if (type == "reg_gene_id") {
      this.regNoListing = this.allRegNoListing.filter(item => {
        if(toLower(item['gs_no']).includes(toLower(keyword)))
          return true;
      });
    } else if (type == "province") {
      this.provinceListing = this.allProvinceListing.filter(item => {
        if(toLower(item['provinceName_en']).includes(toLower(keyword)))
          return true;
      });
    } else if (type == "country") {
      this.countryListing = this.allCountryListing.filter(item => {
        if(toLower(item['countryName_en']).includes(toLower(keyword)))
          return true;
      });
    }
  }

  getAutoCategory(keyword){
    this.plantCategoryService.search(keyword)
      .pipe(first())
      .subscribe(
        ({data}: any) => {
          this.plantCategoriesListing = data;
        });
  }
  selectEvent(item) {
    this.selectedPlantCategory = item._id;
    let warehouses = this.warehouseListing.filter(warehouse => {
      if(warehouse['plant_gene_name'] == undefined) return false;
      if(warehouse['plant_gene_name']['plantCategory'] == undefined) return false;
      if(warehouse['plant_gene_name']['plantCategory']['_id'] == this.selectedPlantCategory) return true;
      return false;
    })
    warehouses.forEach(warehouse => {
      this.allPlantTypesListing.push({_id: warehouse['plant_gene_name']['_id'], plantTypeName_en: warehouse['plant_gene_name']['plantTypeName_en']});
      if(warehouse['gs_no'])
        this.allGsNoListing.push({gs_no: warehouse['gs_no']});
      if(warehouse['reg_gene_id'])
        this.allRegNoListing.push({reg_gene_id: warehouse['reg_gene_id']})
      if(warehouse['source_country']){
        this.allCountryListing.push({_id: warehouse['source_country']['_id'], countryName_en: warehouse['source_country']['countryName_en']})
      }
      if(warehouse['source_province']){
        this.allProvinceListing.push({_id: warehouse['source_province']['_id'], provinceName_en: warehouse['source_province']['provinceName_en']})
      }
    })
    this.allPlantTypesListing = uniqBy(this.allPlantTypesListing, '_id');
    this.plantTypesListing = this.allPlantTypesListing;
    this.allGsNoListing = uniqBy(this.allGsNoListing, 'gs_no');
    this.gsNoListing = this.allGsNoListing;
    this.allRegNoListing = uniqBy(this.allRegNoListing, 'reg_gene_id');
    this.regNoListing = this.allRegNoListing;
    this.allCountryListing = uniqBy(this.allCountryListing, 'countryName_en');
    this.countryListing = this.allCountryListing;
    this.allProvinceListing = uniqBy(this.allProvinceListing, 'provinceName_en');
    this.provinceListing = this.allProvinceListing;
    this.error = false;
  }

  onSubmit() {
    if(this.exportDoaForm.get('plantCategory').value == ""){
      this.error = true;
    } else {
      const data = this.getSearchParams();
      this.warehouseService.exportDoa(data)
        .pipe(first())
        .subscribe(
          ({data}: any) => {
            this.exportAsExcel(data);
          });
    }
    console.log(this.error);
  }
  getSearchParams() {
    let queryParams: any = {};
    const {plantCategory, alterPlant, plantType, gs_no, reg_gene_id, country, province, plantSpecies, plantCommonName, attributes, ...others} = this.exportDoaForm.value;
    queryParams = others;
    if (plantCategory) {
      queryParams.plantCategory = typeof plantCategory === 'string' ? plantCategory : plantCategory.categoryName_en;
    }
    if (plantType) {
      queryParams.plantType = typeof plantType === 'string' ? plantType : plantType.plantTypeName_en;
    }
    
    if(gs_no) {
      queryParams.gs_no = typeof gs_no === 'string' ? gs_no : gs_no.gs_no;
    }
    if(reg_gene_id) {
      queryParams.reg_gene_id = typeof reg_gene_id === 'string' ? reg_gene_id : reg_gene_id.reg_gene_id;
    }
    if(country) {
      queryParams.country = typeof country === 'string' ? country : country.countryName_en;
    }
    if(province) {
      queryParams.province = typeof province === 'string' ? province : province.provinceName_en;
    }
    return queryParams;
  }
  exportAsExcel(data) {
    
    let excelData = [];
    let attr_headers;
    data.forEach((item, key) => {
      let plant_category;
      let plant_type;
      let reg_plant_alert_name;
      let reg_plant_comman_name;
      let scientific_name;
      let department;
      let country;
      let province;
      let address;
      let district;
      let attributes = [];
      attr_headers = [];
      if(item['plant_gene_name'] == undefined) {plant_category = ""; plant_type = "";}
      else {
        plant_category = item['plant_gene_name']['plantCategory']['categoryName_en'];
        plant_type = item['plant_gene_name']['plantTypeName_en'];
      }

      if(item['reg_plant_alert_name'] == null) reg_plant_alert_name = "";
      else reg_plant_alert_name = item['reg_plant_alert_name'];

      if(item['reg_plant_sci_name'] == undefined) {
        reg_plant_comman_name = "";
        scientific_name = "";
      } else {
        reg_plant_comman_name = item['reg_plant_sci_name']['reg_plant_common_name'];
        scientific_name = item['reg_plant_sci_name']['genus'] + ' ' + item['reg_plant_sci_name']['species'];
      }

      if(item['department_name'] == undefined) department = "";
      else department = item['department_name'];

      if(item['source_country'] == undefined) country = "";
      else country = item['source_country']['countryName_en'];

      if(item['source_province'] == undefined) province = "";
      else province = item['source_province']['provinceName_en'];

      if(item['source_address'] == undefined) address = "";
      else address = item['source_address'];

      if(item['source_district'] == undefined) district = "";
      else district = item['source_district']['aumperName_en'];

      if(item['attribute'] == undefined) attributes = [];
      else {
        let attribute_values = item['attribute']['attribute_value'];
        attribute_values.forEach(val => {
          attributes.push(val['name_en']);
          attr_headers.push(val['code']);
        })
      }
      excelData.push([key+1, item.gs_no, plant_category, plant_type, reg_plant_alert_name, reg_plant_comman_name, scientific_name, department, country, address, province, district, ...attributes]);
    });
    this.attribute_headers = attr_headers;
    const workbook = new Workbook();
    const worksheet = workbook.addWorksheet("Export DOA");

    let title = "Export DOA";
    
    // Add Row and formatting
    const titleRow = worksheet.addRow([title]);
    titleRow.font = { size: 14, bold: true };
    titleRow.alignment = { vertical: "middle", horizontal: "center" };

    worksheet.mergeCells("A1:L1");

    let headers = ['No.', 'Gs No', 'Plant', 'Variety/cultivar', 'Other Name', 'Common Name', 'Scientific Name', 'ชื่อหน่วยงาน', 'ประเทศ', 'ที่อยู่', 'จังหวัด', 'อำเภอ'];
    this.attribute_headers.forEach(val => {
      headers.push(val);
    })
    // Add Header Row
    const headerRow = worksheet.addRow(headers);
    // Cell Style : Fill and Border
    headerRow.eachCell((cell, number) => {
      cell.border = {
        top: { style: "thin" },
        left: { style: "thin" },
        bottom: { style: "thin" },
        right: { style: "thin" }
      };
      cell.font = { size: 12, bold: true };
      cell.alignment = { horizontal: "center", vertical: "middle" };
    });

    worksheet.getRow(1).height = 25;
    worksheet.getColumn(1).width = 15;
    worksheet.getColumn(2).width = 15;
    worksheet.getColumn(3).width = 15;
    worksheet.getColumn(4).width = 18;
    worksheet.getColumn(5).width = 15;
    worksheet.getColumn(6).width = 16;
    worksheet.getColumn(7).width = 18;
    worksheet.getColumn(8).width = 15;
    worksheet.getColumn(9).width = 15;
    worksheet.getColumn(10).width = 15;
    worksheet.getColumn(11).width = 15;
    worksheet.getColumn(12).width = 15;
    for(var i = 13; i < 100; i++) { 
      worksheet.getColumn(i).width = 18;
    }
    excelData.forEach(d => {
      const row = worksheet.addRow(d);
      for (let col = 1; col <= 100; col++) {
        const cell = row.getCell(col);
        cell.alignment = { horizontal: "center" };
      }
    });

    workbook.xlsx.writeBuffer().then((data: any) => {
      const blob = new Blob([data], {
        type:
          "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet"
      });
      fs.saveAs(blob, "ExportDoa.xlsx");
    });

  }
}
