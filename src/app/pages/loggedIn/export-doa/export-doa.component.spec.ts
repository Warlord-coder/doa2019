import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { NgbdExportDoaComponent } from './export-doa.component';

describe('ExportDoaComponent', () => {
  let component: NgbdExportDoaComponent;
  let fixture: ComponentFixture<NgbdExportDoaComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ NgbdExportDoaComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(NgbdExportDoaComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
