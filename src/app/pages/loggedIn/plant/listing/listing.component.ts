import {Component} from '@angular/core';
import {DataTableParams, DataTableResource} from 'ngx-datatable-bootstrap4';
import {NgbModal} from '@ng-bootstrap/ng-bootstrap';
import {Router} from '@angular/router';
import * as moment from 'moment';
import {first} from 'rxjs/operators';
import {toLower} from 'lodash';

import {PlantService} from '../../../../services/plant.service';
import {AlertService} from '../../../../services';

@Component({
  selector: 'app-listing-plant-group-page',
  templateUrl: './listing.component.html',
})
export class NgbdListingPlantComponent {
  resources = new DataTableResource<any>([]);
  items: any[] = [];
  count = 0;
  loading = false;
  momentObj: any;
  tempItems: any[] = [];


  constructor(private modalService: NgbModal,
              private alertService: AlertService,
              private plantService: PlantService,
              private router: Router) {
    this.momentObj = moment;
    this.onGetPlantListing();
  }

  reloadData(params: DataTableParams) {
    this.resources.query(params).then(vals => {
      this.tempItems = vals;
      this.items = vals;
    });
  }

  onEdit({_id = ''} = {}) {
    this.router.navigate([`/admin/master/plant/${_id}`]);
  }

  onDelete(item: object, deleteContent) {
    this.modalService.open(deleteContent, {centered: true});
  }

  onSearch({target: {value: searchRole = ''} = {}}) {
    this.items = this.tempItems.filter(({plantTypeName_sci_en}) =>
      plantTypeName_sci_en && toLower(plantTypeName_sci_en).includes(toLower(searchRole)));
  }


  onGetPlantListing() {
    this.plantService.getAll()
      .pipe(first())
      .subscribe(
        ({data = []}: any) => {
          this.resources = new DataTableResource<any>(data);
          this.resources.count().then(count => this.count = count);
          this.reloadData({});
          this.loading = false;
        },
        error => {
          this.loading = false;
          this.alertService.error(error);
        });
  }
}
