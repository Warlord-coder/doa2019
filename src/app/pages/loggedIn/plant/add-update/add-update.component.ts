import {Component, OnInit} from '@angular/core';
import {ActivatedRoute, Router} from '@angular/router';
import {FormBuilder, FormGroup, Validators} from '@angular/forms';
import {AlertService, PlantCategoryService, PlantCharacterService, PlantGroupsService, PlantGroupTypesService} from '../../../../services';
import {getRoleResources} from '../../../../utils';
import {TreeviewConfig, TreeviewItem} from 'ngx-treeview';
import {first} from 'rxjs/operators';
import {PlantService} from '../../../../services/plant.service';

@Component({
  selector: 'app-add-update-plant-group-type-page',
  templateUrl: './add-update.component.html',
})
export class NgbdAddUpdatePlantComponent implements OnInit {
  PlantForm: FormGroup;
  submitted = false;
  file = null;
  roleResources: TreeviewItem[];
  loading = false;
  categoryList = [];
  groupTypeList = [];
  characterList = [];
  groupList = [];
  lang = 'us';
  apiError = false;
  id = null;
  plantImg = null;
  config = TreeviewConfig.create({
    hasAllCheckBox: true,
    hasFilter: true,
    hasCollapseExpand: true,
    decoupleChildFromParent: false,
    maxHeight: 800
  });
  options: any = {
    cropEnabled: false,
    autoUpload: true,
    resizeOnLoad: false,
    thumbnailHeight: 320,
    thumbnailWidth: 320,
    uploadUrl: `https://fancy-image-uploader-demo.azurewebsites.net/api/demo/upload`,
    allowedImageTypes: ['image/png', 'image/jpeg']
  };

  onUpload(data) {
    const {file} = data;
    this.plantImg = null;
    this.file = file;
  }

  constructor(private router: Router,
              private formBuilder: FormBuilder,
              private activeRoute: ActivatedRoute,
              private plantCategoryService: PlantCategoryService,
              private plantGroupTypesService: PlantGroupTypesService,
              private plantCharacterService: PlantCharacterService,
              private plantGroupsService: PlantGroupsService,
              private plantService: PlantService,
              private alertService: AlertService) {
    this.onGetListing('plantCategoryService', 'categoryList');
    this.onGetListing('plantGroupTypesService', 'groupTypeList');
    this.onGetListing('plantCharacterService', 'characterList');
    this.onGetListing('plantGroupsService', 'groupList');
  }

  ngOnInit() {
    this.roleResources = getRoleResources();
    this.PlantForm = this.formBuilder.group({
      plantTypeName_sci_en: ['', Validators.required],
      plantTypeName_sci_global_en: ['', Validators.required],
      plantTypeName_sci_other_en: ['', Validators.required],
      plantTypeName_sci_th: ['', Validators.required],
      plantTypeName_sci_global_th: ['', Validators.required],
      plantTypeName_sci_other_th: ['', Validators.required],
      plant_category: ['', Validators.required],
      plant_character: ['', Validators.required],
      plant_group: ['', Validators.required],
      plant_group_type: ['', Validators.required],
      plant_status: ['', Validators.required],
    });
    this.activeRoute.params.subscribe(params => {
      const {id} = params;
      this.id = id;
      if (id && id !== 'create') {
        this.onGetPlantInformation(id);
      }
    });
  }

  get f() {
    if (!this.apiError && this.submitted && !this.PlantForm.invalid) {
      this.alertService.error(null);
    }

    return this.PlantForm.controls;
  }

  onSubmit() {
    this.submitted = true;
    if (this.PlantForm.invalid) {
      this.alertService.error('Some fields are required. Please fill those fields');
      return;
    }

    this.alertService.reset();
    this.loading = true;

    this.plantService.createOrUpdate({...this.PlantForm.value}, this.id)
      .pipe(first())
      .subscribe(
        () => {
          this.loading = false;
          this.router.navigate(['/admin/master/plant/listing']);
        },
        error => {
          this.loading = false;
          this.apiError = true;
          this.alertService.error(error);
        });
  }

  onSelectedChange($event) {
    console.log('Select', $event);
  }

  onFilterChange($event) {
    console.log('Filter Change', $event);
  }

  onGetListing(serviceName, key) {
    this.loading = true;
    this[serviceName].getAll()
      .pipe(first())
      .subscribe(
        ({data = []}: any) => {
          this.loading = false;
          this[key] = data;
        },
        error => {
          this.loading = false;
          this.alertService.error(error);
        });
  }

  onTabChange({activeId}) {
    if (activeId === 'ngb-tab-0') {
      this.lang = 'th';
      return;
    }
    this.lang = 'us';
  }

  onGetPlantInformation(id: string) {
    this.loading = true;
    this.plantService.getSpecific(id)
      .pipe(first())
      .subscribe(
        ({data = {}}: any) => {
          const {
            plant_status, plant_group_type, plant_group, plant_character, plant_category,
            plantTypeName_sci_other_th, plantTypeName_sci_global_th, plantTypeName_sci_th,
            plantTypeName_sci_other_en, plantTypeName_sci_global_en, plantTypeName_sci_en
          } = data;

          this.PlantForm.get('plantTypeName_sci_en').setValue(plantTypeName_sci_en);
          this.PlantForm.get('plantTypeName_sci_global_en').setValue(plantTypeName_sci_global_en);
          this.PlantForm.get('plantTypeName_sci_other_en').setValue(plantTypeName_sci_other_en);
          this.PlantForm.get('plantTypeName_sci_th').setValue(plantTypeName_sci_th);
          this.PlantForm.get('plantTypeName_sci_global_th').setValue(plantTypeName_sci_global_th);
          this.PlantForm.get('plantTypeName_sci_other_th').setValue(plantTypeName_sci_other_th);
          this.PlantForm.get('plant_category').setValue(plant_category);
          this.PlantForm.get('plant_character').setValue(plant_character);
          this.PlantForm.get('plant_group').setValue(plant_group);
          this.PlantForm.get('plant_group_type').setValue(plant_group_type);
          this.PlantForm.get('plant_status').setValue(plant_status);
          this.loading = false;
        },
        error => {
          this.loading = false;
          this.apiError = true;
          this.alertService.error(error);
        });
  }

  onCancel() {
    this.router.navigate(['/admin/master/plant/listing']);
  }
}
