import {Component, OnInit} from '@angular/core';
import {DataTableParams, DataTableResource} from 'ngx-datatable-bootstrap4';
import {NgbModal} from '@ng-bootstrap/ng-bootstrap';
import {ActivatedRoute, Router} from '@angular/router';
import {first} from 'rxjs/operators';
import {
  AlertService,
  AttributeListService,
  PassportService,
  PlantRegisterService,
  VolumeService
} from '../../../../services';
import * as moment from 'moment';
import {toLower} from 'lodash';

@Component({
  selector: 'app-listing-plant-group-page',
  templateUrl: './listing.component.html',
})
export class NgbdListingRegisterPlant12Component implements OnInit {
  roleResources = new DataTableResource<any>([]);
  items: any[] = [];
  tempItems: any[] = [];
  count = 0;
  volumeId = null;
  loading = false;
  volumeInfo = null;
  momentObj: any;
  passportInfo = null;
  currentStatus = 'draft';

  selectedPlantsForSubmit = [];

  constructor(private modalService: NgbModal,
              private activeRoute: ActivatedRoute,
              private passportService: PassportService,
              private plantRegisterService: PlantRegisterService,
              private attributeListService: AttributeListService,
              private volumeService: VolumeService,
              private alertService: AlertService,
              private router: Router) {
    this.momentObj = moment;
  }

  ngOnInit(): void {
    this.activeRoute.params.subscribe(params => {
      const {volume} = params;
      this.volumeId = volume;
      this.onGetVolumeInfo(this.volumeId);
      this.onGetListing('draft');
    });
  }

  reloadData(params: DataTableParams) {
    console.log(params);
    this.roleResources.query(params).then(vals => {
      this.items = vals;
      this.tempItems = this.items;
    });
  }

  onEdit({_id}) {
    this.router.navigate([`/admin/register/plant/${this.volumeId}/${_id}`]);
  }

  onDelete(item: object, deleteContent) {
    this.modalService.open(deleteContent, {centered: true});
  }

  onSearch({target: {value: searchRole = ''} = {}}) {
    this.items = this.tempItems.filter(({passport}) => {
      const {passport_no = null} = passport || {};
      return passport_no && toLower(passport_no).includes(toLower(searchRole));
    });
  }

  onGetVolumeInfo(id) {
    this.loading = true;
    this.volumeService.getSpecific(id)
      .pipe(first())
      .subscribe(
        ({data = []}: any) => {
          this.loading = false;
          const {passport_no} = data;
          this.volumeInfo = data;
          this.onGetPassportInfo(passport_no);
        },
        error => {
          this.loading = false;
          this.alertService.error(error);
        });
  }

  onGetPassportInfo(id) {
    this.loading = true;
    this.passportService.getSpecific(id)
      .pipe(first())
      .subscribe(
        ({data = []}: any) => {
          this.loading = false;
          const {passport_no} = data;
          this.passportInfo = data;
        },
        error => {
          this.loading = false;
          this.alertService.error(error);
        });
  }

  onGetListing(status) {
    this.currentStatus = status;
    this.loading = true;
    this.plantRegisterService.getAll(this.volumeId, 'plant_listing', status)
      .pipe(first())
      .subscribe(
        ({data = []}: any) => {
          this.loading = false;
          this.roleResources = new DataTableResource<any>(data);
          this.roleResources.count().then(count => this.count = count);
          this.reloadData({});
        },
        error => {
          this.loading = false;
          this.alertService.error(error);
        });
  }

  onChangeItemSelect({_id}) {
    if (this.selectedPlantsForSubmit.includes(_id)) {
      this.selectedPlantsForSubmit = this.selectedPlantsForSubmit.filter(id => id !== _id);
      return;
    }

    this.selectedPlantsForSubmit = [...this.selectedPlantsForSubmit, _id];
  }

  onSubmitPlants() {
    if (!this.selectedPlantsForSubmit || !this.selectedPlantsForSubmit.length) {
      return;
    }
    this.loading = true;
    this.plantRegisterService.submitToModule8(this.selectedPlantsForSubmit)
      .pipe(first())
      .subscribe(
        ({data = {}}: any) => {
          this.onGetListing('draft');
          this.selectedPlantsForSubmit = [];
        },
        error => {
          this.loading = false;
          this.alertService.error(error);
        });
  }
}
