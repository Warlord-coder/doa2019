import {Component, OnInit} from '@angular/core';
import {ActivatedRoute, Router} from '@angular/router';
import {FormArray, FormBuilder, FormGroup, Validators} from '@angular/forms';
import {
  AlertService,
  CommonProvinceService,
  CommonConditionService,
  PassportService,
  PlantCategoryService, PlantCharacterService,
  PlantGroupsService,
  PlantGroupTypesService, PlantTypeService,
  VolumeService, WarehouseService
} from '../../../../services';
import {identity, pickBy} from 'lodash';
import {getRoleResources} from '../../../../utils';
import {TreeviewItem} from 'ngx-treeview';
import {first} from 'rxjs/operators';
import {PlantService} from '../../../../services/plant.service';
import {CommonCountryService} from '../../../../services/common-country.service';
import {CommonAumperService} from '../../../../services/common-aumper.service';
import {environment} from '../../../../../environments/environment';
import {NgbModal,ModalDismissReasons} from '@ng-bootstrap/ng-bootstrap';

@Component({
  selector: 'app-add-update-plant-new-page',
  templateUrl: './add-update.component.html',
})
export class NgbdAddUpdateNewPlantComponent implements OnInit {
  plantGroupTypeForm: FormGroup;
  room5Pattern = {
    0: {pattern: new RegExp('\[0\]')},
    1: {pattern: new RegExp('\[1\]')},
    3: {pattern: new RegExp('\[0-3\]')},
    4: {pattern: new RegExp('\[0-3\]')},
    5: {pattern: new RegExp('\[0-4\]')},
    6: {pattern: new RegExp('\[0-6\]')},
    7: {pattern: new RegExp('\[L\R]')},
  };
  room10Pattern = {
    0: {pattern: new RegExp('\[0\]')},
    1: {pattern: new RegExp('\[1-8\]')},
    3: {pattern: new RegExp('\[0\]')},
    4: {pattern: new RegExp('\[1-5\]')},
    5: {pattern: new RegExp('\[0\]')},
    6: {pattern: new RegExp('\[1-8\]')},
    8: {pattern: new RegExp('\[0\]')},
    9: {pattern: new RegExp('\[1-4\]')},
    7: {pattern: new RegExp('\[L\R]')},
  };
  submitted = false;
  loading = false;
  file = null;
  id = null;
  categoryList = [];
  groupTypeList = [];
  groupList = [];
  provinceList = [];
  aumperList = [];
  countryList = [];
  volumeInfo = null;
  passportInfo = null;
  gs_no = null;
  gs_family = null;
  gs_familly_id = null;
  apiError = false;
  plantsListing = [];
  reg_character_attached_file = null;
  regCharacterAttachedFile = null;
  wh_sample_ref_molecular_file = null;
  wh_sample_ref_seed_file = null;
  seedAttachedFile = null;
  molecularAttachedFile = null;
  reg_seed_no = null;
  reg_gene_id = null;
  submitTo = null;
  disabled = true;
  labInfo: any = {};
  plantTypes = [];
  selectedPlant = null;
  options = [];
  formData: any = {};
  selectedCheckbox = {
    room5: [],
    room10: []
  };
  volumeId = null;
  roleResources: TreeviewItem[];
  config = {
    class: 'autocomplete', max: 5, placeholder: 'Type to search',
    sourceField: ['plantTypeName_sci_en']
  };
  conditionDataYear = [];
  conditionDataPercent = [];
  conditionDataWeight = [];
  conditionData = {}
  conditionIndex = null;
  conditionKey = '';
  conditionYear = '';
  conditionWeight = '';
  conditionPercent = '';
  textRoom = {'room5':[],'room10':[]};
  constructor(private router: Router,
              private formBuilder: FormBuilder,
              private activeRoute: ActivatedRoute,
              private plantCategoryService: PlantCategoryService,
              private plantGroupTypesService: PlantGroupTypesService,
              private plantGroupsService: PlantGroupsService,
              private plantCharacterService: PlantCharacterService,
              private volumeService: VolumeService,
              private conditionService: CommonConditionService,
              private passportService: PassportService,
              private wareHouseService: WarehouseService,
              private plantTypeService: PlantTypeService,
              private countryService: CommonCountryService,
              private provinceService: CommonProvinceService,
              private aumperService: CommonAumperService,
              private modalService: NgbModal,
              private alertService: AlertService) {
    this.onGetListing('plantCharacterService', 'plantsListing');
    this.onGetListing('plantCategoryService', 'categoryList');
    this.onGetListing('plantGroupTypesService', 'groupTypeList');
    this.onGetListing('plantGroupsService', 'groupList');
    this.onGetListing('countryService', 'countryList');
    this.onGetListing('aumperService', 'aumperList');
    this.onGetListing('provinceService', 'provinceList');
    this.onGetListingConditon('conditionService', 'conditionData');
  }

  ngOnInit() {
    this.roleResources = getRoleResources();
    this.plantGroupTypeForm = this.formBuilder.group({
      plant_gene_name: ['', Validators.required],
      reg_gene_type: [''],
      reg_plant_alter_name: [[]],
      reg_plant_sci_name: [''],
      reg_gene_category: ['', Validators.required],
      reg_seed_collect_date: [''],
      selfing_input: [''],
      selfing_dropdown: [''],
      oop_input: [''],
      oop_dropdown: [''],
      other_input: [''],
      other_dropdown: [''],
      department_type: [''],
      department_sub_type: [''],
      department_name: [''],
      source_country: [''],
      source_country_1: [''],
      source_province: ['', Validators.required],
      source_district: ['', Validators.required],
      source_address: ['', Validators.required],
      lat: [''],
      long: [''],
      source_by_department: [''],
      source_sea_level: [''],
      reg_seed_collect_person: ['', Validators.required],
      reg_seed_owner: ['', Validators.required],
      reg_seed_sender: ['', Validators.required],
      reg_character_brief: [''],
      reg_character_for_record: [''],
      reg_sent_lab_date: ['', Validators.required],
      reg_remark: ['', Validators.required],
      selectedBatchRoom: [''],
      room5: this.formBuilder.array([]),
      room10: this.formBuilder.array([]),
      wh_sample_ref_seed_check_box: [false],
      wh_sample_ref_molecular_checkbox: [false],
      wh_create_info_checkbox: [false]
    });
    this.activeRoute.params.subscribe(params => {
      const {id} = params;
      this.id = id;
      if (id && id !== 'create') {
        this.onGetWarehousePlantInfo(id);
      }
    });
  }

  get f() {
    if (!this.apiError && this.submitted && !this.plantGroupTypeForm.invalid) {
      this.alertService.error(null);
    }

    return this.plantGroupTypeForm.controls;
  }

  onSubmit(apiCall = true) {
    this.submitted = false;
    if (this.plantGroupTypeForm.invalid) {
      // this.alertService.error('Some fields are required. Please fill those fields');
      // return;
    }

    let {room5, room10, reg_plant_alter_name = [], ...other} = this.plantGroupTypeForm.value;

    room5 = room5.filter(({batch_no}, index) => this.selectedCheckbox.room5.includes(index));
    room10 = room10.filter(({batch_no}, index) => this.selectedCheckbox.room10.includes(index));
    // room5 = room5.filter(({batch_id}, index) => this.selectedCheckbox.room5.includes(index));
    // room10 = room10.filter(({batch_id}, index) => this.selectedCheckbox.room10.includes(index));
    for (let index = 0; index < room5.length; index++) {
      room5[index].notification_period = {
        year:room5[index].notification_year,
        percent:room5[index].notification_percent,
        weight:room5[index].notification_weight
      }
      
    }
    for (let index = 0; index < room10.length; index++) {
      room10[index].notification_period = {
        year:room10[index].notification_year,
        percent:room10[index].notification_percent,
        weight:room10[index].notification_weight
      }
      
    }
    this.wareHouseService.createOrUpdate(pickBy({
      ...other,
      room5: room5 ? JSON.stringify(room5) : '',
      room10: room10 ? JSON.stringify(room10) : '',
      file: [this.molecularAttachedFile, this.seedAttachedFile],
      isWhSampleRefSeedUpload: Boolean(this.seedAttachedFile),
      isWhSampleRefMolecularUpload: Boolean(this.molecularAttachedFile),
      reg_plant_alter_name: reg_plant_alter_name ? JSON.stringify(reg_plant_alter_name) : [],
      reg_plant_sci_name: this.selectedPlant ? this.selectedPlant._id : null,
    }, identity), this.id)
      .pipe(first())
      .subscribe(
        () => {
          if (apiCall) {
            this.alertService.success('Data Saved Successfully');
            this.onGetWarehousePlantInfo(this.id, false);
            setTimeout(() => {
              this.alertService.reset();
            }, 1500);
          }
        },
        error => {
          this.loading = false;
          this.apiError = true;
          this.alertService.error(error);
        });
  }

  onSelectedChange($event) {
    console.log('Select', $event);
  }

  onFilterChange($event) {
    console.log('Filter Change', $event);
  }

  onGetListing(serviceName, key) {
    this.loading = true;
    this[serviceName].getAll()
      .pipe(first())
      .subscribe(
        ({data = []}: any) => {
          this.loading = false;
          this[key] = data;
        },
        error => {
          this.loading = false;
          this.alertService.error(error);
        });
  }

  onGetListingConditon(serviceName, key) {
    // this.loading = true;
    this[serviceName].getAll()
      .pipe(first())
      .subscribe(
        ({ data = [] }: any) => {
          // this.loading = false;
          for (let index = 0; index < data.length; index++) {
            const element = data[index];
            if (data[index].type == 'germination_percent' && data[index].conditionAt == 'module_8') {
              this.conditionDataPercent.push(data[index]);
            } else if (data[index].type == 'weight' && data[index].conditionAt == 'module_8') {
              this.conditionDataWeight.push(data[index]);
            } else if (data[index].type == 'year' && data[index].conditionAt == 'module_8') {
              this.conditionDataYear.push(data[index]);
            }
          }
        },
        error => {
          this.loading = false;
          this.alertService.error(error);
        });
  }

  onGetWarehousePlantInfo(id, reset = true) {
    this.loading = true;

    this.wareHouseService.getSpecific(id)
      .pipe(first())
      .subscribe(
        ({data = []}: any) => {
          const {
            plant_gene_name, reg_gene_id, reg_seed_no, gs_no,
            reg_plant_sci_name, reg_character_attached_file,
            wh_sample_ref_molecular_file, wh_sample_ref_seed_file,
            volume, m8_lab_id, room5 = [], room10 = [],
            wh_status, submitTo
          } = data;
          if(plant_gene_name!=undefined && plant_gene_name!= null)
            this['plantTypeService'].getSpecific(plant_gene_name) 
            .pipe(first())
            .subscribe(
              ({data = []}: any) => {
                this.plantTypes.push(data )
                this.loading = false;
              },
              error => {
                this.loading = false;
                this.alertService.error(error);
              });
          this.formData = data;
          this.disabled = wh_status === 'complete';
          this.labInfo = m8_lab_id;
          this.submitTo = submitTo;
          this.onGetVolumeInfo(volume);
          Object.keys(data).forEach(key => {
            if (this.plantGroupTypeForm.get(key) && !['room5', 'room10'].includes(key)) {
              if (this.disabled) {
                this.plantGroupTypeForm.get(key).disable({onlySelf: true});
                this.plantGroupTypeForm.get(key).setValue(data[key]);
              } else {
                this.plantGroupTypeForm.get(key).setValue(data[key]);
              }
            }
          });
          this.reg_gene_id = reg_gene_id;
          this.gs_no = gs_no;
          this.reg_seed_no = reg_seed_no;
          this.reg_character_attached_file = reg_character_attached_file;
          this.wh_sample_ref_molecular_file = wh_sample_ref_molecular_file;
          this.wh_sample_ref_seed_file = wh_sample_ref_seed_file;
          this.molecularAttachedFile = this.seedAttachedFile = null;
          this.findPlantSciName(data);
          if (reset) {
            this.selectedCheckbox.room5 = [
              room5[0] ? 0 : false,
              room5[1] ? 1 : false,
              room5[2] ? 2 : false,
              room5[3] ? 3 : false,
              room5[4] ? 4 : false];
            this.addItem(room5[0] || data, 'room5', 1, !room5[0]);
            this.addItem(room5[1] || {}, 'room5', 2, !room5[1]);
            this.addItem(room5[2] || {}, 'room5', 3, !room5[2]);
            this.addItem(room5[3] || {}, 'room5', 4, !room5[3]);
            this.addItem(room5[4] || {}, 'room5', 5, !room5[4]);
            this.displayCondition(0,'room5')
            this.displayCondition(1,'room5')
            this.displayCondition(2,'room5')
            this.displayCondition(3,'room5')
            this.displayCondition(4,'room5')

            this.selectedCheckbox.room10 = [
              room10[0] ? 0 : false,
              room10[1] ? 1 : false,
              room10[2] ? 2 : false,
              room10[3] ? 3 : false,
              room10[4] ? 4 : false];

            this.addItem(room10[0] || data, 'room10', 1, !room10[0]);
            this.addItem(room10[1] || {}, 'room10', 2, !room10[1]);
            this.addItem(room10[2] || {}, 'room10', 3, !room10[2]);
            this.addItem(room10[3] || {}, 'room10', 4, !room10[3]);
            this.addItem(room10[4] || {}, 'room10', 5, !room10[4]);
            this.displayCondition(0,'room10')
            this.displayCondition(1,'room10')
            this.displayCondition(2,'room10')
            this.displayCondition(3,'room10')
            this.displayCondition(4,'room10')
          }
          if(this.gs_no == undefined || this.gs_no == ''){
            this.wareHouseService.getList()
            .pipe(first())
            .subscribe(
              ({data = []}: any) => {
                this.wareHouseService.get_id_by_reg_gene_id(this.reg_gene_id)
                .pipe(first())
                .subscribe(
                  ({data = []}: any) => {
                    let {_id} = data
                    this.gs_familly_id = _id
                    if(this.gs_familly_id != undefined){
                      this.gs_family = true;
                      this.wareHouseService.getSpecific(this.gs_familly_id)
                      .pipe(first())
                      .subscribe(
                        ({data = []}: any) => {
                          const {
                            gs_no, submitTo
                          } = data;
                          this.gs_no = gs_no;
                          this.submitTo = submitTo;
                        },
                        error => {
                          this.loading = false;
                          this.alertService.error(error);
                        })
                    }
                  },
                  error => {
                    this.loading = false;
                    this.alertService.error(error);
                  });                           
              },
              error => {
                this.loading = false;
                this.alertService.error(error);
              });

          }          
          this.loading = false;
        },
        error => {
          this.loading = false;
          this.alertService.error(error);
        });
  }

  openModal(content, event, key, index) {
    try {
      let d = this.plantGroupTypeForm.get(key).value[index].notification_period;
      this.conditionYear = (d.year != null ? d.year : '');
      this.conditionPercent = (d.percent != null ? d.percent : '');
      this.conditionWeight = (d.weight != null ? d.weight : '');
      this.conditionKey = key;
      this.conditionIndex = index;
      event.srcElement.blur();
      event.preventDefault();
      this.modalService.open(content, { centered: true }).result.then((result) => {
        //this.closeResult = `Closed with: ${result}`;
      }, (reason) => {
        //this.closeResult = `Dismissed ${this.getDismissReason(reason)}`;
      });
    } catch (error) {
    }
  }

  addConditionNoti(content) {
    this.conditionData[this.conditionIndex] = {
      year: this.conditionYear,
      percent: this.conditionPercent,
      weight: this.conditionWeight
    }
    setTimeout(() => {
      this.plantGroupTypeForm.get(this.conditionKey).value[this.conditionIndex].notification_period = this.conditionData[this.conditionIndex];
    }, 800);
    setTimeout(() => {
      this.displayCondition(this.conditionIndex,'room5');
    }, 1000);
    this.modalService.dismissAll(content);
   
  }
  displayCondition(index,room){
  
    try {
      let d = this.plantGroupTypeForm.get(room).value[index].notification_period;
      let txt = '';
      if(d!=null){
        if(d.year != null &&  d.year != ''){
          txt += d.year+' ';
        }
        if(d.percent != null &&  d.percent != ''){
          txt += ','+d.percent+' ';
        }
        if(d.weight != null &&  d.weight != ''){
          txt += ','+d.weight+' ';
        }
        if(txt.length != 0){
          this.textRoom[room][index] = txt
        }else{
          this.textRoom[room][index] = '-'
        }
      }
      
      
    } catch (error) {
      this.textRoom[room][index] = '-'
    }
    
  }

  findPlantSciName({reg_plant_sci_name}) {
    if (!reg_plant_sci_name) {
      return;
    }
    for (const plant of  this.plantsListing) {
      if (reg_plant_sci_name === plant._id) {
        this.selectedPlant = plant;
        this.plantGroupTypeForm.get('reg_plant_sci_name').setValue(plant.genus + ' ' + plant.species);
      }
    }
  }

  onSelect(plant) {
    this.selectedPlant = plant;
    this.plantGroupTypeForm.get('reg_plant_sci_name').setValue(plant.genus + ' ' + plant.species);
  }

  onClear() {
    this.selectedPlant = null;
  }

  onGetVolumeInfo(id) {
    this.loading = true;
    this.volumeService.getSpecific(id)
      .pipe(first())
      .subscribe(
        ({data = []}: any) => {
          this.loading = false;
          const {passport_no} = data;
          this.volumeInfo = data;
          this.onGetPassportInfo(passport_no);
        },
        error => {
          this.loading = false;
          this.alertService.error(error);
        });
  }

  onGetPassportInfo(id) {
    this.loading = true;
    this.passportService.getSpecific(id)
      .pipe(first())
      .subscribe(
        ({data = []}: any) => {
          this.loading = false;
          const {passport_no} = data;
          this.passportInfo = data;
        },
        error => {
          this.loading = false;
          this.alertService.error(error);
        });
  }

  getAttachment(basePath, name) {
    if (name) {
      return `${environment.endpoint}/${basePath}/${name}`;
    }

    return null;
  }

  onFileUpload({srcElement: {files = []} = {}}, key = 'regCharacterAttachedFile') {
    this[key] = files[0];
  }

  findItemPlantListing = (dbVal) => {
    for (const item of this.plantsListing) {
      if (item._id === dbVal) {
        this.selectedPlant = item;
        $('#plantSciName').val(item.plantTypeName_sci_en);
        return;
      }
    }
  };

  createOptionValue(data: any, index = 1, disabled = true): FormGroup {
    const {
      lab_10seed_weight = '', lab_test_growth_percent = '',
      lab_test_growth_date = '', lab_test_plant_amount = '',
      lab_test_moisture_date = '', wh_batch_store_date = '',
      notification_period = {}, wh_batch_weight = '',
      wh_batch_store_location = '', wh_batch_remark = '',
      notification_year = '',notification_percent = '',notification_weight = ''
    } = data || {};

    return this.formBuilder.group({
      batch_no: [index],
      // batch_id: [index],
      lab_10seed_weight: [{value: lab_10seed_weight, disabled}, Validators.required],
      lab_test_growth_percent: [{value: lab_test_growth_percent, disabled}, Validators.required],
      lab_test_growth_date: [{value: lab_test_growth_date, disabled}, Validators.required],
      lab_test_plant_amount: [{value: lab_test_plant_amount, disabled}, Validators.required],
      lab_test_moisture_date: [{value: lab_test_moisture_date, disabled}, Validators.required],
      wh_batch_store_date: [{value: wh_batch_store_date, disabled}, Validators.required],
      notification_period: [{value: notification_period, disabled}, Validators.required],
      notification_year:[{value: notification_year, disabled}],
      notification_percent:[{value: notification_percent, disabled}],
      notification_weight:[{value: notification_weight, disabled}],
      wh_batch_weight: [{value: wh_batch_weight, disabled}, Validators.required],
      wh_batch_store_location: [{value: wh_batch_store_location, disabled}, Validators.required],
      wh_batch_remark: [{value: wh_batch_remark, disabled}, Validators.required]
    });
  }

  addItem(data = {}, key, index, disabled = true): void {
    (this.plantGroupTypeForm.get(key) as FormArray).push(this.createOptionValue(data || {}, index, disabled));
  }

  onCancel() {
    const {_id = ''} = this.labInfo || {};
    this.router.navigate([`admin/warehouse/pending-complete/listing/${_id}`]);
  }
  findInvalidControls() {
    const invalid = [];
    const controls = this.plantGroupTypeForm.controls;
    for (const name in controls) {
        if (controls[name].invalid) {
            invalid.push(name);
        }
    }
    return invalid;
}
  onSubmitModule(modelRef = false) {
    this.loading = !modelRef;
    this.submitted = true;
    this.modalService.dismissAll();
    if (this.plantGroupTypeForm.invalid) {
      this.alertService.error('Some fields are required. Please fill those fields');
      this.loading = false;
      return;
    }

    if (this.selectedCheckbox.room5.every((data) => data === false) && this.selectedCheckbox.room10.every((data) => data === false)) {
      this.alertService.error('Please fill atleast one batch of rooms to continue');
      this.loading = false;
      return;
    }


    this.onSubmit(false);
    if (!this.f.selfing_input && !this.f.oop_input && !this.f.other_input) {
      this.alertService.error('Please Select one from Stock (Selfing, OOP, Other');
      this.loading = false;
      return;
    }

    if (modelRef) {
      this.options = [];
      this.modalService.open(modelRef, {centered: true});

      return;
    }

    let symbol = '';
    for (const category of this.categoryList) {
      if(this.selectedPlant != undefined)
      if (category._id === this.selectedPlant.plant_category) {
        symbol = category.symbol;
      }
    }

    this.wareHouseService.submitToModule(this.id, {submitTo: this.submitTo, gs_no: this.gs_no, symbol})
      .pipe(first())
      .subscribe(
        ({data = []}: any) => {
          this.loading = false;
          const {_id = ''} = this.labInfo || {};
          this.router.navigate([`admin/warehouse/pending-complete/listing/${_id}`]);
        },
        error => {
          this.loading = false;
          this.alertService.error(error);
        });
  }

  onChangeItemSelect(key, index) {
    if (this.selectedCheckbox[key].includes(index)) {
      // @ts-ignore
      this.plantGroupTypeForm.get(key).controls[index].disable();

      return this.selectedCheckbox[key] = this.selectedCheckbox[key].filter(val => val !== index);
    }
    // @ts-ignore
    this.plantGroupTypeForm.get(key).controls[index].enable();
    this.selectedCheckbox[key] = [...this.selectedCheckbox[key], index];
  }

  getSubmitToInfo() {
    if (this.submitTo === 'm2') {
      return 'Module 2';
    }
    if (this.submitTo === 'm9') {
      return 'Module 9';
    }
    if (this.submitTo === 'm4') {
      return 'Module 4';
    }
  }

  getBatchRooms() {
    if (this.options && this.options.length) {
      return this.options;
    }

    const {room5, room10} = this.plantGroupTypeForm.value;

    room5.map(({batch_no}, index) => {
    // room5.map(({batch_id}, index) => {
      if (this.selectedCheckbox.room5.includes(index)) {
        this.options.push(`Room 5 - Batch ${batch_no}`);
        // this.options.push(`Room 5 - Batch ${batch_id}`);
      }
    });
    room10.map(({batch_no}, index) => {
    // room10.map(({batch_id}, index) => {
      if (this.selectedCheckbox.room10.includes(index)) {
        this.options.push(`Room 10 - Batch ${batch_no}`);
        // this.options.push(`Room 10 - Batch ${batch_id}`);
      }
    });

    return this.options;
  }
}
