import { Component, OnInit } from '@angular/core';
import { DataTableParams, DataTableResource } from 'ngx-datatable-bootstrap4';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { ActivatedRoute, Router } from '@angular/router';
import { toLower } from 'lodash';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
// @ts-ignore
import { AlertService, DistributionService, PlantCategoryService } from '../../../../services';
import { first } from 'rxjs/operators';
import * as moment from 'moment';

@Component({
  selector: 'app-listing-common-country-page',
  templateUrl: './listing.component.html',
})
export class NgbdRegisterDistributionListPendingComponent implements OnInit {
  resources = new DataTableResource<any>([]);
  items: any[] = [];
  count = 0;
  registerPassportForm: FormGroup;
  tempItems: any[] = [];
  submitted = false;
  id = null;
  momentObj: any;
  file = null;
  distributionInfo = null;
  loading = false;
  selectedPlantsForSubmit: any = [];
  apiError = false;
  categoryList = [];

  constructor(private modalService: NgbModal,
    private formBuilder: FormBuilder,
    private activeRoute: ActivatedRoute,
    private alertService: AlertService,
    private distributionService: DistributionService,
    private plantCategoryService: PlantCategoryService,
    private router: Router) {
    this.onGetListing('plantCategoryService', 'categoryList');
    this.onGetRecoveryListing();
    this.momentObj = moment;
  }

  ngOnInit() {
    this.registerPassportForm = this.formBuilder.group({
      revival_plant: ['', Validators.required],
      date_of_submission: ['', Validators.required],
      place: ['', [Validators.required]]
    });
    this.activeRoute.params.subscribe(params => {
      const { id } = params;
      this.id = id;
      this.onGetSpecificRecovery();
    });
  }

  get f() {
    if (!this.apiError && this.submitted && !this.registerPassportForm.invalid) {
      this.alertService.error(null);
    }

    return this.registerPassportForm.controls;
  }

  reloadData(params: DataTableParams) {
    this.resources.query(params).then(vals => {
      this.items = vals;
      this.tempItems = this.items;
    });
  }

  onSearch({ target: { value: searchRole = '' } = {} }) {
    this.items = this.tempItems.filter(({ categoryName_en}) => categoryName_en && toLower(categoryName_en).includes(toLower(searchRole)));
  }
  onSearch1({ target: { value: searchRole = '' } = {} }) {
    this.items = this.tempItems.filter(({ plant_gene_name: { plantTypeName_en = '' } = {} }) => plantTypeName_en && toLower(plantTypeName_en).includes(toLower(searchRole)));
  }
  onSearch2({ target: { value: searchRole = '' } = {} }) {
    this.items = this.tempItems.filter(({ reg_plant_sci_name: { genus = '' } = {} }) => genus && toLower(genus).includes(toLower(searchRole)));
  }
  onSearch3({ target: { value: searchRole = '' } = {} }) {
    this.items = this.tempItems.filter(({ gs_no  }) => gs_no && toLower(gs_no).includes(toLower(searchRole)));
  }
  onSearch4({ target: { value: searchRole = '' } = {} }) {
    this.items = this.tempItems.filter(({ source_province: { provinceName_en = '' } = {} }) => provinceName_en && toLower(provinceName_en).includes(toLower(searchRole)));
  }


  onGetRecoveryListing() {
    this.loading = true;
    this.distributionService.getAllPending()
      .pipe(first())
      .subscribe(
        ({ data = [] }: any) => {
          let that = this;
          data.forEach(function (value) {
            that.categoryList.forEach(function (category) {
              if (value.reg_plant_sci_name != null)
                if (value.reg_plant_sci_name.plant_category == category._id) {
                  value.categoryName_en = category.categoryName_en;
                }
            });
            const { room10 = [], room5 = [] } = value;
            for (const room of room5) {
              const { wh_batch_store_date } = room;
              value.wh_batch_store_date = wh_batch_store_date;
            }
            for (const room of room10) {
              const { wh_batch_store_date } = room;
              value.wh_batch_store_date = wh_batch_store_date;
            }
          });

          this.resources = new DataTableResource<any>(data);
          this.resources.count().then(count => this.count = count);
          this.reloadData({});
          this.loading = false;
        },
        error => {
          this.loading = false;
          this.alertService.error(error);
        });
  }

  onGetSpecificRecovery() {
    this.loading = true;
    this.distributionService.getSpecific(this.id)
      .pipe(first())
      .subscribe(
        ({ data = [] }: any) => {
          this.distributionInfo = data;
          this.loading = false;
        },
        error => {
          this.loading = false;
          this.alertService.error(error);
        });
  }

  onChangeItemSelect({ _id }) {
    if (this.selectedPlantsForSubmit.includes(_id)) {
      this.selectedPlantsForSubmit = this.selectedPlantsForSubmit.filter(id => id !== _id);
      return;
    }

    this.selectedPlantsForSubmit = [...this.selectedPlantsForSubmit, _id];
  }

  onSubmitPlants() {
    if (!this.selectedPlantsForSubmit || !this.selectedPlantsForSubmit.length) {
      return;
    }
    this.loading = true;
    this.distributionService.submitToDistribution(this.id, this.selectedPlantsForSubmit)
      .pipe(first())
      .subscribe(
        ({ data = {} }: any) => {
          this.selectedPlantsForSubmit = [];
          this.router.navigate([`/admin/distribution/listing`]);
        },
        error => {
          this.loading = false;
          this.alertService.error(error);
        });
  }

  getStockInfo(item) {
    const {
      selfing_input, selfing_dropdown,
      oop_input, oop_dropdown,
      other_input, other_dropdown
    } = item;
    const stock = {
      isSelfing: selfing_input && selfing_dropdown,
      isOOP: oop_input && oop_dropdown,
      isOther: other_input && other_dropdown,
    };
    if (stock.isSelfing) {
      return `Self ${selfing_input} ${selfing_dropdown}`;
    }
    if (stock.isOOP) {
      return `OP ${oop_input} ${oop_dropdown}`;
    }
    if (stock.isOther) {
      return `Other ${other_input} ${other_dropdown}`;

    }
  }

  onGetListing(serviceName, key) {
    this.loading = true;
    this[serviceName].getAll()
      .pipe(first())
      .subscribe(
        ({ data = [] }: any) => {
          this.loading = false;
          this[key] = data;
        },
        error => {
          this.loading = false;
          this.alertService.error(error);
        });
  }

}
