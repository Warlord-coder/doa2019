import {Component, OnInit} from '@angular/core';
import {DataTableParams, DataTableResource} from 'ngx-datatable-bootstrap4';
import {NgbModal} from '@ng-bootstrap/ng-bootstrap';
import {ActivatedRoute, Router} from '@angular/router';
import {toLower} from 'lodash';
import {FormBuilder, FormGroup, Validators} from '@angular/forms';
// @ts-ignore
import {AlertService, DistributionService, WarehouseService} from '../../../../services';
import {first} from 'rxjs/operators';
import * as moment from 'moment';

@Component({
  selector: 'app-listing-common-country-page',
  templateUrl: './listing.component.html',
})
export class NgbdRegisterDistributionPlantsComponent implements OnInit {
  resources = new DataTableResource<any>([]);
  resources1 = new DataTableResource<any>([]);
  resources2 = new DataTableResource<any>([]);
  items: any[] = [];
  items1: any[] = [];
  items2: any[] = [];
  count = 0;
  count1 = 0;
  count2 = 0;
  registerPassportForm: FormGroup;
  tempItems: any[] = [];
  tempItems1: any[] = [];
  tempItems2: any[] = [];
  submitted = false;
  id = null;
  selectedId = null;
  momentObj: any;
  file = null;
  distributionInfo = null;
  loading = false;
  currentStatus = 'draft';
  selectedPlantsForSubmit: any = [];
  apiError = false;
  withdraw_stock_list = null;

  constructor(private modalService: NgbModal,
              private formBuilder: FormBuilder,
              private activeRoute: ActivatedRoute,
              private alertService: AlertService,
              private wareHouseService: WarehouseService,
              private distributionServicec: DistributionService,
              private router: Router) {
    this.momentObj = moment;
  }

  ngOnInit() {
    this.registerPassportForm = this.formBuilder.group({
      distribution_remark: [''],
      distribution_doc_MTA: [''],
      distribution_summary_stock: [''],
      distribution_submit_plant_doc: [''],
      distribution_summit_plant_date: [''],
      distribution_doc_request_date: [''],
      distribution_doc_request: [''],
      distribution_doc_receive_date: [''],
      distribution_approve_date: [''],
      distribution_approve_doc: [''],
      distribution_summary_stock_select: ['กรัม/Gram']
    });
    this.activeRoute.params.subscribe(params => {
      const {id} = params;
      this.id = id;
      this.onGetSpecificWithDraw();
      this.onGetWithDrawPlantListing('draft');
      this.onGetWithDrawPlantListing('pending');
      this.onGetWithDrawPlantListing('complete');
      this.onChangeStatus('draft');
    });
  }

  get f() {
    if (!this.apiError && this.submitted && !this.registerPassportForm.invalid) {
      this.alertService.error(null);
    }

    return this.registerPassportForm.controls;
  }

  reloadData(params: DataTableParams) {
    this.resources.query(params).then(vals => {
      this.items = vals;
      this.tempItems = this.items;
    });
  }
  reloadData1(params: DataTableParams) {
    this.resources1.query(params).then(vals => {
      this.items1 = vals;
      this.tempItems1 = this.items;
    });
  }
  reloadData2(params: DataTableParams) {
    this.resources2.query(params).then(vals => {
      this.items2 = vals;
      this.tempItems2 = this.items;
    });
  }
  onSearch({target: {value: searchRole = ''} = {}}) {
    this.items = this.tempItems.filter(({passport: {passport_no = ''} = {}}) => passport_no && toLower(passport_no).includes(toLower(searchRole)));
  }

  // onGetWithDrawPlantListing(status) {
  //   this.loading = true;
  //   this.currentStatus = status;
  //   this.selectedPlantsForSubmit = [];
  //   this.distributionServicec.getAllPlants(this.id, status)
  //     .pipe(first())
  //     .subscribe(
  //       ({data = []}: any) => {
  //         this.resources = new DataTableResource<any>(data);
  //         this.resources.count().then(count => this.count = count);
  //         this.reloadData({});
  //         this.loading = false;
  //       },
  //       error => {
  //         this.loading = false;
  //         this.alertService.error(error);
  //       });
  // }

  onGetWithDrawPlantListing(status) {
    this.loading = true;
    this.currentStatus = status;
    this.selectedPlantsForSubmit = [];
    this.distributionServicec.getAllPlants(this.id, status)
      .pipe(first())
      .subscribe(
        ({data = []}: any) => {          
          data.forEach(warehousedata => {
            this.wareHouseService.getSpecific(warehousedata.m9plant_warehouse_id , null , true)
            .pipe(first())
            .subscribe(
              ({data = []}: any) => {
                warehousedata.passport = data.passport;
                warehousedata.reg_gene_id = data.reg_gene_id;
                warehousedata.volume = data.volume;
                if(data.oop_input){
                  warehousedata.oop_input = data.oop_input;
                  warehousedata.oop_dropdown = data.oop_dropdown;
                }
                if(data.other_input){
                  warehousedata.other_input = data.other_input;
                  warehousedata.other_dropdown = data.other_dropdown;
                }
                if(data.selfing_input){
                  warehousedata.selfing_input = data.selfing_input;
                  warehousedata.selfing_dropdown = data.selfing_dropdown;
                }
                warehousedata.gs_no = data.gs_no;
                this.withdraw_stock_list = null
                if(data.room5.length)
                {
                  data.room5.forEach(element => {
                    if(element)
                    this.withdraw_stock_list=  element.wh_batch_weight;
                  });
                }
                if(data.room10.length)
                {
                  data.room10.forEach(element => {
                    if(element)
                      this.withdraw_stock_list=element.wh_batch_weight;
                  });
                }
                if(data.room130.length)
                {
                  data.room130.forEach(element => {
                    if(element)
                      this.withdraw_stock_list= element.storage_weight;
                  });
                }
                if(data.anteArray.length)
                {
                  data.anteArray.forEach(element => {
                    if(element)
                      this.withdraw_stock_list= element.storage_weight;
                  });
                }          
                warehousedata.withdraw_stock_list = this.withdraw_stock_list;
                },
                error => {
                  this.loading = false;
                  this.alertService.error(error);
                });
            }); 
            if(status === "draft") {
              this.resources = new DataTableResource<any>(data);
              this.resources.count().then(count => this.count = count);
              this.reloadData({});
            } else if (status === "complete" ) {
              this.resources2 = new DataTableResource<any>(data);
              this.resources2.count().then(count => this.count2 = count);
              this.reloadData2({});
            } else {
              this.resources1 = new DataTableResource<any>(data);
              this.resources1.count().then(count => this.count1 = count);
              this.reloadData1({});
            }
          this.loading = false;
        },
        error => {
          this.loading = false;
          this.alertService.error(error);
        });
  }

  onChangeStatus(status) {
    this.currentStatus = status;
  }
  onGetSpecificWithDraw() {
    this.loading = true;
    this.distributionServicec.getSpecific(this.id)
      .pipe(first())
      .subscribe(
        ({data = []}: any) => {
          this.distributionInfo = data;
          this.loading = false;
        },
        error => {
          this.loading = false;
          this.alertService.error(error);
        });
  }

  onChangeItemSelect({_id}) {
    if (this.selectedPlantsForSubmit.includes(_id)) {
      this.selectedPlantsForSubmit = this.selectedPlantsForSubmit.filter(id => id !== _id);
      return;
    }

    this.selectedPlantsForSubmit = [...this.selectedPlantsForSubmit, _id];
  }

  onSubmitPlants(status, index = 1, createContent) {
    if ((!this.selectedPlantsForSubmit || !this.selectedPlantsForSubmit.length) && index !== 2) {
      return;
    }
    if (index === 2) {
      for (const item of this.items) {
        if (!item.admin_amount) {
          this.alertService.error('Amount is missing for ' + item.reg_gene_id);
          return;
        }
      }

      this.modalService.open(createContent, {centered: true});

      return;
    }

    this.loading = true;
    this.distributionServicec.changedistributionStatus(status, this.selectedPlantsForSubmit)
      .pipe(first())
      .subscribe(
        ({data = {}}: any) => {
          this.selectedPlantsForSubmit = [];
          this.onGetWithDrawPlantListing(this.currentStatus);
        },
        error => {
          this.loading = false;
          this.alertService.error(error);
        });
  }

  getStockInfo(item) {
    const {
      selfing_input, selfing_dropdown,
      oop_input, oop_dropdown,
      other_input, other_dropdown
    } = item;
    const stock = {
      isSelfing: selfing_input && selfing_dropdown,
      isOOP: oop_input && oop_dropdown,
      isOther: other_input && other_dropdown,
    };
    if (stock.isSelfing) {
      return `Self ${selfing_input} ${selfing_dropdown}`;
    }
    if (stock.isOOP) {
      return `OP ${oop_input} ${oop_dropdown}`;
    }
    if (stock.isOther) {
      return `Other ${other_input} ${other_dropdown}`;

    }
  }

  onDetails({_id, m5_assigned = ''}) {
    this.router.navigate([`/admin/distribution/form-bank/${_id}/${m5_assigned}`]);
  }

  onFileUpload({srcElement: {files = []} = {}}, key) {
    this[`${key}_attached`] = files[0];
  }

  onSubmit() {
    this.loading = true;
    const filePlaces: any = {
      0: 'distribution_approve_doc_attached',
      1: 'distribution_doc_for_getting_service_attached',
      2: 'distribution_doc_53_attached',
      3: 'distribution_doc_citizenID_attached',
      4: 'distribution_doc_research_attached',
      5: 'distribution_doc_teaching_plan_attached',
      6: 'distribution_doc_request_attached',
      7: 'distribution_submit_plant_doc_attached',
      8: 'distribution_doc_MTA_attached'
    };
    // @ts-ignore
    const file = [this.distribution_approve_doc_attached, this.distribution_doc_for_getting_service_attached, this.distribution_doc_53_attached, this.distribution_doc_citizenID_attached, this.distribution_doc_research_attached, this.distribution_doc_teaching_plan_attached, this.distribution_doc_request_attached, this.distribution_submit_plant_doc_attached, this.distribution_doc_MTA_attached];
    const fileLoc = [];
    file.forEach((fileName, index) => {
      if (fileName) {
        fileLoc.push(filePlaces[index]);
      }
    });
    this.modalService.dismissAll();
    if(this.currentStatus == 'pending'){
      this.distributionServicec.completePlants({
        ...this.registerPassportForm.value,
        file,
        fileLoc,
        plants: this.items1.map(({_id}) => _id)
      }, this.id)
        .pipe(first())
        .subscribe(
          ({data = {}}: any) => {
            this.selectedPlantsForSubmit = [];
            this.onGetWithDrawPlantListing(this.currentStatus);
            Object.keys(filePlaces).forEach(key => {
              this[key] = null;
            });
          },
          error => {
            this.loading = false;
            this.alertService.error(error);
          });

    }
    else{
      this.distributionServicec.completePlants({
        ...this.registerPassportForm.value,
        file,
        fileLoc,
        plants: this.items.map(({_id}) => _id)
      }, this.id)
        .pipe(first())
        .subscribe(
          ({data = {}}: any) => {
            this.selectedPlantsForSubmit = [];
            this.onGetWithDrawPlantListing(this.currentStatus);
            Object.keys(filePlaces).forEach(key => {
              this[key] = null;
            });
          },
          error => {
            this.loading = false;
            this.alertService.error(error);
          });

    }
  }
}
