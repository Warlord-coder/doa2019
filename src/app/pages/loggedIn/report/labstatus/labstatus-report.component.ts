import { Component, OnInit } from "@angular/core";
import { DataTableParams, DataTableResource } from "ngx-datatable-bootstrap4";
import { NgbModal } from "@ng-bootstrap/ng-bootstrap";
import { Router } from "@angular/router";
import * as _ from "lodash";
import { FormBuilder, FormGroup, Validators } from "@angular/forms";
import {
  AlertService,
  PassportService,
  PlantCategoryService,
  PlantRegisterService,
  CommonProvinceService,
  CommonCountryService
} from "../../../../services";
import { first } from "rxjs/operators";
import * as moment from "moment";
import { Workbook } from "exceljs";
import * as fs from "file-saver";

@Component({
  selector: "app-month-report-page",
  templateUrl: "./labstatus-report.component.html"
})
export class NgbdLabStatusReportComponent implements OnInit {
  resources = new DataTableResource<any>([]);
  items: any[] = [];
  count = 0;
  tempItems: any[] = [];
  submitted = false;
  momentObj: any;
  file = null;
  loading = false;
  apiError = false;

  currentYear;
  thaiCurrentYear;

  startDt: any;
  endDt: any;

  fileName = "ExcelSheet.xlsx";
  categories = [];

  formTypes = [];
  plantTypes = [];
  departmentTypes = [];
  sourceProvinces = [];
  sourceCountries = [];
  search = {
    endDt: moment().format("YYYY-MM-DD"),
    startDt: moment()
      .subtract(1, "year")
      .format("YYYY-MM-DD")
  };

  month_names = ["Jan", "Feb", "Mar", "Apr", "May", "Jun", "Jul", "Aug", "Sep", "Oct", "Nov", "Dec"];
  dateRanges = [];
  plants = [];

  excelData = [];
  constructor(
    private modalService: NgbModal,
    private formBuilder: FormBuilder,
    private alertService: AlertService,
    private passportService: PassportService,
    private plantCategoryService: PlantCategoryService,
    private plantRegisterService: PlantRegisterService,
    private commonProvinceService: CommonProvinceService,
    private commonCountryService: CommonCountryService,
    private router: Router
  ) {
    this.momentObj = moment;
  }

  ngOnInit() {
  }
  filter() {
    this.loading = true;
    this.plantRegisterService
      .filterLabStatus(this.search)
      .pipe(first())
      .subscribe(
        ({ data = [] }: any) => {
          let plantRegisters = data["filterResult"];
          let results = [];
          plantRegisters.forEach(plant => {
            if(!plant['m8_lab_id']) return;
            if(plant['createdAt'].split('T')[0] <  this.search.startDt) return;
            if(plant['createdAt'].split('T')[0] >  this.search.endDt) return;
            let lab_id = plant['m8_lab_id']['_id'];
            let lab_status = plant['m8_lab_status'];
            let lab_name = plant['m8_lab_id']['lab_no'];
            let date = plant['createdAt'];
            let flag = false;
            results.forEach(item => {
              if(item['id'] === lab_id) {
                if(lab_status == "draft") item['draft']++;
                if(lab_status == "complete") item['complete']++;
                item['total'] = item['draft'] + item['complete'];
                flag = true;
                return;
              }
            });
            if(flag == false) {
              let item = {id: lab_id, draft: 0, complete: 0, total: 0, name: lab_name, date: moment(date).format('MM/DD') + "/" + (parseInt(moment(date).format('Y'))+543)};
              if(lab_status == "complete") item['complete'] = 1;
              if(lab_status == "draft") item['draft'] = 1;
              item['total'] = item['draft'] + item['complete'];
              results.push(item);
            }
          })
          this.resources = new DataTableResource<any>(results);
          this.resources.count().then(count => (this.count = count));
          this.reloadData({});
          this.loading = false;
          results.forEach(item => {
            this.excelData.push([item['date'], item['name'], item['complete'], item['draft'], item['total']]);
          });
        },
        error => {
          this.loading = false;
        }
      );
  }

  
  dateRange(startDate, endDate) {
    var start      = startDate.split('-');
    var end        = endDate.split('-');
    var startYear  = parseInt(start[0]);
    var endYear    = parseInt(end[0]);
    var dates      = [];

    for(var i = startYear; i <= endYear; i++) {
      var endMonth = i != endYear ? 11 : parseInt(end[1]) - 1;
      var startMon = i === startYear ? parseInt(start[1])-1 : 0;
      for(var j = startMon; j <= endMonth; j = j > 12 ? j % 12 || 11 : j+1) {
        var month = j+1;
        var displayMonth = month < 10 ? '0'+month : month;
        dates.push([i, displayMonth].join('-'));
      }
    }
    return dates;
  }

  reloadData(params: DataTableParams) {
    params = {offset: 0, limit: 50};
    this.resources.query(params).then(vals => {
      this.plants = vals;
    });
  }

  export(){
    // Create workbook and worksheet
    const workbook = new Workbook();
    const worksheet = workbook.addWorksheet("Lab Status");
    const title = "รายงานสถานะของระบบปฏิบัติการของวันที่ " + this.search.startDt + " / " + this.search.endDt;

     // Add Row and formatting
     const titleRow = worksheet.addRow([title]);
     titleRow.font = { size: 16, bold: true };
     titleRow.alignment = { vertical: "middle", horizontal: "center" };

     worksheet.mergeCells("A1:E1");

     const headers = [
      "Create At (L No)",
      "LAB",
      "Completed",
      "Draft",
      "Total"
    ];

    // Add Header Row
    const headerRow = worksheet.addRow(headers);

    // Cell Style : Fill and Border
    headerRow.eachCell((cell, number) => {
      cell.border = {
        top: { style: "thin" },
        left: { style: "thin" },
        bottom: { style: "thin" },
        right: { style: "thin" }
      };
      cell.font = { size: 14, bold: true };
      cell.alignment = { horizontal: "center", vertical: "middle" };
    });
    // worksheet.addRows(data);
    worksheet.getRow(2).height = 30;
    worksheet.getRow(1).height = 35;
    worksheet.getColumn(1).width = 20;
    worksheet.getColumn(2).width = 12;
    worksheet.getColumn(3).width = 40;
    worksheet.getColumn(4).width = 60;

    this.excelData.forEach(d => {
      const row = worksheet.addRow(d);
      for (let col = 1; col <= 5; col++) {
        const cell = row.getCell(col);
        cell.alignment = { horizontal: "center" };
      }
    });
    workbook.xlsx.writeBuffer().then((data: any) => {
      const blob = new Blob([data], {
        type:
          "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet"
      });
      fs.saveAs(blob, "LabStatusReport.xlsx");
    });
    
  }
}
