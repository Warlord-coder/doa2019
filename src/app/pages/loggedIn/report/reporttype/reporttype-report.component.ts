import { Component, OnInit } from "@angular/core";
import { DataTableParams, DataTableResource } from "ngx-datatable-bootstrap4";
import { NgbModal } from "@ng-bootstrap/ng-bootstrap";
import { Router } from "@angular/router";
import * as _ from "lodash";
import { FormBuilder, FormGroup, Validators } from "@angular/forms";
import {
  AlertService,
  PassportService,
  PlantCategoryService,
  PlantRegisterService,
  WarehouseService
} from "../../../../services";
import { first } from "rxjs/operators";
import * as moment from "moment";
import { Workbook } from "exceljs";
import * as fs from "file-saver";

@Component({
  selector: "app-reporttype-report-page",
  templateUrl: "./reporttype-report.component.html"
})
export class NgbdReportTypeReportComponent implements OnInit {
  resources = new DataTableResource<any>([]);
  items: any[] = [];
  count = 0;
  tempItems: any[] = [];
  submitted = false;
  momentObj: any;
  file = null;
  loading = false;
  apiError = false;

  currentYear;
  thaiCurrentYear;

  startDt: any;
  endDt: any;

  fileName = "ExcelSheet.xlsx";
  categories = [];

  formTypes = [];
  plantTypes = [];
  departmentTypes = [];
  sourceProvinces = [];
  sourceCountries = [];
  search = {
    endDt: moment().format("YYYY-MM-DD"),
    startDt: moment()
      .subtract(1, "year")
      .format("YYYY-MM-DD"),
    reportType: ""
  };

  month_names = ["Jan", "Feb", "Mar", "Apr", "May", "Jun", "Jul", "Aug", "Sep", "Oct", "Nov", "Dec"];
  dateRanges = [];
  plants = [];

  excelData = [];
  constructor(
    private modalService: NgbModal,
    private formBuilder: FormBuilder,
    private alertService: AlertService,
    private passportService: PassportService,
    private plantCategoryService: PlantCategoryService,
    private plantRegisterService: PlantRegisterService,
    private warehouseService: WarehouseService,
    private router: Router
  ) {
    this.momentObj = moment;
  }

  ngOnInit() {
    this.search.reportType = "new_doa";
    this.initPlantCategory();
  }

  initPlantCategory() {
    this.plantCategoryService
      .getAll()
      .pipe(first())
      .subscribe(
        ({ data = [] }: any) => {
          this.categories = data;
        },
        error => {
          this.loading = false;
        }
      );
  }

  filter() {
    this.loading = true;
    this.warehouseService
      .getListAll()
      .pipe(first())
      .subscribe(
        ({ data = [] }: any) => {
          let results = [];
          this.excelData = [];
          let warehouses = _.filter(data, item => {
            if(item['volume'] == null) return false;
            if(item['reg_plant_sci_name'] == null) return;
            if(this.search.reportType == "new_doa") {
              if(item['volume']['form_type'] == "Register 1/2" || item['volume']['form_type'] == "Register 4") {
                return item;
              }
            } else if(this.search.reportType == "d_only") {
              if(item['volume']['form_type'] == "Register 3") {
                return item;
              }
            }
          })
          if(this.search.reportType == "new_doa") {
            _.forEach(warehouses, warehouse => {
              if(warehouse['reg_plant_sci_name'] == null) return;
              if(warehouse['gs_no'] == undefined) return;
              let category_id = warehouse['reg_plant_sci_name']['plant_category'];
              let category = _.filter(this.categories, category => {
                return category['_id'] == category_id;
              })
              if(category.length == 0) return;
              let flag = false;
              results.forEach(result => {
                if(result['category_id'] == category_id) {
                  result['count']++;
                  if(warehouse['gs_no'] == undefined) {
                    result['gs_no'].push(warehouse['reg_gene_id']);
                  } else {
                    result['gs_no'].push(warehouse['gs_no']);
                  }
                  
                  flag = true;
                }
              })
              if(flag == false) {
                let item;
                item = {category_id: category_id, category_name: category[0]['categoryName_en'], count: 1, gs_no: [warehouse['gs_no']]};
                results.push(item);
              }
            })
            results.forEach(result => {
              let gs_array = result['gs_no'];
              let gs_key = gs_array[0].substring(0, 5);
              if(gs_array.length == 1) {
                result['gs_str'] = gs_array[0];
              } else if (gs_array.length == 2) {
                result['gs_str'] = gs_array[0] + ", " + gs_array[1];
              } else {
                let gs_value = [];
                gs_array.forEach(val => {
                  gs_value.push(parseInt( val.slice(5).trim()));
                })
                let min = _.min(gs_value);
                let max = _.max(gs_value);
                result['gs_str'] = gs_key + " " + _.padStart(min, 5, '0') + "-" + _.padStart(max, 5, '0'); 
              }
              this.excelData.push([result['category_name'], result['count'], result['gs_str']]);
            });
          } else if(this.search.reportType == "d_only") {
            _.forEach(warehouses, warehouse => {
              let category_id = warehouse['reg_plant_sci_name']['plant_category'];
              let category = _.filter(this.categories, category => {
                return category['_id'] == category_id;
              })
              let flag = false;
              results.forEach(result => {
                if(result['category_id'] == category_id) {
                  result['count']++;      
                  flag = true;
                }
              })
              if(flag == false) {
                let item;
                item = {category_id: category_id, category_name: category[0]['categoryName_en'], count: 1};
                results.push(item);
              }
            });
            results.forEach(result => {
              this.excelData.push([result['category_name'], result['count']]);
            })
          }
          
          this.resources = new DataTableResource<any>(results);
          this.resources.count().then(count => (this.count = count));
          this.reloadData({});
          this.loading = false;
        },
        error => {
          this.loading = false;
        }
      );
  }

  changeReportType(){
    //this.filter();
  }

  reloadData(params: DataTableParams) {
    params = {offset: 0, limit: 15};
    this.resources.query(params).then(vals => {
      this.plants = vals;
    });
  }

  export(){
    // Create workbook and worksheet
    const workbook = new Workbook();
    const worksheet = workbook.addWorksheet("Warehouse Status");
    let title = "";

    let headers = [];

    if(this.search.reportType == "new_doa"){
      title = "ตารางสรุปประจำปี พืชออก DOA ใหม่ " + this.search.startDt.split('-')[0];
      headers = ["ชนิดพืช", "จำนวนตัวอย่างพันธุ์ใหม่้", "พืชที่ออก DOA ให้"];
    }else if(this.search.reportType == "d_only"){
      title = "ตารางสรุปพืชจากการฝากเก็บปี " + this.search.startDt.split('-')[0];
      headers = ["ชนิดพืช", "Total Plant list"];
    } else {
      title = "ตารางสรุปพืชจากฟื้นฟูปี " + this.search.startDt.split('-')[0];
      headers.push("สถานะใหม่");
    }
    // Add Row and formatting
    const titleRow = worksheet.addRow([title]);
    titleRow.font = { size: 16, bold: true };
    titleRow.alignment = { vertical: "middle", horizontal: "center" };

    worksheet.mergeCells("A1:C1");
    // Add Header Row
    const headerRow = worksheet.addRow(headers);

    // Cell Style : Fill and Border
    headerRow.eachCell((cell, number) => {
      cell.border = {
        top: { style: "thin" },
        left: { style: "thin" },
        bottom: { style: "thin" },
        right: { style: "thin" }
      };
      cell.font = { size: 14, bold: true };
      cell.alignment = { horizontal: "center", vertical: "middle" };
    });
    // worksheet.addRows(data);
    worksheet.getRow(2).height = 30;
    worksheet.getRow(1).height = 35;
    worksheet.getColumn(1).width = 20;
    worksheet.getColumn(2).width = 20;
    worksheet.getColumn(3).width = 30;
    worksheet.getColumn(4).width = 20;
    worksheet.getColumn(5).width = 20;

    let sum = ["รวม"];
    let total = 0;
    this.excelData.forEach(d => {
      const row = worksheet.addRow(d);
      for (let col = 1; col <= 3; col++) {
        const cell = row.getCell(col);
        cell.alignment = { horizontal: "center" };
      }

      total += d[1];
    });
    sum.push(total.toString());
    const sumRow = worksheet.addRow(sum);
    for (let col = 1; col <= 4; col++) {
      const cell = sumRow.getCell(col);
      cell.alignment = { horizontal: "center" };
    }

    workbook.xlsx.writeBuffer().then((data: any) => {
      const blob = new Blob([data], {
        type:
          "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet"
      });
      fs.saveAs(blob, "ReporrTypeReport.xlsx");
    });
    
  }
}
