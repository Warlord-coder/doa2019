import { Component, OnInit } from "@angular/core";
import { DataTableParams, DataTableResource } from "ngx-datatable-bootstrap4";
import { NgbModal } from "@ng-bootstrap/ng-bootstrap";
import { Router } from "@angular/router";
import * as _ from "lodash";
import { FormBuilder, FormGroup, Validators } from "@angular/forms";
import {
  AlertService,
  PassportService,
  PlantCategoryService,
  PlantRegisterService,
  DistributionService
} from "../../../../services";
import { first } from "rxjs/operators";
import * as moment from "moment";
import { Workbook } from "exceljs";
import * as fs from "file-saver";

@Component({
  selector: "app-formtype-report-page",
  templateUrl: "./distribution-report.component.html"
})
export class NgbdDistributionReportComponent implements OnInit {
  resources = new DataTableResource<any>([]);
  items: any[] = [];
  count = 0;
  tempItems: any[] = [];
  submitted = false;
  momentObj: any;
  file = null;
  loading = false;
  apiError = false;

  currentYear;
  thaiCurrentYear;

  startDt: any;
  endDt: any;

  categories = [];

  formTypes = [];
  plantTypes = [];
  departmentTypes = [];
  sourceProvinces = [];
  sourceCountries = [];
  search = {
    endDt: moment().format("YYYY-MM-DD"),
    startDt: moment()
      .subtract(1, "year")
      .format("YYYY-MM-DD"),
      reportType: ""
  };

  dateRanges = [];
  withdraws = [];

  excelData = [];
  constructor(
    private modalService: NgbModal,
    private formBuilder: FormBuilder,
    private alertService: AlertService,
    private passportService: PassportService,
    private plantCategoryService: PlantCategoryService,
    private plantRegisterService: PlantRegisterService,
    private distributionService: DistributionService,
    private router: Router
  ) {
    this.momentObj = moment;
  }

  ngOnInit() {
    this.search.reportType = "department";
  }


  filter() {
    this.loading = true;
    if(this.search.reportType == "department") {
      this.distributionService
        .getByDepartment({startDt: this.search.startDt, endDt: this.search.endDt})
        .pipe(first())
        .subscribe(
          ({ data = [] }: any) => {
            let results = [];
            this.excelData = [];
            let withdraws = _.filter(data, item => {
              if(item['m5_assigned']['updatedAt'].split('T')[0] <  this.search.startDt) return;
              if(item['m5_assigned']['updatedAt'].split('T')[0] >  this.search.endDt) return;
              if(item['m5_assigned']['distribution_request_department'] === "null") return;
              if(item['m5_assigned']['distribution_request_department'] === undefined) return;
              let department = item['m5_assigned']['distribution_request_department'];
              let status = item['distributionStatus'];
              let flag = false;

              results.forEach(result => {
                if(result['department'] === department) {
                  if(status == "pending") result['pending']++;
                  if(status == "complete") result['complete']++;
                  result['total'] = result['pending'] + result['complete'];
                  flag = true;
                  return;
                }
              })

              if(flag == false) {
                let result = {department: department, pending: 0, complete: 0, total: 0};
                if(status == "complete") result['complete'] = 1;
                if(status == "pending") result['pending'] = 1;
                result['total'] = result['pending'] + result['complete'];
                results.push(result);
              }
            })
            results.forEach(result => {
              this.excelData.push([result['department'], result['total'], result['complete']]);
            })

            this.resources = new DataTableResource<any>(results);
            this.resources.count().then(count => (this.count = count));
            this.reloadData({});
            this.loading = false;
          },
          error => {
            this.loading = false;
          }
      );
    } else {
      this.distributionService
        .getByPlantType({startDt: this.search.startDt, endDt: this.search.endDt})
        .pipe(first())
        .subscribe(
          ({ data = [] }: any) => {
            let results = [];
            this.excelData = [];
            let withdraws = _.filter(data['withdraws'], item => {
              
              if(item['m5_assigned']['updatedAt'].split('T')[0] <  this.search.startDt) return;
              if(item['m5_assigned']['updatedAt'].split('T')[0] >  this.search.endDt) return;
              if(item['m5_assigned']['distribution_request_department'] === "null") return;
              if(item['m5_assigned']['distribution_request_department'] === undefined) return;

              let m5_assigned = item['m5_assigned']['_id'];
              let warehouse = null;
              data['warehouse'].forEach(obj => {
                if(obj.m5_assigned == m5_assigned) {
                  warehouse = obj;
                }
              });
              if(warehouse == null) return;
              if(warehouse['plant_gene_name'] == null) return;
              if(warehouse['plant_gene_name']['plantCategory'] == null) return;
              let category_id = warehouse['plant_gene_name']['plantCategory']['_id'];
              let category_en = warehouse['plant_gene_name']['plantCategory']['categoryName_en'];
              let category_th = warehouse['plant_gene_name']['plantCategory']['categoryName_th'];
              let status = item['distributionStatus'];
              let flag = false;
              results.forEach(result => {
                if(result['category_id'] === category_id) {
                  if(status == "complete") result['complete']++;
                  flag = true;
                  return;
                }
              })
              if(flag == false) {
                let result = {category_id: category_id, complete: 0, category_en: category_en, category_th: category_th};
                if(status === "complete") result['complete'] = 1;
                results.push(result);
              }
            })
            results.forEach(result => {
              this.excelData.push([result['category_en'], result['complete']]);
            })

            this.resources = new DataTableResource<any>(results);
            this.resources.count().then(count => (this.count = count));
            this.reloadData({});
            this.loading = false;
          },
          error => {
            this.loading = false;
          }
      );
    }
    
  }

  changeReportType(){
    this.resources = new DataTableResource<any>([]);
    this.resources.count().then(count => (this.count = count));
    this.reloadData({});
    //this.filter();
  }

  reloadData(params: DataTableParams) {
    params = {offset: 0, limit: 50};
    this.resources.query(params).then(vals => {
      this.withdraws = vals;
    });
  }

  export(){
    // Create workbook and worksheet
    const workbook = new Workbook();
    const worksheet = workbook.addWorksheet("Withdraw Department Report");
    let title = "";

    let headers = [];
    if(this.search.reportType == "department"){
      title = "Distribution By Department Report " + this.search.startDt + " - " + this.search.endDt;
      headers = ["Department", "จำนวนผู้ขอรับบริการ", "จำนวนตัวอย่าง (Plant list)ที่ให้บริการ"];
    }else if(this.search.reportType == "reg_plant_type"){
      title = "Distribution By Plant Category Report " + this.search.startDt + " - " + this.search.endDt;
      headers = ["Reg Plant Type", "Total plant complete"];
    } 
     // Add Row and formatting
     const titleRow = worksheet.addRow([title]);
     titleRow.font = { size: 14, bold: true };
     titleRow.alignment = { vertical: "middle", horizontal: "center" };

     if(this.search.reportType == "department"){
      worksheet.mergeCells("A1:C1");
     } else {
      worksheet.mergeCells("A1:B");
     }
    // Add Header Row
    const headerRow = worksheet.addRow(headers);

    // Cell Style : Fill and Border
    headerRow.eachCell((cell, number) => {
      cell.border = {
        top: { style: "thin" },
        left: { style: "thin" },
        bottom: { style: "thin" },
        right: { style: "thin" }
      };
      cell.font = { size: 12, bold: true };
      cell.alignment = { horizontal: "center", vertical: "middle" };
    });
    // worksheet.addRows(data);
    worksheet.getRow(2).height = 30;
    worksheet.getRow(1).height = 35;
    worksheet.getColumn(1).width = 25;
    worksheet.getColumn(2).width = 25;
    worksheet.getColumn(3).width = 40;

    this.excelData.forEach(d => {
      const row = worksheet.addRow(d);
      for (let col = 1; col <= 5; col++) {
        const cell = row.getCell(col);
        cell.alignment = { horizontal: "center" };
      }
    });
    workbook.xlsx.writeBuffer().then((data: any) => {
      const blob = new Blob([data], {
        type:
          "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet"
      });
      fs.saveAs(blob, "DistributionReport.xlsx");
    });
    
  }
}
