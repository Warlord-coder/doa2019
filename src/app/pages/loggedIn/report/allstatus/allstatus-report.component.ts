import { Component, OnInit } from "@angular/core";
import { DataTableParams, DataTableResource } from "ngx-datatable-bootstrap4";
import { NgbModal } from "@ng-bootstrap/ng-bootstrap";
import { Router } from "@angular/router";
import * as _ from "lodash";
import { FormBuilder, FormGroup, Validators } from "@angular/forms";
import {
  AlertService,
  PassportService,
  PlantCategoryService,
  PlantRegisterService,
  WarehouseService
} from "../../../../services";
import { first } from "rxjs/operators";
import * as moment from "moment";
import { Workbook } from "exceljs";
import * as fs from "file-saver";

@Component({
  selector: "app-formtype-report-page",
  templateUrl: "./allstatus-report.component.html"
})
export class NgbdAllstatusReportComponent implements OnInit {
  resources = new DataTableResource<any>([]);
  items: any[] = [];
  count = 0;
  tempItems: any[] = [];
  submitted = false;
  momentObj: any;
  file = null;
  loading = false;
  apiError = false;

  currentYear;
  thaiCurrentYear;

  startDt: any;
  endDt: any;


  dateRanges = [];
  reports = [];

  excelData = [];

  constructor(
    private modalService: NgbModal,
    private formBuilder: FormBuilder,
    private alertService: AlertService,
    private passportService: PassportService,
    private warehouseService: WarehouseService,
    private router: Router
  ) {
    this.momentObj = moment;
  }

  ngOnInit() {
    this.currentYear = new Date().getFullYear();
  }


  filter() {
    this.loading = true;
    this.warehouseService
        .getListAll()
        .pipe(first())
        .subscribe(
          ({ data = [] }: any) => {
            let results = [
              {year: this.currentYear + 543, new_doa: 0, old_doa: 0, r_only: 0, plant_remove: 0, d_id: 0},
              {year: this.currentYear + 543 - 1, new_doa: 0, old_doa: 0, r_only: 0, plant_remove: 0, d_id: 0},
              {year: this.currentYear + 543 - 2, new_doa: 0, old_doa: 0, r_only: 0, plant_remove: 0, d_id: 0},
              {year: this.currentYear + 543 - 3, new_doa: 0, old_doa: 0, r_only: 0, plant_remove: 0, d_id: 0},
              {year: this.currentYear + 543 - 4, new_doa: 0, old_doa: 0, r_only: 0, plant_remove: 0, d_id: 0},
              {year: this.currentYear + 543 - 5, new_doa: 0, old_doa: 0, r_only: 0, plant_remove: 0, d_id: 0},
              {year: this.currentYear + 543 - 6, new_doa: 0, old_doa: 0, r_only: 0, plant_remove: 0, d_id: 0},
              {year: this.currentYear + 543 - 7, new_doa: 0, old_doa: 0, r_only: 0, plant_remove: 0, d_id: 0},
              {year: this.currentYear + 543 - 8, new_doa: 0, old_doa: 0, r_only: 0, plant_remove: 0, d_id: 0},
              {year: this.currentYear + 543 - 9, new_doa: 0, old_doa: 0, r_only: 0, plant_remove: 0, d_id: 0}

            ];
            this.excelData = [];
            let reports = _.filter(data, item => {
              if(item['volume'] != null) {
                if(item.volume.form_type == "Register 1/2" && item.wh_status == 'complete' && item.submitTo == 'm2') {
                  results.forEach(result => {
                    if(item.createdAt.split('-')[0] == result.year - 543) result.new_doa++;
                  })
                }
                if(item.volume.form_type == "Register 4" && item.wh_status == 'complete' && item.reg_gene_id == '-') {
                  results.forEach(result => {
                    if(item.createdAt.split('-')[0] == result.year - 543) result.old_doa++;
                  })
                }
                if(item.wh_status == 'complete' && item.reg_gene_id != '-') {
                  results.forEach(result => {
                    if(item.createdAt.split('-')[0] == result.year - 543) result.r_only++;
                  })
                }
                if(item.wh_status == 'complete' && item.submitTo == "m5") {
                  results.forEach(result => {
                    if(item.createdAt.split('-')[0] == result.year - 543) result.d_id++;
                  })
                }
              }
              if(item.wh_status == 'complete' && item.submitTo == 'm9') {
                results.forEach(result => {
                  if(item.createdAt.split('-')[0] == result.year - 543) result.plant_remove++;
                })
              }
            })
            results.forEach(result => {
              this.excelData.push([parseInt(result['year']), result['new_doa'], result['old_doa'], result['r_only'], result['plant_remove'], result['d_id']]);
            })

            this.resources = new DataTableResource<any>(results);
            this.resources.count().then(count => (this.count = count));
            this.reloadData({});
            this.loading = false;
          },
          error => {
            this.loading = false;
          }
      );
    
  }

  changeReportType(){
    this.resources = new DataTableResource<any>([]);
    this.resources.count().then(count => (this.count = count));
    this.reloadData({});
    //this.filter();
  }

  reloadData(params: DataTableParams) {
    params = {offset: 0, limit: 50};
    this.resources.query(params).then(vals => {
      this.reports = vals;
    });
  }

  export(){
    // Create workbook and worksheet
    const workbook = new Workbook();
    const worksheet = workbook.addWorksheet("Withdraw Department Report");
    let title = "";

    let headers = [];

    title = "ตารางสรุปประจำปี  " + (this.currentYear + 543 - 9) + " - " + (this.currentYear + 543);
    headers = ["ปี", "พืชออก DOA ใหม่", "พืช DOA เดิม จากฟื้นฟู", "พืช hold", "พืชตัดทิ้ง", "พืชฝากเก็บ"];
     // Add Row and formatting
     const titleRow = worksheet.addRow([title]);
     titleRow.font = { size: 14, bold: true };
     titleRow.alignment = { vertical: "middle", horizontal: "center" };

    worksheet.mergeCells("A1:F1");
    // Add Header Row
    const headerRow = worksheet.addRow(headers);

    // Cell Style : Fill and Border
    headerRow.eachCell((cell, number) => {
      cell.border = {
        top: { style: "thin" },
        left: { style: "thin" },
        bottom: { style: "thin" },
        right: { style: "thin" }
      };
      cell.font = { size: 12, bold: true };
      cell.alignment = { horizontal: "center", vertical: "middle" };
    });
    // worksheet.addRows(data);
    worksheet.getRow(2).height = 30;
    worksheet.getRow(1).height = 35;
    worksheet.getColumn(1).width = 10;
    worksheet.getColumn(2).width = 25;
    worksheet.getColumn(3).width = 35;
    worksheet.getColumn(4).width = 25;
    worksheet.getColumn(5).width = 25;
    worksheet.getColumn(5).width = 25;
    this.excelData.forEach(d => {
      const row = worksheet.addRow(d);
      for (let col = 1; col <= 6; col++) {
        const cell = row.getCell(col);
        cell.alignment = { horizontal: "center" };
      }
    });
    workbook.xlsx.writeBuffer().then((data: any) => {
      const blob = new Blob([data], {
        type:
          "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet"
      });
      fs.saveAs(blob, "AllstatusReport.xlsx");
    });
    
  }
}
