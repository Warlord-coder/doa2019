import { Component, OnInit } from "@angular/core";
import { DataTableParams, DataTableResource } from "ngx-datatable-bootstrap4";
import { NgbModal } from "@ng-bootstrap/ng-bootstrap";
import { Router } from "@angular/router";
import * as _ from "lodash";
import { FormBuilder, FormGroup, Validators } from "@angular/forms";
import { AlertService, PassportService } from "../../../../services";
import { first } from "rxjs/operators";
import * as moment from "moment";
import { Workbook } from "exceljs";
import * as fs from "file-saver";

@Component({
  selector: "app-year-report-page",
  templateUrl: "./year-report.component.html"
})
export class NgbdPassportYearReportComponent implements OnInit {
  resources = new DataTableResource<any>([]);
  items: any[] = [];
  count = 0;
  tempItems: any[] = [];
  submitted = false;
  momentObj: any;
  file = null;
  loading = false;
  apiError = false;

  currentYear;
  thaiCurrentYear;

  startDt: any;
  endDt: any;

  excelCategories = [];
  fileName = "ExcelSheet.xlsx";
  categories = [];
  constructor(
    private modalService: NgbModal,
    private formBuilder: FormBuilder,
    private alertService: AlertService,
    private passportService: PassportService,
    private router: Router
  ) {
    this.momentObj = moment;
  }

  ngOnInit() {

    this.startDt = moment()
      .subtract(1, "year")
      .format("YYYY-MM-DD");
    this.endDt = this.momentObj(new Date()).format("YYYY-MM-DD");
    this.currentYear = this.momentObj(new Date()).format("YYYY");
    this.thaiCurrentYear = parseInt(this.currentYear) + 543;
    
  }
  export() {
    const headers = [
      "Reg plant type ชนิดพืช",
      "Register 1 (Total plant in reg1/2)",
      "Register 3 (Total plant in reg3)",
      "Register 4 (Total plant in reg4)",
      "Total"
    ];

    // Create workbook and worksheet
    const workbook = new Workbook();
    const worksheet = workbook.addWorksheet("Year Report");
    const title =
      "รายงานการลงทะเบียนประจำปี " +
      this.thaiCurrentYear +
      " (Yearly Report)";
    // Add Row and formatting
    const titleRow = worksheet.addRow([title]);
    titleRow.font = { size: 16, bold: true };
    titleRow.alignment = { vertical: "middle", horizontal: "center" };

    worksheet.mergeCells("A1:E1");

    // Add Header Row
    const headerRow = worksheet.addRow(headers);

    // Cell Style : Fill and Border
    headerRow.eachCell((cell, number) => {
      cell.border = {
        top: { style: "thin" },
        left: { style: "thin" },
        bottom: { style: "thin" },
        right: { style: "thin" }
      };
      cell.font = { size: 14, bold: true };
      cell.alignment = { horizontal: "center", vertical: "middle" };
    });
    // worksheet.addRows(data);
    worksheet.getRow(2).height = 30;
    worksheet.getRow(1).height = 35;
    worksheet.getColumn(1).width = 40;
    worksheet.getColumn(2).width = 40;
    worksheet.getColumn(3).width = 40;
    worksheet.getColumn(4).width = 40;
    worksheet.getColumn(5).width = 40;

    let sum = ["รวม (Total By Form Type)"];
    let sum_reg1 = 0;
    let sum_reg3 = 0;
    let sum_reg4 = 0;
    let sum_total = 0;
    this.excelCategories.forEach(d => {
      let total = 0;
      for (let i = 1; i < d.length; i++) {
        total += d[i];
      }
      sum_reg1 += d[1];
      sum_reg3 += d[2];
      sum_reg4 += d[3];
      sum_total += total;
      d.push(total);
      const row = worksheet.addRow(d);
      for (let col = 1; col <= 5; col++) {
        const cell = row.getCell(col);
        cell.alignment = { horizontal: "center" };
      }
    });
    sum.push(sum_reg1.toString());
    sum.push(sum_reg3.toString());
    sum.push(sum_reg4.toString());
    sum.push(sum_total.toString());
    const sumRow = worksheet.addRow(sum);
    for (let col = 1; col <= 5; col++) {
      const cell = sumRow.getCell(col);
      cell.alignment = { horizontal: "center" };
    }
    const totalRow = sumRow.getCell(1);
    totalRow.font = { size: 14, bold: true };
    workbook.xlsx.writeBuffer().then((data: any) => {
      const blob = new Blob([data], {
        type:
          "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet"
      });
      fs.saveAs(blob, "PlantRegisterYearReport.xlsx");
    });
    this.loading = false;
  }
  search() {
    this.loading = true;
    this.passportService
      .exportExcel(this.startDt, this.endDt)
      .pipe(first())
      .subscribe(
        ({ data = [] }: any) => {
          this.categories = [];
          let categories = data["result"]["categories"];
          let registers = data["result"]["registers"];
          registers.forEach(register => {
            if(register["reg_plant_sci_name"] == null) return;
            if(register["volume"] == null) return;
            if(register['updatedAt'].split('T')[0] <=  this.startDt) return;
            if(register['updatedAt'].split('T')[0] >=  this.endDt) return;
            let category_id = register["reg_plant_sci_name"]["plant_category"];
            let form_type = register["volume"]["form_type"];
            categories.forEach(category => {
              if (category.register1 == undefined) category.register1 = 0;
              if (category.register3 == undefined) category.register3 = 0;
              if (category.register4 == undefined) category.register4 = 0;
              if (category["_id"] == category_id) {
                if (form_type == "Register 1/2") {
                  category.register1 = category.register1 + 1;
                } else if (form_type == "Register 3") {
                  category.register3 = category.register3 + 1;
                } else {
                  category.register4 = category.register4 + 1;
                }
                return;
              }
            });
          });

          this.excelCategories = [];

          categories.forEach(category => {
            if (
              category["register1"] == 0 &&
              category["register3"] == 0 &&
              category["register4"] == 0
            )
              return;
            if (
              category["register1"] == undefined &&
              category["register3"] == undefined &&
              category["register4"] == undefined
            )
              return;
            let item = {};

            this.excelCategories.push([
              category["categoryName_en"],
              category["register1"],
              category["register3"],
              category["register4"]
            ]);
            this.categories.push(category);
          });

          if (this.categories.length == 0){
            this.loading = false;
            return;
          }
          this.resources = new DataTableResource<any>(this.categories);
          this.resources.count().then(count => (this.count = count));
          this.reloadData({});
          this.loading = false;
        });
  }

  reloadData(params: DataTableParams) {
    params = {offset: 0, limit: 15};
    this.resources.query(params).then(vals => {
      this.items = vals;
      this.tempItems = this.items;
    });
  }
}
