import { Component, OnInit } from "@angular/core";
import { DataTableParams, DataTableResource } from "ngx-datatable-bootstrap4";
import { NgbModal } from "@ng-bootstrap/ng-bootstrap";
import { Router } from "@angular/router";
import * as _ from "lodash";
import { FormBuilder, FormGroup, Validators } from "@angular/forms";
import {
  AlertService,
  PassportService,
  PlantCategoryService,
  PlantRegisterService,
  CommonProvinceService,
  CommonCountryService
} from "../../../../services";
import { first } from "rxjs/operators";
import * as moment from "moment";
import { Workbook } from "exceljs";
import * as fs from "file-saver";

@Component({
  selector: "app-month-report-page",
  templateUrl: "./month-report.component.html"
})
export class NgbdPlantMonthReportComponent implements OnInit {
  resources = new DataTableResource<any>([]);
  items: any[] = [];
  count = 0;
  tempItems: any[] = [];
  submitted = false;
  momentObj: any;
  file = null;
  loading = false;
  apiError = false;

  currentYear;
  thaiCurrentYear;

  startDt: any;
  endDt: any;

  fileName = "ExcelSheet.xlsx";
  categories = [];

  formTypes = [];
  plantTypes = [];
  departmentTypes = [];
  sourceProvinces = [];
  sourceCountries = [];
  search = {
    form_type: "",
    plant_type: "",
    department_type: "",
    source_province: "",
    source_country: "",
    endDt: moment().format("YYYY-MM-DD"),
    startDt: moment()
      .subtract(1, "year")
      .format("YYYY-MM-DD")
  };

  month_names = ["Jan", "Feb", "Mar", "Apr", "May", "Jun", "Jul", "Aug", "Sep", "Oct", "Nov", "Dec"];
  dateRanges = [];
  plants = [];

  excelData = [];
  constructor(
    private modalService: NgbModal,
    private formBuilder: FormBuilder,
    private alertService: AlertService,
    private passportService: PassportService,
    private plantCategoryService: PlantCategoryService,
    private plantRegisterService: PlantRegisterService,
    private commonProvinceService: CommonProvinceService,
    private commonCountryService: CommonCountryService,
    private router: Router
  ) {
    this.momentObj = moment;
  }

  ngOnInit() {
    this.initInstance();

    this.dateRanges = this.dateRange(this.search.startDt, this.search.endDt);
  }

  initInstance() {
    this.formTypes = ["Register 1/2", "Register 3", "Register 4"];
    this.initPlantCategory();
    this.initDepartmentType();
    this.initSourceProvince();
    this.initSourceCountry();
  }
  initPlantCategory() {
    this.plantCategoryService
      .getAll()
      .pipe(first())
      .subscribe(
        ({ data = [] }: any) => {
          this.plantTypes = data;
        },
        error => {
          this.loading = false;
        }
      );
  }
  initDepartmentType() {
    this.plantRegisterService
      .getDepartmentTypes()
      .pipe(first())
      .subscribe(
        ({ data = [] }: any) => {
          this.departmentTypes = data;
          //this.plantTypes = data;
        },
        error => {
          this.loading = false;
        }
      );
  }
  initSourceProvince() {
    this.commonProvinceService
      .getAll()
      .pipe(first())
      .subscribe(
        ({ data = [] }: any) => {
          this.sourceProvinces = data;
        },
        error => {
          this.loading = false;
        }
      );
  }
  initSourceCountry() {
    this.commonCountryService
      .getAll()
      .pipe(first())
      .subscribe(
        ({ data = [] }: any) => {
          this.sourceCountries = data;
        },
        error => {
          this.loading = false;
        }
      );
  }
  filter() {
    this.loading = true;
    this.plantRegisterService
      .filter(this.search)
      .pipe(first())
      .subscribe(
        ({ data = [] }: any) => {
          let categories = data["categories"];
          let registers = data["filterResult"];
          let results = [];
          this.excelData = [];
          this.dateRanges.forEach(date => {
            categories.forEach(category => {
              let totalByCat = 0;
              registers.forEach(plant => {
                if(plant['createdAt'].split('T')[0] <  this.search.startDt) return;
                if(plant['createdAt'].split('T')[0] >  this.search.endDt) return;
                if(plant["reg_plant_sci_name"] == null) return;
                if(category["_id"] == plant["reg_plant_sci_name"]["plant_category"] && plant['updatedAt'].indexOf(date) > -1) {
                  totalByCat ++;
                }
              })
              if(totalByCat > 0) {
                let item = {year: parseInt(date.split("-")[0]) + 543, month: this.month_names[parseInt( date.split("-")[1] ) - 1], category: category['categoryName_en'], total: totalByCat};
                this.excelData.push([item['year'], item['month'], item['category'], item['total']]);
                results.push(item);
              }
            });
          });
          this.plants = results;
          this.resources = new DataTableResource<any>(results);
          this.resources.count().then(count => (this.count = count));
          this.reloadData({});
          this.loading = false;
        },
        error => {
          this.loading = false;
        }
      );
  }

  
  dateRange(startDate, endDate) {
    var start      = startDate.split('-');
    var end        = endDate.split('-');
    var startYear  = parseInt(start[0]);
    var endYear    = parseInt(end[0]);
    var dates      = [];

    for(var i = startYear; i <= endYear; i++) {
      var endMonth = i != endYear ? 11 : parseInt(end[1]) - 1;
      var startMon = i === startYear ? parseInt(start[1])-1 : 0;
      for(var j = startMon; j <= endMonth; j = j > 12 ? j % 12 || 11 : j+1) {
        var month = j+1;
        var displayMonth = month < 10 ? '0'+month : month;
        dates.push([i, displayMonth].join('-'));
      }
    }
    return dates;
  }

  reloadData(params: DataTableParams) {
    params = {offset: 0, limit: 15};
    this.resources.query(params).then(vals => {
      this.plants = vals;
    });
  }

  export(){
    // Create workbook and worksheet
    const workbook = new Workbook();
    const worksheet = workbook.addWorksheet("Monthly Report");
    const title = "รายงานประจำปีตั้งแต่วันที่ " + this.search.startDt + " / " + this.search.endDt;

     // Add Row and formatting
     const titleRow = worksheet.addRow([title]);
     titleRow.font = { size: 16, bold: true };
     titleRow.alignment = { vertical: "middle", horizontal: "center" };

     worksheet.mergeCells("A1:D1");

     const headers = [
      "Year",
      "Month",
      "ชนิดพืช (Reg Plant Type)",
      "Total Amount (Number of plant at register)"
    ];

    // Add Header Row
    const headerRow = worksheet.addRow(headers);

    // Cell Style : Fill and Border
    headerRow.eachCell((cell, number) => {
      cell.border = {
        top: { style: "thin" },
        left: { style: "thin" },
        bottom: { style: "thin" },
        right: { style: "thin" }
      };
      cell.font = { size: 14, bold: true };
      cell.alignment = { horizontal: "center", vertical: "middle" };
    });
    // worksheet.addRows(data);
    worksheet.getRow(2).height = 30;
    worksheet.getRow(1).height = 35;
    worksheet.getColumn(1).width = 12;
    worksheet.getColumn(2).width = 12;
    worksheet.getColumn(3).width = 40;
    worksheet.getColumn(4).width = 60;

    let sum = ["", "", "รวม"];
    let total = 0;
    this.excelData.forEach(d => {
      const row = worksheet.addRow(d);
      for (let col = 1; col <= 5; col++) {
        const cell = row.getCell(col);
        cell.alignment = { horizontal: "center" };
      }

      total += d[3];
    });
    sum.push(total.toString());
    const sumRow = worksheet.addRow(sum);
    for (let col = 1; col <= 4; col++) {
      const cell = sumRow.getCell(col);
      cell.alignment = { horizontal: "center" };
    }

    workbook.xlsx.writeBuffer().then((data: any) => {
      const blob = new Blob([data], {
        type:
          "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet"
      });
      fs.saveAs(blob, "PlantRegisterMonthReport.xlsx");
    });
    
  }
}
