import { Component, OnInit } from "@angular/core";
import { DataTableParams, DataTableResource } from "ngx-datatable-bootstrap4";
import { NgbModal } from "@ng-bootstrap/ng-bootstrap";
import { Router } from "@angular/router";
import * as _ from "lodash";
import { FormBuilder, FormGroup, Validators } from "@angular/forms";
import {
  AlertService,
  PassportService,
  PlantCategoryService,
  PlantRegisterService,
  CommonProvinceService,
  CommonCountryService
} from "../../../../services";
import { first } from "rxjs/operators";
import * as moment from "moment";
import { Workbook } from "exceljs";
import * as fs from "file-saver";

@Component({
  selector: "app-month-report-page",
  templateUrl: "./labplant-report.component.html"
})
export class NgbdLabPlantReportComponent implements OnInit {
  resources = new DataTableResource<any>([]);
  items: any[] = [];
  count = 0;
  tempItems: any[] = [];
  submitted = false;
  momentObj: any;
  file = null;
  loading = false;
  apiError = false;

  currentYear;
  thaiCurrentYear;

  startDt: any;
  endDt: any;

  fileName = "ExcelSheet.xlsx";
  categories = [];

  formTypes = [];
  plantTypes = [];
  departmentTypes = [];
  sourceProvinces = [];
  sourceCountries = [];
  search = {
    endDt: moment().format("YYYY-MM-DD"),
    startDt: moment()
      .subtract(1, "year")
      .format("YYYY-MM-DD")
  };

  month_names = ["Jan", "Feb", "Mar", "Apr", "May", "Jun", "Jul", "Aug", "Sep", "Oct", "Nov", "Dec"];
  dateRanges = [];
  plants = [];

  excelData = [];
  constructor(
    private modalService: NgbModal,
    private formBuilder: FormBuilder,
    private alertService: AlertService,
    private passportService: PassportService,
    private plantCategoryService: PlantCategoryService,
    private plantRegisterService: PlantRegisterService,
    private commonProvinceService: CommonProvinceService,
    private commonCountryService: CommonCountryService,
    private router: Router
  ) {
    this.momentObj = moment;
  }

  ngOnInit() {
  }
  filter() {
    this.loading = true;
    this.plantRegisterService
      .filterLabStatus(this.search)
      .pipe(first())
      .subscribe(
        ({ data = [] }: any) => {
          let plantRegisters = data["filterResult"];
          let categories = data["categories"];
          let results = [];
          plantRegisters.forEach(plant => {
            if(!plant['m8_lab_id']) return;
            let lab_id = plant['m8_lab_id']['_id'];
            let lab_name = plant['m8_lab_id']['lab_no'];
            let date = plant['m8_lab_id']['createdAt'];
            let gs_no = plant['reg_gene_id'];
            if(plant['reg_plant_sci_name'] == null) return;
            if(plant['updatedAt'].split('T')[0] <  this.search.startDt) return;
            if(plant['updatedAt'].split('T')[0] >  this.search.endDt) return;
            let plant_type = "";
            categories.forEach(category => {
              if(category["_id"] ==  plant["reg_plant_sci_name"]["plant_category"]) {
                plant_type = category['categoryName_en']+"("+category['categoryName_th']+")";
                return;
              }
            })
            if(plant['lab_test_growth_percent'] != undefined){
              let item = {gs_no: gs_no, plant_type: plant_type, lab_name: lab_name, lab_test_growth_percent: plant['lab_test_growth_percent'], lab_test_strong_percent: plant['lab_test_strong_percent'], lab_test_alive_percent: plant['lab_test_alive_percent'], lab_seed_est_amount: plant['lab_seed_est_amount']};
              results.push(item);
            }           
          })
          this.resources = new DataTableResource<any>(results);
          this.resources.count().then(count => (this.count = count));
          this.reloadData({});
          this.loading = false;
          results.forEach(item => {
            this.excelData.push([item['lab_name'], item['gs_no'], item['plant_type'], item['lab_test_growth_percent'], item['lab_test_strong_percent'], item['lab_test_alive_percent'], item['lab_seed_est_amount']]);
          });
        },
        error => {
          this.loading = false;
        }
      );
  }


  reloadData(params: DataTableParams) {
    params = {offset: 0, limit: 15};
    this.resources.query(params).then(vals => {
      this.plants = vals;
    });
  }

  export(){
    // Create workbook and worksheet
    const workbook = new Workbook();
    const worksheet = workbook.addWorksheet("Lab Plant");
    const title = "รายการชนิดพืชตามรหัสระบบปฏิบัติการ " + this.search.startDt + " / " + this.search.endDt;

     // Add Row and formatting
     const titleRow = worksheet.addRow([title]);
     titleRow.font = { size: 16, bold: true };
     titleRow.alignment = { vertical: "middle", horizontal: "center" };

     worksheet.mergeCells("A1:G1");

     const headers = [
      "LAB",
      "GS No.",
      "ชนิดพืช",
      "%ความงอก",
      "%ความชื้น",
      "%ความมีชีวิต",
      "จำนวนเมล็ดโดยประมาณ (เมล็ด)"
    ];

    // Add Header Row
    const headerRow = worksheet.addRow(headers);

    // Cell Style : Fill and Border
    headerRow.eachCell((cell, number) => {
      cell.border = {
        top: { style: "thin" },
        left: { style: "thin" },
        bottom: { style: "thin" },
        right: { style: "thin" }
      };
      cell.font = { size: 14, bold: true };
      cell.alignment = { horizontal: "center", vertical: "middle" };
    });
    // worksheet.addRows(data);
    worksheet.getRow(2).height = 30;
    worksheet.getRow(1).height = 35;
    worksheet.getColumn(1).width = 18;
    worksheet.getColumn(2).width = 12;
    worksheet.getColumn(3).width = 18;
    worksheet.getColumn(4).width = 18;
    worksheet.getColumn(5).width = 18;
    worksheet.getColumn(6).width = 18;
    worksheet.getColumn(7).width = 40;
    this.excelData.forEach(d => {
      const row = worksheet.addRow(d);
      for (let col = 1; col <= 7; col++) {
        const cell = row.getCell(col);
        cell.alignment = { horizontal: "center" };
      }
    });
    workbook.xlsx.writeBuffer().then((data: any) => {
      const blob = new Blob([data], {
        type:
          "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet"
      });
      fs.saveAs(blob, "LabPlantReport.xlsx");
    });
    
  }
}
