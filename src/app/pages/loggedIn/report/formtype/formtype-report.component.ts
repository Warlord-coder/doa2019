import { Component, OnInit } from "@angular/core";
import { DataTableParams, DataTableResource } from "ngx-datatable-bootstrap4";
import { NgbModal } from "@ng-bootstrap/ng-bootstrap";
import { Router } from "@angular/router";
import * as _ from "lodash";
import { FormBuilder, FormGroup, Validators } from "@angular/forms";
import {
  AlertService,
  PassportService,
  PlantCategoryService,
  PlantRegisterService,
  WarehouseService
} from "../../../../services";
import { first } from "rxjs/operators";
import * as moment from "moment";
import { Workbook } from "exceljs";
import * as fs from "file-saver";

@Component({
  selector: "app-formtype-report-page",
  templateUrl: "./formtype-report.component.html"
})
export class NgbdFormTypeReportComponent implements OnInit {
  resources = new DataTableResource<any>([]);
  items: any[] = [];
  count = 0;
  tempItems: any[] = [];
  submitted = false;
  momentObj: any;
  file = null;
  loading = false;
  apiError = false;

  currentYear;
  thaiCurrentYear;

  startDt: any;
  endDt: any;
  startLabNo: any;
  endLabNo: any;
  startVolNo: any;
  endVolNo: any;

  fileName = "ExcelSheet.xlsx";
  categories = [];

  formTypes = [];
  plantTypes = [];
  departmentTypes = [];
  sourceProvinces = [];
  sourceCountries = [];
  search = {
    endDt: moment().format("YYYY-MM-DD"),
    startDt: moment()
      .subtract(1, "year")
      .format("YYYY-MM-DD"),
    formType: ""
  };

  month_names = ["Jan", "Feb", "Mar", "Apr", "May", "Jun", "Jul", "Aug", "Sep", "Oct", "Nov", "Dec"];
  dateRanges = [];
  plants = [];

  excelData = [];
  constructor(
    private modalService: NgbModal,
    private formBuilder: FormBuilder,
    private alertService: AlertService,
    private passportService: PassportService,
    private plantCategoryService: PlantCategoryService,
    private plantRegisterService: PlantRegisterService,
    private warehouseService: WarehouseService,
    private router: Router
  ) {
    this.momentObj = moment;
  }

  ngOnInit() {
    this.search.formType = "Register 1";
    this.initPlantCategory();
  }

  initPlantCategory() {
    this.plantCategoryService
      .getAll()
      .pipe(first())
      .subscribe(
        ({ data = [] }: any) => {
          this.categories = data;
        },
        error => {
          this.loading = false;
        }
      );
  }

  filter() {
    this.loading = true;
    this.warehouseService
      .getListAll()
      .pipe(first())
      .subscribe(
        ({ data = [] }: any) => {
          let results = [];
          this.excelData = [];
          let warehouses = _.filter(data, item => {
            if(item['volume'] == null) return;
            if(this.search.formType == 'Register 1' && item['volume']['form_type'] == 'Register 1/2' && item['passport']['reg_seed_sent_no'] == undefined ) {
              return item;
            } else if(this.search.formType == 'Register 2' && item['volume']['form_type'] == 'Register 1/2' && item['passport']['reg_seed_sent_no'] != undefined ) {
              return item;
            } else if(item['volume']['form_type'] == this.search.formType) {
              return item;
            }
            
          })
          this.startLabNo = 999999999;
          this.endLabNo = 0;
          this.startVolNo = 99999;
          this.endVolNo = 0;
          _.forEach(warehouses, warehouse => {
            if(warehouse['reg_plant_sci_name'] == null) return;
            if(warehouse['plant_gene_name'] == null) return;
            let category_id = warehouse['reg_plant_sci_name']['plant_category'];
            let category = _.filter(this.categories, category => {
              return category['_id'] == category_id;
            })
            let item;
            let gs_no = warehouse['reg_gene_id'];
            let reg_gene_id;
            if("reg_gene_id" in warehouse == false) reg_gene_id = '-';
            else reg_gene_id = warehouse.reg_gene_id;

            if(category.length == 0) return;
            if(gs_no == undefined) gs_no = '-';
            let lab_no = warehouse['m8_lab_id']['lab_no'];
            let lab_year = lab_no.split('/')[1];
            let lab_num = lab_no.split('/')[0].split('L')[1];
            let lab_id = parseInt(lab_year + lab_num);
              
            if(this.startLabNo > lab_id) this.startLabNo = lab_id;
            if(this.endLabNo < lab_id) this.endLabNo = lab_id;

            let vol_no = warehouse['volume']['volume_no']
            if(this.startVolNo > vol_no) this.startVolNo = vol_no;
            if(this.endVolNo < vol_no) this.endVolNo = vol_no;

            if(this.search.formType == "Register 1" || this.search.formType == "Register 4" || this.search.formType == "Register 2"){
              item = {reg_gene_id: reg_gene_id, volume_no: warehouse['volume']['volume_no'], plant_category: category[0]['categoryName_en'], plant_type: warehouse['plant_gene_name']['plantTypeName_en'], gs_no: gs_no};
              this.excelData.push([item['reg_gene_id'], item['plant_category'], item['plant_type'], item['volume_no'], item['gs_no']]);
            } else if(this.search.formType == "Register 3"){
              item = {reg_gene_id: gs_no, volume_no: warehouse['volume']['volume_no'], plant_category: category[0]['categoryName_en'], plant_type: warehouse['plant_gene_name']['plantTypeName_en'], department: warehouse['department_type'] + "-" + warehouse['department_name']};
              this.excelData.push([item['reg_gene_id'], item['plant_category'], item['plant_type'], item['volume_no'], item['gs_no']]);
            } 
            
            results.push(item);
          })
          
          this.resources = new DataTableResource<any>(results);
          this.resources.count().then(count => (this.count = count));
          this.reloadData({});
          this.loading = false;
        },
        error => {
          this.loading = false;
        }
      );
  }

  changeFormType(){
    this.resources = new DataTableResource<any>([]);
    this.resources.count().then(count => (this.count = count));
    this.reloadData({});
    //this.filter();
  }

  reloadData(params: DataTableParams) {
    params = {offset: 0, limit: 50};
    this.resources.query(params).then(vals => {
      this.plants = vals;
    });
  }

  export(){
    if(this.excelData.length == 0) return;
    // Create workbook and worksheet
    const workbook = new Workbook();
    const worksheet = workbook.addWorksheet("Warehouse Status");
    let title = "";

    let headers = [];
    let dateStr = '';
    if(this.search.startDt.split('-')[0] != this.search.endDt.split('-')[0]) {
      dateStr = this.search.startDt.split('-')[0] + ' - ' + this.search.endDt.split('-')[0];
    } else {
      dateStr = this.search.startDt.split('-')[0];
    }

    
    let lab_start_str = parseInt(this.startLabNo.toString().substring(4, 9)).toString() + '/' + this.startLabNo.toString().substring(0, 4);
    let lab_end_str = parseInt(this.endLabNo.toString().substring(4, 9)).toString() + '/' + this.endLabNo.toString().substring(0, 4);
    
    let volStr = parseInt(this.startVolNo).toString() + ' - ' + parseInt(this.endVolNo).toString();

    if(this.search.formType == "Register 1/2"){
      title = "ตารางสรุปพืชจากการสำรวจ/รวบรวมปี " + dateStr + ' ข้อมูลจาก L.' + lab_start_str + ' - ' + lab_end_str + ' (No. ' + volStr + ')';
      headers = ["หมายเลขประจำพันธุ์", "ชนิดพืช", "พันธุ์", "ชุดที่", "ออก DOA ใหม่"];
    }else if(this.search.formType == "Register 3"){
      title = "ตารางสรุปพืชจากการฝากเก็บปี " + dateStr + ' ข้อมูลจาก L.' + lab_start_str + ' - ' + lab_end_str + ' (No. ' + volStr + ')';
      headers = ["หมายเลขประจำพันธุ์", "ชนิดพืช", "พันธุ์", "ชุดที่", "หน่วยงาน"];
    } else {
      title = "ตารางสรุปพืชจากฟื้นฟูปี " + dateStr + ' ข้อมูลจาก L.' + lab_start_str + ' - ' + lab_end_str + ' (No. ' + volStr + ')';
      headers = ["หมายเลขประจำพันธุ์", "ชนิดพืช", "พันธุ์", "ชุดที่", "สถานะใหม่"];
    }
     // Add Row and formatting
     const titleRow = worksheet.addRow([title]);
     titleRow.font = { size: 14, bold: true };
     titleRow.alignment = { vertical: "middle", horizontal: "center" };

     worksheet.mergeCells("A1:E1");

    // Add Header Row
    const headerRow = worksheet.addRow(headers);

    // Cell Style : Fill and Border
    headerRow.eachCell((cell, number) => {
      cell.border = {
        top: { style: "thin" },
        left: { style: "thin" },
        bottom: { style: "thin" },
        right: { style: "thin" }
      };
      cell.font = { size: 14, bold: true };
      cell.alignment = { horizontal: "center", vertical: "middle" };
    });
    // worksheet.addRows(data);
    worksheet.getRow(2).height = 30;
    worksheet.getRow(1).height = 35;
    worksheet.getColumn(1).width = 22;
    worksheet.getColumn(2).width = 15;
    worksheet.getColumn(3).width = 40;
    worksheet.getColumn(4).width = 20;
    worksheet.getColumn(5).width = 30;

    this.excelData.forEach(d => {
      const row = worksheet.addRow(d);
      for (let col = 1; col <= 5; col++) {
        const cell = row.getCell(col);
        cell.alignment = { horizontal: "center" };
      }
    });
    workbook.xlsx.writeBuffer().then((data: any) => {
      const blob = new Blob([data], {
        type:
          "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet"
      });
      fs.saveAs(blob, "WarehouseReport.xlsx");
    });
    
  }
}
