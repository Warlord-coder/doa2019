import { Component, OnInit } from "@angular/core";
import { DataTableParams, DataTableResource } from "ngx-datatable-bootstrap4";
import { NgbModal } from "@ng-bootstrap/ng-bootstrap";
import { Router } from "@angular/router";
import * as _ from "lodash";
import { FormBuilder, FormGroup, Validators } from "@angular/forms";
import {
  AlertService,
  PassportService,
  PlantCategoryService,
  PlantRegisterService,
  RecoveryService
} from "../../../../services";
import { first } from "rxjs/operators";
import * as moment from "moment";
import { Workbook } from "exceljs";
import * as fs from "file-saver";

@Component({
  selector: "app-formtype-report-page",
  templateUrl: "./recovery-report.component.html"
})
export class NgbdRecoveryReportComponent implements OnInit {
  resources = new DataTableResource<any>([]);
  items: any[] = [];
  count = 0;
  tempItems: any[] = [];
  submitted = false;
  momentObj: any;
  file = null;
  loading = false;
  apiError = false;

  currentYear;
  thaiCurrentYear;

  startDt: any;
  endDt: any;

  categories = [];

  formTypes = [];
  plantTypes = [];
  departmentTypes = [];
  sourceProvinces = [];
  sourceCountries = [];
  search = {
    endDt: moment().format("YYYY-MM-DD"),
    startDt: moment()
      .subtract(1, "year")
      .format("YYYY-MM-DD"),
      reportType: ""
  };

  dateRanges = [];
  recoveries = [];

  excelData = [];
  constructor(
    private modalService: NgbModal,
    private formBuilder: FormBuilder,
    private alertService: AlertService,
    private passportService: PassportService,
    private plantCategoryService: PlantCategoryService,
    private plantRegisterService: PlantRegisterService,
    private recoveryService: RecoveryService,
    private router: Router
  ) {
    this.momentObj = moment;
  }

  ngOnInit() {
    this.search.reportType = "place";
  }


  filter() {
    this.loading = true;
    if(this.search.reportType == "place") {
      this.recoveryService
        .getByPlace({startDt: this.search.startDt, endDt: this.search.endDt})
        .pipe(first())
        .subscribe(
          ({ data = [] }: any) => {
            let results = [];
            this.excelData = [];
            let recoveries = _.filter(data, item => {
              if(item['m4recovery_plant_submittedAt'].split('T')[0] <  this.search.startDt) return;
              if(item['m4recovery_plant_submittedAt'].split('T')[0] >  this.search.endDt) return;
              let m_no = item.recovery_id.m_no;
              let recovery_place = item.recovery_place;

              if(recovery_place == undefined) return;
              let flag = false;
              results.forEach(result => {
                if(result['m_no'] === m_no && result['recovery_place'] === recovery_place) {
                  result['place_total']++;
                  if( this.requireFields(item)) result.require_place_total++;
                  if(item.recovery_rec_detail === 'มี') result.recovery_rec_yes_detail++;
                  else result.recovery_rec_no_detail++;

                  if(item.recovery_status === "complete" || item.recovery_status === "Complete") result.recovery_complete++;

                  flag = true;
                  return;
                }
              })

              if(flag == false) {
                let result = {year: parseInt(item.recovery_id.createdAt.split("-")[0] ) + 543, m_no: m_no, recovery_place: recovery_place, place_total: 1, require_place_total: 1, recovery_rec_yes_detail: 0,  recovery_rec_no_detail: 0, recovery_complete: 0};
                if( !this.requireFields(item)) result.require_place_total = 0;
                if(item.recovery_rec_detail === 'มี') result.recovery_rec_yes_detail = 1;
                if(item.recovery_status === "complete" || item.recovery_status === "Complete") result.recovery_complete = 1;
                else result.recovery_rec_no_detail = 1;

                results.push(result);
              }
            })
            this.recoveries = results;
            results.forEach(result => {
              this.excelData.push([result['year'], result['m_no'], result['recovery_place'], result['place_total'], result['require_place_total'], result['recovery_rec_yes_detail'], result['recovery_rec_no_detail'], result['recovery_complete']]);
            })

            this.resources = new DataTableResource<any>(results);
            this.resources.count().then(count => (this.count = count));
            this.reloadData({});
            this.loading = false;
          },
          error => {
            this.loading = false;
          }
      );
    } else {
      this.recoveryService
        .getByCategory({startDt: this.search.startDt, endDt: this.search.endDt})
        .pipe(first())
        .subscribe(
          ({ data = [] }: any) => {
            let results = [];
            this.excelData = [];
            let recoveries = _.filter(data, item => {
              if(item['m4recovery_plant_submittedAt'].split('T')[0] <  this.search.startDt) return;
              if(item['m4recovery_plant_submittedAt'].split('T')[0] >  this.search.endDt) return;
              if(item['reg_plant_sci_name'] == undefined) return;
              let m_no = item.recovery_id.m_no;

              let flag = false;
              let category_id = item['reg_plant_sci_name']['plant_category']['_id'];
              let category_en = item['reg_plant_sci_name']['plant_category']['categoryName_en'];
              let category_th = item['reg_plant_sci_name']['plant_category']['categoryName_th'];

              results.forEach(result => {
                if(result['m_no'] === m_no && result['category_id'] === category_id) {
                  result['category_total']++;
                  if( this.requireFields(item)) result.require_category_total++;
                  if(item.recovery_rec_detail === 'มี') result.recovery_rec_yes_detail++;
                  else result.recovery_rec_no_detail++;

                  if(item.recovery_status === "complete" || item.recovery_status === "Complete") result.recovery_complete++;

                  flag = true;
                  return;
                }
              })

              if(flag == false) {
                let result = {year: parseInt(item.recovery_id.createdAt.split("-")[0] ) + 543, m_no: m_no, category_id: category_id, category_name: category_en, category_total: 1, require_category_total: 1, recovery_rec_yes_detail: 0,  recovery_rec_no_detail: 0, recovery_complete: 0};
                if( !this.requireFields(item)) result.require_category_total = 0;
                if(item.recovery_rec_detail === 'มี') result.recovery_rec_yes_detail = 1;
                if(item.recovery_status === "complete" || item.recovery_status === "Complete") result.recovery_complete = 1;
                else result.recovery_rec_no_detail = 1;

                results.push(result);
              }
            })
            this.recoveries = results;
            results.forEach(result => {
              this.excelData.push([result['year'], result['m_no'], result['category_name'], result['category_total'], result['require_category_total'], result['recovery_rec_yes_detail'], result['recovery_rec_no_detail'], result['recovery_complete']]);
            })

            this.resources = new DataTableResource<any>(results);
            this.resources.count().then(count => (this.count = count));
            this.reloadData({});
            this.loading = false;
          },
          error => {
            this.loading = false;
          }
      );
    }
    
  }

  requireFields(item) {
    if(item.recovery_rec_recover_date == undefined) return false;

    return true;
  }
  changeReportType(){
    this.resources = new DataTableResource<any>([]);
    this.resources.count().then(count => (this.count = count));
    this.reloadData({});
    //this.filter();
  }

  reloadData(params: DataTableParams) {
    params = {offset: 0, limit: 50};
    this.resources.query(params).then(vals => {
      this.recoveries = vals;
    });
  }

  export(){
    // Create workbook and worksheet
    const workbook = new Workbook();
    const worksheet = workbook.addWorksheet("Withdraw Department Report");
    let title = "";

    let headers = [];
    if(this.search.reportType == "place"){
      title = "รายการ รอบวันที่ " + this.search.startDt + " - " + this.search.endDt;
      headers = ["ปี", "รหัสส่งฟื้นฟู", "สถานที่ฟื้นฟู", "จำนวนตัวอย่างพันธุ์ที่ส่งฟื้นฟู", "จำนวนตัวอย่างพันธุ์ที่ได้รับจากการฟื้นฟู", "ข้อมูลการประเมินเชื้อพันธุ์พืช (มี)", "ข้อมูลการประเมินเชื้อพันธุ์พืช (ไม่มี)", "จำนวนตัวอย่างพันธุ์ที่ส่งลงทะเบียน"];
    }else if(this.search.reportType == "reg_plant_type"){
      title = "รายการ รอบวันที่ " + this.search.startDt + " - " + this.search.endDt;
      headers = ["ปี", "รหัสส่งฟื้นฟู", "ชนิดพืช", "จำนวนตัวอย่างพันธุ์ที่ส่งฟื้นฟู", "จำนวนตัวอย่างพันธุ์ที่ได้รับจากการฟื้นฟู", "ข้อมูลการประเมินเชื้อพันธุ์พืช (มี)", "ข้อมูลการประเมินเชื้อพันธุ์พืช (ไม่มี)", "จำนวนตัวอย่างพันธุ์ที่ส่งลงทะเบียน"];
    } 
     // Add Row and formatting
     const titleRow = worksheet.addRow([title]);
     titleRow.font = { size: 14, bold: true };
     titleRow.alignment = { vertical: "middle", horizontal: "center" };

     
     worksheet.mergeCells("A1:H1");
    // Add Header Row
    const headerRow = worksheet.addRow(headers);

    // Cell Style : Fill and Border
    headerRow.eachCell((cell, number) => {
      cell.border = {
        top: { style: "thin" },
        left: { style: "thin" },
        bottom: { style: "thin" },
        right: { style: "thin" }
      };
      cell.font = { size: 12, bold: true };
      cell.alignment = { horizontal: "center", vertical: "middle" };
    });
    // worksheet.addRows(data);
    worksheet.getRow(2).height = 30;
    worksheet.getRow(1).height = 35;
    worksheet.getColumn(1).width = 8;
    worksheet.getColumn(2).width = 18;
    worksheet.getColumn(3).width = 18;
    worksheet.getColumn(4).width = 32;
    worksheet.getColumn(5).width = 42;
    worksheet.getColumn(6).width = 36;
    worksheet.getColumn(7).width = 36;
    worksheet.getColumn(8).width = 36;

    this.excelData.forEach(d => {
      const row = worksheet.addRow(d);
      for (let col = 1; col <= 8; col++) {
        const cell = row.getCell(col);
        cell.alignment = { horizontal: "center" };
      }
    });
    workbook.xlsx.writeBuffer().then((data: any) => {
      const blob = new Blob([data], {
        type:
          "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet"
      });
      fs.saveAs(blob, "RecoveryReport.xlsx");
    });
    
  }
}
