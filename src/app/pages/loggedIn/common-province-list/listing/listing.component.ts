import {Component} from '@angular/core';
import {DataTableParams, DataTableResource} from 'ngx-datatable-bootstrap4';
import {NgbModal} from '@ng-bootstrap/ng-bootstrap';
import {Router, ActivatedRoute} from '@angular/router';
import {first} from 'rxjs/operators';
import * as moment from 'moment';
import {toLower} from 'lodash';

import {AlertService, CommonProvinceService} from '../../../../services';

@Component({
  selector: 'app-listing-common-province-list-page',
  templateUrl: './listing.component.html',
})
export class NgbdCommonProvinceListComponent {
  resources = new DataTableResource<any>([]);
  items: any[] = [];
  count = 0;
  loading = false;
  momentObj: any;
  tempItems: any[] = [];
  deletedId = null;
  roles = [];
  constructor(private modalService: NgbModal,
              private alertService: AlertService,
              private commonProvinceService: CommonProvinceService,
              private router: Router,
              private route: ActivatedRoute) {
    this.onGetCommonProvinceListing();
    this.momentObj = moment;
  }

  ngOnInit() {
    this.route
      .data
      .subscribe(data => {
        this.roles = data['roles'];
      });
  }
  reloadData(params: DataTableParams) {
    this.resources.query(params).then(vals => {
      this.tempItems = vals;
      this.items = vals;
    });
  }

  onEdit({_id}) {
    this.router.navigate([`/admin/common/province-list/${_id}`]);
  }

  onDelete(item: any, deleteContent) {
    this.deletedId = item._id;
    this.modalService.open(deleteContent, {centered: true});
  }

  onSearch({target: {value: searchRole = ''} = {}}) {
    this.items = this.tempItems.filter(({provinceName_en}) => provinceName_en && toLower(provinceName_en).includes(toLower(searchRole)));
  }

  onGetCommonProvinceListing() {
    this.loading = true;
    this.commonProvinceService.getAll(true)
      .pipe(first())
      .subscribe(
        ({data = []}: any) => {
          this.loading = false;
          this.resources = new DataTableResource<any>(data);
          this.resources.count().then(count => this.count = count);
          this.reloadData({});
        },
        error => {
          this.loading = false;
          this.alertService.error(error);
        });
  }

  onDeleteCommonProvince() {
    this.loading = true;
    this.commonProvinceService.delete(this.deletedId)
      .pipe(first())
      .subscribe(
        ({data = []}: any) => {
          this.onGetCommonProvinceListing();
        },
        error => {
          this.loading = false;
          this.alertService.error(error);
        });
  }
  checkRole(index){
    if(localStorage.getItem('superAdmin') === "true") return true;
    if(localStorage.getItem('rolesId').indexOf(this.roles[index]) > -1) return true;
    return false;
  }
}
