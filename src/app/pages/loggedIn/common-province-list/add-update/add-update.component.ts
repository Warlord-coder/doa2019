import {Component, OnInit} from '@angular/core';
import {ActivatedRoute, Router} from '@angular/router';
import {FormBuilder, FormGroup, Validators} from '@angular/forms';
import {AlertService, CommonProvinceService} from '../../../../services';
import {getRoleResources} from '../../../../utils';
import {TreeviewConfig, TreeviewItem} from 'ngx-treeview';
import {first} from 'rxjs/operators';

@Component({
  selector: 'app-add-update-common-province-list-page',
  templateUrl: './add-update.component.html',
})
export class NgbdAddUpdateCommonProvinceListComponent implements OnInit {
  CommonProvinceListForm: FormGroup;
  submitted = false;
  roleResources: TreeviewItem[];
  loading = false;
  apiError = false;
  id = null;
  config = TreeviewConfig.create({
    hasAllCheckBox: true,
    hasFilter: true,
    hasCollapseExpand: true,
    decoupleChildFromParent: false,
    maxHeight: 800
  });


  constructor(private router: Router,
              private formBuilder: FormBuilder,
              private commonProvinceService: CommonProvinceService,
              private activeRoute: ActivatedRoute,
              private alertService: AlertService) {
  }

  ngOnInit() {
    this.roleResources = getRoleResources();
    this.CommonProvinceListForm = this.formBuilder.group({
      status: ['', Validators.required],
      provinceName_en: ['', Validators.required],
      provinceName_th: ['', Validators.required]
    });
    this.activeRoute.params.subscribe(params => {
      const {id} = params;
      this.id = id;
      if (id && id !== 'create') {
        this.onGetCommonProvinceInformation(id);
      }
    });
  }

  get f() {
    if (!this.apiError && this.submitted && !this.CommonProvinceListForm.invalid) {
      this.alertService.error(null);
    }

    return this.CommonProvinceListForm.controls;
  }

  onSubmit() {
    this.submitted = true;
    if (this.CommonProvinceListForm.invalid) {
      this.alertService.error('Some fields are required. Please fill those fields');
      return;
    }

    this.commonProvinceService.createOrUpdate({...this.CommonProvinceListForm.value}, this.id)
      .pipe(first())
      .subscribe(
        () => {
          this.loading = false;
          this.router.navigate(['admin/common/province-list/listing']);
        },
        error => {
          this.loading = false;
          this.apiError = true;
          this.alertService.error(error);
        });
  }

  onSelectedChange($event) {
    console.log('Select', $event);
  }

  onGetCommonProvinceInformation(id: string) {
    this.loading = true;
    this.commonProvinceService.getSpecific(id)
      .pipe(first())
      .subscribe(
        ({data = {}}: any) => {
          const {status, provinceName_en, provinceName_th} = data;
          this.CommonProvinceListForm.get('status').setValue(status);
          this.CommonProvinceListForm.get('provinceName_en').setValue(provinceName_en);
          this.CommonProvinceListForm.get('provinceName_th').setValue(provinceName_th);
          this.loading = false;
        },
        error => {
          this.loading = false;
          this.apiError = true;
          this.alertService.error(error);
        });
  }

  onCancel() {
    this.router.navigate(['admin/common/province-list/listing']);
  }
}
