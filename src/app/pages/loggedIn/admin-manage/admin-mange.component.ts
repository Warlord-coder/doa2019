import {Component} from '@angular/core';
import {Router} from '@angular/router';

@Component({
  selector: 'app-admin-manage-page',
  templateUrl: './admin-manage.component.html',
})
export class NgbdAdminManageComponent {
  constructor(private router: Router) {
  }

  onNavigate(route) {
    this.router.navigate([route]);
  }
}
