import {Component, OnInit} from '@angular/core';
import {ActivatedRoute, Router} from '@angular/router';
import {FormBuilder, FormGroup, Validators} from '@angular/forms';
import {
  AlertService,
  CommonProvinceService, LabAlertFormService, LabService, PassportService,
  PlantCategoryService, PlantCharacterService,
  PlantGroupsService,
  PlantGroupTypesService,
  PlantRegisterService, PlantTypeService, VolumeService
} from '../../../../services';
import {getRoleResources} from '../../../../utils';
import {identity, pickBy} from 'lodash';
import {TreeviewItem} from 'ngx-treeview';
import {first} from 'rxjs/operators';
import {CommonCountryService} from '../../../../services/common-country.service';
import {CommonAumperService} from '../../../../services/common-aumper.service';
import {environment} from '../../../../../environments/environment';

@Component({
  selector: 'app-add-update-plant-register-page',
  templateUrl: './add-update.component.html',
})
export class NgbdAddUpdateLabAlertLabComponent implements OnInit {
  plantGroupTypeForm: FormGroup;
  submitted = false;
  loading = false;
  file = null;
  id = null;
  categoryList = [];
  groupTypeList = [];
  groupList = [];
  provinceList = [];
  plantTypes = [];
  aumperList = [];
  countryList = [];
  volumeInfo = null;
  disabled = false;
  passportInfo = null;
  reg_gene_id = null;
  reg_seed_no = null;
  reg_character_attached_file = null;
  apiError = false;
  labInfo = null;
  plantsListing = [];
  plantInfo: any = null;
  labId = null;
  regCharacterAttachedFile = null;
  selectedPlant = null;
  volumeId = null;
  roleResources: TreeviewItem[];
  growth = '-';
  config = {
    class: 'autocomplete', max: 5, placeholder: 'Type to search',
    sourceField: ['plantTypeName_sci_en']
  };

  constructor(private router: Router,
              private formBuilder: FormBuilder,
              private activeRoute: ActivatedRoute,
              private plantCategoryService: PlantCategoryService,
              private plantGroupTypesService: PlantGroupTypesService,
              private plantGroupsService: PlantGroupsService,
              private plantCharacterService: PlantCharacterService,
              private volumeService: VolumeService,
              private passportService: PassportService,
              private labAlertFormService: LabAlertFormService,
              private plantRegisterService: PlantRegisterService,
              private countryService: CommonCountryService,
              private plantTypeService: PlantTypeService,
              private labService: LabService,
              private provinceService: CommonProvinceService,
              private aumperService: CommonAumperService,
              private alertService: AlertService) {
    this.onGetListing('plantCharacterService', 'plantsListing');
    this.onGetListing('plantCategoryService', 'categoryList');
    this.onGetListing('plantGroupTypesService', 'groupTypeList');
    this.onGetListing('plantGroupsService', 'groupList');
    this.onGetListing('countryService', 'countryList');
    this.onGetListing('aumperService', 'aumperList');
    this.onGetListing('plantTypeService', 'plantTypes');
    this.onGetListing('provinceService', 'provinceList');
  }

  ngOnInit() {
    this.roleResources = getRoleResources();
    this.plantGroupTypeForm = this.formBuilder.group({
      //selection_alert: [''],
      alert_test_amount: [''],

      plant_gene_name: [''],
      reg_gene_type: [''],
      reg_gene_category: [''],
      reg_seed_collect_date: [''],
      selfing_input: [''],
      selfing_dropdown: [''],
      oop_input: [''],
      oop_dropdown: [''],
      other_input: [''],
      other_dropdown: [''],
      department_type: [''],
      reg_check_seed_ref: [''],
      reg_check_molecular: [''],
      department_sub_type: [''],
      department_name: [''],
      source_country: [''],
      source_country_1: [''],
      source_province: [''],
      source_district: [''],
      source_address: [''],
      lat: [''],
      long: [''],
      source_by_department: [''],
      reg_seed_collect_person: [''],
      reg_seed_owner: [''],
      reg_seed_sender: [''],
      reg_character_brief: [''],
      reg_character_for_record: [''],
      reg_sent_lab_date: [''],
      reg_remark: [''],
      reg_plant_sci_name: [''],
      reg_plant_alter_name: [[]],

      lab_test_check_seed_date: [''],
      lab_test_check_seed_name: [''],
      wh_batch_moisture_percent: ['', Validators.required],
      lab_test_moisture_date: ['', Validators.required],
      lab_test_moisture_method: ['', Validators.required],
      lab_test_moisture_amount: [''],
      lab_test_moisture_person: ['', Validators.required],
      lab_test_growth_percent: ['', Validators.required],
      lab_test_growth_date: ['', Validators.required],
      lab_test_growth_method: ['', Validators.required],
      lab_test_growth_amount: ['', Validators.required],
      lab_test_growth_person: ['', Validators.required],
      lab_test_strong_percent: ['', Validators.required],
      lab_test_strong_date: ['', Validators.required],
      lab_test_strong_method: ['', Validators.required],
      lab_test_strong_amount: ['', Validators.required],
      lab_test_strong_person: ['', Validators.required],
      lab_test_alive_percent: ['', Validators.required],
      lab_test_alive_date: ['', Validators.required],
      lab_test_alive_method: ['', Validators.required],
      lab_test_alive_amount: ['', Validators.required],
      lab_test_alive_person: ['', Validators.required],
      lab_seed_rest: ['มี', Validators.required],
      lab_seed_rest_fix_method: [''],
      lab_seed_rest_fix_date: [''],
      lab_seed_amount: [''],
      lab_10seed_weight: [''],
      //lab_test_plant_amount: ['', Validators.required],
      lab_seed_est_amount: [''],
      lab_plaint_character: [''],
      lab_admin_name: ['', Validators.required],
      lab_remark: [''],
    });
    this.activeRoute.params.subscribe(params => {
      const {id} = params;
      this.id = id;
      this.onGetLabAlertFormInfo();
      this.getGrowth();
    });
  }

  get f() {
    if (!this.apiError && this.submitted && !this.plantGroupTypeForm.invalid) {
      this.alertService.error(null);
    }

    return this.plantGroupTypeForm.controls;
  }

  onSubmit() {
   
    // this.submitted = true;
    if (this.plantGroupTypeForm.invalid) {
      // this.alertService.error('Some fields are required. Please fill those fields');
      // return;
    }

    if (!this.selectedPlant) {
      // this.alertService.error('Please Select Reg Plant Sci Name');
      // return;
    }

    const requestObj = {};
    const formValues = this.plantGroupTypeForm.value;
    const KEYS = ['lab_test_check_seed_date', 'lab_test_check_seed_name',
      'wh_batch_moisture_percent', 'lab_test_moisture_date', 'lab_test_moisture_method',
      'lab_test_moisture_amount', 'lab_test_moisture_person', 'lab_test_growth_percent',
      'lab_test_growth_date', 'lab_test_growth_method', 'lab_test_growth_amount', 'lab_test_growth_person',
      'lab_test_strong_percent', 'lab_test_strong_date', 'lab_test_strong_method', 'lab_test_strong_amount',
      'lab_test_strong_person', 'lab_test_alive_percent', 'lab_test_alive_date', 'lab_test_alive_method',
      'lab_test_alive_amount', 'lab_test_alive_person', 'lab_seed_rest', 'lab_seed_rest_fix_method',
      'lab_seed_rest_fix_date', 'lab_plaint_character',
      'lab_admin_name', 'lab_remark', 'alert_test_amount','lab_seed_amount','lab_10seed_weight'];
    Object.keys(formValues).forEach(key => {
      if (KEYS.includes(key)) {
        requestObj[key] = formValues[key];
      }
    });

    this.labAlertFormService.update(pickBy({
      ...requestObj
    }, identity), this.id)
      .pipe(first())
      .subscribe(
        () => {
          this.loading = false;
          this.onCancel();
        },
        error => {
          this.loading = false;
          this.apiError = true;
          this.alertService.error(error);
        });
  }

  onChangeCheckbox({target: {checked = ''}}, key) {
    if (checked) {
      this.plantGroupTypeForm.get(key).setValue('-');
      return;
    }

    this.plantGroupTypeForm.get(key).setValue('');
  }

  onSelectedChange($event) {
    //console.log('Select', $event);
  }

  onFilterChange($event) {
    //console.log('Filter Change', $event);
  }

  onGetListing(serviceName, key) {
    this.loading = true;
    this[serviceName].getAll()
      .pipe(first())
      .subscribe(
        ({data = []}: any) => {
          this.loading = false;
          this[key] = data;
        },
        error => {
          this.loading = false;
          this.alertService.error(error);
        });
  }

  getGrowth(){
    this.labAlertFormService.getSpecificGrowth(this.id)
      .pipe(first())
      .subscribe(
        ({data = []}: any) => {
          this.growth = data.warehouse['lab_test_growth_percent'];
        },error => {
          this.loading = false;
          this.alertService.error(error);
        })
  }

  onGetLabAlertFormInfo() {
    this.loading = true;

    this.labAlertFormService.getSpecific(this.id)
      .pipe(first())
      .subscribe(
        ({data = []}: any) => {
          
          const {reg_gene_id, reg_seed_no,alertFormStatus, reg_plant_sci_name, reg_character_attached_file, m8_lab_status, volume} = data;
          this.plantInfo = data;
          Object.keys(data).forEach(key => {
            if (this.plantGroupTypeForm.get(key)) {
              this.plantGroupTypeForm.get(key).setValue(data[key]);
            }
          });
          if(alertFormStatus === 'complete'){
            this.disabled = true;
          }
          this.reg_gene_id = reg_gene_id;
          this.onGetVolumeInfo(volume);
          this.reg_seed_no = reg_seed_no;
          this.reg_character_attached_file = reg_character_attached_file;
          this.findPlantSciName(data);
          this.loading = false;
        },
        error => {
          this.loading = false;
          this.alertService.error(error);
        });
  }

  onGetVolumeInfo(id) {
    this.loading = true;
    this.volumeService.getSpecific(id)
      .pipe(first())
      .subscribe(
        ({data = []}: any) => {
          this.loading = false;
          const {passport_no} = data;
          this.volumeInfo = data;
          this.onGetPassportInfo(passport_no);
        },
        error => {
          this.loading = false;
          this.alertService.error(error);
        });
  }

  onGetPassportInfo(id) {
    this.loading = true;
    this.passportService.getSpecific(id)
      .pipe(first())
      .subscribe(
        ({data = []}: any) => {
          this.loading = false;
          this.passportInfo = data;
        },
        error => {
          this.loading = false;
          this.alertService.error(error);
        });
  }

  getAttachment(basePath, name) {
    if (name) {
      return `${environment.endpoint}/${basePath}/${name}`;
    }

    return null;
  }

  onSelect(plant) {
    this.selectedPlant = plant;
    this.plantGroupTypeForm.get('reg_plant_sci_name').setValue(plant.genus + ' ' + plant.species);
  }


  findPlantSciName({reg_plant_sci_name}) {
    if (!reg_plant_sci_name) {
      return;
    }
    for (const plant of  this.plantsListing) {
      if (reg_plant_sci_name === plant._id) {
        this.selectedPlant = plant;
        this.plantGroupTypeForm.get('reg_plant_sci_name').setValue(plant.genus + ' ' + plant.species);
      }
    }

  }

  onCancel() {
    this.router.navigate([`/admin/labs/alert-plants/${this.plantInfo.labId._id}`]);
  }

  onGetLabInfo(lab) {
    this.loading = true;
    this.labService.getSpecific(lab)
      .pipe(first())
      .subscribe(
        ({data = {}}: any) => {
          this.loading = false;
          this.labInfo = data;
        },
        error => {
          this.loading = false;
          this.alertService.error(error);
          return this.router.navigate([`/admin/labs/listing`]);
        });
  }
}
