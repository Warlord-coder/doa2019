import {Component} from '@angular/core';
import {DataTableParams, DataTableResource} from 'ngx-datatable-bootstrap4';
import {NgbModal} from '@ng-bootstrap/ng-bootstrap';
import {ActivatedRoute, Router} from '@angular/router';
import {toLower} from 'lodash';
import {FormBuilder, FormGroup} from '@angular/forms';
// @ts-ignore
import {AlertService, LabService,WarehouseService} from '../../../../services';
import {first} from 'rxjs/operators';
import * as moment from 'moment';

@Component({
  selector: 'app-listing-common-country-page',
  templateUrl: './listing.component.html',
})
export class NgbdLabAlertListPendingComponent {
  resources = new DataTableResource<any>([]);
  items: any[] = [];
  count = 0;
  labInfo = null;
  tempItems: any[] = [];
  submitted = false;
  id = null;
  momentObj: any;
  file = null;
  loading = false;
  selectedPlantsForSubmit: any = [];
  apiError = false;

  constructor(private modalService: NgbModal,
              private formBuilder: FormBuilder,
              private whservice: WarehouseService,
              private activeRoute: ActivatedRoute,
              private labService: LabService,
              private alertService: AlertService,
              private router: Router) {
    this.onGetPendingAlertListing();
    this.momentObj = moment;
  }

  reloadData(params: DataTableParams) {
    this.resources.query(params).then(vals => {
      this.items = vals;
      this.tempItems = this.items;
    });
    this.activeRoute.params.subscribe(params => {
      const {labId} = params;
      this.id = labId;
      this.onGetSpecificLabInfo();
    });
  }

  onSearch({target: {value: searchRole = ''} = {}}) {
    this.items = this.tempItems.filter(({passport: {passport_no = ''} = {}}) => passport_no && toLower(passport_no).includes(toLower(searchRole)));
  }

  onGetPendingAlertListing() {
    this.loading = true;
    this.whservice.getAlertList()
      .pipe(first())
      .subscribe(
        ({data = []}: any) => {
          this.resources = new DataTableResource<any>(data);
          this.resources.count().then(count => this.count = count);
          this.reloadData({});
          this.loading = false;
        },
        error => {
          this.loading = false;
          this.alertService.error(error);
        });
  }

  onGetSpecificLabInfo() {
    this.loading = true;
    this.labService.getSpecific(this.id)
      .pipe(first())
      .subscribe(
        ({data = []}: any) => {
          this.labInfo = data;
          this.loading = false;
        },
        error => {
          this.loading = false;
          this.alertService.error(error);
        });
  }


  onChangeItemSelect({_id}) {
    if (this.selectedPlantsForSubmit.includes(_id)) {
      this.selectedPlantsForSubmit = this.selectedPlantsForSubmit.filter(id => id !== _id);
      return;
    }

    this.selectedPlantsForSubmit = [...this.selectedPlantsForSubmit, _id];
  }

  onSubmitPlants() {
    if (!this.selectedPlantsForSubmit || !this.selectedPlantsForSubmit.length) {
      return;
    }
    this.loading = true;
    this.labService.submitAlertPlant(this.id, this.selectedPlantsForSubmit)
      .pipe(first())
      .subscribe(
        ({data = {}}: any) => {
          this.selectedPlantsForSubmit = [];
          this.router.navigate([`admin/labs/alert-plants/${this.id}`]);
        },
        error => {
          this.loading = false;
          this.alertService.error(error);
        });
  }

  getStockInfo(item) {
    const {
      selfing_input, selfing_dropdown,
      oop_input, oop_dropdown,
      other_input, other_dropdown
    } = item;
    const stock = {
      isSelfing: selfing_input && selfing_dropdown,
      isOOP: oop_input && oop_dropdown,
      isOther: other_input && other_dropdown,
    };
    if (stock.isSelfing) {
      return `Self ${selfing_input} ${selfing_dropdown}`;
    }
    if (stock.isOOP) {
      return `OP ${oop_input} ${oop_dropdown}`;
    }
    if (stock.isOther) {
      return `Other ${other_input} ${other_dropdown}`;

    }
  }
}
