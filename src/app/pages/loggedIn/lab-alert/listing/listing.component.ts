import {Component, OnInit} from '@angular/core';
import {DataTableParams, DataTableResource} from 'ngx-datatable-bootstrap4';
import {NgbModal} from '@ng-bootstrap/ng-bootstrap';
import {Router, ActivatedRoute} from '@angular/router';
import {first} from 'rxjs/operators';
import {toLower, max} from 'lodash';
import * as moment from 'moment';

import {AlertService, LabService, LabAlertFormService} from '../../../../services';
import {FormBuilder, FormGroup, Validators} from '@angular/forms';

@Component({
  selector: 'app-listing-plant-lab-page',
  templateUrl: './listing.component.html',
})
export class NgbdListingLabAlertComponent {
  resources = new DataTableResource<any>([]);
  createLabForm: FormGroup;
  createLabAlertForm: FormGroup;
  items: any[] = [];
  tempItems: any[] = [];
  count = 0;
  loading = false;
  momentObj: any;
  apiError = null;
  submitted = false;
  roles = [];
  plantRegisters = [];
  constructor(private modalService: NgbModal,
              private alertService: AlertService,
              private formBuilder: FormBuilder,
              private labService: LabService,
              private labAlertService: LabAlertFormService,
              private router: Router,
              private route: ActivatedRoute) {
    this.onGetPlantListing();
    
    this.momentObj = moment;
  }
  ngOnInit(): void {	
    this.route
      .data
      .subscribe(data => {
        this.roles = data['roles'];
      });
    this.createLabForm = this.formBuilder.group({	
      reg_seed_receive: ['', Validators.required],	
      volume_no: ['', Validators.required],	
      stock_type: ['', Validators.required]	
    });	
    this.createLabAlertForm = this.formBuilder.group({	
      notification_date: ['', Validators.required],	
    });	
  }
  reloadData(params: DataTableParams) {
    this.resources.query(params).then(vals => {
      this.tempItems = vals;
      this.items = vals;
    });
  }

  onGetPlantListing() {
    this.labAlertService.getTotalAlert()
          .pipe(first())
          .subscribe(       
            ({data = []}: any) => {
              this.plantRegisters = data.filter(item => {
                if(item['labId'] != undefined) return true;
                else return false;
              })
              this.onGetLabAlertListing();
            },
            error => {
              this.loading = false;
              this.alertService.error(error);
            });
  }
  onDetails({_id}) {
    this.router.navigate([`/admin/labs/alert-plants/${_id}`]);
  }

  get f() {
    if (!this.apiError && this.submitted && !this.createLabForm.invalid) {
      this.alertService.error(null);
    }

    return this.createLabForm.controls;
  }

  onDelete(item: object, deleteContent) {
    this.modalService.open(deleteContent, {centered: true});
  }

  onSearch({target: {value: searchRole = ''} = {}}) {
    this.items = this.tempItems.filter(({lab_no}) => lab_no && toLower(lab_no).includes(toLower(searchRole)));
  }

  onGetLabAlertListing() {
    this.loading = true;
    this.labService.getAllM8(true)
      .pipe(first())
      .subscribe(
        ({data = []}: any) => {
          this.loading = false;
          let values = [];
          for(var i = 0; i < data.length; i++) {
            let lab_id = data[i]['_id'];
            values = [];
            let plantRegisters = this.plantRegisters;
            for(var j = 0; j < plantRegisters.length; j++) {
              if(plantRegisters[j]['labId'] == lab_id) values.push(plantRegisters[j]['updatedAt']);
            }
            data[i].updatedAt = max(values);
          }
          this.resources = new DataTableResource<any>(data);
          this.resources.count().then(count => this.count = count);
          this.reloadData({});
        },
        error => {
          this.loading = false;
          this.alertService.error(error);
        });
  }
  	
  onSubmit(isAlert = false) {	
    this.submitted = true;	
    this.apiError = false;	
    if (isAlert ? this.createLabAlertForm.invalid : this.createLabForm.invalid) {	
      this.alertService.error('Some fields are required. Please fill those fields');	
      return;	
    }	
    this.alertService.reset();	
    this.loading = true;	
    this.modalService.dismissAll();	
    this.labService.createOrUpdate(isAlert ? {...this.createLabAlertForm.value, is_alert: true} : this.createLabForm.value, null)	
      .pipe(first())	
      .subscribe(	
        ({data = {}}: any) => {	
          const {volume_no, _id} = data;	
          if (isAlert) {	
            return this.router.navigate([`/admin/labs/pending-alert-plants/${_id}`]);	
          }	
          this.router.navigate([`/admin/labs/${volume_no}/${_id}`]);	
          this.loading = false;	
        },	
        error => {	
          this.createLabForm.reset();	
          this.submitted = false;	
          this.loading = false;	
          this.apiError = true;	
          this.alertService.error(error);	
        });	
  }	
  onCreate(createContent) {	
    this.createLabForm.reset();	
    this.modalService.open(createContent, {centered: true});	
  }

  checkRole(index){
    if(localStorage.getItem('superAdmin') === "true") return true;
    if(localStorage.getItem('rolesId').indexOf(this.roles[index]) > -1) return true;
    return false;
  }
}
