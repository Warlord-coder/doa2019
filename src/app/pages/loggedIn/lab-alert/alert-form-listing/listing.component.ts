import {Component, OnInit} from '@angular/core';
import {DataTableParams, DataTableResource} from 'ngx-datatable-bootstrap4';
import {NgbModal} from '@ng-bootstrap/ng-bootstrap';
import {ActivatedRoute, Router} from '@angular/router';
import {toLower} from 'lodash';
import {FormBuilder, FormGroup, Validators} from '@angular/forms';
// @ts-ignore
import {AlertService, LabAlertFormService, LabService, RecoveryService} from '../../../../services';
import {first} from 'rxjs/operators';
import * as moment from 'moment';
import {FORM_REQUIRED_KEYS, isFormSubmissionError} from '../../../../utils';

@Component({
  selector: 'app-listing-common-country-page',
  templateUrl: './listing.component.html',
})
export class NgbdRegisterAlertFormListPendingCompleteComponent implements OnInit {
  resources = new DataTableResource<any>([]);
  items: any[] = [];
  count = 0;
  resources1= new DataTableResource<any>([]);
  items1: any[] = [];
  count1 = 0;
  resources2 = new DataTableResource<any>([]);
  items2: any[] = [];
  count2 = 0;
  registerPassportForm: FormGroup;
  tempItems: any[] = [];
  tempItems1: any[] = [];
  tempItems2: any[] = [];
  submitted = false;
  id = null;
  momentObj: any;
  file = null;
  labInfo = null;
  loading = false;
  selectedPlantsForSubmit: any = [];
  selectedPlantsForSubmit1: any = [];
  apiError = false;
  currentStatus = null;
  ignorFields = ['lab_plaint_character','lab_seed_amount','lab_10seed_weight','lab_seed_est_amount'];
  constructor(private modalService: NgbModal,
              private formBuilder: FormBuilder,
              private activeRoute: ActivatedRoute,
              private labService: LabService,
              private alertService: AlertService,
              private labAlertFormService: LabAlertFormService,
              private recoveryService: RecoveryService,
              private router: Router) {
    this.momentObj = moment;
  }

  ngOnInit() {
    this.activeRoute.params.subscribe(params => {
      const {labId} = params;
      this.id = labId;
      this.onGetSpecificLab();
      this.onGetAlertListing('draft');
      this.onGetAlertListing1('pending');
      this.onGetAlertListing2('complete');
    });
  }

  get f() {
    if (!this.apiError && this.submitted && !this.registerPassportForm.invalid) {
      this.alertService.error(null);
    }

    return this.registerPassportForm.controls;
  }

  reloadData(params: DataTableParams) {
    this.resources.query(params).then(vals => {
      this.items = vals;
      this.tempItems = this.items;
    });
  }
  reloadData1(params: DataTableParams) {
    this.resources1.query(params).then(vals => {
      this.items1 = vals;
      this.tempItems1 = this.items2;
    });
  }
  reloadData2(params: DataTableParams) {
    this.resources2.query(params).then(vals => {
      this.items2 = vals;
      this.tempItems2 = this.items2;
    });
  }

  onSearch({target: {value: searchRole = ''} = {}}) {
    this.items = this.tempItems.filter(({passport: {passport_no = ''} = {}}) => passport_no && toLower(passport_no).includes(toLower(searchRole)));
  }

  onGetAlertListing(status) {
    this.loading = true;
    this.currentStatus = status;
    this.labAlertFormService.getAll(this.id, status)
      .pipe(first())
      .subscribe(
        ({data = []}: any) => {
          this.resources = new DataTableResource<any>(data);
          this.resources.count().then(count => this.count = count);
          this.reloadData({});
          this.loading = false;
        },
        error => {
          this.loading = false;
          this.alertService.error(error);
        });
  }
  onGetAlertListing1(status) {
    this.loading = true;
    this.currentStatus = status;
    this.labAlertFormService.getAll(this.id, status)
      .pipe(first())
      .subscribe(
        ({data = []}: any) => {
          this.resources1 = new DataTableResource<any>(data);
          this.resources1.count().then(count => this.count1 = count);
          this.reloadData1({});
          this.loading = false;
        },
        error => {
          this.loading = false;
          this.alertService.error(error);
        });
  }
  onGetAlertListing2(status) {
    this.loading = true;
    this.currentStatus = status;
    this.labAlertFormService.getAll(this.id, status)
      .pipe(first())
      .subscribe(
        ({data = []}: any) => {
          this.resources2 = new DataTableResource<any>(data);
          this.resources2.count().then(count => this.count2 = count);
          this.reloadData2({});
          this.loading = false;
        },
        error => {
          this.loading = false;
          this.alertService.error(error);
        });
  }

  onGetSpecificLab() {
    this.loading = true;
    this.labService.getSpecific(this.id)
      .pipe(first())
      .subscribe(
        ({data = []}: any) => {
          this.labInfo = data;
          this.loading = false;
        },
        error => {
          this.loading = false;
          this.alertService.error(error);
        });
  }

  onChangeItemSelect({_id}) {
    if (this.selectedPlantsForSubmit.includes(_id)) {
      this.selectedPlantsForSubmit = this.selectedPlantsForSubmit.filter(id => id !== _id);
      return;
    }

    this.selectedPlantsForSubmit = [...this.selectedPlantsForSubmit, _id];
  }
  onChangeItemSelectComplete({_id}) {
    if (this.selectedPlantsForSubmit1.includes(_id)) {
      this.selectedPlantsForSubmit1 = this.selectedPlantsForSubmit1.filter(id => id !== _id);
      return;
    }

    this.selectedPlantsForSubmit1 = [...this.selectedPlantsForSubmit1, _id];
  }

  onSubmitPlants() {
    if (!this.selectedPlantsForSubmit || !this.selectedPlantsForSubmit.length) {
      return;
    }
    
    this.loading = true;
    // var filteredAry = FORM_REQUIRED_KEYS.ALERT_LAB_FORM.filter((e)=> { return this.ignorFields.indexOf(e) == -1 })
 
    // for (const item of this.items) {
    //   const {_id}: any = item;
    //   if (this.selectedPlantsForSubmit.includes(_id)) {
    //       const record = isFormSubmissionError(item, filteredAry);
    //       if (record) {
    //         this.loading = false;
    //         return this.alertService.error(`Validation Failed for record Reg Gene Id: ${record.reg_gene_id}`);
    //       }
    //   }
    // }
    this.loading = true;
    this.labAlertFormService.submitToModule9(this.selectedPlantsForSubmit)
      .pipe(first())
      .subscribe(
        ({data = {}}: any) => {
          this.selectedPlantsForSubmit = [];
          this.onGetAlertListing('draft');
        },
        error => {
          this.loading = false;
          this.alertService.error(error);
        });
  }
  onDetails1({_id}) {	
    this.labAlertFormService.getSpecificId(_id)	
    .pipe(first())	
    .subscribe(	
      ({data = {}}: any) => {	
        this.router.navigate([`/admin/warehouse/form-new-plant/${data.warehouse}`]);	
      },	
      error => {	
        this.loading = false;	
        this.alertService.error(error);	
      });	
    	
  }
  onDetails2({_id}) {	
    this.labAlertFormService.getSpecificId(_id)	
    .pipe(first())	
    .subscribe(	
      ({data = {}}: any) => {	
        this.router.navigate([`/admin/labs/alert-plants/details/${_id}`]);	
      },	
      error => {	
        this.loading = false;	
        this.alertService.error(error);	
      });	
    	
  }
  onSubmitPlantsComplete() {
    if (!this.selectedPlantsForSubmit1 || !this.selectedPlantsForSubmit1.length) {
      return;
    }
    //var filteredAry = FORM_REQUIRED_KEYS.ALERT_LAB_FORM.filter((e)=> { return this.ignorFields.indexOf(e) == -1 })
 
    this.loading = true;
    for (const item of this.items1) {
      const {_id}: any = item;
      if (this.selectedPlantsForSubmit1.includes(_id)) {
        item['lab_plaint_character'] = (item['lab_plaint_character']!=null ?item['lab_plaint_character']:'-')
        item['lab_10seed_weight'] = (item['lab_10seed_weight']!=null ?item['lab_10seed_weight']:'-')
        item['lab_seed_amount'] = (item['lab_seed_amount']!=null ?item['lab_seed_amount']:'-')
        item['lab_seed_rest_fix_method'] = (item['lab_seed_rest_fix_method']!=null ?item['lab_seed_rest_fix_method']:'-')
        item['lab_test_check_seed_date'] = (item['lab_test_check_seed_date']!=null ?item['lab_test_check_seed_date']:'-')
        item['lab_test_check_seed_name'] = (item['lab_test_check_seed_name']!=null ?item['lab_test_check_seed_name']:'-')
        const record = isFormSubmissionError(item, FORM_REQUIRED_KEYS.ALERT_LAB_FORM);
        if (record) {
          this.loading = false;
          return this.alertService.error(`Validation Failed for record Reg Gene Id: ${record.reg_gene_id}`);
        }
      }
    }
    this.loading = true;
    this.labAlertFormService.submitToModule9Complete(this.selectedPlantsForSubmit1)
      .pipe(first())
      .subscribe(
        ({data = {}}: any) => {
          this.selectedPlantsForSubmit1 = [];
          this.onGetAlertListing1('pending');
          this.onGetAlertListing2('complete');
        },
        error => {
          this.loading = false;
          this.alertService.error(error);
        });
  }

  onDetails({_id}) {
    this.router.navigate([`/admin/labs/alert-plants/details/${_id}`]);
  }

  getStockInfo(item: any) {
    const {
      selfing_input, selfing_dropdown,
      oop_input, oop_dropdown,
      other_input, other_dropdown
    } = item;
    const stock = {
      isSelfing: selfing_input && selfing_dropdown,
      isOOP: oop_input && oop_dropdown,
      isOther: other_input && other_dropdown,
    };
    if (stock.isSelfing) {
      return `Self ${selfing_input} ${selfing_dropdown}`;
    }
    if (stock.isOOP) {
      return `OP ${oop_input} ${oop_dropdown}`;
    }
    if (stock.isOther) {
      return `Other ${other_input} ${other_dropdown}`;

    }
  }
}
