import {Component, OnInit} from '@angular/core';
import {DataTableParams, DataTableResource} from 'ngx-datatable-bootstrap4';
import {NgbModal} from '@ng-bootstrap/ng-bootstrap';
import {Router, ActivatedRoute} from '@angular/router';
import {first, startWith} from 'rxjs/operators';
import {toLower} from 'lodash';
import * as moment from 'moment';
import {AlertService, LabService, PlantCategoryService,WarehouseService,PlantCharacterService} from '../../../../services';
import {FormBuilder, FormGroup, Validators} from '@angular/forms';


@Component({
  selector: 'app-list-lab-page',
  templateUrl: './alert-list.component.html',
})
export class NgbdListingAlertComponent {
  resources = new DataTableResource<any>([]);
  resources1 = new DataTableResource<any>([]);
  resources2 = new DataTableResource<any>([]);
  createLabForm: FormGroup;
  items: any[] = [];
  tempItems: any[] = [];
  items1: any[] = [];
  tempItems1: any[] = [];
  items2: any[] = [];
  tempItems2: any[] = [];
  plantsListing = [];
  count = 0;
  count1 = 0;
  count2 = 0;
  loading = false;
  momentObj: any;
  apiError = null;
  submitted = false;
  roles = [];
  constructor(private modalService: NgbModal,
              private alertService: AlertService,
              private formBuilder: FormBuilder,
              private labService: LabService,
              private plantCharacterService: PlantCharacterService,
              private whservice: WarehouseService,
              private router: Router,
              private route: ActivatedRoute) {
    this.onGetLabAlertListingTab1();
    this.onGetLabAlertListingTab2();
    this.onGetLabAlertListingTab3();
    this.onGetListing();
    this.momentObj = moment;
  }

  ngOnInit(): void {
    this.route
      .data
      .subscribe(data => {
        this.roles = data['roles'];
      });
  }
  onGetListing() {
    this.loading = true;
    this.plantCharacterService.getAll()
      .pipe(first())
      .subscribe(
        ({data = []}: any) => {
          this.loading = false;
          this.plantsListing = data;
        },
        error => {
          this.loading = false;
          this.alertService.error(error);
        });
  }
  reloadData(params: DataTableParams) {
    this.resources.query(params).then(vals => {
      this.tempItems = vals;
      this.items = vals;
    });
  }
  reloadData1(params: DataTableParams) {
    this.resources1.query(params).then(vals => {
      this.tempItems1 = vals;
      this.items1 = vals;
    });
  }
  reloadData2(params: DataTableParams) {
    this.resources2.query(params).then(vals => {
      this.tempItems2 = vals;
      this.items2 = vals;
    });
  }

  onDetails({_id}) {
    //this.router.navigate([`/admin/labs/alert-plants/${_id}`]);
    this.router.navigate([`/admin/labs/alert-plants/details/${_id}`]);
  }

  onDetailsPending({_id}){	
    this.router.navigate([`/admin/warehouse/form-new-plant/${_id}`]);	
  }

  get f() {
    if (!this.apiError && this.submitted && !this.createLabForm.invalid) {
      this.alertService.error(null);
    }

    return this.createLabForm.controls;
  }

  onDelete(item: object, deleteContent) {
    this.modalService.open(deleteContent, {centered: true});
  }

  onSearchCate({target: {value: searchRole = ''} = {}}) {
    this.items = this.tempItems.filter(({plant_gene_name}) => {
      return toLower(plant_gene_name.plantCategory.categoryName_th).includes(toLower(searchRole))
    });
    this.items1 = this.tempItems1.filter(({plant_gene_name}) => {
      return toLower(plant_gene_name.plantCategory.categoryName_th).includes(toLower(searchRole))
    });
    this.items2 = this.tempItems2.filter(({plant_gene_name}) => {
      return toLower(plant_gene_name.plantCategory.categoryName_th).includes(toLower(searchRole))
    });
  }
  onSearchNo({target: {value: searchRole = ''} = {}}) {
    this.items = this.tempItems.filter(({reg_gene_id}) => {
      return toLower(reg_gene_id).includes(toLower(searchRole))
    });
    this.items1 = this.tempItems1.filter(({reg_gene_id}) => {
      return toLower(reg_gene_id).includes(toLower(searchRole))
    });
    this.items2 = this.tempItems2.filter(({reg_gene_id}) => {
      return toLower(reg_gene_id).includes(toLower(searchRole))
    });
  }
  onSearchType({target: {value: searchRole = ''} = {}}) {
    this.items = this.tempItems.filter(({plant_gene_name}) => {
      return toLower(plant_gene_name.plantTypeName_th).includes(toLower(searchRole))
    });
    this.items1 = this.tempItems1.filter(({plant_gene_name}) => {
      return toLower(plant_gene_name.plantTypeName_th).includes(toLower(searchRole))
    });
    this.items2 = this.tempItems2.filter(({plant_gene_name}) => {
      return toLower(plant_gene_name.plantTypeName_th).includes(toLower(searchRole))
    });
  }
  onSearchSci({target: {value: searchRole = ''} = {}}) {
    this.items = this.tempItems.filter(({reg_plant_sci_name}) => {
      return toLower(reg_plant_sci_name).includes(toLower(searchRole))
    });
    this.items1 = this.tempItems1.filter(({reg_plant_sci_name}) => {
      return toLower(reg_plant_sci_name.genus).includes(toLower(searchRole))
    });
    this.items2 = this.tempItems2.filter(({reg_plant_sci_name}) => {
      return toLower(reg_plant_sci_name.genus).includes(toLower(searchRole))
    });
  }

  onGetLabAlertListingTab1() {
    this.loading = true;
    this.whservice.getAlertList()
      .pipe(first())
      .subscribe(
        ({data = []}: any) => {
          this.loading = false;
          this.resources = new DataTableResource<any>(data);
          this.resources.count().then(count => this.count = count);
          this.reloadData({});
        },
        error => {
          this.loading = false;
          this.alertService.error(error);
        });
  }
  onGetLabAlertListingTab2() {
    this.loading = true;
    this.whservice.getLabAlertM9('pending')
      .pipe(first())
      .subscribe(
        ({data = []}: any) => {
          this.loading = false;
          this.resources1 = new DataTableResource<any>(data);
          this.resources1.count().then(count => this.count1 = count);
          this.reloadData1({});
        },
        error => {
          this.loading = false;
          this.alertService.error(error);
        });
  }
  onGetLabAlertListingTab3() {
    this.loading = true;
    this.whservice.getLabAlertM9('approve')
      .pipe(first())
      .subscribe(
        ({data = []}: any) => {
          this.loading = false;
          this.resources2 = new DataTableResource<any>(data);
          this.resources2.count().then(count => this.count2 = count);
          this.reloadData2({});
        },
        error => {
          this.loading = false;
          this.alertService.error(error);
        });
  }
  getStockInfo(item: any) {
    const {
      selfing_input, selfing_dropdown,
      oop_input, oop_dropdown,
      other_input, other_dropdown
    } = item;
    const stock = {
      isSelfing: selfing_input && selfing_dropdown,
      isOOP: oop_input && oop_dropdown,
      isOther: other_input && other_dropdown,
    };
    if (stock.isSelfing) {
      return `Self ${selfing_input} ${selfing_dropdown}`;
    }
    if (stock.isOOP) {
      return `OP ${oop_input} ${oop_dropdown}`;
    }
    if (stock.isOther) {
      return `Other ${other_input} ${other_dropdown}`;

    }
  }
  findPlantSciName(reg_plant_sci_name) {
    if (!reg_plant_sci_name) {
      return '-';
    }
    for (const plant of  this.plantsListing) {
      if (reg_plant_sci_name === plant._id) {
        return  plant.genus + ' ' + plant.species;
      }
    }

  }
  checkRole(index){
    if(localStorage.getItem('superAdmin') === "true") return true;
    if(localStorage.getItem('rolesId').indexOf(this.roles[index]) > -1) return true;
    return false;
  }
}
