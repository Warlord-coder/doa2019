import {Component, OnInit} from '@angular/core';
import {ActivatedRoute, Router} from '@angular/router';
import {FormBuilder, FormGroup, Validators} from '@angular/forms';
import {AlertService, CommonConditionService} from '../../../../services';
import {getRoleResources} from '../../../../utils';
import {TreeviewConfig, TreeviewItem} from 'ngx-treeview';
import {first} from 'rxjs/operators';

@Component({
  selector: 'app-add-update-common-country-page',
  templateUrl: './add-update.component.html',
})
export class NgbdAddUpdateCommonConditionComponent implements OnInit {
  ConditionForm: FormGroup;
  submitted = false;
  roleResources: TreeviewItem[];
  loading = false;
  apiError = false;
  id = null;
  config = TreeviewConfig.create({
    hasAllCheckBox: true,
    hasFilter: true,
    hasCollapseExpand: true,
    decoupleChildFromParent: false,
    maxHeight: 800
  });


  constructor(private router: Router,
              private formBuilder: FormBuilder,
              private activeRoute: ActivatedRoute,
              private commonConditionService: CommonConditionService,
              private alertService: AlertService) {
  }

  ngOnInit() {
    this.roleResources = getRoleResources();
    this.ConditionForm = this.formBuilder.group({
      status: ['', Validators.required],
      name: ['', Validators.required],
      type: ['', Validators.required],
      value: ['', Validators.required],
      conditionAt: ['', Validators.required]
    });
    this.activeRoute.params.subscribe(params => {
      const {id} = params;
      this.id = id;
      if (id && id !== 'create') {
        this.onGetCommonConditionInformation(id);
      }
    });
  }

  get f() {
    if (!this.apiError && this.submitted && !this.ConditionForm.invalid) {
      this.alertService.error(null);
    }

    return this.ConditionForm.controls;
  }

  onSubmit() {
    this.submitted = true;
    if (this.ConditionForm.invalid) {
      this.alertService.error('Some fields are required. Please fill those fields');
      return;
    }

    this.loading = true;
    this.apiError = false;
    this.commonConditionService.createOrUpdate({...this.ConditionForm.value}, this.id)
      .pipe(first())
      .subscribe(
        () => {
          this.loading = false;
          this.router.navigate(['admin/common/condition/listing']);
        },
        error => {
          this.loading = false;
          this.apiError = true;
          this.alertService.error(error);
        });
  }

  onSelectedChange($event) {
    console.log('Select', $event);
  }

  onGetCommonConditionInformation(id: string) {
    this.loading = true;
    this.commonConditionService.getSpecific(id)
      .pipe(first())
      .subscribe(
        ({data = {}}: any) => {
          const {status, type, value, conditionAt, name} = data;
          this.ConditionForm.get('status').setValue(status);
          this.ConditionForm.get('name').setValue(name);
          this.ConditionForm.get('type').setValue(type);
          this.ConditionForm.get('value').setValue(value);
          this.ConditionForm.get('conditionAt').setValue(conditionAt);
          this.loading = false;
        },
        error => {
          this.loading = false;
          this.apiError = true;
          this.alertService.error(error);
        });
  }

  onCancel() {
    this.router.navigate(['admin/common/condition/listing']);
  }
}
