import {Component} from '@angular/core';
import {DataTableParams, DataTableResource} from 'ngx-datatable-bootstrap4';
import {NgbModal} from '@ng-bootstrap/ng-bootstrap';
import {Router, ActivatedRoute} from '@angular/router';
import {AlertService, CommonConditionService} from '../../../../services';
import {toLower} from 'lodash';
import * as moment from 'moment';
import {first} from 'rxjs/operators';

@Component({
  selector: 'app-listing-common-country-page',
  templateUrl: './listing.component.html',
})
export class NgbdCommonConditionComponent {
  resources = new DataTableResource<any>([]);
  items: any[] = [];
  count = 0;
  loading = false;
  momentObj: any;
  tempItems: any[] = [];
  deletedId = null;
  roles = [];
  constructor(private modalService: NgbModal,
              private alertService: AlertService,
              private commonConditionService: CommonConditionService,
              private router: Router,
              private route: ActivatedRoute) {
    this.onGetCommonConditionListing();
    this.momentObj = moment;
  }

  ngOnInit() {
    this.route
      .data
      .subscribe(data => {
        this.roles = data['roles'];
      });
  }
  
  reloadData(params: DataTableParams) {
    this.resources.query(params).then(vals => {
      this.tempItems = vals;
      this.items = vals;
    });
  }

  onEdit({_id}) {
    this.router.navigate([`/admin/common/condition/${_id}`]);
  }

  onDelete(item: any, deleteContent) {
    this.deletedId = item._id;
    this.modalService.open(deleteContent, {centered: true});
  }

  onSearch({target: {value: searchRole = ''} = {}}) {
    this.items = this.tempItems.filter(({name}) => name && toLower(name).includes(toLower(searchRole)));
  }

  onGetCommonConditionListing() {
    this.loading = true;
    this.commonConditionService.getAll(true)
      .pipe(first())
      .subscribe(
        ({data = []}: any) => {
          this.loading = false;
          this.resources = new DataTableResource<any>(data);
          this.resources.count().then(count => this.count = count);
          this.reloadData({});
        },
        error => {
          this.loading = false;
          this.alertService.error(error);
        });
  }

  onDeleteCommonCondition() {
    this.loading = true;
    this.commonConditionService.delete(this.deletedId)
      .pipe(first())
      .subscribe(
        ({data = []}: any) => {
          this.onGetCommonConditionListing();
        },
        error => {
          this.loading = false;
          this.alertService.error(error);
        });
  }
  checkRole(index){
    if(localStorage.getItem('superAdmin') === "true") return true;
    if(localStorage.getItem('rolesId').indexOf(this.roles[index]) > -1) return true;
    return false;
  }
}
