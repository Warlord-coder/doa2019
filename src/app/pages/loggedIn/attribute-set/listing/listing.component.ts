import { Component } from '@angular/core';
import {AlertService, AttributeSetService} from '../../../../services';
import {first} from 'rxjs/operators';
import {ActivatedRoute, Router} from '@angular/router';

import {NgbModal} from '@ng-bootstrap/ng-bootstrap';

import * as moment from 'moment';
@Component({
  selector: 'app-distribution-status',
  templateUrl: './listing.component.html'
})
export class NgbdAttributeSetListComponent {
  
  info = [];

  dtOptions: DataTables.Settings = {};
  momentObj: any;
  loading = false;

  selectedItem = null;
  roles = [];
  constructor(private modalService: NgbModal,
      private attributeSetService: AttributeSetService,
      private activeRoute: ActivatedRoute,
      private alertService: AlertService,
      private router: Router,
      private route: ActivatedRoute) {
  this.momentObj = moment;
  }

  ngOnInit() {
    this.route
      .data
      .subscribe(data => {
        this.roles = data['roles'];
      });
    this.dtOptions = {
      pagingType: 'full_numbers',
      pageLength: 25,
      serverSide: true,
      lengthChange: true,
      responsive:true,
      ajax: (dataTablesParameters: any, callback) => {
        const {start, length} = dataTablesParameters;
        const keyword = dataTablesParameters.search.value;
        this.attributeSetService.getFilterList({start: start, length: length, keyword: keyword})
        .pipe(first())
        .subscribe(
          ({data = []}: any) => {
            this.loading = false;
            this.info = data['data'];
            callback({
              recordsTotal: data['total_rows'],
              recordsFiltered: data['total_rows'],
              data: []
            })
          },
          error => {
            this.loading = false;
            this.alertService.error(error);
          });
        
      },
    };
  }

  getStockInfo(item) {
    const {
      selfing_input, selfing_dropdown,
      oop_input, oop_dropdown,
      other_input, other_dropdown
    } = item;
    const stock = {
      isSelfing: selfing_input && selfing_dropdown,
      isOOP: oop_input && oop_dropdown,
      isOther: other_input && other_dropdown,
    };
    if (stock.isSelfing) {
      return `Self ${selfing_input} ${selfing_dropdown}`;
    }
    if (stock.isOOP) {
      return `OP ${oop_input} ${oop_dropdown}`;
    }
    if (stock.isOther) {
      return `Other ${other_input} ${other_dropdown}`;

    }
  }

  onEdit({_id = ''} = {}) {
    this.router.navigate([`/admin/manage-data/attribute/${_id}`]);
  }

  onSubmit() {
    this.loading = true;
    this.modalService.dismissAll();
    this.attributeSetService.createOrUpdate({isDistribution: true}, this.selectedItem._id)
      .pipe(first())
      .subscribe(
        ({data = []}: any) => {
          //this.onGetWareHouseLabListing();
          this.loading = false;
        },
        error => {
          this.loading = false;
          this.alertService.error(error);
        });
  }
  checkRole(index){
    if(localStorage.getItem('superAdmin') === "true") return true;
    if(localStorage.getItem('rolesId').indexOf(this.roles[index]) > -1) return true;
    return false;
  }
}
