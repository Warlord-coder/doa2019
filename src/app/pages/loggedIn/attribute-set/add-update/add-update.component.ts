import {Component, OnInit} from '@angular/core';
import {ActivatedRoute, Router} from '@angular/router';
import {FormArray, FormBuilder, FormGroup, Validators} from '@angular/forms';
import {AlertService, AttributeSetService} from '../../../../services';
import {getRoleResources} from '../../../../utils';
import {TreeviewConfig, TreeviewItem} from 'ngx-treeview';
import {first} from 'rxjs/operators';


@Component({
  selector: 'app-add-update-common-country-page',
  templateUrl: './add-update.component.html',

})
export class NgbdAddUpdateAttributeSetComponent implements OnInit {
  AttributeSetForm: FormGroup;
  submitted = false;
  loading = false;
  apiError = false;
  roleResources: TreeviewItem[];
  id = null;

  IsManageValue = false;
  IsMinMax = false;

  config = TreeviewConfig.create({
    hasAllCheckBox: true,
    hasFilter: true,
    hasCollapseExpand: true,
    decoupleChildFromParent: false,
    maxHeight: 800
  });


  constructor(private router: Router,
              private formBuilder: FormBuilder,
              private activeRoute: ActivatedRoute,
              private attributeSetService: AttributeSetService,
              private alertService: AlertService) {
  }

  ngOnInit() {
    this.roleResources = getRoleResources();
    this.AttributeSetForm = this.formBuilder.group({
      code: ['', Validators.required],
      min: [''],
      max: [''],
      name_en: ['', Validators.required],
      name_th: ['', Validators.required],
      require: ['', Validators.required],
      fieldInput: ['', Validators.required],
      isVisibleFrontend: ['', Validators.required],
      optionValue: this.formBuilder.array([])
    });
    this.activeRoute.params.subscribe(params => {
      const {id} = params;
      this.id = id;
      if (id && id !== 'create') {
        this.onGetAttributeSetInformation(id);
      }
    });
  }

  get f() {
    if (!this.apiError && this.submitted && !this.AttributeSetForm.invalid) {
      this.alertService.error(null);
    }
    return this.AttributeSetForm.controls;
  }

  onGetAttributeSetInformation(id: string) {
    this.loading = true;
    this.attributeSetService.getSpecific(id)
      .pipe(first())
      .subscribe(
        ({data = {}}: any) => {
          const {optionValue = [], fieldInput} = data;
          if(fieldInput == "dropdown" || fieldInput == "radio"|| fieldInput == "checkbox") this.IsManageValue = true; this.IsMinMax = false;
          if(fieldInput == "text") this.IsMinMax = true;
          Object.keys(data).forEach(key => {
            if (this.AttributeSetForm.get(key) && key !== 'optionValue') {
              this.AttributeSetForm.get(key).setValue(data[key]);
            }
          });
          optionValue.forEach((data) => {
            this.addItem(data);
          });
          this.loading = false;
        },
        error => {
          this.alertService.error(error);
          this.apiError = true;
          this.loading = false;
        });
  }

  createOptionValue({position_en, position_th, value_en, value_th}: any): FormGroup {
    return this.formBuilder.group({
      position_en: [position_en, Validators.required],
      position_th: [position_th, Validators.required],
      value_en: [value_en, Validators.required],
      value_th: [value_th, Validators.required]
    });
  }

  addItem(data = {}): void {
    (this.AttributeSetForm.get('optionValue') as FormArray).push(this.createOptionValue(data || {}));
  }

  onSubmit() {
    this.submitted = true;
    this.apiError = false;
    if (this.AttributeSetForm.invalid) {
      this.alertService.error('Some fields are required. Please fill those fields');
      return;
    }

    this.loading = true;
    this.attributeSetService.createOrUpdate({attributeSet: this.AttributeSetForm.value}, this.id)
      .pipe(first())
      .subscribe(
        () => {
          this.loading = false;
          this.router.navigate(['admin/manage-data/attribute/listing']);
        },
        error => {
          this.loading = false;
          this.apiError = true;
          this.alertService.error(error);
        });
  }
  typeChange($event){
    let type = $event.target.value;
    if(type == "dropdown" || type == "radio" || type == "checkbox") {
      this.IsManageValue = true;
      this.IsMinMax = false;
    }
    if(type == "text") {
      this.IsMinMax = true;
      this.IsManageValue = false;
    }
  }

  optionRemove(index){
    (this.AttributeSetForm.get('optionValue') as FormArray).removeAt(index);
  }
  cancel(){
    this.router.navigate(['admin/manage-data/attribute/listing']);
  }
}
