import {Component} from '@angular/core';
import {DataTableParams, DataTableResource} from 'ngx-datatable-bootstrap4';
import {NgbModal} from '@ng-bootstrap/ng-bootstrap';
import {Router, ActivatedRoute} from '@angular/router';
import {first} from 'rxjs/operators';
import * as moment from 'moment';
import {toLower} from 'lodash';

import {AlertService, PlantGroupsService} from '../../../../services';

@Component({
  selector: 'app-listing-plant-group-page',
  templateUrl: './listing.component.html',
})
export class NgbdListingPlantGroupComponent {
  resources = new DataTableResource<any>([]);
  items: any[] = [];
  count = 0;
  loading = false;
  momentObj: any;
  tempItems: any[] = [];
  deletedId = null;
  roles = [];

  constructor(private modalService: NgbModal,
              private alertService: AlertService,
              private plantGroupsService: PlantGroupsService,
              private router: Router,
              private route: ActivatedRoute) {
    this.momentObj = moment;
    this.onGetPlantGroupListing();
  }

  ngOnInit() {
    this.route
      .data
      .subscribe(data => {
        this.roles = data['roles'];
      });
    
  }

  reloadData(params: DataTableParams) {
    this.resources.query(params).then(vals => {
      this.tempItems = vals;
      this.items = vals;
    });
  }

  onEdit({_id = ''} = {}) {
    this.router.navigate([`/admin/master/group/${_id}`]);
  }

  onDelete(item: any, deleteContent) {
    this.deletedId = item._id;
    this.modalService.open(deleteContent, {centered: true});
  }

  onSearch({target: {value: searchRole = ''} = {}}) {
    this.items = this.tempItems.filter(({groupName_en}) => groupName_en && toLower(groupName_en).includes(toLower(searchRole)));
  }

  onGetPlantGroupListing() {
    this.loading = true;
    this.plantGroupsService.getAll(true)
      .pipe(first())
      .subscribe(
        ({data = []}: any) => {
          this.resources = new DataTableResource<any>(data);
          this.resources.count().then(count => this.count = count);
          this.reloadData({});
          this.loading = false;
        },
        error => {
          this.loading = false;
          this.alertService.error(error);
        });
  }

  onDeleteGroup() {
    this.loading = true;
    this.plantGroupsService.delete(this.deletedId)
      .pipe(first())
      .subscribe(
        ({data = []}: any) => {
          this.onGetPlantGroupListing();
        },
        error => {
          this.loading = false;
          this.alertService.error(error);
        });
  }

  checkRole(index){
    if(localStorage.getItem('superAdmin') === "true") return true;
    if(localStorage.getItem('rolesId').indexOf(this.roles[index]) > -1) return true;
    return false;
  }
}
