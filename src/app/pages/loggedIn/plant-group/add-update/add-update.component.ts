import {Component, OnInit} from '@angular/core';
import {ActivatedRoute, Router} from '@angular/router';
import {FormBuilder, FormGroup, Validators} from '@angular/forms';
import {AlertService, PlantCharacterService, PlantGroupsService} from '../../../../services';
import {getRoleResources} from '../../../../utils';
import {TreeviewConfig, TreeviewItem} from 'ngx-treeview';
import {first} from 'rxjs/operators';

@Component({
  selector: 'app-add-update-plant-group-page',
  templateUrl: './add-update.component.html',
})
export class NgbdAddUpdatePlantGroupComponent implements OnInit {
  plantGroupForm: FormGroup;
  submitted = false;
  roleResources: TreeviewItem[];
  loading = false;
  apiError = false;
  id = null;
  config = TreeviewConfig.create({
    hasAllCheckBox: true,
    hasFilter: true,
    hasCollapseExpand: true,
    decoupleChildFromParent: false,
    maxHeight: 800
  });


  constructor(private router: Router,
              private formBuilder: FormBuilder,
              private activeRoute: ActivatedRoute,
              private plantGroupService: PlantGroupsService,
              private alertService: AlertService) {
  }

  ngOnInit() {
    this.roleResources = getRoleResources();
    this.plantGroupForm = this.formBuilder.group({
      status: ['', Validators.required],
      groupName_en: ['', Validators.required],
      groupName_th: ['', Validators.required]
    });
    this.activeRoute.params.subscribe(params => {
      const {id} = params;
      this.id = id;
      if (id && id !== 'create') {
        this.onGetPlantGroupInformation(id);
      }
    });
  }

  get f() {
    if (!this.apiError && this.submitted && !this.plantGroupForm.invalid) {
      this.alertService.error(null);
    }

    return this.plantGroupForm.controls;
  }

  onSubmit() {
    this.submitted = true;
    this.apiError = false;
    if (this.plantGroupForm.invalid) {
      this.alertService.error('Some fields are required. Please fill those fields');
      return;
    }

    this.alertService.reset();
    this.loading = true;

    this.plantGroupService.createOrUpdate({...this.plantGroupForm.value}, this.id)
      .pipe(first())
      .subscribe(
        () => {
          this.loading = false;
          this.router.navigate(['/admin/master/group/listing']);
        },
        error => {
          this.loading = false;
          this.apiError = true;
          this.alertService.error(error);
        });
  }

  onFilterChange($event) {
    console.log('Filter Change', $event);
  }


  onGetPlantGroupInformation(id: string) {
    this.loading = true;
    this.plantGroupService.getSpecific(id)
      .pipe(first())
      .subscribe(
        ({data = {}}: any) => {
          const {status, groupName_en, groupName_th} = data;
          this.plantGroupForm.get('status').setValue(status);
          this.plantGroupForm.get('groupName_en').setValue(groupName_en);
          this.plantGroupForm.get('groupName_th').setValue(groupName_th);
          this.loading = false;
        },
        error => {
          this.loading = false;
          this.apiError = true;
          this.alertService.error(error);
        });
  }

  onCancel() {
    this.router.navigate(['/admin/master/group/listing']);
  }
}
