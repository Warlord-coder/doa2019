import {Component, OnInit} from '@angular/core';
import {ActivatedRoute, Router} from '@angular/router';
import {FormBuilder, FormGroup, Validators} from '@angular/forms';
import {
  AlertService,
  CommonAumperService,
  CommonCountryService,
  CommonProvinceService,
  PassportService,
  PlantCategoryService,
  PlantGroupsService,
  PlantGroupTypesService,
  PlantRegisterService,
  VolumeService, PlantCharacterService, PlantTypeService
} from '../../../services';
import {TreeviewItem} from 'ngx-treeview';
import {first} from 'rxjs/operators';
import {identity, pickBy} from 'lodash';
import {environment} from '../../../../environments/environment';

@Component({
  selector: 'app-add-update-plant-register-page',
  templateUrl: './add-update.component.html',
})
export class NgbdAddUpdatePlantRegister4Component implements OnInit {
  plantGroupTypeForm: FormGroup;
  submitted = false;
  loading = false;
  file = null;
  id = null;
  categoryList = [];
  groupTypeList = [];
  groupList = [];
  provinceList = [];
  aumperList = [];
  countryList = [];
  plantTypes = [];
  volumeInfo = null;
  passportInfo = null;
  apiError = false;
  plantsListing = [];
  plantInfo: any = {};
  selectedPlant = null;
  volumeId = null;
  disabled = false;
  formData: any = {};
  roleResources: TreeviewItem[];
  regNames = [];
  config = {
    class: 'autocomplete', max: 5, placeholder: 'Type to search',
    sourceField: ['plantTypeName_sci_en']
  };

  constructor(private router: Router,
              private formBuilder: FormBuilder,
              private activeRoute: ActivatedRoute,
              private plantCategoryService: PlantCategoryService,
              private plantGroupTypesService: PlantGroupTypesService,
              private plantGroupsService: PlantGroupsService,
              private plantCharacterService: PlantCharacterService,
              private plantTypeService: PlantTypeService,
              private volumeService: VolumeService,
              private passportService: PassportService,
              private plantRegisterService: PlantRegisterService,
              private countryService: CommonCountryService,
              private provinceService: CommonProvinceService,
              private aumperService: CommonAumperService,
              private alertService: AlertService) {
    this.onGetListing('plantCharacterService', 'plantsListing');
    this.onGetListing('plantCategoryService', 'categoryList');
    this.onGetListing('plantGroupTypesService', 'groupTypeList');
    this.onGetListing('plantGroupsService', 'groupList');
    this.onGetListing('countryService', 'countryList');
    this.onGetListing('aumperService', 'aumperList');
    this.onGetListing('provinceService', 'provinceList');
  }

  ngOnInit() {
    this.plantGroupTypeForm = this.formBuilder.group({
      recovery_place: [''],
      recovery_province: [''],
      recovery_district: [''],
      recovery_sub_district: [''],
      recovery_address: [''],
      recovery_lat: [''],
      recovery_long: [''],
      recovery_sea_level: [''],
      reg_seed_recovery_date: [''],
      reg_seed_receive_date: [''],
      reg_seed_sent_no: [''],
      selfing_input: [''],
      selfing_dropdown: [''],
      oop_input: [''],
      oop_dropdown: [''],
      other_input: [''],
      reg_plant_sci_name: [''],
      reg_plant_alter_name: [''],
      other_dropdown: [''],
      reg_gene_type: [''],
      plant_gene_name: [''],
      reg_gene_category: [''],
      reg_seed_collect_date: [''],
      department_type: [''],
      department_sub_type: [''],
      department_name: [''],
      source_country: [''],
      source_country_1: [''],
      source_province: [''],
      source_district: [''],
      source_address: [''],
      lat: [''],
      long: [''],
      source_by_department: [''],
      reg_seed_collect_person: [''],
      reg_seed_sender: [''],
      reg_character_brief: [''],
      reg_character_for_record: [''],
      reg_check_seed_ref: [''],
      reg_check_molecular: [''],
      reg_sent_lab_date: [''],
      reg_remark: [''],
      recovery_rec_remark: [''],
      recovery_seed_amount: ['']
    });
    this.activeRoute.params.subscribe(params => {
      const {id, volume} = params;
      this.id = id;
      this.volumeId = volume;
      this.onGetVolumeInfo(this.volumeId);
      if (id && id !== 'create') {
        this.onGetPlantRegisterInfo(id);
      }
    });
    setTimeout(() => {
      this.findRegGeneName();
    }, 500);
  }

  get f() {
    if (!this.apiError && this.submitted && !this.plantGroupTypeForm.invalid) {
      this.alertService.error(null);
    }

    return this.plantGroupTypeForm.controls;
  }

  onSubmit() {
    // this.submitted = true;
    if (this.plantGroupTypeForm.invalid) {
      // this.alertService.error('Some fields are required. Please fill those fields');
      // return;
    }

    if (!this.selectedPlant) {
      // this.alertService.error('Please Select Reg Plant Sci Name');
      // return;
    }

    const {reg_plant_alter_name = []} = this.plantGroupTypeForm.value;
    this.plantRegisterService.createOrUpdate(pickBy({
      ...this.plantGroupTypeForm.value,
      passport: this.passportInfo._id,
      volume: this.volumeId,
      reg_plant_alter_name: reg_plant_alter_name ? JSON.stringify(reg_plant_alter_name) : [],
      reg_plant_sci_name: this.selectedPlant._id,
      reg_gene_id: this.plantInfo.reg_gene_id
    }, identity), this.id)
      .pipe(first())
      .subscribe(
        () => {
          this.loading = false;
          this.router.navigate([`/admin/register/plant/${this.volumeId}/listing`]);
        },
        error => {
          this.loading = false;
          this.apiError = true;
          this.alertService.error(error);
        });
  }

  onSelectedChange($event) {
    console.log('Select', $event);
  }

  onFilterChange($event) {
    console.log('Filter Change', $event);
  }

  onGetListing(serviceName, key) {
    this.loading = true;
    this[serviceName].getAll()
      .pipe(first())
      .subscribe(
        ({data = []}: any) => {
          this.loading = false;
          this[key] = data;
        },
        error => {
          this.loading = false;
          this.alertService.error(error);
        });
  }

  onGetPlantRegisterInfo(id) {
    this.loading = true;

    this.plantRegisterService.getSpecific(id, 'Register 4')
      .pipe(first())
      .subscribe(
        ({data = []}: any) => {
          const {plant_gene_name, reg_plant_sci_name, m3_status} = data;
          if(plant_gene_name!=undefined && plant_gene_name!= null)
            this['plantTypeService'].getSpecific(plant_gene_name) 
            .pipe(first())
            .subscribe(
              ({data = []}: any) => {
                this.plantTypes.push(data )
                this.loading = false;
              },
              error => {
                this.loading = false;
                this.alertService.error(error);
              });
          this.formData = data;
          this.disabled = m3_status === 'complete';
          Object.keys(data).forEach(key => {
            if (this.plantGroupTypeForm.get(key)) {
              this.plantGroupTypeForm.get(key).setValue(data[key]);
              if (this.disabled) {
                this.plantGroupTypeForm.get(key).disable({onlySelf: true});
              }
            }
          });
          this.plantInfo = data;
          this.findPlantSciName(data);
          this.loading = false;
        },
        error => {
          this.loading = false;
          this.alertService.error(error);
        });
  }

  onGetVolumeInfo(id) {
    this.loading = true;
    this.volumeService.getSpecific(id)
      .pipe(first())
      .subscribe(
        ({data = []}: any) => {
          this.loading = false;
          const {passport_no} = data;
          this.volumeInfo = data;
          this.onGetPassportInfo(passport_no);
        },
        error => {
          this.loading = false;
          this.alertService.error(error);
        });
  }

  onGetPassportInfo(id) {
    this.loading = true;
    this.passportService.getSpecific(id)
      .pipe(first())
      .subscribe(
        ({data = []}: any) => {
          this.loading = false;
          const {passport_no} = data;
          this.passportInfo = data;
        },
        error => {
          this.loading = false;
          this.alertService.error(error);
        });
  }

  getAttachment(basePath, name) {
    if (name) {
      return `${environment.endpoint}/${basePath}/${name}`;
    }

    return null;
  }

  onFileUpload({srcElement: {files = []} = {}}) {
  }

  onSelect(item: any) {
    this.selectedPlant = item;
  }

  findPlantSciName({reg_plant_sci_name}) {
    if (!reg_plant_sci_name) {
      return;
    }
    for (const plant of  this.plantsListing) {
      if (reg_plant_sci_name === plant._id) {
        this.selectedPlant = plant;
        this.plantGroupTypeForm.get('reg_plant_sci_name').setValue(plant.genus + ' ' + plant.species);
      }
    }

  }

  findRegGeneName() {
    var i = 0 ; 
    for (const plant of  this.plantTypes) {
      this.regNames[i] = plant.plantTypeName_en;
      i ++ ;
    }
    for (const plant of  this.plantsListing) {
      this.regNames[i] = plant.genus + ' ' + plant.species;
      i ++ ;
    }
  }

  onCancel() {
    this.router.navigate([`/admin/register/plant/${this.volumeId}/listing`]);
  }
}
