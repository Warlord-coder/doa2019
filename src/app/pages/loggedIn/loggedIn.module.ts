import {NgModule} from '@angular/core';
import {RouterModule} from '@angular/router';
import {CommonModule} from '@angular/common';
import {FormsModule, ReactiveFormsModule} from '@angular/forms';
import {NgxLoadingModule} from 'ngx-loading';
import {NgbModule} from '@ng-bootstrap/ng-bootstrap';
import {DataTableModule} from 'ngx-datatable-bootstrap4';
import {DataTablesModule} from 'angular-datatables';
import {TreeviewModule} from 'ngx-treeview';
import {NgxFlagIconCssModule} from 'ngx-flag-icon-css';
import {ImageUploaderModule} from 'ngx-image-uploader';
import {AutocompleteModule} from 'ng2-input-autocomplete';
import {NgxMaskModule} from 'ngx-mask';
import {AutocompleteLibModule} from 'angular-ng-autocomplete';
import {TagInputModule} from 'ngx-chips';
import {AngularEditorModule} from '@kolkov/angular-editor';

import {LoggedInRoutes} from './loggedIn.routing';
// @ts-ignore
import {NgbdAlertListComponent} from './withdraw/alert-list/alertList.component';
import {NgbdAlertListDisComponent} from './distribution/alert-list/alertList.component';
import {NgbdCreateUpdateTeamMemberComponent} from './team-members/create-update/team-members.component';
import {NgbdListingTeamMemberComponent} from './team-members/listing/listing.component';
import {NgbdAdminManageComponent} from './admin-manage/admin-mange.component';
import {NgbdAddRoleComponent} from './role-resource/add-role/add-role.component';
import {NgbdListingRoleComponent} from './role-resource/listing/listing.component';
import {ComponentsModule} from '../../component/component.module';
import {NgbdListingPlantCategoryComponent} from './plant-category/listing/listing.component';
import {NgbdAddUpdatePlantCategoryComponent} from './plant-category/add-update/add-update.component';
import {NgbdAddUpdatePlantCharacterComponent} from './plant-character/add-update/add-update.component';
import {NgbdListingPlantCharacterComponent} from './plant-character/listing/listing.component';
import {NgbdListingPlantGroupComponent} from './plant-group/listing/listing.component';
import {NgbdAddUpdatePlantGroupComponent} from './plant-group/add-update/add-update.component';
import {NgbdAddUpdatePlantGroupTypeComponent} from './plant-group-type/add-update/add-update.component';
import {NgbdListingPlantGroupTypeComponent} from './plant-group-type/listing/listing.component';
import {NgbdAddUpdatePlantTypeComponent} from './plant-type/add-update/add-update.component';
import {NgbdListingPlantTypeComponent} from './plant-type/listing/listing.component';
import {NgbdCommonProvinceListComponent} from './common-province-list/listing/listing.component';
import {NgbdAddUpdateCommonProvinceListComponent} from './common-province-list/add-update/add-update.component';
import {NgbdAddUpdateCommonUnitListComponent} from './common-unit-list/add-update/add-update.component';
import {NgbdCommonUnitListComponent} from './common-unit-list/listing/listing.component';
import {NgbdCommonAmuperListComponent} from './common-aumper/listing/listing.component';
import {NgbdAddUpdateAumperCountryComponent} from './common-aumper/add-update/add-update.component';
import {NgbdCommonConditionComponent} from './common-condition/listing/listing.component';
import {NgbdAddUpdateCommonConditionComponent} from './common-condition/add-update/add-update.component';
import {NgbdAddUpdateCommonCountryComponent} from './common-country/add-update/add-update.component';
import {NgbdCommonCountryListComponent} from './common-country/listing/listing.component';
import {NgbdRegisterPassportListComponent} from './register-passport/listing/listing.component';
import {NgbdPassportYearReportComponent} from './report/year/year-report.component';
import {NgbdPlantMonthReportComponent} from './report/month/month-report.component';
import {NgbdLabStatusReportComponent} from './report/labstatus/labstatus-report.component';
import {NgbdLabPlantReportComponent} from './report/labplant/labplant-report.component';
import {NgbdFormTypeReportComponent} from './report/formtype/formtype-report.component';
import {NgbdDistributionReportComponent} from './report/distribution/distribution-report.component';
import {NgbdWithdrawReportComponent} from './report/withdraw/withdraw-report.component';
import {NgbdRecoveryReportComponent} from './report/recovery/recovery-report.component';
import {NgbdAllstatusReportComponent} from './report/allstatus/allstatus-report.component';
import {NgbdReportTypeReportComponent} from './report/reporttype/reporttype-report.component';
import {NgbdRegisterVolsListComponent} from './register-vols/listing/listing.component';
import {NgbdListingRegisterPlant12Component} from './register-plant-1-2/listing/listing.component';
import {NgbdListingRegisterPlant3Component} from './register-plant-3/listing/listing.component';
import {NgbdAttributeSetListComponent} from './attribute-set/listing/listing.component';
import {NgbdAddUpdateAttributeSetComponent} from './attribute-set/add-update/add-update.component';
import {NgbdAddUpdateFormKeepComponent} from './form-keep/add-update/add-update.component';
import {NgbdAttributeGroupSetListComponent} from './attribute-set-list/listing/listing.component';
import {NgbdAddUpdateAttributeSetListComponent} from './attribute-set-list/add-update/add-update.component';
import {NgbdAttributePlantDOAListComponent} from './plant-doa/listing/listing.component';
import {NgbdAddUpdatePlantDOAComponent} from './plant-doa/add-update/add-update.component';
import {NgbdAddUpdatePlantComponent} from './plant/add-update/add-update.component';
import {NgbdListingPlantComponent} from './plant/listing/listing.component';
import {NgbdAddUpdatePlantRegisterComponent} from './register-plant-1-2/add-update/add-update.component';
import {NgbdAddUpdatePlantLabComponent} from './lab-manage/add-update/add-update.component';
import {NgbdListingLabComponent} from './lab-manage/listing/listing.component';
import {NgbdCreateLabComponent} from './lab-manage/listing-create/listing.component';
import {NgbdPlantsLabComponent} from './lab-manage/listing-pending/listing.component';
import {NgbdPlantsCompletedComponent} from './lab-manage/plant-completed/listing.component';
import {NgbdWHLabListingComponent} from './warehouse/lab-no-listing/listing.component';
import {NgbdWHLabAlertListingComponent} from './warehouse/lab-no-listing-alert/listing.component';
import {NgbdWHLabPlantComponent} from './warehouse/lab-plant/lab-plant.component';
import {NgbdAddUpdateNewPlantComponent} from './new-plant/add-update/add-update.component';
import {NgbdWHPendingCompleteListingComponent} from './warehouse/listing-pending-complete/listing.component';
import {NgbdRegisterRecoveryListComponent} from './recovery/listing/listing.component';
import {NgbdRegisterRecoveryListPendingComponent} from './recovery/listing-pending/listing.component';
import {NgbdRegisterRecoveryListPendingCompleteComponent} from './recovery/recover-listing/listing.component';
import {NgbdSendReceiveComponent} from './recovery/send-receive/add-update.component';
import {NgbdRegisterPlantUnassignedComponent} from './plant-unassign-list/listing.component';
import {NgbdAddUpdatePlantRegister4Component} from './register-plant-4/add-update.component';
import {NgbdLabAlertListPendingComponent} from './lab-alert/listing-pending/listing.component';
import {NgbdRegisterAlertFormListPendingCompleteComponent} from './lab-alert/alert-form-listing/listing.component';
import {NgbdListingLabAlertComponent} from './lab-alert/listing/listing.component';
import {NgbdAddUpdateLabAlertLabComponent} from './lab-alert/add-update/add-update.component';
import {NgbdPlantAlertComponent} from './warehouse/plant-alert/plant-alert.component';
import {NgbdWHAlertPendingCompleteListingComponent} from './warehouse/alert-listing-pending-complete/listing.component';
import {NgbdAddUpdateNewPlantAlertComponent} from './form-new-plant-alert/add-update/add-update.component';
import {OnlyNumberDirective} from '../../directive/OnlyNumber';
import {NgbdAddUpdatePlantRegister3Component} from './register-plant-3/add-update/add-update.component';
import {NgbdWithDrawListComponent} from './withdraw/listing/listing.component';
import {NgbdRegisterWithDrawListPendingComponent} from './withdraw/listing-pending/listing.component';
import {NgbdRegisterWithDrawListPlantsComponent} from './withdraw/listing-withdraw-plants/listing.component';
import {NgbdAddUpdateFormBankOfPlantComponent} from './withdraw/form-bank-of-plant/add-update/add-update.component';
import {NgbdWithWareHouseDrawListComponent} from './warehouse/withdraw-listing/listing.component';
import {NgbdRegisterWithDrawListPlantsWarehouseComponent} from './warehouse/listing-withdraw-plants/listing.component';
import {NgbdWithDistributionListComponent} from './distribution/listing/listing.component';
import {NgbdRegisterDistributionListPendingComponent} from './distribution/listing-pending/listing.component';
import {NgbdRegisterDistributionPlantsComponent} from './distribution/listing-withdraw-plants/listing.component';
import {NgbdAddUpdateDistributionFormBankOfPlantComponent} from './distribution/form-bank-of-plant/add-update/add-update.component';
import {NgbdWithWareHouseDistributionListComponent} from './warehouse/distribution-listing/listing.component';
import {NgbdRegisterDistributionListPlantsWarehouseComponent} from './warehouse/listing-distribution-plants/listing.component';
import {NgbdWHDistributionStatusComponent} from './warehouse/distrubtion-status/listing.component';
import {NgbdListingEventNewsCategoryComponent} from './frontend/event-news/listing/listing.component';
import {NgbdAddUpdateEventNewsComponent} from './frontend/event-news/add-update/add-update.component';
import {NgbdListingKnowledgeNewsCategoryComponent} from './frontend/knowledge/listing/listing.component';
import {NgbdAddUpdateKnowledgeComponent} from './frontend/knowledge/add-update/add-update.component';
import {NgbdListingBannerCategoryComponent} from './frontend/banner/listing/listing.component';
import {NgbdAddUpdateBannerComponent} from './frontend/banner/add-update/add-update.component';
import {NgbdAddUpdateIntroComponent} from './frontend/intro/add-update/add-update.component';
import {NgbdListingIntroCategoryComponent} from './frontend/intro/listing/listing.component';
import {NgbdListingAccessmentCategoryComponent} from './frontend/accessments/listing/listing.component';
import {NgbdListingFrontendUserComponent} from './frontend/users/listing/listing.component';
import {NgbdAddUpdateUserFrontendComponent} from './frontend/users/add-update/add-update.component';
import {NgbdAddUpdateWithdrawFormKeepComponent} from './withdraw/form-keep/add-update/add-update.component';
import {NgbdRegisterAlertDistributionListPlantsWarehouseComponent} from './warehouse/listing-alert-distribution-plants/listing.component';
import {NgbdWithWareHouseRecoveryAlertListComponent} from './warehouse/recovery-alert-listing/listing.component';	
import {NgbdRegisterAlertRecoveryListPlantsWarehouseComponent} from './warehouse/listing-alert-recovery-plants/listing.component';
import {NgbdListingAlertComponent} from './lab-alert/alert-list/alert-list.component';	
import {NgbdListingRecoveryAlertComponent} from './recovery/alert-list/alert-list.component';
import {NgbdWithWareHouseDistributionAlertListComponent} from './warehouse/distribution-alert-listing/listing.component';
import {NgbdM4CompletedListComponent} from './m4/listing/listing.component';
import { NgbdExportDoaComponent } from './export-doa/export-doa.component';

@NgModule({
  imports: [
    NgxLoadingModule.forRoot({}),
    TreeviewModule.forRoot(),
    CommonModule,
    RouterModule.forChild(LoggedInRoutes),
    NgxMaskModule.forRoot({}),
    FormsModule,
    ReactiveFormsModule,
    AutocompleteLibModule,
    NgbModule,
    AngularEditorModule,
    NgxFlagIconCssModule,
    DataTableModule,
    DataTablesModule,
    TagInputModule,
    ImageUploaderModule,
    ComponentsModule,
    AutocompleteModule.forRoot()
  ],
  declarations: [
    NgbdCreateUpdateTeamMemberComponent,
    NgbdListingTeamMemberComponent,
    OnlyNumberDirective,
    NgbdAdminManageComponent,
    NgbdAddRoleComponent,
    NgbdListingRoleComponent,
    NgbdAddUpdatePlantCategoryComponent,
    NgbdListingPlantCategoryComponent,
    NgbdListingPlantCharacterComponent,
    NgbdAddUpdatePlantCharacterComponent,
    NgbdAddUpdatePlantGroupComponent,
    NgbdListingPlantGroupComponent,
    NgbdAddUpdatePlantGroupTypeComponent,
    NgbdListingPlantGroupTypeComponent,
    NgbdListingPlantTypeComponent,
    NgbdAddUpdatePlantTypeComponent,
    NgbdCommonProvinceListComponent,
    NgbdAddUpdateCommonProvinceListComponent,
    NgbdAddUpdateCommonUnitListComponent,
    NgbdCommonUnitListComponent,
    NgbdAddUpdateAumperCountryComponent,
    NgbdCommonAmuperListComponent,
    NgbdCommonConditionComponent,
    NgbdAlertListComponent,
    NgbdAlertListDisComponent,
    NgbdAddUpdateCommonConditionComponent,
    NgbdAddUpdateCommonCountryComponent,
    NgbdCommonCountryListComponent,
    NgbdRegisterPassportListComponent,
    NgbdRegisterVolsListComponent,
    NgbdListingRegisterPlant12Component,
    NgbdListingRegisterPlant3Component,
    NgbdAddUpdatePlantRegister3Component,
    NgbdAttributeSetListComponent,
    NgbdAddUpdateAttributeSetComponent,
    NgbdAddUpdateFormKeepComponent,
    NgbdRegisterWithDrawListPendingComponent,
    NgbdAddUpdateFormBankOfPlantComponent,
    NgbdWithDrawListComponent,
    NgbdRegisterWithDrawListPlantsComponent,
    NgbdAttributeGroupSetListComponent,
    NgbdWithWareHouseDrawListComponent,
    NgbdAddUpdateAttributeSetListComponent,
    NgbdAttributePlantDOAListComponent,
    NgbdAddUpdatePlantDOAComponent,
    NgbdRegisterWithDrawListPlantsWarehouseComponent,
    NgbdWithDistributionListComponent,
    NgbdRegisterDistributionListPendingComponent,
    NgbdRegisterDistributionPlantsComponent,
    NgbdAddUpdateDistributionFormBankOfPlantComponent,
    NgbdAddUpdatePlantComponent,
    NgbdRegisterPlantUnassignedComponent,
    NgbdAddUpdatePlantRegister4Component,
    NgbdListingPlantComponent,
    NgbdAddUpdatePlantRegisterComponent,
    NgbdListingLabComponent,
    NgbdRegisterDistributionListPlantsWarehouseComponent,
    NgbdWHDistributionStatusComponent,
    NgbdCreateLabComponent,
    NgbdPlantsLabComponent,
    NgbdAddUpdatePlantLabComponent,
    NgbdPlantsCompletedComponent,
    NgbdWithWareHouseDistributionListComponent,
    NgbdWHLabListingComponent,
    NgbdWHLabAlertListingComponent,
    NgbdWHLabPlantComponent,
    NgbdWHPendingCompleteListingComponent,
    NgbdAddUpdateNewPlantComponent,
    NgbdRegisterRecoveryListComponent,
    NgbdRegisterRecoveryListPendingComponent,
    NgbdRegisterRecoveryListPendingCompleteComponent,
    NgbdSendReceiveComponent,
    NgbdLabAlertListPendingComponent,
    NgbdRegisterAlertFormListPendingCompleteComponent,
    NgbdListingLabAlertComponent,
    NgbdAddUpdateLabAlertLabComponent,
    NgbdAddUpdateNewPlantAlertComponent,
    NgbdWHAlertPendingCompleteListingComponent,
    NgbdListingEventNewsCategoryComponent,
    NgbdAddUpdateEventNewsComponent,
    NgbdListingKnowledgeNewsCategoryComponent,
    NgbdAddUpdateKnowledgeComponent,
    NgbdListingBannerCategoryComponent,
    NgbdAddUpdateBannerComponent,
    NgbdAddUpdateIntroComponent,
    NgbdListingIntroCategoryComponent,
    NgbdListingAccessmentCategoryComponent,
    NgbdListingFrontendUserComponent,
    NgbdAddUpdateUserFrontendComponent,
    NgbdPassportYearReportComponent,
    NgbdPlantMonthReportComponent,
    NgbdLabStatusReportComponent,
    NgbdLabPlantReportComponent,
    NgbdFormTypeReportComponent,
    NgbdReportTypeReportComponent,
    NgbdDistributionReportComponent,
    NgbdWithdrawReportComponent,
    NgbdRecoveryReportComponent,
    NgbdAllstatusReportComponent,
    NgbdAddUpdateWithdrawFormKeepComponent,
    NgbdListingAlertComponent,	
    NgbdAlertListComponent,	
    NgbdAlertListDisComponent,
    NgbdWithWareHouseRecoveryAlertListComponent,	
    NgbdRegisterAlertRecoveryListPlantsWarehouseComponent,
    NgbdListingRecoveryAlertComponent,
    NgbdWithWareHouseDistributionAlertListComponent,
    NgbdRegisterAlertDistributionListPlantsWarehouseComponent,
    NgbdM4CompletedListComponent,
    NgbdPlantAlertComponent,
    NgbdExportDoaComponent
  ],
  providers: []
})
export class LoggedInModule {
}
