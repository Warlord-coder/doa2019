import {Component, OnInit} from '@angular/core';
import {DataTableParams, DataTableResource} from 'ngx-datatable-bootstrap4';
import {NgbModal} from '@ng-bootstrap/ng-bootstrap';
import {ActivatedRoute, Router} from '@angular/router';
import {AlertService, PlantCharacterService, WarehouseService} from '../../../../services';
import {first} from 'rxjs/operators';
import {toLower} from 'lodash';
import * as moment from 'moment';

@Component({
  selector: 'app-wh-plant-alert-page',
  templateUrl: './plant-alert.component.html',
})
export class NgbdPlantAlertComponent implements OnInit {
  resources = new DataTableResource<any>([]);
  items: any[] = [];
  tempItems: any[] = [];
  count = 0;
  loading = false;
  momentObj: any;
  alert = false;

  constructor(private modalService: NgbModal,
              private warehouseService: WarehouseService,
              private activeRoute: ActivatedRoute,
              private alertService: AlertService,
              private router: Router) {
    this.momentObj = moment;
  }

  ngOnInit(): void {
    this.activeRoute.params.subscribe(params => {
      if (params.id === 'alert') {
        this.alert = true;
      }
      this.onGetWareHouseLabListing();
    });
  }

  reloadData(params: DataTableParams) {
    this.resources.query(params).then(vals => {
      this.tempItems = vals;
      this.items = vals;
    });
  }

  // onEdit({_id = ''}) {
  //   if (this.alert) {
  //     return this.router.navigate([`/admin/warehouse/alert-pending-complete/listing/${_id}`]);
  //   }
  //   this.router.navigate([`/admin/warehouse/pending-complete/listing/${_id}`]);
  // }

  onEdit(item) {
    let plant_id = item['register_plant'];
    let volume_id = item['volume']['_id'];
    let lab_id = item['m8_lab_id']['_id'];
    this.router.navigate([`/admin/labs/plants/${plant_id}/${volume_id}/${lab_id}`]);
  }

  onDelete(item: object, deleteContent) {
    this.modalService.open(deleteContent, {centered: true});
  }

  onSearch({target: {value: searchRole = ''} = {}}) {
    this.items = this.tempItems.filter(({lab_no = ''}) => lab_no && toLower(lab_no).includes(toLower(searchRole)));
  }
  onSearchRegis({target: {value: searchRole = ''} = {}}) {
    this.items = this.tempItems.filter(({reg_gene_id}) => {
      try {
        return toLower(reg_gene_id).includes(toLower(searchRole))
      } catch (error) {
        
      }
    });
  }
  onSearchGs({target: {value: searchRole = ''} = {}}) {
    this.items = this.tempItems.filter(({gs_no}) => {
      try {
        return toLower(gs_no).includes(toLower(searchRole))
      } catch (error) {
        
      }
    });
  }
  onSearchCate({target: {value: searchRole = ''} = {}}) {
    this.items = this.tempItems.filter(({plant_gene_name}) => {
      try {
        return toLower(plant_gene_name.plantCategory.categoryName_th).includes(toLower(searchRole))
      } catch (error) {
        
      }
    });
  }
  onSearchType({target: {value: searchRole = ''} = {}}) {
    this.items = this.tempItems.filter(({plant_gene_name}) => {
      try {
        return toLower(plant_gene_name.plantTypeName_th).includes(toLower(searchRole))
      } catch (error) {
        
      } 
    });
  }
  onSearchSubmit({target: {value: searchRole = ''} = {}}){
    this.items = this.tempItems.filter((item) => {
      try {
         if(item.submitTo == 'm4' || item.recovery_status == 'pending'|| item.recovery_status == 'approve'|| item.recovery_status == 'complete'){
            if(searchRole == 'M4'){
              return true
            }else if(searchRole == 'M8'){
              return false
            } else{
              return false
            } 
          }else if(this.checkNotiRoom(item) || item.alertFormStatus == 'pending'|| item.alertFormStatus == 'approve'||item.alertFormStatus == 'complete'){
            if(searchRole == 'M4'){
              return false
            }else if(searchRole == 'M8'){
              return true
            } else{
              return false
            } 
          }else{
            if(searchRole == 'M4'){
              return false
            }else if(searchRole == 'M8'){
              return false
            } else{
              return true
            } 
          }

      } catch (error) {
        return false
      } 
    });
    this.count = this.items.length;
  }

  onGetWareHouseLabListing() {
    this.loading = true;
    this.warehouseService.getAllAlertPlant()
      .pipe(first())
      .subscribe(
        ({data = []}: any) => {
          this.loading = false;
          this.resources = new DataTableResource<any>(data);
          this.resources.count().then(count => this.count = count);
          this.reloadData({});
        },
        error => {
          this.loading = false;
          this.alertService.error(error);
        });
  }
  checkCondition(item){
    try {
      if(item.submitTo == 'm4' || item.recovery_status == 'pending'|| item.recovery_status == 'approve'|| item.recovery_status == 'complete'){
        return '../../assets/images/m4-red.png';
      }else if(this.checkNotiRoom(item) || item.alertFormStatus == 'pending'|| item.alertFormStatus == 'approve'||item.alertFormStatus == 'complete'){
        return '../../assets/images/m8-yellow.png';
      }else{
        return '../../assets/images/not-gray.png';
      }
    } catch (error) {
      return '../../assets/images/not-gray.png';
    }
  }
  checkNotiRoom(data){
    let d = [];	
    if(data.room5 != null){	
        let d5 = data.room5;	
        for(let j=0;j<d5.length;j++){	
            if(parseFloat(d5[j].notification_period.weight) < parseFloat(d5[j].wh_batch_weight) && 	
            parseFloat(d5[j].notification_period.percent) < parseFloat(d5[j].lab_test_growth_percent)	
              && this.checkYear(parseFloat(d5[j].notification_period.year),d5[j].wh_batch_store_date)){	
                d.push(j);	
            }	
        }	
    }	
    if(data.room10 != null){	
        let d5 = data.room10;	
        for(let j=0;j<d5.length;j++){	
            if(parseFloat(d5[j].notification_period.weight) < parseFloat(d5[j].wh_batch_weight) && 	
            parseFloat(d5[j].notification_period.percent) < parseFloat(d5[j].lab_test_growth_percent)	
              && this.checkYear(parseFloat(d5[j].notification_period.year),d5[j].wh_batch_store_date)){	
                d.push(j);	
            }	
        }	
    }	
    if(d.length!= 0){
      return true;
    }else{
      return false;
    }
  }
  checkYear(val,date: any) {	
    let d = new Date(date);	
    let year = d.getFullYear();	
    if(!isNaN(year)){	
        if(val <= year){	
            return true	
        }else{	
            return false	
        }	
    }	
    return false;	
}

  getStockInfo(item) {
    const {
      selfing_input, selfing_dropdown,
      oop_input, oop_dropdown,
      other_input, other_dropdown
    } = item;
    const stock = {
      isSelfing: selfing_input && selfing_dropdown,
      isOOP: oop_input && oop_dropdown,
      isOther: other_input && other_dropdown,
    };
    if (stock.isSelfing) {
      return `Self ${selfing_input} ${selfing_dropdown}`;
    }
    if (stock.isOOP) {
      return `OP ${oop_input} ${oop_dropdown}`;
    }
    if (stock.isOther) {
      return `Other ${other_input} ${other_dropdown}`;

    }
  }
}
