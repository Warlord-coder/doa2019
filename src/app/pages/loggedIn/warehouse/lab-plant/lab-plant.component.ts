import {Component, OnInit} from '@angular/core';
import {DataTableParams, DataTableResource} from 'ngx-datatable-bootstrap4';
import {NgbModal} from '@ng-bootstrap/ng-bootstrap';
import {ActivatedRoute, Router} from '@angular/router';
import {AlertService, PlantCharacterService, WarehouseService} from '../../../../services';
import {first} from 'rxjs/operators';
import {toLower, sortBy} from 'lodash';
import * as moment from 'moment';

@Component({
  selector: 'app-wh-lab-plant-page',
  templateUrl: './lab-plant.component.html',
})
export class NgbdWHLabPlantComponent implements OnInit {
  resources = new DataTableResource<any>([]);
  items: any[] = [];
  tempItems: any[] = [];
  count = 0;
  loading = false;
  momentObj: any;
  alert = false;

  constructor(private modalService: NgbModal,
              private warehouseService: WarehouseService,
              private activeRoute: ActivatedRoute,
              private alertService: AlertService,
              private router: Router) {
    this.momentObj = moment;
  }

  ngOnInit(): void {
    this.activeRoute.params.subscribe(params => {
      if (params.id === 'alert') {
        this.alert = true;
      }
      this.onGetWareHouseLabListing();
    });
  }

  reloadData(params: DataTableParams) {
    this.resources.query(params).then(vals => {
      this.tempItems = vals;
      this.items = vals;
    });
  }

  // onEdit({_id = ''}) {
  //   if (this.alert) {
  //     return this.router.navigate([`/admin/warehouse/alert-pending-complete/listing/${_id}`]);
  //   }
  //   this.router.navigate([`/admin/warehouse/pending-complete/listing/${_id}`]);
  // }

  onEdit(item) {
    let plant_id = item['register_plant'];
    let volume_id = item['volume']['_id'];
    let lab_id = item['m8_lab_id']['_id'];
    this.router.navigate([`/admin/labs/plants/${plant_id}/${volume_id}/${lab_id}`]);
  }

  onDelete(item: object, deleteContent) {
    this.modalService.open(deleteContent, {centered: true});
  }

  onSearch({target: {value: searchRole = ''} = {}}) {
    this.items = this.tempItems.filter(item => {
      if(item['m8_lab_id'] && toLower(item['m8_lab_id']['lab_no']).includes(toLower(searchRole)) || item['reg_gene_id'] && toLower(item['reg_gene_id']).includes(toLower(searchRole))) return true;
    });
  }

  onGetWareHouseLabListing() {
    this.loading = true;
    this.warehouseService.getSpecificLab('all')
      .pipe(first())
      .subscribe(
        ({data = []}: any) => {
          let res = sortBy(data, "createdAt").reverse();
          this.loading = false;
          this.resources = new DataTableResource<any>(res);
          this.resources.count().then(count => this.count = count);
          this.reloadData({offset: 0, limit: 50});
        },
        error => {
          this.loading = false;
          this.alertService.error(error);
        });
  }

  getStockInfo(item) {
    const {
      selfing_input, selfing_dropdown,
      oop_input, oop_dropdown,
      other_input, other_dropdown
    } = item;
    const stock = {
      isSelfing: selfing_input && selfing_dropdown,
      isOOP: oop_input && oop_dropdown,
      isOther: other_input && other_dropdown,
    };
    if (stock.isSelfing) {
      return `Self ${selfing_input} ${selfing_dropdown}`;
    }
    if (stock.isOOP) {
      return `OP ${oop_input} ${oop_dropdown}`;
    }
    if (stock.isOther) {
      return `Other ${other_input} ${other_dropdown}`;

    }
  }
}
