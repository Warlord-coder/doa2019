import {Component, OnInit} from '@angular/core';
import {DataTableParams, DataTableResource} from 'ngx-datatable-bootstrap4';
import {NgbModal} from '@ng-bootstrap/ng-bootstrap';
import {ActivatedRoute, Router} from '@angular/router';
import {toLower} from 'lodash';
import {FormBuilder, FormGroup, Validators} from '@angular/forms';
// @ts-ignore
import {AlertService, DistributionService,LabAlertFormService,RecoveryService} from '../../../../services';
import {first} from 'rxjs/operators';
import * as moment from 'moment';

@Component({
  selector: 'app-listing-common-country-page',
  templateUrl: './listing.component.html',
})
export class NgbdRegisterAlertRecoveryListPlantsWarehouseComponent implements OnInit {
  resources = new DataTableResource<any>([]);
  items: any[] = [];
  count = 0;
  registerPassportForm: FormGroup;
  tempItems: any[] = [];
  submitted = false;
  id = null;
  selectedItem = null;
  momentObj: any;
  file = null;
  distributionInfo = null;
  loading = false;
  currentStatus = 'draft';
  selectedPlantsForSubmit: any = [];
  apiError = false;
  options = [];
  max = 0;
  error = null;
  labTXT = '';
  constructor(private modalService: NgbModal,
              private formBuilder: FormBuilder,
              private labAlert: LabAlertFormService,
              private activeRoute: ActivatedRoute,
              private reco: RecoveryService,
              private alertService: AlertService,
              private distributionService: DistributionService,
              private router: Router) {
    this.momentObj = moment;
  }

  ngOnInit() {
    this.registerPassportForm = this.formBuilder.group({
      admin_amount: ['', Validators.required],
      batch: ['', Validators.required]
    });
    this.activeRoute.params.subscribe(params => {
      const {id} = params;
      this.id = id;
      //this.onGetSpecificWithDraw();
      this.onGetWithDrawPlantListing('pending');
    });
  }

  get f() {
    if (!this.apiError && this.submitted && !this.registerPassportForm.invalid) {
      this.alertService.error(null);
    }

    return this.registerPassportForm.controls;
  }

  reloadData(params: DataTableParams) {
    this.resources.query(params).then(vals => {
      this.items = vals;
      this.tempItems = this.items;
    });
  }

  onSearch({target: {value: searchRole = ''} = {}}) {
    this.items = this.tempItems.filter(({passport: {passport_no = ''} = {}}) => passport_no && toLower(passport_no).includes(toLower(searchRole)));
  }

  onGetWithDrawPlantListing(status) {
    this.loading = true;
    this.currentStatus = status;
    this.selectedPlantsForSubmit = [];
    this.reco.getRecoveryAlertM9(this.id)
      .pipe(first())
      .subscribe(
        ({data = []}: any) => {
          this.resources = new DataTableResource<any>(data);
          this.resources.count().then(count => {
            this.count = count
            if(this.count != 0){
              this.labTXT = data[0].recovery_id.m_no;
            }
          });
          this.reloadData({});
          this.loading = false;
          
          
        },
        error => {
          this.loading = false;
          this.alertService.error(error);
        });
  }

  // onGetSpecificWithDraw() {
  //   this.loading = true;
  //   this.labAlert.getSpecific(this.id)
  //     .pipe(first())
  //     .subscribe(
  //       ({data = []}: any) => {
  //         this.distributionInfo = data;
  //         this.loading = false;
  //       },
  //       error => {
  //         this.loading = false;
  //         this.alertService.error(error);
  //       });
  // }

  onChangeItemSelect({_id}) {
    if (this.selectedPlantsForSubmit.includes(_id)) {
      this.selectedPlantsForSubmit = this.selectedPlantsForSubmit.filter(id => id !== _id);
      return;
    }

    this.selectedPlantsForSubmit = [...this.selectedPlantsForSubmit, _id];
  }

  onSubmitPlants(status, index = 1) {
    if (!this.selectedPlantsForSubmit || !this.selectedPlantsForSubmit.length) {
      return;
    }
    if (index === 1) {
      for (const plant of this.selectedPlantsForSubmit) {
        for (const item of this.items) {
          if (item._id === plant && !item.withdraw_request_stock) {
            this.alertService.error('ปริมาณเมล็ดเชื้อพันธุ์ที่ให้บริการ is missing for ' + item.reg_gene_id);
            return;
          }
        }
      }
    }

    if (index === 2) {
      for (const plant of this.selectedPlantsForSubmit) {
        for (const item of this.items) {
          if (item._id === plant && !item.admin_amount) {
            this.alertService.error('Amount is missing for ' + item.reg_gene_id);
            return;
          }
        }
      }

      return;
    }

    this.loading = true;
    this.reco.changeRecoveryStatus(status, this.selectedPlantsForSubmit)
      .pipe(first())
      .subscribe(
        ({data = {}}: any) => {
          this.selectedPlantsForSubmit = [];
          this.onGetWithDrawPlantListing('pending');
        },
        error => {
          this.loading = false;
          this.alertService.error(error);
        });
  }

  getStockInfo(item) {
    const {
      selfing_input, selfing_dropdown,
      oop_input, oop_dropdown,
      other_input, other_dropdown
    } = item;
    const stock = {
      isSelfing: selfing_input && selfing_dropdown,
      isOOP: oop_input && oop_dropdown,
      isOther: other_input && other_dropdown,
    };
    if (stock.isSelfing) {
      return `Self ${selfing_input} ${selfing_dropdown}`;
    }
    if (stock.isOOP) {
      return `OP ${oop_input} ${oop_dropdown}`;
    }
    if (stock.isOther) {
      return `Other ${other_input} ${other_dropdown}`;

    }
  }

  onEdit(createContent, item) {
    this.selectedItem = item;
    this.setBatchOptions();
    this.registerPassportForm.reset();
    this.file = null;
    this.modalService.open(createContent, {centered: true});
  }

  onSubmit() {
    this.submitted = true;
    if (this.registerPassportForm.invalid) {
      return;
    }
    if (this.max < this.registerPassportForm.value.admin_amount) {
      this.error = 'Stock amount must be less than or equal to warehouse batch weight';
      setTimeout(() => {
        this.error = null;
      }, 2500);
      return;
    }

    this.modalService.dismissAll();
    this.alertService.reset();
    this.loading = true;
    this.reco.updateStatusM9(this.selectedItem._id, this.registerPassportForm.value)
      .pipe(first())
      .subscribe(
        ({data = []}: any) => {
          this.distributionInfo = data;
          this.onGetWithDrawPlantListing('pending');
          this.registerPassportForm.reset();
          this.submitted = false;
        },
        error => {
          this.loading = false;
          this.submitted = false;
          this.alertService.error(error);
        });
  }

  setBatchOptions() {
    const {room10 = [], room5 = []} = this.selectedItem;
    const options = [];
    for (const room of room10) {
      const {wh_batch_weight, batch_no} = room;
      options.push({value: `room -10 batch ${batch_no} - ${wh_batch_weight} gram`, max: wh_batch_weight});
    }
    for (const room of room5) {
      const {wh_batch_weight, batch_no} = room;
      options.push({value: `room -5 batch ${batch_no} - ${wh_batch_weight} gram`, max: wh_batch_weight});
    }

    this.options = options;
  }

  setMaxStock({target: {value = ''} = {}}) {
    for (const option of this.options) {
      if (option.value === value) {
        this.max = option.max;
      }
    }
  }
}
