import {Component, OnInit} from '@angular/core';
import {DataTableParams, DataTableResource} from 'ngx-datatable-bootstrap4';
import {NgbModal} from '@ng-bootstrap/ng-bootstrap';
import {ActivatedRoute, Router} from '@angular/router';
import {AlertService, LabService, PlantCharacterService, WarehouseService} from '../../../../services';
import {first} from 'rxjs/operators';
import {toLower} from 'lodash';
import * as moment from 'moment';

@Component({
  selector: 'app-wh-pending-complete-listing-page',
  templateUrl: './listing.component.html',
})
export class NgbdWHPendingCompleteListingComponent implements OnInit {
  resources = new DataTableResource<any>([]);
  items: any[] = [];
  tempItems: any[] = [];
  count = 0;
  loading = false;
  momentObj: any;
  id = null;
  status = null;
  labInfo = null;

  constructor(private modalService: NgbModal,
              private plantCharacterService: PlantCharacterService,
              private warehouseService: WarehouseService,
              private activeRoute: ActivatedRoute,
              private labService: LabService,
              private alertService: AlertService,
              private router: Router) {
    this.momentObj = moment;
  }

  ngOnInit(): void {
    this.activeRoute.params.subscribe(params => {
      this.id = params.id;
      this.onGetWHPlantListing();
      this.onGetLabInfo();
    });
  }

  reloadData(params: DataTableParams) {
    this.resources.query(params).then(vals => {
      this.tempItems = vals;
      this.items = vals;
    });
  }

  onEdit({_id, volume }: any) {
    const {form_type = ''} = volume || {}
    if (volume && form_type === 'Register 3') {
      this.router.navigate([`/admin/warehouse/form-keep/${_id}`]);
      return;
    }

    this.router.navigate([`/admin/warehouse/form-new-plant/${_id}`]);
  }

  onDelete(item: object, deleteContent) {
    this.modalService.open(deleteContent, {centered: true});
  }

  onSearch({target: {value: searchRole = ''} = {}}) {
    this.items = this.tempItems.filter(({reg_gene_id}) => reg_gene_id && toLower(reg_gene_id).includes(toLower(searchRole)));
  }

  onGetWHPlantListing(status = 'draft') {
    this.loading = true;
    this.status = status;
    this.warehouseService.getSpecificLab(this.id, status)
      .pipe(first())
      .subscribe(
        ({data = []}: any) => {
          this.loading = false;
          this.resources = new DataTableResource<any>(data);
          this.resources.count().then(count => this.count = count);
          this.reloadData({});
        },
        error => {
          this.loading = false;
          this.alertService.error(error);
        });
  }

  onGetLabInfo() {
    this.loading = true;
    this.labService.getSpecific(this.id)
      .pipe(first())
      .subscribe(
        ({data = []}: any) => {
          this.loading = false;
          this.labInfo = data;
        },
        error => {
          this.loading = false;
          this.alertService.error(error);
        });
  }

  getStockInfo(item) {
    const {
      selfing_input, selfing_dropdown,
      oop_input, oop_dropdown,
      other_input, other_dropdown
    } = item;
    const stock = {
      isSelfing: selfing_input && selfing_dropdown,
      isOOP: oop_input && oop_dropdown,
      isOther: other_input && other_dropdown,
    };
    if (stock.isSelfing) {
      return `Self ${selfing_input} ${selfing_dropdown}`;
    }
    if (stock.isOOP) {
      return `OP ${oop_input} ${oop_dropdown}`;
    }
    if (stock.isOther) {
      return `Other ${other_input} ${other_dropdown}`;

    }
  }
}
