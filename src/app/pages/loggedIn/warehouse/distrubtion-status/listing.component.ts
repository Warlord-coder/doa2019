import { Component } from '@angular/core';
import {AlertService, PlantCharacterService, WarehouseService} from '../../../../services';
import {first} from 'rxjs/operators';
import {ActivatedRoute, Router} from '@angular/router';

import {NgbModal} from '@ng-bootstrap/ng-bootstrap';

import * as moment from 'moment';
@Component({
  selector: 'app-distribution-status',
  templateUrl: './listing.component.html'
})
export class NgbdWHDistributionStatusComponent {
  
  info = [];

  title = 'angulardatatables';
  dtOptions: DataTables.Settings = {};
  momentObj: any;
  loading = false;

  selectedItem = null;
  isDistribution;
  selectedIndex;
  roles = [];
  constructor(private modalService: NgbModal,
      private warehouseService: WarehouseService,
      private activeRoute: ActivatedRoute,
      private alertService: AlertService,
      private router: Router,
      private route: ActivatedRoute) {
  this.momentObj = moment;
  }

  ngOnInit() {
    this.route
      .data
      .subscribe(data => {
        this.roles = data['roles'];
      });
    this.dtOptions = {
      pagingType: 'full_numbers',
      pageLength: 25,
      serverSide: true,
      lengthChange: true,
      responsive:true,
      ajax: (dataTablesParameters: any, callback) => {
        const {start, length} = dataTablesParameters;
        const keyword = dataTablesParameters.search.value;
        this.warehouseService.getFilterList({start: start, length: length, keyword: keyword})
        .pipe(first())
        .subscribe(
          ({data = []}: any) => {
            this.loading = false;
            this.info = data['data'];
            data['data'].forEach(element => {
              if(element.isDistribution !=undefined && element.isDistribution == true){
                element.dis_status = 'Open'
              }
              else {
                element.dis_status = 'Close'
              }
            });        
            callback({
              recordsTotal: data['total_rows'],
              recordsFiltered: data['total_rows'],
              data: []
            })
          },
          error => {
            this.loading = false;
            this.alertService.error(error);
          });
        
      },
    };
  }

  getStockInfo(item) {
    const {
      selfing_input, selfing_dropdown,
      oop_input, oop_dropdown,
      other_input, other_dropdown
    } = item;
    const stock = {
      isSelfing: selfing_input && selfing_dropdown,
      isOOP: oop_input && oop_dropdown,
      isOther: other_input && other_dropdown,
    };
    if (stock.isSelfing) {
      return `Self ${selfing_input} ${selfing_dropdown}`;
    }
    if (stock.isOOP) {
      return `OP ${oop_input} ${oop_dropdown}`;
    }
    if (stock.isOther) {
      return `Other ${other_input} ${other_dropdown}`;

    }
  }

  onEdit(createContent, item, index) {
    this.selectedItem = item;
    this.isDistribution = item.isDistribution;
    this.selectedIndex = index;
    this.modalService.open(createContent, {centered: true});
  }

  onSubmit() {
    this.loading = true;
    this.modalService.dismissAll();
    this.warehouseService.createOrUpdate({isDistribution: this.isDistribution}, this.selectedItem._id)
      .pipe(first())
      .subscribe(
        ({data = []}: any) => {
          if(this.isDistribution == true){
            this.info[this.selectedIndex].dis_status = "Open";
            this.info[this.selectedIndex].isDistribution = true;
          } else {
            this.info[this.selectedIndex].dis_status = "Close";
            this.info[this.selectedIndex].isDistribution = false;
          }
            
          //this.selectedItem.isDistribution = true;
          //this.onGetWareHouseLabListing();
          this.loading = false;
        },
        error => {
          this.loading = false;
          this.alertService.error(error);
        });
  }
  checkRole(index){
    if(localStorage.getItem('superAdmin') === "true") return true;
    if(localStorage.getItem('rolesId').indexOf(this.roles[index]) > -1) return true;
    return false;
  }
}
