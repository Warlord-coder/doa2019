import {Component, OnInit} from '@angular/core';
import {DataTableParams, DataTableResource} from 'ngx-datatable-bootstrap4';
import {NgbModal} from '@ng-bootstrap/ng-bootstrap';
import {ActivatedRoute, Router} from '@angular/router';
import {toLower} from 'lodash';
import {FormBuilder, FormGroup, Validators} from '@angular/forms';
// @ts-ignore
import {AlertService, DistributionService, WarehouseService} from '../../../../services';
import {first} from 'rxjs/operators';
import * as moment from 'moment';

@Component({
  selector: 'app-listing-common-country-page',
  templateUrl: './listing.component.html',
})
export class NgbdRegisterDistributionListPlantsWarehouseComponent implements OnInit {
  resources = new DataTableResource<any>([]);
  items: any[] = [];
  count = 0;
  registerPassportForm: FormGroup;
  tempItems: any[] = [];
  submitted = false;
  id = null;
  selectedItem = null;
  momentObj: any;
  file = null;
  distributionInfo = null;
  loading = false;
  currentStatus = 'draft';
  selectedPlantsForSubmit: any = [];
  apiError = false;
  options = [];
  max = 0;
  error = null;
  withdraw_stock_list = null;

  constructor(private modalService: NgbModal,
              private formBuilder: FormBuilder,
              private activeRoute: ActivatedRoute,
              private alertService: AlertService,
              private distributionService: DistributionService,
              private wareHouseService: WarehouseService,
              private router: Router) {
    this.momentObj = moment;
  }

  ngOnInit() {
    this.registerPassportForm = this.formBuilder.group({
      admin_amount: ['', Validators.required],
      batch: ['', Validators.required]
    });
    this.activeRoute.params.subscribe(params => {
      const {id} = params;
      this.id = id;
      this.onGetSpecificWithDraw();
      // this.onGetWithDrawPlantListing('pending');
    });
  }

  get f() {
    if (!this.apiError && this.submitted && !this.registerPassportForm.invalid) {
      this.alertService.error(null);
    }

    return this.registerPassportForm.controls;
  }

  reloadData(params: DataTableParams) {
    this.resources.query(params).then(vals => {
      this.items = vals;
      this.tempItems = this.items;
    });
  }

  onSearch({target: {value: searchRole = ''} = {}}) {
    this.items = this.tempItems.filter(({passport: {passport_no = ''} = {}}) => passport_no && toLower(passport_no).includes(toLower(searchRole)));
  }

  onGetWithDrawPlantListing(status) {
    this.loading = true;
    this.currentStatus = status;
    this.selectedPlantsForSubmit = [];
    this.distributionService.getAllPlants(this.id, status)
    .pipe(first())
    .subscribe(
      ({data = []}: any) => {         
        data.forEach(warehousedata => {
          this.wareHouseService.getSpecific(warehousedata.m9plant_warehouse_id , null , true)
          .pipe(first())
          .subscribe(
            ({data = []}: any) => {
              warehousedata.passport = data.passport;
              warehousedata.reg_gene_id = data.reg_gene_id;
              warehousedata.volume = data.volume;
              warehousedata.gs_no = data.gs_no;
              if(data.oop_input){
                warehousedata.oop_input = data.oop_input;
                warehousedata.oop_dropdown = data.oop_dropdown;
              }
              if(data.other_input){
                warehousedata.other_input = data.other_input;
                warehousedata.other_dropdown = data.other_dropdown;
              }
              if(data.selfing_input){
                warehousedata.selfing_input = data.selfing_input;
                warehousedata.selfing_dropdown = data.selfing_dropdown;
              }    
              
            this.withdraw_stock_list = null;
            if(data.room5.length)
            {
              data.room5.forEach(element => {
                if(element)
                this.withdraw_stock_list= "Room-5 Batch" +element.batch_no +"- "+ element.wh_batch_weight + " ";
                // this.withdraw_stock_list= "Room-5 Batch" +element.batch_no +"- "+ element.wh_batch_weight + " " + (this.withdraw_request_stockSelect?'Seed':'เมล็ด');
              });
            }
            if(data.room10.length)
            {
              data.room10.forEach(element => {
                if(element)
                  this.withdraw_stock_list= "Room-10 Batch" +element.batch_no +"- "+ element.wh_batch_weight + " " ;
                  // this.withdraw_stock_list= "Room-10 Batch" +element.batch_no +"- "+ element.wh_batch_weight + " " + (this.withdraw_request_stockSelect?'Seed':'เมล็ด');
              });
            }
            if(data.room130.length)
            {
              data.room130.forEach(element => {
                if(element)
                  // this.withdraw_stock_list= "ห้อง 130 Batch" +element.storage_weight + " " + (this.withdraw_request_stockSelect?'Seed':'เมล็ด');
                  this.withdraw_stock_list= "ห้อง 130 Batch" + " " + element.storage_weight;
              });
            }
            if(data.anteArray.length)
            {
              data.anteArray.forEach(element => {
                if(element)
                  this.withdraw_stock_list= "ห้อง Ante " + " " +  element.storage_weight;
                  // this.withdraw_stock_list= "ห้อง Ante " + element.storage_weight + " " + (this.withdraw_request_stockSelect?'Seed':'เมล็ด');
                  // this.withdraw_stock_list= "ห้อง Ante Batch" +element.batch_no +"- "+ element.wh_batch_weight + " " + (this.withdraw_request_stockSelect?'Seed':'เมล็ด');
              });
            }          
            warehousedata.withdraw_stock_list = this.withdraw_stock_list;
            },
            error => {
              this.loading = false;
              this.alertService.error(error);
            });
        }); 
        this.resources = new DataTableResource<any>(data);
        this.resources.count().then(count => this.count = count);
        this.reloadData({});
        this.loading = false;     
      },
      error => {
        this.loading = false;
        this.alertService.error(error);
      });
}

  onGetSpecificWithDraw() {
    this.loading = true;
    this.distributionService.getAll('w9')
    .pipe(first())
    .subscribe(
      ({data = []}: any) => {
        data.forEach(element => {
          if(element._id == this.id){
            this.distributionInfo = element;
            this.loading = false;
            const {pending, draft, total, approve} = element; 
            if (pending || draft || total === 0) {
              this.onGetWithDrawPlantListing('pending');
              return;
            }
            if (approve) {
              this.onGetWithDrawPlantListing('pending');
              return;
            }
            this.onGetWithDrawPlantListing('complete');
            return;
          }
        });
      },
      error => {
        this.loading = false;
        this.alertService.error(error);
      });
//     this.distributionService.getSpecific(this.id)
//     .pipe(first())
//     .subscribe(
//       ({data = []}: any) => {  
//       },
//       error => {
//         this.loading = false;
//         this.alertService.error(error);
//       });
}

  onChangeItemSelect({_id}) {
    if (this.selectedPlantsForSubmit.includes(_id)) {
      this.selectedPlantsForSubmit = this.selectedPlantsForSubmit.filter(id => id !== _id);
      return;
    }

    this.selectedPlantsForSubmit = [...this.selectedPlantsForSubmit, _id];
  }

  onSubmitPlants(status, index = 1) {
    if (!this.selectedPlantsForSubmit || !this.selectedPlantsForSubmit.length) {
      return;
    }
    if (index === 1) {
      for (const plant of this.selectedPlantsForSubmit) {
        for (const item of this.items) {
          if (item._id === plant && !item.withdraw_request_stock) {
            this.alertService.error('ปริมาณเมล็ดเชื้อพันธุ์ที่ให้บริการ is missing for ' + item.reg_gene_id);
            return;
          }
        }
      }
    }

    if (index === 2) {
      for (const plant of this.selectedPlantsForSubmit) {
        for (const item of this.items) {
          if (item._id === plant && !item.admin_amount) {
            this.alertService.error('Amount is missing for ' + item.reg_gene_id);
            return;
          }
        }
      }

      return;
    }

    this.loading = true;
    this.distributionService.changedistributionStatus(status, this.selectedPlantsForSubmit)
      .pipe(first())
      .subscribe(
        ({data = {}}: any) => {
          this.selectedPlantsForSubmit = [];
          this.onGetWithDrawPlantListing('pending');
        },
        error => {
          this.loading = false;
          this.alertService.error(error);
        });
  }

  getStockInfo(item) {
    const {
      selfing_input, selfing_dropdown,
      oop_input, oop_dropdown,
      other_input, other_dropdown
    } = item;
    const stock = {
      isSelfing: selfing_input && selfing_dropdown,
      isOOP: oop_input && oop_dropdown,
      isOther: other_input && other_dropdown,
    };
    if (stock.isSelfing) {
      return `Self ${selfing_input} ${selfing_dropdown}`;
    }
    if (stock.isOOP) {
      return `OP ${oop_input} ${oop_dropdown}`;
    }
    if (stock.isOther) {
      return `Other ${other_input} ${other_dropdown}`;

    }
  }

  onEdit(createContent, item) {
    this.selectedItem = item;
    this.setBatchOptions();
    this.registerPassportForm.reset();
    this.file = null;
    this.modalService.open(createContent, {centered: true});
  }

  onSubmit() {
    this.submitted = true;
    if (this.registerPassportForm.invalid) {
      return;
    }

    if (this.max < this.registerPassportForm.value.admin_amount) {
      this.error = 'Stock amount must be less than or equal to warehouse batch weight';
      setTimeout(() => {
        this.error = null;
      }, 2500);
      return;
    }

    this.modalService.dismissAll();
    this.alertService.reset();
    this.loading = true;

    const admin_amount = this.registerPassportForm.value.admin_amount 
    this.distributionService.updatePlant(this.selectedItem._id, this.registerPassportForm.value)
      .pipe(first())
      .subscribe(
        ({data = []}: any) => {
          this.distributionInfo = data;
          this.onGetWithDrawPlantListing('pending');
          this.registerPassportForm.reset();
          this.submitted = false;          
          const _id = this.selectedItem.m9plant_warehouse_id
          this.wareHouseService.getSpecific( _id, null , true)
          .pipe(first())
          .subscribe(
            ({data = []}: any) => {         
            this.withdraw_stock_list = null
            var updatedata = {}
            if(data.room5.length)
            {
              data.room5.forEach(element => {
                if(element){
                  data.room5[element.batch_no-1].wh_batch_weight -= admin_amount
                }
              });
              updatedata['room5'] = JSON.stringify(data.room5)
            }
            if(data.room10.length)
            {
              data.room10.forEach(element => {
                if(element){
                  data.room10[element.batch_no-1].wh_batch_weight -= admin_amount
                }
              });
              updatedata['room10']  = JSON.stringify(data.room10)
            }
            if(data.room130.length)
            {
              data.room130.forEach(element => {
                if(element){
                  data.room130[0].storage_weight -= admin_amount
                }
              });
              updatedata['room130'] = JSON.stringify(data.room130)
            }
            if(data.anteArray.length)
            {
              data.anteArray.forEach(element => {
                if(element){
                  data.anteArray[0].storage_weight -= admin_amount
                }
              });
              updatedata['anteArray'] = JSON.stringify(data.anteArray)
            }          
            this.wareHouseService.createOrUpdate(updatedata ,_id)
            .pipe(first())
            .subscribe(
              () => {
                this.loading = false;
                setTimeout(() => {
                  this.alertService.reset();
                }, 1500);
              },
              error => {
                this.loading = false;
                this.apiError = true;
                this.alertService.error(error);
              });
            this.ngOnInit();
            },
            error => {
              this.loading = false;
              this.alertService.error(error);
            });
        },
        error => {
          this.loading = false;
          this.submitted = false;
          this.alertService.error(error);
        });
  }

  setBatchOptions() {
    // const {room10 = [], room5 = []} = this.selectedItem;
    this.wareHouseService.getSpecific(this.selectedItem.m9plant_warehouse_id)
    .subscribe(
      ({data = []}: any) => {
        const {room10 = [], room5 = []} = data;
        // this.distributionInfo = data;
        // this.onGetWithDrawPlantListing('pending');
        // this.registerPassportForm.reset();
        // this.submitted = false;
        const options = [];
        for (const room of room10) {
          const {wh_batch_weight, batch_no} = room;
          // const {wh_batch_weight, batch_id} = room;
          options.push({value: `room -10 batch ${batch_no} - ${wh_batch_weight} gram`, max: wh_batch_weight});
          // options.push({value: `room -10 batch ${batch_id} - ${wh_batch_weight} gram`, max: wh_batch_weight});
        }
        for (const room of room5) {
          const {wh_batch_weight, batch_no} = room;
          // const {wh_batch_weight, batch_id} = room;
          options.push({value: `room -5 batch ${batch_no} - ${wh_batch_weight} gram`, max: wh_batch_weight});
          // options.push({value: `room -5 batch ${batch_id} - ${wh_batch_weight} gram`, max: wh_batch_weight});
        }
    
        this.options = options;
      },
      error => {
        this.loading = false;
        this.submitted = false;
        this.alertService.error(error);
      });

  }

  setMaxStock({target: {value = ''} = {}}) {
    for (const option of this.options) {
      if (option.value === value) {
        this.max = option.max;
      }
    }
  }
}
