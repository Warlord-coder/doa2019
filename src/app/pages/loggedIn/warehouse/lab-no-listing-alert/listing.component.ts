import {Component, OnInit} from '@angular/core';
import {DataTableParams, DataTableResource} from 'ngx-datatable-bootstrap4';
import {NgbModal} from '@ng-bootstrap/ng-bootstrap';
import {ActivatedRoute, Router} from '@angular/router';
import {AlertService, PlantCharacterService, WarehouseService} from '../../../../services';
import {first} from 'rxjs/operators';
import {toLower} from 'lodash';
import * as moment from 'moment';

@Component({
  selector: 'app-wh-lab-listing-page',
  templateUrl: './listing.component.html',
})
export class NgbdWHLabAlertListingComponent implements OnInit {
  info: any;
  resources = new DataTableResource<any>([]);
  items: any[] = [];
  tempItems: any[] = [];
  count = 0;
  loading = false;
  momentObj: any;
  alert = false;
  roles = [];
  dtOptions: DataTables.Settings = {};
  constructor(private modalService: NgbModal,
              private warehouseService: WarehouseService,
              private activeRoute: ActivatedRoute,
              private alertService: AlertService,
              private router: Router,
              private route: ActivatedRoute) {
    this.momentObj = moment;
  }

  ngOnInit(): void {
    this.route
      .data
      .subscribe(data => {
        this.roles = data['roles'];
      });
    this.activeRoute.params.subscribe(params => {
      if (params.id === 'alert') {
        this.alert = true;
      }
      this.dtOptions = {
        pagingType: 'full_numbers',
        pageLength: 20,
        serverSide: true,
        lengthChange: true,
        responsive:true,
        ajax: (dataTablesParameters: any, callback) => {
          const {start, length} = dataTablesParameters;
          const keyword = dataTablesParameters.search.value;
          this.warehouseService.getAlertFilterList({start: start, length: length, keyword: keyword, alert: this.alert})
          .pipe(first())
          .subscribe(
            ({data = []}: any) => {
              this.loading = false;
              this.info = [];
              data['data'].forEach(labs => {
                if(labs.length == 0) return;                
                let item = {lab_id: '', lab_no: '', total: labs.length, completed: 0, createdAt: '', updatedAt: '', status: (labs.status?labs.status:'Draft')};
                let count = 0;
                labs.forEach(lab => {
                  if(lab.status == 'complete') count++;
                });
                item.completed = count;
                item.lab_no = labs[0].lab_no;
                item.lab_id = labs[0].lab_id;
                item.createdAt =  this.momentObj(labs[0].createdAt).format('MMM Do YYYY');
                item.updatedAt = this.momentObj(labs[0].updatedAt).format('MMM Do YYYY');
                if(item.completed == item.total) item.status = "Complete";
                this.info.push(item);
              });
              callback({
                recordsTotal: data['total_rows'],
                recordsFiltered: data['total_rows'],
                data: []
              })
            },
            error => {
              this.loading = false;
              this.alertService.error(error);
            });
          
        },
      };
    });
  }

  reloadData(params: DataTableParams) {
    this.resources.query(params).then(vals => {
      this.tempItems = vals;
      this.items = vals;
    });
  }

  onEdit({lab_id = ''}) {
    if (this.alert) {
      return this.router.navigate([`/admin/warehouse/alert-pending-complete/listing/${lab_id}`]);
    }
    this.router.navigate([`/admin/warehouse/pending-complete/listing/${lab_id}`]);
  }

  onDelete(item: object, deleteContent) {
    this.modalService.open(deleteContent, {centered: true});
  }

  onSearch({target: {value: searchRole = ''} = {}}) {
    this.items = this.tempItems.filter(item => {
      if(item['m8_lab_id'] && toLower(item['m8_lab_id']['lab_no']).includes(toLower(searchRole))) return true;
    });
  }

  onGetWareHouseLabListing() {
    this.loading = true;
    this.warehouseService.getAllLab(this.alert)
      .pipe(first())
      .subscribe(
        ({data = []}: any) => {
          this.loading = false;
          this.resources = new DataTableResource<any>(data);
          this.resources.count().then(count => this.count = count);
          this.reloadData({});
        },
        error => {
          this.loading = false;
          this.alertService.error(error);
        });
  }
  checkRole(index){
    if(localStorage.getItem('superAdmin') === "true") return true;
    if(localStorage.getItem('rolesId').indexOf(this.roles[index]) > -1) return true;
    return false;
  }
}
