import {Component, OnInit} from '@angular/core';
import {DataTableParams, DataTableResource} from 'ngx-datatable-bootstrap4';
import {NgbModal} from '@ng-bootstrap/ng-bootstrap';
import {Router, ActivatedRoute} from '@angular/router';
import {toLower} from 'lodash';
import {FormBuilder, FormGroup, Validators} from '@angular/forms';
import {AlertService, DistributionService,LabAlertFormService,LabService,RecoveryService} from '../../../../services';
import {first} from 'rxjs/operators';
import * as moment from 'moment';

@Component({
  selector: 'app-listing-common-country-page',
  templateUrl: './listing.component.html',
})
export class NgbdWithWareHouseRecoveryAlertListComponent implements OnInit {
  resources = new DataTableResource<any>([]);
  items: any[] = [];
  count = 0;
  registerPassportForm: FormGroup;
  tempItems: any[] = [];
  submitted = false;
  momentObj: any;
  file = null;
  loading = false;
  apiError = false;
  roles = [];
  constructor(private modalService: NgbModal,
              private labAlertFormService: LabAlertFormService,
              private formBuilder: FormBuilder,
              private labService: LabService,
              private alertService: AlertService,
              private reco: RecoveryService,
              private distributionService: DistributionService,
              private router: Router,
              private route: ActivatedRoute) {
    this.onGetRecoveryListing();
    this.momentObj = moment;
  }

  ngOnInit() {
    this.route
      .data
      .subscribe(data => {
        this.roles = data['roles'];
      });
    this.registerPassportForm = this.formBuilder.group({
      dateOfReceiving: [''],
      serviceRequestLetter: [''],
      agenciesRequestSeedGermination: [''],
      agenciesSubRequestSeedGermination: [''],
      agenciesSubRequestSeedGerminationInput: [''],
      numberOfRequestor: [''],
      requestorAddress: [''],
      telephoneNumber: [''],
      email: [''],
      objective: [''],
      owner: ['']
    });
  }

  get f() {
    if (!this.apiError && this.submitted && !this.registerPassportForm.invalid) {
      this.alertService.error(null);
    }

    return this.registerPassportForm.controls;
  }

  reloadData(params: DataTableParams) {
    this.resources.query(params).then(vals => {
      this.items = vals;
      this.tempItems = this.items;
    });
  }

  onDetails(_id,lab) {
    this.router.navigate([`/admin/warehouse/stock/recovery-alert/${_id}`]);
  }

  onDelete(item: object, deleteContent) {
    this.modalService.open(deleteContent, {centered: true});
  }

  onSearch({target: {value: searchRole = ''} = {}}) {
    this.items = this.tempItems.filter(({recovery_id}) => {
      return toLower(recovery_id.m_no).includes(toLower(searchRole))
    });
    this.count = this.items.length;
  }

  onCreate(createContent) {
    this.registerPassportForm.reset();
    this.file = null;
    this.modalService.open(createContent, {centered: true});
  }

  onFileUpload({srcElement: {files = []} = {}}) {
    this.file = files[0];
  }

  onSubmit() {
    this.submitted = true;
    if (this.registerPassportForm.invalid) {
      this.alertService.error('Some fields are required. Please fill those fields');
      return;
    }

    this.loading = true;
    this.apiError = false;

    this.alertService.reset();
    this.loading = true;

    this.modalService.dismissAll();
    this.distributionService.createOrUpdate({...this.registerPassportForm.value, file: this.file}, null)
      .pipe(first())
      .subscribe(
        ({data = {}}: any) => {
          this.loading = false;
          const {_id} = data;
          this.router.navigate([`/admin/warehouse/stock/recovery-listing`]);
        },
        error => {
          this.loading = false;
          this.apiError = true;
          this.alertService.error(error);
        });
  }

  onGetRecoveryListing() {
    this.loading = true;
    this.reco.getAlertRecoveryM9()
      .pipe(first())
      .subscribe(
        ({data = []}: any) => {
          this.resources = new DataTableResource<any>(data);
          this.resources.count().then(count => this.count = count);
          this.reloadData({});
          this.loading = false;
        },
        error => {
          this.loading = false;
          this.alertService.error(error);
        });
  }

  getStatus(item) {
    const {pending, draft, total, approve} = item;
    if (pending || draft || total === 0) {
      return 'pending';
    }

    if (approve) {
      return ' approve';
    }

    return 'complete';
  }
  checkRole(index){
    if(localStorage.getItem('superAdmin') === "true") return true;
    if(localStorage.getItem('rolesId').indexOf(this.roles[index]) > -1) return true;
    return false;
  }
}
