import {Component, OnInit} from '@angular/core';
import {DataTableParams, DataTableResource} from 'ngx-datatable-bootstrap4';
import {NgbModal} from '@ng-bootstrap/ng-bootstrap';
import {ActivatedRoute, Router} from '@angular/router';
import {toLower} from 'lodash';
import {FormBuilder, FormGroup, Validators} from '@angular/forms';
// @ts-ignore
import {first} from 'rxjs/operators';
import * as moment from 'moment';
import {AlertService, RecoveryService, PlantCategoryService} from '../../../services';

@Component({
  selector: 'app-listing-unassign-plant-page',
  templateUrl: './listing.component.html',
})
export class NgbdRegisterPlantUnassignedComponent implements OnInit {
  resources = new DataTableResource<any>([]);
  items: any[] = [];
  count = 0;
  registerPassportForm: FormGroup;
  tempItems: any[] = [];
  submitted = false;
  id = null;
  momentObj: any;
  file = null;
  recoverInfo = null;
  loading = false;
  selectedPlantsForSubmit: any = [];
  apiError = false;
  categoryList = [];

  constructor(private modalService: NgbModal,
              private formBuilder: FormBuilder,
              private activeRoute: ActivatedRoute,
              private alertService: AlertService,
              private plantCategoryService: PlantCategoryService,
              private recoveryService: RecoveryService,
              private router: Router) {
    this.momentObj = moment;
  }

  ngOnInit() {
    this.activeRoute.params.subscribe(params => {
      const {volume} = params;
      this.id = volume;
      this.onGetplantCategoryListing('plantCategoryService', 'categoryList');
      setTimeout(() => {
        this.onGetRecoveryListing();
      }, 1500);
    });
  }

  get f() {
    if (!this.apiError && this.submitted && !this.registerPassportForm.invalid) {
      this.alertService.error(null);
    }

    return this.registerPassportForm.controls;
  }

  reloadData(params: DataTableParams) {
    this.resources.query(params).then(vals => {
      this.items = vals;
      this.tempItems = this.items;
    });
  }

  onSearch({target: {value: searchRole = ''} = {}}) {
    this.items = this.tempItems.filter(({passport_no}) => passport_no && toLower(passport_no).includes(toLower(searchRole)));
  }

  onGetRecoveryListing() {
    this.loading = true;
    this.recoveryService.listCompleteRecoveryRegister()
      .pipe(first())
      .subscribe(
        ({data = []}: any) => {
          this.resources = new DataTableResource<any>(data);
          this.resources.count().then(count => this.count = count);
          this.reloadData({});
          this.loading = false;
          let that = this;
          data.forEach(function (value) {
            that.categoryList.forEach(function (category) {
              if(value.reg_plant_sci_name != null)
              if(value.reg_plant_sci_name.plant_category == category._id){
                value.categoryName_en = category.categoryName_en;
              }
            }); 
          });
        },
        error => {
          this.loading = false;
          this.alertService.error(error);
        });
  }

  onChangeItemSelect({_id}) {
    if (this.selectedPlantsForSubmit.includes(_id)) {
      this.selectedPlantsForSubmit = this.selectedPlantsForSubmit.filter(id => id !== _id);
      return;
    }

    this.selectedPlantsForSubmit = [...this.selectedPlantsForSubmit, _id];
  }

  onGetplantCategoryListing(serviceName, key) {
    this.loading = true;
    this[serviceName].getAll()
      .pipe(first())
      .subscribe(
        ({data = []}: any) => {
          this.loading = false;
          this[key] = data;
        },
        error => {
          this.loading = false;
          this.alertService.error(error);
        });
  }

  onSubmitPlants() {
    if (!this.selectedPlantsForSubmit || !this.selectedPlantsForSubmit.length) {
      return;
    }
    this.loading = true;
    this.recoveryService.submitToRegisterModule(this.id, this.selectedPlantsForSubmit)
      .pipe(first())
      .subscribe(
        ({data = {}}: any) => {
          this.selectedPlantsForSubmit = [];
          this.router.navigate([`/admin/register/plant/${this.id}/listing`]);
        },
        error => {
          this.loading = false;
          this.alertService.error(error);
        });
  }
}
