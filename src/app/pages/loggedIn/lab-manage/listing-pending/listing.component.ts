import {Component, OnInit} from '@angular/core';
import {DataTableParams, DataTableResource} from 'ngx-datatable-bootstrap4';
import {NgbModal} from '@ng-bootstrap/ng-bootstrap';
import {ActivatedRoute, Router} from '@angular/router';
import {first} from 'rxjs/operators';
import {AlertService, LabService, PassportService, PlantRegisterService, VolumeService, PlantTypeService, PlantCategoryService } from '../../../../services';
import * as moment from 'moment';
import {toLower} from 'lodash';
import {FORM_REQUIRED_KEYS, isFormSubmissionError} from '../../../../utils';

@Component({
  selector: 'app-lab-plants-listing-page',
  templateUrl: './listing.component.html',
})
export class NgbdPlantsLabComponent implements OnInit {
  roleResources = new DataTableResource<any>([]);
  items: any[] = [];
  tempItems: any[] = [];
  count = 0;
  volumeId = null;
  loading = false;
  labInfo = null;
  volumeInfo = null;
  momentObj: any;
  apiError = false;
  passportInfo = null;
  currentStatus = null;
  labId = null;
  selectedPlantsForSubmit = [];
  plantTypes = [];
  categoryList = [];
  constructor(private modalService: NgbModal,
              private activeRoute: ActivatedRoute,
              private passportService: PassportService,
              private plantRegisterService: PlantRegisterService,
              private plantTypeService: PlantTypeService,
              private volumeService: VolumeService,
              private labService: LabService,
              private alertService: AlertService,
              private plantCategoryService: PlantCategoryService,
              private router: Router) {
    this.momentObj = moment;
    this.onGetPlantTypes();
  }

  ngOnInit(): void {
    this.activeRoute.params.subscribe(params => {
      const {volume, labId} = params;
      this.volumeId = volume;
      this.labId = labId;
      this.onGetplantCategoryListing('plantCategoryService', 'categoryList');
      this.onGetVolumeInfo(this.volumeId);
      this.onGetListing('draft');
      this.onGetLabInfo(this.labId);
    });
  }

  reloadData(params: DataTableParams) {
    this.roleResources.query(params).then(vals => {
      this.items = vals;
      this.tempItems = this.items;
    });
  }

  onSearch({target: {value: searchRole = ''} = {}}) {
    this.items = this.tempItems.filter(({passport}) => {
      const {passport_no = null} = passport || {};
      return passport_no && toLower(passport_no).includes(toLower(searchRole));
    });
  }

  onGetPlantTypes() {
    this.loading = true;
    this['plantTypeService'].getAll()
      .pipe(first())
      .subscribe(
        ({data = []}: any) => {
          this.loading = false;
          this.plantTypes = data;
        },
        error => {
          this.loading = false;
          this.alertService.error(error);
        });
  }

  onGetVolumeInfo(id) {
    this.loading = true;
    this.onGetPlantTypes();
    this.volumeService.getSpecific(id)
      .pipe(first())
      .subscribe(
        ({data = []}: any) => {
          this.loading = false;
          const {passport_no} = data;
          this.volumeInfo = data;
          this.onGetPassportInfo(passport_no);
        },
        error => {
          this.loading = false;
          this.alertService.error(error);
        });
  }

  onGetPassportInfo(id) {
    this.loading = true;
    this.passportService.getSpecific(id)
      .pipe(first())
      .subscribe(
        ({data = []}: any) => {
          this.loading = false;
          const {passport_no} = data;
          this.passportInfo = data;
        },
        error => {
          this.loading = false;
          this.alertService.error(error);
        });
  }

  onGetListing(status) {
    this.loading = true;
    this.currentStatus = status;
    this.plantRegisterService.getAll(this.volumeId, 'lab_plants', status, null, this.labId)
      .pipe(first())
      .subscribe(
        ({data = []}: any) => {
          this.loading = false;
          this.roleResources = new DataTableResource<any>(data);
          this.roleResources.count().then(count => this.count = count);
          this.reloadData({});
          let that = this;
          data.forEach(function (value) {
            that.categoryList.forEach(function (category) {
              if(value.reg_plant_sci_name != null)
              if(value.reg_plant_sci_name.plant_category == category._id){
                value.categoryName_en = category.categoryName_en;
              }
            }); 
          });       
        },
        error => {
          this.loading = false;
          this.alertService.error(error);
        });
  }

  onGetLabInfo(lab) {
    this.loading = true;
    this.labService.getSpecific(lab)
      .pipe(first())
      .subscribe(
        ({data = {}}: any) => {
          this.loading = false;
          this.labInfo = data;
        },
        error => {
          this.loading = false;
          this.alertService.error(error);
          return this.router.navigate([`/admin/labs/listing`]);
        });
  }

  onChangeItemSelect({_id}) {
    if (this.selectedPlantsForSubmit.includes(_id)) {
      this.selectedPlantsForSubmit = this.selectedPlantsForSubmit.filter(id => id !== _id);
      return;
    }

    this.selectedPlantsForSubmit = [...this.selectedPlantsForSubmit, _id];
  }

  onSubmitPlants() {
    if (!this.selectedPlantsForSubmit || !this.selectedPlantsForSubmit.length) {
      return;
    }
    this.loading = true;
    for (const item of this.items) {
      const {_id}: any = item;
      if (this.selectedPlantsForSubmit.includes(_id)) {
        const record = isFormSubmissionError(item, FORM_REQUIRED_KEYS.LAB_FORM);
        if (record) {
          this.loading = false;
          return this.alertService.error(`Validation Failed for record Reg Seed No: ${record.reg_seed_no}`);
        }
      }
    }

    this.labService.submitToModule9(this.selectedPlantsForSubmit)
      .pipe(first())
      .subscribe(
        ({data = {}}: any) => {
          this.selectedPlantsForSubmit = [];
          this.onGetListing('draft');
        },
        error => {
          this.loading = false;
          this.alertService.error(error);
        });
  }

  onEdit({_id}) {
    this.router.navigate([`admin/labs/plants/${_id}/${this.volumeId}/${this.labId}`]);
  }

  getPlantStockType(item) {
    const {
      selfing_input, selfing_dropdown,
      oop_input, oop_dropdown,
      other_input, other_dropdown
    } = item;
    const stock = {
      isSelfing: selfing_input && selfing_dropdown,
      isOOP: oop_input && oop_dropdown,
      isOther: other_input && other_dropdown,
    };
    if (stock.isSelfing) {
      return `Self ${selfing_input} ${selfing_dropdown}`;
    }
    if (stock.isOOP) {
      return `OP ${oop_input} ${oop_dropdown}`;
    }
    if (stock.isOther) {
      return `Other ${other_input} ${other_dropdown}`;

    }
  }


  onGetplantCategoryListing(serviceName, key) {
    this.loading = true;
    this[serviceName].getAll()
      .pipe(first())
      .subscribe(
        ({data = []}: any) => {
          this.loading = false;
          this[key] = data;
        },
        error => {
          this.loading = false;
          this.alertService.error(error);
        });
  }

  addPlantsToLab() {
    this.router.navigate([`admin/labs/${this.volumeId}/${this.labId}`]);
  }
}
