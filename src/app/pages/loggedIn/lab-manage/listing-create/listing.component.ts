import {Component, OnInit} from '@angular/core';
import {DataTableParams, DataTableResource} from 'ngx-datatable-bootstrap4';
import {NgbModal} from '@ng-bootstrap/ng-bootstrap';
import {ActivatedRoute, Router} from '@angular/router';
import {first} from 'rxjs/operators';
import {AlertService, LabService, PassportService, PlantRegisterService, VolumeService} from '../../../../services';
import * as moment from 'moment';
import {toLower} from 'lodash';

@Component({
  selector: 'app-lab-create-listing-page',
  templateUrl: './listing.component.html',
})
export class NgbdCreateLabComponent implements OnInit {
  roleResources = new DataTableResource<any>([]);
  items: any[] = [];
  tempItems: any[] = [];
  count = 0;
  volumeId = null;
  loading = false;
  labInfo = null;
  volumeInfo = null;
  momentObj: any;
  apiError = false;
  passportInfo = null;
  labId = null;
  selectedPlantsForSubmit = [];
  plant_list = [];
  constructor(private modalService: NgbModal,
              private activeRoute: ActivatedRoute,
              private passportService: PassportService,
              private plantRegisterService: PlantRegisterService,
              private volumeService: VolumeService,
              private labService: LabService,
              private alertService: AlertService,
              private router: Router) {
    this.momentObj = moment;
  }

  ngOnInit(): void {
    this.activeRoute.params.subscribe(params => {
      const {volume, labId} = params;
      this.volumeId = volume;
      this.labId = labId;
      this.onGetVolumeInfo(this.volumeId);
      this.onGetLabInfo(this.labId);
      this.onGetListing(this.volumeId);
    });
  }

  reloadData(params: DataTableParams) {
    this.roleResources.query(params).then(vals => {
      this.items = vals;
      this.tempItems = this.items;
    });
  }

  onSearch({target: {value: searchRole = ''} = {}}) {
    this.items = this.tempItems.filter(({passport}) => {
      const {passport_no = null} = passport || {};
      return passport_no && toLower(passport_no).includes(toLower(searchRole));
    });
  }

  onGetVolumeInfo(id) {
    this.loading = true;
    this.volumeService.getSpecific(id)
      .pipe(first())
      .subscribe(
        ({data = []}: any) => {
          this.loading = false;
          const {passport_no} = data;
          this.volumeInfo = data;
          this.onGetPassportInfo(passport_no);
        },
        error => {
          this.loading = false;
          this.alertService.error(error);
        });
  }

  onGetPassportInfo(id) {
    this.loading = true;
    this.passportService.getSpecific(id)
      .pipe(first())
      .subscribe(
        ({data = []}: any) => {
          this.loading = false;
          const {passport_no} = data;
          this.passportInfo = data;
        },
        error => {
          this.loading = false;
          this.alertService.error(error);
        });
  }

  onGetListing(volume) {
    this.loading = true;
    this.plantRegisterService.getAll(volume, 'complete_plant_m3')
      .pipe(first())
      .subscribe(
        ({data = []}: any) => {
          this.loading = false; 
          this.plant_list = [];
          data.forEach(temp => {
            if(this.getStockType(temp) == this.labInfo.stock_type){
              this.plant_list.push(temp)
            }
          });
          this.roleResources = new DataTableResource<any>(this.plant_list);
          this.roleResources.count().then(count => this.count = count);
          this.reloadData({});
        },
        error => {
          this.loading = false;
          this.alertService.error(error);
        });
  }

  onGetLabInfo(lab) {
    this.loading = true;
    this.labService.getSpecific(lab)
      .pipe(first())
      .subscribe(
        ({data = {}}: any) => {
          this.loading = false;
          this.labInfo = data;
        },
        error => {
          this.loading = false;
          this.alertService.error(error);
          return this.router.navigate([`/admin/labs/listing`]);
        });
  }

  onChangeItemSelect({_id}) {
    if (this.selectedPlantsForSubmit.includes(_id)) {
      this.selectedPlantsForSubmit = this.selectedPlantsForSubmit.filter(id => id !== _id);
      return;
    }

    this.selectedPlantsForSubmit = [...this.selectedPlantsForSubmit, _id];
  }

  onSubmitPlants() {
    if (!this.selectedPlantsForSubmit || !this.selectedPlantsForSubmit.length) {
      return;
    }
    this.loading = true;
    this.labService.submit(this.labId, this.selectedPlantsForSubmit)
      .pipe(first())
      .subscribe(
        ({data = {}}: any) => {
          this.loading = false;
          this.labInfo = data;
          return this.router.navigate([`/admin/labs/listing`]);
        },
        error => {
          this.loading = false;
          this.alertService.error(error);
        });
  }
  
  getPlantStockType(item) {
    const {
      selfing_input, selfing_dropdown,
      oop_input, oop_dropdown,
      other_input, other_dropdown
    } = item;
    const stock = {
      isSelfing: selfing_input && selfing_dropdown,
      isOOP: oop_input && oop_dropdown,
      isOther: other_input && other_dropdown,
    };
    if (stock.isSelfing) {
      return `Self ${selfing_input} ${selfing_dropdown}`;
    }
    if (stock.isOOP) {
      return `OP ${oop_input} ${oop_dropdown}`;
    }
    if (stock.isOther) {
      return `Other ${other_input} ${other_dropdown}`;
    }
  }  
  getStockType(item) {
    const {
      selfing_input, selfing_dropdown,
      oop_input, oop_dropdown,
      other_input, other_dropdown
    } = item;
    const stock = {
      isSelfing: selfing_input && selfing_dropdown,
      isOOP: oop_input && oop_dropdown,
      isOther: other_input && other_dropdown,
    };
    if (stock.isSelfing) {
      return 'selfing';
    }
    if (stock.isOOP) {
      return 'OP';
    }
    if (stock.isOther) {
      return 'Other';
    }
  }

}
