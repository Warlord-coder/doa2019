import {Component, OnInit} from '@angular/core';
import {ActivatedRoute, Router} from '@angular/router';
import {FormBuilder, FormGroup, Validators} from '@angular/forms';
import {
  AlertService,
  CommonProvinceService, LabService, PassportService,
  PlantCategoryService, PlantCharacterService,
  PlantGroupsService,
  PlantGroupTypesService,
  PlantRegisterService, PlantTypeService, VolumeService
} from '../../../../services';
import {FORM_REQUIRED_KEYS, getRoleResources, isFormSubmissionError} from '../../../../utils';
import {identity, pickBy} from 'lodash';
import {TreeviewConfig, TreeviewItem} from 'ngx-treeview';
import {first} from 'rxjs/operators';
import {PlantService} from '../../../../services/plant.service';
import {CommonCountryService} from '../../../../services/common-country.service';
import {CommonAumperService} from '../../../../services/common-aumper.service';
import {environment} from '../../../../../environments/environment';

@Component({
  selector: 'app-add-update-plant-register-page',
  templateUrl: './add-update.component.html',
})
export class NgbdAddUpdatePlantLabComponent implements OnInit {
  plantGroupTypeForm: FormGroup;
  submitted = false;
  loading = false;
  file = null;
  id = null;
  categoryList = [];
  groupTypeList = [];
  groupList = [];
  plantTypes = [];
  provinceList = [];
  aumperList = [];
  countryList = [];
  volumeInfo = null;
  disabled = false;
  passportInfo = null;
  reg_gene_id = null;
  reg_seed_no = null;
  reg_character_attached_file = null;
  apiError = false;
  labInfo = null;
  plantsListing = [];
  labId = null;
  regCharacterAttachedFile = null;
  selectedPlant = null;
  volumeId = null;
  roleResources: TreeviewItem[];
  config = {
    class: 'autocomplete', max: 5, placeholder: 'Type to search',
    sourceField: ['plantTypeName_sci_en']
  };

  constructor(private router: Router,
              private formBuilder: FormBuilder,
              private activeRoute: ActivatedRoute,
              private plantCategoryService: PlantCategoryService,
              private plantGroupTypesService: PlantGroupTypesService,
              private plantGroupsService: PlantGroupsService,
              private plantCharacterService: PlantCharacterService,
              private volumeService: VolumeService,
              private passportService: PassportService,
              private plantTypeService: PlantTypeService,
              private plantRegisterService: PlantRegisterService,
              private countryService: CommonCountryService,
              private labService: LabService,
              private provinceService: CommonProvinceService,
              private aumperService: CommonAumperService,
              private alertService: AlertService) {
    this.onGetListing('plantCharacterService', 'plantsListing');
    this.onGetListing('plantCategoryService', 'categoryList');
    this.onGetListing('plantGroupTypesService', 'groupTypeList');
    this.onGetListing('plantGroupsService', 'groupList');
    this.onGetListing('countryService', 'countryList');
    this.onGetListing('aumperService', 'aumperList');
    this.onGetListing('provinceService', 'provinceList');
  }

  ngOnInit() {
    this.roleResources = getRoleResources();
    this.plantGroupTypeForm = this.formBuilder.group({
      plant_gene_name: [''],
      reg_gene_type: [''],
      reg_gene_category: [''],
      reg_seed_collect_date: [''],
      reg_plant_alter_name: [[]],
      selfing_input: [''],
      selfing_dropdown: [''],
      reg_plant_sci_name: [''],
      oop_input: [''],
      oop_dropdown: [''],
      other_input: [''],
      other_dropdown: [''],
      department_type: [''],
      department_sub_type: [''],
      department_name: [''],
      source_country: [''],
      source_country_1: [''],
      source_province: [''],
      source_district: [''],
      source_address: [''],
      lat: [''],
      long: [''],
      source_by_department: [''],
      reg_seed_collect_person: [''],
      reg_seed_owner: [''],
      reg_seed_sender: [''],
      reg_character_brief: [''],
      reg_character_for_record: [''],
      reg_sent_lab_date: [''],
      reg_remark: [''],

      lab_test_plant_amount_input: [''],
      lab_test_plant_amount_dropdown: [''],
      lab_test_plant_amount: ['', Validators.required],
      lab_test_moisture_date: [''],
      lab_test_moisture_method: ['', Validators.required],
      lab_test_moisture_amount: ['', Validators.required],
      lab_test_moisture_person: ['', Validators.required],
      lab_test_growth_percent: ['', Validators.required],
      lab_test_growth_date: [''],
      lab_test_growth_method: ['', Validators.required],
      lab_test_growth_amount: ['', Validators.required],
      lab_test_growth_person: ['', Validators.required],
      lab_test_strong_percent: ['', Validators.required],
      lab_test_strong_date: [''],
      lab_test_strong_method: ['', Validators.required],
      lab_test_strong_amount: ['', Validators.required],
      lab_test_strong_person: ['', Validators.required],
      lab_test_alive_percent: ['', Validators.required],
      lab_test_alive_date: [''],
      lab_test_alive_method: ['', Validators.required],
      lab_test_alive_amount: ['', Validators.required],
      lab_test_alive_person: ['', Validators.required],
      lab_seed_rest: ['มี', Validators.required],
      lab_seed_rest_fix_method: [''],
      lab_seed_rest_fix_date: [''],
      lab_seed_amount: [''],
      lab_10seed_weight: [''],
      lab_seed_est_amount: [''],
      lab_plaint_character: [''],
      lab_admin_name: ['', Validators.required],
      lab_remark: [''],
    });
    this.activeRoute.params.subscribe(params => {
      const {id, volume, labId} = params;
      this.id = id;
      this.labId = labId;
      this.volumeId = volume;
      this.onGetVolumeInfo(this.volumeId);
      this.onGetLabInfo(this.labId);
      if (id && id !== 'create') {
        this.onGetPlantRegisterInfo(id);
      }
    });
  }

  get f() {
    if (!this.apiError && this.submitted && !this.plantGroupTypeForm.invalid) {
      this.alertService.error(null);
    }

    return this.plantGroupTypeForm.controls;
  }

  onSubmit(redirect = true, callbackName) {
    // this.submitted = true;
    if (this.plantGroupTypeForm.invalid) {
      // this.alertService.error('Some fields are required. Please fill those fields');
      // return;
    }

    if (!this.selectedPlant) {
      // this.alertService.error('Please Select Reg Plant Sci Name');
      // return;
    }

    const requestObj = {};
    const formValues = this.plantGroupTypeForm.value;
    const KEYS = ['lab_test_plant_amount_input', 'lab_test_plant_amount_dropdown',
      'lab_test_plant_amount', 'lab_test_moisture_date', 'lab_test_moisture_method',
      'lab_test_moisture_amount', 'lab_test_moisture_person', 'lab_test_growth_percent',
      'lab_test_growth_date', 'lab_test_growth_method', 'lab_test_growth_amount', 'lab_test_growth_person',
      'lab_test_strong_percent', 'lab_test_strong_date', 'lab_test_strong_method', 'lab_test_strong_amount',
      'lab_test_strong_person', 'lab_test_alive_percent', 'lab_test_alive_date', 'lab_test_alive_method',
      'lab_test_alive_amount', 'lab_test_alive_person', 'lab_seed_rest', 'lab_10seed_weight', 'lab_seed_est_amount', 'lab_plaint_character',
      'lab_admin_name', 'lab_remark'];

    Object.keys(formValues).forEach(key => {
      if (KEYS.includes(key)) {
        requestObj[key] = formValues[key];
      }
    });
    this.plantRegisterService.createOrUpdate(pickBy({
      ...requestObj
    }, identity), this.id)
      .pipe(first())
      .subscribe(
        () => {
          if (callbackName) {
            this[callbackName]();
          }

          if (redirect) {
            this.router.navigate([`/admin/labs/plants/${this.volumeId}/${this.labId}`]);
          }
        },
        error => {
          this.loading = false;
          this.apiError = true;
          this.alertService.error(error);
        });
  }

  onSelectedChange($event) {
    console.log('Select', $event);
  }

  onFilterChange($event) {
    console.log('Filter Change', $event);
  }

  onGetListing(serviceName, key) {
    this.loading = true;
    this[serviceName].getAll()
      .pipe(first())
      .subscribe(
        ({data = []}: any) => {
          this.loading = false;
          this[key] = data;
        },
        error => {
          this.loading = false;
          this.alertService.error(error);
        });
  }

  onGetPlantRegisterInfo(id) {
    this.loading = true;

    this.plantRegisterService.getSpecific(id)
      .pipe(first())
      .subscribe(
        ({data = []}: any) => {
          const {plant_gene_name, reg_gene_id, reg_seed_no, reg_plant_sci_name, reg_character_attached_file, m8_lab_status} = data;
          if(plant_gene_name!=undefined && plant_gene_name!= null)
            this['plantTypeService'].getSpecific(plant_gene_name) 
            .pipe(first())
            .subscribe(
              ({data = []}: any) => {
                this.plantTypes.push(data )
                this.loading = false;
              },
              error => {
                this.loading = false;
                this.alertService.error(error);
              });
          this.disabled = m8_lab_status === 'complete';
          if (this.disabled) {
            $('input').attr('disabled');
          }
          Object.keys(data).forEach(key => {
            if (this.plantGroupTypeForm.get(key)) {
              this.plantGroupTypeForm.get(key).setValue(data[key]);
              if (this.disabled) {
                this.plantGroupTypeForm.get(key).disable({
                  onlySelf: true
                });
              }
            }
          });
          this.reg_gene_id = reg_gene_id;
          this.reg_seed_no = reg_seed_no;
          this.reg_character_attached_file = reg_character_attached_file;
          this.findPlantSciName(data);
          this.loading = false;
        },
        error => {
          this.loading = false;
          this.alertService.error(error);
        });
  }

  onGetVolumeInfo(id) {
    this.loading = true;
    this.volumeService.getSpecific(id)
      .pipe(first())
      .subscribe(
        ({data = []}: any) => {
          this.loading = false;
          const {passport_no} = data;
          this.volumeInfo = data;
          this.onGetPassportInfo(passport_no);
        },
        error => {
          this.loading = false;
          this.alertService.error(error);
        });
  }

  onGetPassportInfo(id) {
    this.loading = true;
    this.passportService.getSpecific(id)
      .pipe(first())
      .subscribe(
        ({data = []}: any) => {
          this.loading = false;
          this.passportInfo = data;
        },
        error => {
          this.loading = false;
          this.alertService.error(error);
        });
  }

  getAttachment(basePath, name) {
    if (name) {
      return `${environment.endpoint}/${basePath}/${name}`;
    }

    return null;
  }

  onSelect(plant) {
    this.selectedPlant = plant;
    this.plantGroupTypeForm.get('reg_plant_sci_name').setValue(plant.genus + ' ' + plant.species);
  }

  onClear() {
    this.selectedPlant = null;
  }

  onCancel() {
    this.router.navigate([`/admin/labs/plants/${this.volumeId}/${this.labId}`]);
  }

  onGetLabInfo(lab) {
    this.loading = true;
    this.labService.getSpecific(lab)
      .pipe(first())
      .subscribe(
        ({data = {}}: any) => {
          this.loading = false;
          this.labInfo = data;
        },
        error => {
          this.loading = false;
          this.alertService.error(error);
          return this.router.navigate([`/admin/labs/listing`]);
        });
  }

  findPlantSciName({reg_plant_sci_name}) {
    if (!reg_plant_sci_name) {
      return;
    }
    for (const plant of  this.plantsListing) {
      if (reg_plant_sci_name === plant._id) {
        this.selectedPlant = plant;
        this.plantGroupTypeForm.get('reg_plant_sci_name').setValue(plant.genus + ' ' + plant.species);
      }
    }

  }

  onSubmitPlants() {
    this.submitted = true;
    if (this.plantGroupTypeForm.invalid) {
      this.alertService.error('Some fields are required. Please fill those fields');
      return;
    }
    this.onSubmit(false, 'onSubmitToModule9');
    this.loading = true;
  }

  onSubmitToModule9() {
    this.labService.submitToModule9([this.id])
      .pipe(first())
      .subscribe(
        ({data = {}}: any) => {
          this.router.navigate([`/admin/labs/plants/${this.volumeId}/${this.labId}`]);
        },
        error => {
          this.loading = false;
          this.alertService.error(error);
        });
  }


  onChangeCheckbox({target: {checked = ''}}, key) {
    if (checked) {
      this.plantGroupTypeForm.get(key).setValue('-');
      return;
    }

    this.plantGroupTypeForm.get(key).setValue('');
  }
}
