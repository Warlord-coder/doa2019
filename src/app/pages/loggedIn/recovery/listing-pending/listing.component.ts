import {Component, OnInit} from '@angular/core';
import {DataTableParams, DataTableResource} from 'ngx-datatable-bootstrap4';
import {NgbModal} from '@ng-bootstrap/ng-bootstrap';
import {ActivatedRoute, Router} from '@angular/router';
import {toLower} from 'lodash';
import {FormBuilder, FormGroup, Validators} from '@angular/forms';
// @ts-ignore
import {AlertService, RecoveryService, PlantTypeService} from '../../../../services';
import {first} from 'rxjs/operators';
import * as moment from 'moment';
import {FORM_REQUIRED_KEYS, isFormSubmissionError} from '../../../../utils';

@Component({
  selector: 'app-listing-common-country-page',
  templateUrl: './listing.component.html',
})
export class NgbdRegisterRecoveryListPendingComponent implements OnInit {
  resources = new DataTableResource<any>([]);
  items: any[] = [];
  count = 0;
  registerPassportForm: FormGroup;
  tempItems: any[] = [];
  submitted = false;
  id = null;
  momentObj: any;
  file = null;
  recoverInfo = null;
  loading = false;
  selectedPlantsForSubmit: any = [];
  apiError = false;
  plantTypes = [];

  constructor(private modalService: NgbModal,
              private formBuilder: FormBuilder,
              private activeRoute: ActivatedRoute,
              private alertService: AlertService,
              private recoveryService: RecoveryService,
              private plantTypeService: PlantTypeService,
              private router: Router) {
    this.onGetRecoveryListing();
    this.momentObj = moment;
  }

  ngOnInit() {
    this.registerPassportForm = this.formBuilder.group({
      revival_plant: ['', Validators.required],
      date_of_submission: ['', Validators.required],
      place: ['', [Validators.required]]
    });
    this.activeRoute.params.subscribe(params => {
      const {id} = params;
      this.id = id;
      this.onGetSpecificRecovery();
    });
  }

  get f() {
    if (!this.apiError && this.submitted && !this.registerPassportForm.invalid) {
      this.alertService.error(null);
    }

    return this.registerPassportForm.controls;
  }

  reloadData(params: DataTableParams) {
    this.resources.query(params).then(vals => {
      this.items = vals;
      this.tempItems = this.items;
    });
  }

  // onSearch({target: {value: searchRole = ''} = {}}) {
  //   this.items = this.tempItems.filter(({passport_no}) => 
  //     passport_no && toLower(passport_no).includes(toLower(searchRole))
  //   );
  // }
  onSearchType({target: {value: searchRole = ''} = {}}) {
    this.items = this.tempItems.filter(({plant_gene_name}) => {
      try {
        return toLower(plant_gene_name.plantCategory.categoryName_th).includes(toLower(searchRole))
      } catch (error) {
        
      }
    });
  }
  onSearchReg({target: {value: searchRole = ''} = {}}) {
    this.items = this.tempItems.filter(({reg_gene_id}) => {
      try {
        return toLower(reg_gene_id).includes(toLower(searchRole))
      } catch (error) {
        
      }
    });
  }
  onSearchGane({target: {value: searchRole = ''} = {}}) {
    this.items = this.tempItems.filter(({gs_no}) => {
      try {
        return toLower(gs_no).includes(toLower(searchRole))
      } catch (error) {
        
      } 
    });
  }


  onGetRecoveryListing() {
    this.loading = true;
    this['plantTypeService'].getAll()
    .pipe(first())
    .subscribe(
      ({data = []}: any) => {
        this.loading = false;
        this.plantTypes = data;
      },
      error => {
        this.loading = false;
        this.alertService.error(error);
      });
    this.recoveryService.getAllPendingM4()
      .pipe(first())
      .subscribe(
        ({data = []}: any) => {
          this.resources = new DataTableResource<any>(data);
          this.resources.count().then(count => this.count = count);
          this.reloadData({});
          this.loading = false;
          let that = this;
          data.forEach(function (value) {
            that.plantTypes.forEach(function (planttype) {
              if(value.plant_gene_name._id == planttype._id){
                value.gene_plantTypeName_en = planttype.plantTypeName_en;
              }
            }); 
          }); 
        },
        error => {
          this.loading = false;
          this.alertService.error(error);
        });
  }

  onGetSpecificRecovery() {
    this.loading = true;
    this.recoveryService.getSpecific(this.id)
      .pipe(first())
      .subscribe(
        ({data = []}: any) => {
          this.recoverInfo = data;
          this.loading = false;
        },
        error => {
          this.loading = false;
          this.alertService.error(error);
        });
  }

  onChangeItemSelect({_id}) {
    if (this.selectedPlantsForSubmit.includes(_id)) {
      this.selectedPlantsForSubmit = this.selectedPlantsForSubmit.filter(id => id !== _id);
      return;
    }

    this.selectedPlantsForSubmit = [...this.selectedPlantsForSubmit, _id];
  }

  onSubmitPlants() {
    if (!this.selectedPlantsForSubmit || !this.selectedPlantsForSubmit.length) {
      return;
    }
    this.loading = true;
    this.recoveryService.submitToRecovery(this.id, this.selectedPlantsForSubmit)
      .pipe(first())
      .subscribe(
        ({data = {}}: any) => {
          this.selectedPlantsForSubmit = [];
          this.router.navigate([`/admin/recovery/listing`]);
        },
        error => {
          this.loading = false;
          this.alertService.error(error);
        });
  }

  getStockInfo(item) {
    const {
      selfing_input, selfing_dropdown,
      oop_input, oop_dropdown,
      other_input, other_dropdown
    } = item;
    const stock = {
      isSelfing: selfing_input && selfing_dropdown,
      isOOP: oop_input && oop_dropdown,
      isOther: other_input && other_dropdown,
    };
    if (stock.isSelfing) {
      return `Self ${selfing_input} ${selfing_dropdown}`;
    }
    if (stock.isOOP) {
      return `OP ${oop_input} ${oop_dropdown}`;
    }
    if (stock.isOther) {
      return `Other ${other_input} ${other_dropdown}`;

    }
  }
}
