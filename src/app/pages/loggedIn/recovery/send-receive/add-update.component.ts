import {Component, OnInit} from '@angular/core';
import {ActivatedRoute, Router} from '@angular/router';
import {FormBuilder, FormGroup} from '@angular/forms';
import {
  AlertService,
  CommonProvinceService, PlantCategoryService,
  PlantCharacterService,
  PlantGroupTypesService,
  RecoveryService,
  WarehouseService
} from '../../../../services';
import {getRoleResources} from '../../../../utils';
import {TreeviewItem} from 'ngx-treeview';
import {pickBy, identity} from 'lodash';
import {first} from 'rxjs/operators';
import {PlantService} from '../../../../services/plant.service';
import {CommonAumperService} from '../../../../services/common-aumper.service';
import {environment} from '../../../../../environments/environment';

@Component({
  selector: 'app-add-update-plant-register-page',
  templateUrl: './add-update.component.html',
})
export class NgbdSendReceiveComponent implements OnInit {
  plantGroupTypeForm: FormGroup;
  submitted = false;
  loading = false;
  file = null;
  id = null;
  provinceList = [];
  aumperList = [];
  plantsListing = [];
  apiError = false;
  plantInfo = null;
  isAlert = null;
  selectedPlant = null;
  groupTypeList = [];
  categoryList = [];
  regCharacterAttachedFile = null;
  recoverInfo = null;
  roleResources: TreeviewItem[];
  recoverStatus:string = '';
  config = {
    class: 'autocomplete', max: 5, placeholder: 'Type to search',
    sourceField: ['plantTypeName_sci_en']
  };

  constructor(private router: Router,
              private formBuilder: FormBuilder,
              private activeRoute: ActivatedRoute,
              private plantCategoryService: PlantCategoryService,
              private warehouseService: WarehouseService,
              private provinceService: CommonProvinceService,
              private plantCharacterService: PlantCharacterService,
              private plantService: PlantService,
              private aumperService: CommonAumperService,
              private recoveryService: RecoveryService,
              private plantGroupTypesService: PlantGroupTypesService,
              private alertService: AlertService) {
    this.onGetListing('plantCharacterService', 'plantsListing');
    this.onGetListing('aumperService', 'aumperList');
    this.onGetListing('plantCategoryService', 'categoryList');
    this.onGetListing('plantGroupTypesService', 'groupTypeList');
    this.onGetListing('provinceService', 'provinceList');
  }

  ngOnInit() {
    this.roleResources = getRoleResources();
    this.plantGroupTypeForm = this.formBuilder.group({
      recovery_sent_date: [''],
      recovery_amount: [''],
      // recovery_amount_dropdown: [''],
      recovery_place: [''],
      reg_plant_sci_name: [''],
      recovery_province: [''],
      recovery_district: [''],
      recovery_sub_district: [''],
      recovery_address: [''],
      recovery_lat: [''],
      recovery_long: [''],
      recovery_sea_level: [''],
      recovery_rec_recover_date: [''],
      recovery_rec_recover_collect_date: [''],
      recovery_rec_recover_collect_person: [''],
      recovery_rec_date: [''],
      recovery_rec_no: [''],
      recovery_rec_co_person: [''],
      recovery_rec_detail: ['มี'],
      recovery_rec_method: [''],
      recovery_rec_gene_check: [''],
      recovery_rec_remark: [''],
      recovery_test_growth_percent: [''],
      recovery_seed_amount: ['']
    });
    this.activeRoute.params.subscribe(params => {
      const {id, recoveryId} = params;
      this.id = id;
      const isAlert = this.activeRoute.snapshot.queryParamMap.get('isAlert');
      if (isAlert) {
        this.isAlert = true;
      }
      this.onGetPlantRegisterInfo(id);
      this.onGetSpecificRecovery(recoveryId);
    });
  }

  get f() {
    if (!this.apiError && this.submitted && !this.plantGroupTypeForm.invalid) {
      this.alertService.error(null);
    }

    return this.plantGroupTypeForm.controls;
  }

  onSubmit() {
    // this.submitted = true;
    if (this.plantGroupTypeForm.invalid) {
      // this.alertService.error('Some fields are required. Please fill those fields');
      // return;
    }

    this.loading = true;
    const {reg_plant_sci_name, ...otherFormValues} = this.plantGroupTypeForm.value;
    this.warehouseService.createOrUpdate(pickBy({
      ...otherFormValues
    }, identity), this.id)
      .pipe(first())
      .subscribe(
        () => {
          const {recovery_id} = this.plantInfo;
          this.router.navigate([`/admin/recovery/listing-plants/${recovery_id}`]);
        },
        error => {
          this.loading = false;
          this.apiError = true;
          this.alertService.error(error);
        });
  }

  onSelectedChange($event) {
    console.log('Select', $event);
  }

  onFilterChange($event) {
    console.log('Filter Change', $event);
  }

  onGetListing(serviceName, key) {
    this.loading = true;
    this[serviceName].getAll()
      .pipe(first())
      .subscribe(
        ({data = []}: any) => {
          this.loading = false;
          this[key] = data;
        },
        error => {
          this.loading = false;
          this.alertService.error(error);
        });
  }

  onGetPlantRegisterInfo(id) {
    this.loading = true;

    this.warehouseService.getSpecific(id, this.isAlert)
      .pipe(first())
      .subscribe(
        ({data = []}: any) => {
          const {reg_plant_sci_name} = data;
          this.plantInfo = data;
          this.recoverStatus = data['recovery_status']
          Object.keys(data).forEach(key => {
            if (this.plantGroupTypeForm.get(key) && key !== 'reg_plant_sci_name') {
              this.plantGroupTypeForm.get(key).setValue(data[key]);
            }
          });
          this.findPlantSciName(data);
          this.loading = false;
        },
        error => {
          this.loading = false;
          this.alertService.error(error);
        });
  }

  getAttachment(basePath, name) {
    if (name) {
      return `${environment.endpoint}/${basePath}/${name}`;
    }

    return null;
  }

  onFileUpload({srcElement: {files = []} = {}}) {
    this.regCharacterAttachedFile = files[0];
  }

  onSelect(plant) {
    this.selectedPlant = plant;
    this.plantGroupTypeForm.get('reg_plant_sci_name').setValue(plant.genus + ' ' + plant.species);
  }

  findPlantSciName({reg_plant_sci_name}) {
    
    if (!reg_plant_sci_name) {
      return;
    }
    for (const plant of  this.plantsListing) {
      if (reg_plant_sci_name === plant._id) {
        this.selectedPlant = plant;
        this.plantGroupTypeForm.get('reg_plant_sci_name').setValue(plant.genus + ' ' + plant.species);
      }
    }
  }

  getCategoryName(id: string) {
    for (const category of this.categoryList) {
      if (category._id === id) {
        return category.categoryName_en || '-';
      }
    }
    return '-';
  }

  onCancel() {
    const {recovery_id} = this.plantInfo;
    this.router.navigate([`/admin/recovery/listing-plants/${recovery_id}`]);
  }

  onGetSpecificRecovery(recoveryId) {
    this.loading = true;
    this.recoveryService.getSpecific(recoveryId)
      .pipe(first())
      .subscribe(
        ({data = []}: any) => {
          this.recoverInfo = data;
          this.loading = false;
        },
        error => {
          this.loading = false;
          this.alertService.error(error);
        });
  }

}
