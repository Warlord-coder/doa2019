import {Component, OnInit} from '@angular/core';
import {DataTableParams, DataTableResource} from 'ngx-datatable-bootstrap4';
import {NgbModal} from '@ng-bootstrap/ng-bootstrap';
import {Router, ActivatedRoute} from '@angular/router';
import {first, startWith} from 'rxjs/operators';
import {toLower} from 'lodash';
import * as moment from 'moment';

import {AlertService, LabService, PlantCategoryService,WarehouseService,RecoveryService} from '../../../../services';
import {FormBuilder, FormGroup, Validators} from '@angular/forms';

@Component({
  selector: 'app-list-reco-page',
  templateUrl: './alert-list.component.html',
})
export class NgbdListingRecoveryAlertComponent {
  resources = new DataTableResource<any>([]);
  resources1 = new DataTableResource<any>([]);
  resources2 = new DataTableResource<any>([]);
  createLabForm: FormGroup;
  items: any[] = [];
  tempItems: any[] = [];
  items1: any[] = [];
  tempItems1: any[] = [];
  items2: any[] = [];
  tempItems2: any[] = [];
  count = 0;
  count1 = 0;
  count2 = 0;
  loading = false;
  momentObj: any;
  apiError = null;
  submitted = false;
  roles = [];
  constructor(private modalService: NgbModal,
              private alertService: AlertService,
              private formBuilder: FormBuilder,
              private labService: LabService,
              private recoveryService: RecoveryService,
              private whservice: WarehouseService,
              private router: Router,
              private route: ActivatedRoute) {
    this.onGetLabAlertListing();
    this.onGetLabAlertListing1();
    this.onGetLabAlertListing2();
    
    this.momentObj = moment;
  }

  ngOnInit(): void {
    this.route
      .data
      .subscribe(data => {
        this.roles = data['roles'];
      });
    
  }
  reloadData(params: DataTableParams) {
    this.resources.query(params).then(vals => {
      this.tempItems = vals;
      this.items = vals;
    });
  }
  reloadData1(params: DataTableParams) {
    this.resources1.query(params).then(vals => {
      this.tempItems1 = vals;
      this.items1 = vals;
    });
  }
  reloadData2(params: DataTableParams) {
    this.resources2.query(params).then(vals => {
      this.tempItems2 = vals;
      this.items2 = vals;
    });
  }

  onDetails({_id}) {
    this.router.navigate([`/admin/warehouse/form-new-plant/${_id}`]);
  }
  onDetails1({recovery_id}) {
    this.router.navigate([`/admin/recovery/listing-plants/${recovery_id._id}`]);
  }

  get f() {
    if (!this.apiError && this.submitted && !this.createLabForm.invalid) {
      this.alertService.error(null);
    }

    return this.createLabForm.controls;
  }

  onDelete(item: object, deleteContent) {
    this.modalService.open(deleteContent, {centered: true});
  }

  onSearchType({target: {value: searchRole = ''} = {}}) {
    this.items = this.tempItems.filter(({plant_gene_name}) => {
      try {
        return toLower(plant_gene_name.plantCategory.categoryName_th).includes(toLower(searchRole))
      } catch (error) {
        
      }
    });
    this.items1 = this.tempItems1.filter(({plant_gene_name}) => {
      try {
        return toLower(plant_gene_name.plantCategory.categoryName_th).includes(toLower(searchRole))
      } catch (error) {
        
      }
    });
    this.items2 = this.tempItems2.filter(({plant_gene_name}) => {
      try {
        return toLower(plant_gene_name.plantCategory.categoryName_th).includes(toLower(searchRole))
      } catch (error) {
        
      }
    });
  }
  onSearchName({target: {value: searchRole = ''} = {}}) {
    this.items = this.tempItems.filter(({plant_gene_name}) => {
      try {
        return toLower(plant_gene_name.plantTypeName_th).includes(toLower(searchRole))
      } catch (error) {
        
      }
    });
    this.items1 = this.tempItems1.filter(({plant_gene_name}) => {
      try {
        return toLower(plant_gene_name.plantTypeName_th).includes(toLower(searchRole))
      } catch (error) {
        
      }
    });
    this.items2 = this.tempItems2.filter(({plant_gene_name}) => {
      try {
        return toLower(plant_gene_name.plantTypeName_th).includes(toLower(searchRole))
      } catch (error) {
        
      }
    });
  }
  onSearchReg({target: {value: searchRole = ''} = {}}) {
    this.items = this.tempItems.filter(({reg_gene_id}) => {
      try {
        return toLower(reg_gene_id).includes(toLower(searchRole))
      } catch (error) {
        
      }
    });
    this.items1 = this.tempItems1.filter(({reg_gene_id}) => {
      try {
        return toLower(reg_gene_id).includes(toLower(searchRole))
      } catch (error) {
        
      }
    });
    this.items2 = this.tempItems2.filter(({reg_gene_id}) => {
      try {
        return toLower(reg_gene_id).includes(toLower(searchRole))
      } catch (error) {
        
      }
    });
  }
  onSearchGane({target: {value: searchRole = ''} = {}}) {
    this.items = this.tempItems.filter(({gs_no}) => {
      try {
        return toLower(gs_no).includes(toLower(searchRole))
      } catch (error) {
        
      } 
    });
    this.items1 = this.tempItems1.filter(({gs_no}) => {
      try {
        return toLower(gs_no).includes(toLower(searchRole))
      } catch (error) {
        
      }
    });
    this.items2 = this.tempItems2.filter(({gs_no}) => {
      try {
        return toLower(gs_no).includes(toLower(searchRole))
      } catch (error) {
        
      }
    });
  }
  onSearchSci({target: {value: searchRole = ''} = {}}) {
    this.items = this.tempItems.filter(({plant_gene_name}) => {
      try {
        return toLower(plant_gene_name.plantSpecie.genus).includes(toLower(searchRole))
      } catch (error) {
        
      } 
    });
    this.items1 = this.tempItems1.filter(({plant_gene_name}) => {
      try {
        return toLower(plant_gene_name.plantSpecie.genus).includes(toLower(searchRole))
      } catch (error) {
        
      }
    });
    this.items2 = this.tempItems2.filter(({plant_gene_name}) => {
      try {
        return toLower(plant_gene_name.plantSpecie.genus).includes(toLower(searchRole))
      } catch (error) {
        
      }
    });
  }

  onGetLabAlertListing() {
    this.loading = true;
    this.recoveryService.getAllPendingM4()
      .pipe(first())
      .subscribe(
        ({data = []}: any) => {
          this.loading = false;
          this.resources = new DataTableResource<any>(data);
          this.resources.count().then(count => this.count = count);
          this.reloadData({});
        },
        error => {
          this.loading = false;
          this.alertService.error(error);
        });
  }
  onGetLabAlertListing1() {
    this.loading = true;
    this.recoveryService.getRecoveryByStatus('pending')
      .pipe(first())
      .subscribe(
        ({data = []}: any) => {
          this.loading = false;
          this.resources1 = new DataTableResource<any>(data);
          this.resources1.count().then(count => this.count1 = count);
          this.reloadData1({});
        },
        error => {
          this.loading = false;
          this.alertService.error(error);
        });
  }
  onGetLabAlertListing2() {
    this.loading = true;
    this.recoveryService.getRecoveryByStatus('approve')
      .pipe(first())
      .subscribe(
        ({data = []}: any) => {
          this.loading = false;
          this.resources2 = new DataTableResource<any>(data);
          this.resources2.count().then(count => this.count2 = count);
          this.reloadData2({});
        },
        error => {
          this.loading = false;
          this.alertService.error(error);
        });
  }
  checkRole(index){
    if(localStorage.getItem('superAdmin') === "true") return true;
    if(localStorage.getItem('rolesId').indexOf(this.roles[index]) > -1) return true;
    return false;
  }
}
