import {Component, OnInit} from '@angular/core';
import {DataTableParams, DataTableResource} from 'ngx-datatable-bootstrap4';
import {NgbModal} from '@ng-bootstrap/ng-bootstrap';
import {Router, ActivatedRoute} from '@angular/router';
import {toLower} from 'lodash';
import {FormBuilder, FormGroup, Validators} from '@angular/forms';
import {AlertService, RecoveryService} from '../../../../services';
import {first} from 'rxjs/operators';
import * as moment from 'moment';

@Component({
  selector: 'app-listing-common-country-page',
  templateUrl: './listing.component.html',
})
export class NgbdRegisterRecoveryListComponent implements OnInit {
  resources = new DataTableResource<any>([]);
  items: any[] = [];
  count = 0;
  registerPassportForm: FormGroup;
  tempItems: any[] = [];
  submitted = false;
  momentObj: any;
  file = null;
  loading = false;
  apiError = false;
  roles = [];

  constructor(private modalService: NgbModal,
              private formBuilder: FormBuilder,
              private alertService: AlertService,
              private recoveryService: RecoveryService,
              private router: Router,
              private route: ActivatedRoute) {
    this.onGetRecoveryListing();
    this.momentObj = moment;
  }

  ngOnInit() {
    this.route
      .data
      .subscribe(data => {
        this.roles = data['roles'];
      });
    this.registerPassportForm = this.formBuilder.group({
      revival_plant: [''],
      date_of_submission: ['', Validators.required],
      place: ['', [Validators.required]]
    });
  }

  get f() {
    if (!this.apiError && this.submitted && !this.registerPassportForm.invalid) {
      this.alertService.error(null);
    }

    return this.registerPassportForm.controls;
  }

  reloadData(params: DataTableParams) {
    this.resources.query(params).then(vals => {
      this.items = vals;
      this.tempItems = this.items;
    });
  }

  onDetails({_id}) {
    this.router.navigate([`/admin/recovery/listing-plants/${_id}`]);
  }

  onDelete(item: object, deleteContent) {
    this.modalService.open(deleteContent, {centered: true});
  }

  onSearch({target: {value: searchRole = ''} = {}}) {
    this.items = this.tempItems.filter(({m_no}) => m_no && toLower(m_no).includes(toLower(searchRole)));
  }

  onCreate(createContent) {
    this.registerPassportForm.reset();
    this.file = null;
    this.modalService.open(createContent, {centered: true});
  }

  onFileUpload({srcElement: {files = []} = {}}) {
    this.file = files[0];
  }

  onSubmit() {
    this.submitted = true;
    if (this.registerPassportForm.invalid) {
      this.alertService.error('Some fields are required. Please fill those fields');
      return;
    }

    this.loading = true;
    this.apiError = false;

    this.alertService.reset();
    this.loading = true;

    this.modalService.dismissAll();
    this.recoveryService.createOrUpdate({...this.registerPassportForm.value, file: this.file}, null)
      .pipe(first())
      .subscribe(
        ({data = {}}: any) => {
          this.loading = false;
          const {_id} = data;
          this.router.navigate([`/admin/recovery/listing-pending/${_id}`]);
        },
        error => {
          this.loading = false;
          this.apiError = true;
          this.alertService.error(error);
        });
  }

  onGetRecoveryListing() {
    this.loading = true;
    this.recoveryService.getAll()
      .pipe(first())
      .subscribe(
        ({data = []}: any) => {
          this.resources = new DataTableResource<any>(data);
          this.resources.count().then(count => this.count = count);
          this.reloadData({});
          this.loading = false;
        },
        error => {
          this.loading = false;
          this.alertService.error(error);
        });
  }
  checkRole(index){
    if(localStorage.getItem('superAdmin') === "true") return true;
    if(localStorage.getItem('rolesId').indexOf(this.roles[index]) > -1) return true;
    return false;
  }
}
