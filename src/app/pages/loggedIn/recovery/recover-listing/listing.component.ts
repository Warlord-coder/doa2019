import {Component, OnInit} from '@angular/core';
import {DataTableParams, DataTableResource} from 'ngx-datatable-bootstrap4';
import {NgbModal} from '@ng-bootstrap/ng-bootstrap';
import {ActivatedRoute, Router} from '@angular/router';
import {toLower} from 'lodash';
import {FormBuilder, FormGroup, Validators} from '@angular/forms';
// @ts-ignore
import {AlertService, RecoveryService, PlantTypeService} from '../../../../services';
import {first} from 'rxjs/operators';
import * as moment from 'moment';
import {FORM_REQUIRED_KEYS, isFormSubmissionError} from '../../../../utils';

@Component({
  selector: 'app-listing-common-country-page',
  templateUrl: './listing.component.html',
})
export class NgbdRegisterRecoveryListPendingCompleteComponent implements OnInit {
  resources = new DataTableResource<any>([]);
  items: any[] = [];
  tempItems1: any[] = [];
  tempItems2: any[] = [];
  count = 0;
  resources1 = new DataTableResource<any>([]);
  items1: any[] = [];
  count1 = 0;
  resources2 = new DataTableResource<any>([]);
  items2: any[] = [];
  count2 = 0;
  registerPassportForm: FormGroup;
  tempItems: any[] = [];
  submitted = false;
  id = null;
  momentObj: any;
  file = null;
  recoverInfo = null;
  loading = false;
  selectedPlantsForSubmit: any = [];
  selectedPlantsForSubmit1: any = [];
  apiError = false;
  currentStatus = null;
  plantTypes = [];

  constructor(private modalService: NgbModal,
              private formBuilder: FormBuilder,
              private activeRoute: ActivatedRoute,
              private alertService: AlertService,
              private recoveryService: RecoveryService,
              private plantTypeService: PlantTypeService,
              private router: Router) {
    this.momentObj = moment;
  }

  ngOnInit() {
    this.registerPassportForm = this.formBuilder.group({
      revival_plant: ['', Validators.required],
      date_of_submission: ['', Validators.required],
      place: ['', [Validators.required]]
    });
    this.activeRoute.params.subscribe(params => {
      const {id} = params;
      this.id = id;
      this.onGetSpecificRecovery();
      this.onGetRecoveryListing('draft');
      this.onGetRecoveryListing1('pending');
      this.onGetRecoveryListing2('complete');
    });
  }

  get f() {
    if (!this.apiError && this.submitted && !this.registerPassportForm.invalid) {
      this.alertService.error(null);
    }

    return this.registerPassportForm.controls;
  }

  reloadData(params: DataTableParams) {
    this.resources.query(params).then(vals => {
      this.items = vals;
      this.tempItems = this.items;
    });
  }
  reloadData1(params: DataTableParams) {
    this.resources1.query(params).then(vals => {
      this.items1 = vals;
      this.tempItems1 = this.items;
    });
  }
  reloadData2(params: DataTableParams) {
    this.resources2.query(params).then(vals => {
      this.items2 = vals;
      this.tempItems2 = this.items;
    });
  }


  onSearch({target: {value: searchRole = ''} = {}}) {
    this.items = this.tempItems.filter(({passport_no}) => passport_no && toLower(passport_no).includes(toLower(searchRole)));
  }

  onGetRecoveryListing(status) {
    this.loading = true;
    this['plantTypeService'].getAll()
    .pipe(first())
    .subscribe(
      ({data = []}: any) => {
        this.loading = false;
        this.plantTypes = data;
      },
      error => {
        this.loading = false;
        this.alertService.error(error);
      });
    this.currentStatus = status;
    this.recoveryService.getPlants(this.id, status)
      .pipe(first())
      .subscribe(
        ({data = []}: any) => {
          this.resources = new DataTableResource<any>(data);
          this.resources.count().then(count => this.count = count);
          this.reloadData({});
          let that = this;
          data.forEach(function (value) {
            that.plantTypes.forEach(function (planttype) {
              if(value.plant_gene_name._id == planttype._id){
                value.gene_plantTypeName_en = planttype.plantTypeName_en;
              }
            }); 
          }); 
          this.loading = false;
        },
        error => {
          this.loading = false;
          this.alertService.error(error);
        });
  }
  onGetRecoveryListing1(status) {
    this.loading = true;
    this.currentStatus = status;
    this.recoveryService.getPlants(this.id, status)
      .pipe(first())
      .subscribe(
        ({data = []}: any) => {
          this.resources1 = new DataTableResource<any>(data);
          this.resources1.count().then(count => this.count1 = count);
          this.reloadData1({});
          this.loading = false;
        },
        error => {
          this.loading = false;
          this.alertService.error(error);
        });
  }
  onGetRecoveryListing2(status) {
    this.loading = true;
    this.currentStatus = status;
    this.recoveryService.getPlants(this.id, status)
      .pipe(first())
      .subscribe(
        ({data = []}: any) => {
          this.resources2 = new DataTableResource<any>(data);
          this.resources2.count().then(count => this.count2 = count);
          this.reloadData2({});
          this.loading = false;
        },
        error => {
          this.loading = false;
          this.alertService.error(error);
        });
  }

  onGetSpecificRecovery() {
    this.loading = true;
    this.recoveryService.getSpecific(this.id)
      .pipe(first())
      .subscribe(
        ({data = []}: any) => {
          this.recoverInfo = data;
          this.loading = false;
        },
        error => {
          this.loading = false;
          this.alertService.error(error);
        });
  }

  onChangeItemSelect({_id}) {
    if (this.selectedPlantsForSubmit.includes(_id)) {
      this.selectedPlantsForSubmit = this.selectedPlantsForSubmit.filter(id => id !== _id);
      return;
    }

    this.selectedPlantsForSubmit = [...this.selectedPlantsForSubmit, _id];
  }
  onChangeItemSelect1({_id}) {
    if (this.selectedPlantsForSubmit1.includes(_id)) {
      this.selectedPlantsForSubmit1 = this.selectedPlantsForSubmit1.filter(id => id !== _id);
      return;
    }

    this.selectedPlantsForSubmit1 = [...this.selectedPlantsForSubmit1, _id];
  }

  onSubmitPlants() {
    if (!this.selectedPlantsForSubmit || !this.selectedPlantsForSubmit.length) {
      return;
    }
    this.loading = true;
    for (const item of this.items1) {
      const {_id}: any = item;
      if (this.selectedPlantsForSubmit.includes(_id)) {
        const record = isFormSubmissionError(item, FORM_REQUIRED_KEYS.RECOVER_FORM);
        if (record) {
          this.loading = false;
          return this.alertService.error(`Validation Failed for record Reg Gene Id: ${record.reg_gene_id}`);
        }
      }
    }
    this.loading = true;
    this.recoveryService.markRecoveryStatusComplete(this.id, this.selectedPlantsForSubmit)
      .pipe(first())
      .subscribe(
        ({data = {}}: any) => {
          this.selectedPlantsForSubmit = [];
          this.onGetRecoveryListing('draft');
          this.onGetRecoveryListing1('pending');
          this.onGetRecoveryListing2('complete');
        },
        error => {
          this.loading = false;
          this.alertService.error(error);
        });
  }

  onDetails({_id, labId}) {
    let url = `/admin/recovery/send-receive/${this.id}/${_id}`;
    this.router.navigate([url], labId ? {queryParams: {isAlert: true}} : {});
  }

  getStockInfo(item: any) {
    const {
      selfing_input, selfing_dropdown, oop_input, oop_dropdown,
      other_input, other_dropdown
    } = item;
    if (selfing_input && selfing_dropdown) {
      return selfing_dropdown + '/' + selfing_input;
    }
    if (oop_input && oop_dropdown) {
      return oop_dropdown + '/' + oop_input;
    }
    if (other_dropdown && other_input) {
      return other_dropdown + '/' + other_input;
    }
  }

  addPlantsToRecovery() {
    this.router.navigate([`admin/recovery/listing-pending/${this.id}`]);
  }
  onSubmitPlantsPending() {
    if (!this.selectedPlantsForSubmit1 || !this.selectedPlantsForSubmit1.length) {
      return;
    }
    this.loading = true;
    this.recoveryService.submitToRecovery(this.id, this.selectedPlantsForSubmit1,'pending')
      .pipe(first())
      .subscribe(
        ({data = {}}: any) => {
          this.selectedPlantsForSubmit1 = [];
          this.router.navigate([`/admin/recovery/listing`]);
        },
        error => {
          this.loading = false;
          this.alertService.error(error);
        });
  }
}
