import {Component, OnInit} from '@angular/core';
import {ActivatedRoute, Router} from '@angular/router';
import {FormBuilder, FormGroup, Validators} from '@angular/forms';
import {AlertService, RoleService} from '../../../../services';
import {getRoleResources} from '../../../../utils';
import {TreeviewConfig, TreeviewItem} from 'ngx-treeview';
import {first} from 'rxjs/operators';

@Component({
  selector: 'app-add-role-page',
  templateUrl: './add-role.component.html',
})
export class NgbdAddRoleComponent implements OnInit {
  roleForm: FormGroup;
  submitted = false;
  id = null;
  apiError = false;
  loading = false;
  roleResources: TreeviewItem[];
  selectedResources: string[];
  config = TreeviewConfig.create({
    hasAllCheckBox: true,
    hasFilter: true,
    hasCollapseExpand: true,
    decoupleChildFromParent: false
  });


  constructor(private router: Router,
              private formBuilder: FormBuilder,
              private roleService: RoleService,
              private activeRoute: ActivatedRoute,
              private alertService: AlertService) {
  }

  ngOnInit() {
    this.roleResources = getRoleResources();
    this.roleForm = this.formBuilder.group({
      name: ['', Validators.required],
      departmentName_en: ['', Validators.required],
      departmentName_th: ['', Validators.required]
    });
    this.activeRoute.params.subscribe(params => {
      const {id} = params;
      this.id = id;
      if (id && id !== 'create') {
        this.onGetRolesInformation(id);
      }
    });
  }

  get f() {
    if (!this.apiError && this.submitted && !this.roleForm.invalid) {
      this.alertService.error(null);
    }

    return this.roleForm.controls;
  }

  onSubmit() {
    this.submitted = true;
    this.apiError = false;
    if (this.roleForm.invalid) {
      this.alertService.error('Some fields are required. Please fill those fields');

      return;
    }

    this.alertService.reset();
    this.loading = true;

    this.roleService.createOrUpdate({...this.roleForm.value, rolesInfo: this.selectedResources}, this.id)
      .pipe(first())
      .subscribe(
        () => {
          this.loading = false;
          this.router.navigate(['/admin/manage/roles/listing']);
        },
        error => {
          this.loading = false;
          this.apiError = true;
          this.alertService.error(error);
        });
  }

  onSelectedChange(selectedNodes: any) {
    this.selectedResources = selectedNodes;
  }

  onFilterChange($event) {
    console.log('Filter Change', $event);
  }

  onGetRolesInformation(id: string) {
    this.loading = true;
    this.roleService.getSpecific(id)
      .pipe(first())
      .subscribe(
        ({data = {}}: any) => {
          const {name, department_en, department_th, rolesInfo} = data;
          this.roleForm.get('name').setValue(name);
          this.roleForm.get('departmentName_en').setValue(department_en);
          this.roleForm.get('departmentName_th').setValue(department_th);
          this.roleResources = getRoleResources(rolesInfo, false);
          this.loading = false;
        },
        error => {
          this.loading = false;
          this.apiError = true;
          this.alertService.error(error);
        });
  }

  onCancel() {
    this.router.navigate(['/admin/manage/roles/listing']);
  }
}
