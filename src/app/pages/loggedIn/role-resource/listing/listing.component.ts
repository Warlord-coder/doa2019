import {Component} from '@angular/core';
import {DataTableParams, DataTableResource} from 'ngx-datatable-bootstrap4';
import {toLower} from 'lodash';
import {NgbModal} from '@ng-bootstrap/ng-bootstrap';
import {Router, ActivatedRoute} from '@angular/router';
import {AlertService, RoleService} from '../../../../services';
import {first} from 'rxjs/operators';
import * as moment from 'moment';

@Component({
  selector: 'app-listing-role-page',
  templateUrl: './listing.component.html',
})
export class NgbdListingRoleComponent {
  roleResources = new DataTableResource<any>([]);
  items: any[] = [];
  tempItems: any[] = [];
  count = 0;
  loading = true;
  momentObj: any;
  currentSelectedItem: any;
  roles = [];
  constructor(private modalService: NgbModal,
              private roleService: RoleService,
              private alertService: AlertService,
              private router: Router,
              private route: ActivatedRoute) {
    this.onGetRolesListing();
    this.momentObj = moment;
  }

  ngOnInit() {
    this.route
      .data
      .subscribe(data => {
        this.roles = data['roles'];
      });
    
  }
  reloadData(params: DataTableParams) {
    this.roleResources.query(params).then(vals => {
      this.items = vals;
      this.tempItems = this.items;
    });
  }

  onEdit({_id = ''} = {}) {
    this.router.navigate([`/admin/manage/roles/${_id}`]);
  }

  onDelete(item: object, deleteContent) {
    this.currentSelectedItem = item;
    this.modalService.open(deleteContent, {centered: true});
  }

  onSearch({target: {value: searchRole = ''} = {}}) {
    this.items = this.tempItems.filter(({name}) => name && toLower(name).includes(toLower(searchRole)));
  }

  onGetRolesListing() {
    this.roleService.getAll()
      .pipe(first())
      .subscribe(
        ({data = []}: any) => {
          this.roleResources = new DataTableResource<any>(data);
          this.roleResources.count().then(count => this.count = count);
          this.reloadData({});
          this.loading = false;
        },
        error => {
          this.loading = false;
          this.alertService.error(error);
        });
  }
  checkRole(index){
    if(localStorage.getItem('superAdmin') === "true") return true;
    if(localStorage.getItem('rolesId').indexOf(this.roles[index]) > -1) return true;
    return false;
  }
}
