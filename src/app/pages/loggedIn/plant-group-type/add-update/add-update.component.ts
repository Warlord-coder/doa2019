import {Component, OnInit} from '@angular/core';
import {ActivatedRoute, Router} from '@angular/router';
import {FormBuilder, FormGroup, Validators} from '@angular/forms';
import {AlertService, PlantGroupTypesService} from '../../../../services';
import {getRoleResources} from '../../../../utils';
import {TreeviewConfig, TreeviewItem} from 'ngx-treeview';
import {first} from 'rxjs/operators';

@Component({
  selector: 'app-add-update-plant-group-type-page',
  templateUrl: './add-update.component.html',
})
export class NgbdAddUpdatePlantGroupTypeComponent implements OnInit {
  plantGroupTypeForm: FormGroup;
  submitted = false;
  roleResources: TreeviewItem[];
  loading = false;
  apiError = false;
  id = null;
  config = TreeviewConfig.create({
    hasAllCheckBox: true,
    hasFilter: true,
    hasCollapseExpand: true,
    decoupleChildFromParent: false,
    maxHeight: 800
  });


  constructor(private router: Router,
              private formBuilder: FormBuilder,
              private plantGroupTypeService: PlantGroupTypesService,
              private activeRoute: ActivatedRoute,
              private alertService: AlertService) {
  }

  ngOnInit() {
    this.roleResources = getRoleResources();
    this.plantGroupTypeForm = this.formBuilder.group({
      status: ['', Validators.required],
      groupTypeName_en: ['', Validators.required],
      groupTypeName_th: ['', Validators.required]
    });
    this.activeRoute.params.subscribe(params => {
      const {id} = params;
      this.id = id;
      if (id && id !== 'create') {
        this.onGetPlantGroupTypeInformation(id);
      }
    });
  }

  get f() {
    if (!this.apiError && this.submitted && !this.plantGroupTypeForm.invalid) {
      this.alertService.error(null);
    }

    return this.plantGroupTypeForm.controls;
  }

  onSubmit() {
    this.submitted = true;
    if (this.plantGroupTypeForm.invalid) {
      this.alertService.error('Some fields are required. Please fill those fields');
      return;
    }

    this.alertService.reset();
    this.loading = true;

    this.plantGroupTypeService.createOrUpdate({...this.plantGroupTypeForm.value}, this.id)
      .pipe(first())
      .subscribe(
        () => {
          this.loading = false;
          this.router.navigate(['/admin/master/group-type/listing']);
        },
        error => {
          this.loading = false;
          this.apiError = true;
          this.alertService.error(error);
        });
  }

  onSelectedChange($event) {
    console.log('Select', $event);
  }

  onGetPlantGroupTypeInformation(id: string) {
    this.loading = true;
    this.plantGroupTypeService.getSpecific(id)
      .pipe(first())
      .subscribe(
        ({data = {}}: any) => {
          const {status, groupTypeName_en, groupTypeName_th} = data;
          this.plantGroupTypeForm.get('status').setValue(status);
          this.plantGroupTypeForm.get('groupTypeName_en').setValue(groupTypeName_en);
          this.plantGroupTypeForm.get('groupTypeName_th').setValue(groupTypeName_th);
          this.loading = false;
        },
        error => {
          this.loading = false;
          this.apiError = true;
          this.alertService.error(error);
        });
  }

  onCancel() {
    this.router.navigate(['/admin/master/group-type/listing']);
  }
}
