import {Component, OnInit} from '@angular/core';
import {ActivatedRoute, Router} from '@angular/router';
import {FormBuilder, FormGroup, Validators} from '@angular/forms';
import {AlertService, AttributeListService, AttributeSetService} from '../../../../services';
import {TreeviewConfig, TreeviewItem} from 'ngx-treeview';
import {filter} from 'lodash';
import {first} from 'rxjs/operators';
import {DataTableParams, DataTableResource} from 'ngx-datatable-bootstrap4';

@Component({
  selector: 'app-add-update-set-list-page',
  templateUrl: './add-update.component.html',
})
export class NgbdAddUpdateAttributeSetListComponent implements OnInit {
  resources = new DataTableResource<any>([]);
  AttributeSetListForm: FormGroup;
  items = [];
  tempItems = [];
  attributeIds = [];
  count = 0;
  submitted = false;
  loading = false;
  apiError = false;
  id = null;
  roleResources: TreeviewItem[];
  selectedPlantsForSubmit = [];
  config = TreeviewConfig.create({
    hasAllCheckBox: true,
    hasFilter: true,
    hasCollapseExpand: true,
    decoupleChildFromParent: false,
    maxHeight: 800
  });


  constructor(private router: Router,
              private formBuilder: FormBuilder,
              private activeRoute: ActivatedRoute,
              private attributeSetService: AttributeSetService,
              private attributeListService: AttributeListService,
              private alertService: AlertService) {
  }

  ngOnInit() {
    this.AttributeSetListForm = this.formBuilder.group({
      setName: ['', Validators.required]
    });
    this.onGetAttributeListInfo();
    this.activeRoute.params.subscribe(params => {
      const {id} = params;
      this.id = id;
      if (this.id  !== 'create') {
        this.onGetAttributeListInformation(this.id);
      }
    });
  }

  reloadData(params: DataTableParams) {
    this.resources.query(params).then(vals => {
      this.items = vals;
      this.tempItems = this.items;
    });
  }

  onGetAttributeListInfo() {
    this.loading = true;
    this.attributeSetService.getAll()
      .pipe(first())
      .subscribe(
        ({data = []}: any) => {
          this.loading = false;
          this.resources = new DataTableResource<any>(data);
          this.resources.count().then(count => this.count = count);
          this.reloadData({});
        },
        error => {
          this.loading = false;
          this.alertService.error(error);
        });
  }

  get f() {
    if (!this.apiError && this.submitted && !this.AttributeSetListForm.invalid) {
      this.alertService.error(null);
    }

    return this.AttributeSetListForm.controls;
  }

  onChangeItemSelect({_id}) {
    if (this.selectedPlantsForSubmit.includes(_id)) {
      this.selectedPlantsForSubmit = this.selectedPlantsForSubmit.filter(id => id !== _id);
      return;
    }

    this.selectedPlantsForSubmit = [...this.selectedPlantsForSubmit, _id];
  }

  onSubmit() {
    this.submitted = true;
    if (this.AttributeSetListForm.invalid) {
      this.alertService.error('Some fields are required. Please fill those fields');
      return;
    }

    this.apiError = false;

    this.attributeListService.createOrUpdate({
      attributeList: {...this.AttributeSetListForm.value, attributes: this.selectedPlantsForSubmit}
    }, this.id)
      .pipe(first())
      .subscribe(
        ({data = []}: any) => {
          this.loading = false;
          this.router.navigate(['/admin/manage-data/attribute-list/listing']);
        }, error => {
          this.loading = false;
          this.apiError = true;
          this.alertService.error(error);
        });
  }

  onGetAttributeListInformation(id: string) {
    this.loading = true;
    this.attributeListService.getSpecific(id)
      .pipe(first())
      .subscribe(
        ({data = {}}: any) => {
          const {setName = '', attributes = []} = data;
          this.AttributeSetListForm.get('setName').setValue(setName);
          this.selectedPlantsForSubmit = attributes;
          this.loading = false;
        },
        error => {
          this.alertService.error(error);
          this.apiError = true;
          this.loading = false;
        });
  }

  onCancel() {
    this.router.navigate(['/admin/manage-data/attribute-list/listing']);
  }
}
