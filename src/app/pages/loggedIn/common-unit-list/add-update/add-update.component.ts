import {Component, OnInit} from '@angular/core';
import {ActivatedRoute, Router} from '@angular/router';
import {FormBuilder, FormGroup, Validators} from '@angular/forms';
import {AlertService} from '../../../../services';
import {getRoleResources} from '../../../../utils';
import {TreeviewConfig, TreeviewItem} from 'ngx-treeview';
import {first} from 'rxjs/operators';
import {CommonUnitService} from '../../../../services/common-unit.service';

@Component({
  selector: 'app-add-update-common-unit-list-page',
  templateUrl: './add-update.component.html',
})
export class NgbdAddUpdateCommonUnitListComponent implements OnInit {
  CommonUnitListForm: FormGroup;
  submitted = false;
  roleResources: TreeviewItem[];
  loading = false;
  apiError = false;
  id = null;
  config = TreeviewConfig.create({
    hasAllCheckBox: true,
    hasFilter: true,
    hasCollapseExpand: true,
    decoupleChildFromParent: false,
    maxHeight: 800
  });


  constructor(private router: Router,
              private formBuilder: FormBuilder,
              private activeRoute: ActivatedRoute,
              private commonUnitService: CommonUnitService,
              private alertService: AlertService) {
  }

  ngOnInit() {
    this.roleResources = getRoleResources();
    this.CommonUnitListForm = this.formBuilder.group({
      status: ['', Validators.required],
      unitName_en: ['', Validators.required],
      unitName_th: ['', Validators.required]
    });
    this.activeRoute.params.subscribe(params => {
      const {id} = params;
      this.id = id;
      if (id && id !== 'create') {
        this.onGetCommonUnitInformation(id);
      }
    });
  }

  get f() {
    if (!this.apiError && this.submitted && !this.CommonUnitListForm.invalid) {
      this.alertService.error(null);
    }

    return this.CommonUnitListForm.controls;
  }

  onSubmit() {
    this.submitted = true;
    if (this.CommonUnitListForm.invalid) {
      this.alertService.error('Some fields are required. Please fill those fields');
      return;
    }

    this.commonUnitService.createOrUpdate({...this.CommonUnitListForm.value}, this.id)
      .pipe(first())
      .subscribe(
        () => {
          this.loading = false;
          this.router.navigate(['admin/common/unit/listing']);
        },
        error => {
          this.loading = false;
          this.apiError = true;
          this.alertService.error(error);
        });
  }

  onSelectedChange($event) {
    console.log('Select', $event);
  }

  onGetCommonUnitInformation(id: string) {
    this.loading = true;
    this.commonUnitService.getSpecific(id)
      .pipe(first())
      .subscribe(
        ({data = {}}: any) => {
          const {status, unitName_en, unitName_th} = data;
          this.CommonUnitListForm.get('status').setValue(status);
          this.CommonUnitListForm.get('unitName_en').setValue(unitName_en);
          this.CommonUnitListForm.get('unitName_th').setValue(unitName_th);
          this.loading = false;
        },
        error => {
          this.loading = false;
          this.apiError = true;
          this.alertService.error(error);
        });
  }

  onCancel() {
    this.router.navigate(['admin/common/unit/listing']);
  }
}
