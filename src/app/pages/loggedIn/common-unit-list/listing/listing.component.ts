import {Component} from '@angular/core';
import {DataTableParams, DataTableResource} from 'ngx-datatable-bootstrap4';
import {NgbModal} from '@ng-bootstrap/ng-bootstrap';
import {Router, ActivatedRoute} from '@angular/router';
import * as moment from 'moment';
import {AlertService, CommonUnitService} from '../../../../services';
import {first} from 'rxjs/operators';
import {toLower} from 'lodash';

@Component({
  selector: 'app-listing-common-unit-list-page',
  templateUrl: './listing.component.html',
})
export class NgbdCommonUnitListComponent {
  resources = new DataTableResource<any>([]);
  items: any[] = [];
  count = 0;
  loading = false;
  momentObj: any;
  tempItems: any[] = [];
  deletedId = null;
  roles = [];
  constructor(private modalService: NgbModal,
              private alertService: AlertService,
              private commonUnitService: CommonUnitService,
              private router: Router,
              private route: ActivatedRoute) {
    this.onGetCommonUnitListing();
    this.momentObj = moment;
  }

  ngOnInit() {
    this.route
      .data
      .subscribe(data => {
        this.roles = data['roles'];
      });
  }
  
  reloadData(params: DataTableParams) {
    this.resources.query(params).then(vals => {
      this.items = vals;
      this.tempItems = vals;
    });
  }

  onEdit({_id = ''} = {}) {
    this.router.navigate([`/admin/common/unit/${_id}`]);
  }

  onDelete(item: any, deleteContent) {
    this.deletedId = item._id;
    this.modalService.open(deleteContent, {centered: true});
  }

  onSearch({target: {value: searchRole = ''} = {}}) {
    this.items = this.tempItems.filter(({unitName_en}) => unitName_en && toLower(unitName_en).includes(toLower(searchRole)));
  }

  onGetCommonUnitListing() {
    this.loading = true;
    this.commonUnitService.getAll(true)
      .pipe(first())
      .subscribe(
        ({data = []}: any) => {
          this.loading = false;
          this.resources = new DataTableResource<any>(data);
          this.resources.count().then(count => this.count = count);
          this.reloadData({});
        },
        error => {
          this.loading = false;
          this.alertService.error(error);
        });
  }

  onDeleteUnit() {
    this.loading = true;
    this.commonUnitService.delete(this.deletedId)
      .pipe(first())
      .subscribe(
        ({data = []}: any) => {
          this.onGetCommonUnitListing();
        },
        error => {
          this.loading = false;
          this.alertService.error(error);
        });
  }
  checkRole(index){
    if(localStorage.getItem('superAdmin') === "true") return true;
    if(localStorage.getItem('rolesId').indexOf(this.roles[index]) > -1) return true;
    return false;
  }
}
