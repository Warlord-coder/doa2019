import {Component} from '@angular/core';
import {DataTableParams, DataTableResource} from 'ngx-datatable-bootstrap4';
import {NgbModal} from '@ng-bootstrap/ng-bootstrap';
import {Router, ActivatedRoute} from '@angular/router';
import {first} from 'rxjs/operators';
import {toLower} from 'lodash';
import * as moment from 'moment';

import {CommonCountryService} from '../../../../services/common-country.service';
import {AlertService} from '../../../../services';

@Component({
  selector: 'app-listing-common-country-page',
  templateUrl: './listing.component.html',
})
export class NgbdCommonCountryListComponent {
  resource = new DataTableResource<any>([]);
  items: any[] = [];
  tempItems: any[] = [];
  count = 0;
  loading = true;
  momentObj: any;
  deletedId = null;
  roles = [];
  constructor(private modalService: NgbModal,
              private commonCountryService: CommonCountryService,
              private alertService: AlertService,
              private router: Router,
              private route: ActivatedRoute) {
    this.momentObj = moment;
    this.onGetCommonCountryListing();
  }

  ngOnInit() {
    this.route
      .data
      .subscribe(data => {
        this.roles = data['roles'];
      });
  }
  
  reloadData(params: DataTableParams) {
    this.resource.query(params).then(vals => {
      this.items = vals;
      this.tempItems = vals;
    });
  }

  onEdit({_id = ''} = {}) {
    this.router.navigate([`/admin/common/country/${_id}`]);
  }

  onDelete(item: any, deleteContent) {
    this.deletedId = item._id;
    this.modalService.open(deleteContent, {centered: true});
  }

  onSearch({target: {value: searchRole = ''} = {}}) {
    this.items = this.tempItems.filter(({countryName_en}) => countryName_en && toLower(countryName_en).includes(toLower(searchRole)));
  }


  onGetCommonCountryListing() {
    this.loading = true;
    this.commonCountryService.getAll(true)
      .pipe(first())
      .subscribe(
        ({data = []}: any) => {
          this.resource = new DataTableResource<any>(data);
          this.resource.count().then(count => this.count = count);
          this.reloadData({});
          this.loading = false;
        },
        error => {
          this.loading = false;
          this.alertService.error(error);
        });
  }

  onDeleteGroup() {
    this.loading = true;
    this.commonCountryService.delete(this.deletedId)
      .pipe(first())
      .subscribe(
        ({data = []}: any) => {
          this.onGetCommonCountryListing();
        },
        error => {
          this.loading = false;
          this.alertService.error(error);
        });
  }
  checkRole(index){
    if(localStorage.getItem('superAdmin') === "true") return true;
    if(localStorage.getItem('rolesId').indexOf(this.roles[index]) > -1) return true;
    return false;
  }
}
