import {Component, OnInit} from '@angular/core';
import {ActivatedRoute, Router} from '@angular/router';
import {FormBuilder, FormGroup, Validators} from '@angular/forms';
import {AlertService} from '../../../../services';
import {getRoleResources} from '../../../../utils';
import {TreeviewConfig, TreeviewItem} from 'ngx-treeview';
import {first} from 'rxjs/operators';
import {CommonCountryService} from '../../../../services/common-country.service';

@Component({
  selector: 'app-add-update-common-country-page',
  templateUrl: './add-update.component.html',
})
export class NgbdAddUpdateCommonCountryComponent implements OnInit {
  CommonCountryForm: FormGroup;
  submitted = false;
  roleResources: TreeviewItem[];
  loading = false;
  apiError = false;
  id = null;
  config = TreeviewConfig.create({
    hasAllCheckBox: true,
    hasFilter: true,
    hasCollapseExpand: true,
    decoupleChildFromParent: false,
    maxHeight: 800
  });


  constructor(private router: Router,
              private formBuilder: FormBuilder,
              private commonCountryService: CommonCountryService,
              private activeRoute: ActivatedRoute,
              private alertService: AlertService) {
  }

  ngOnInit() {
    this.roleResources = getRoleResources();
    this.CommonCountryForm = this.formBuilder.group({
      status: ['', Validators.required],
      countryName_en: ['', Validators.required],
      countryName_th: ['', Validators.required]
    });
    this.activeRoute.params.subscribe(params => {
      const {id} = params;
      this.id = id;
      if (id && id !== 'create') {
        this.onGetCommonCountryInformation(id);
      }
    });
  }

  get f() {
    if (!this.apiError && this.submitted && !this.CommonCountryForm.invalid) {
      this.alertService.error(null);
    }

    return this.CommonCountryForm.controls;
  }

  onSubmit() {
    this.submitted = true;
    if (this.CommonCountryForm.invalid) {
      this.alertService.error('Some fields are required. Please fill those fields');
      return;
    }

    this.commonCountryService.createOrUpdate({...this.CommonCountryForm.value}, this.id)
      .pipe(first())
      .subscribe(
        () => {
          this.loading = false;
          this.router.navigate(['admin/common/country/listing']);
        },
        error => {
          this.loading = false;
          this.apiError = true;
          this.alertService.error(error);
        });
  }

  onSelectedChange($event) {
    console.log('Select', $event);
  }

  onGetCommonCountryInformation(id: string) {
    this.loading = true;
    this.commonCountryService.getSpecific(id)
      .pipe(first())
      .subscribe(
        ({data = {}}: any) => {
          const {status, countryName_en, countryName_th} = data;
          this.CommonCountryForm.get('status').setValue(status);
          this.CommonCountryForm.get('countryName_en').setValue(countryName_en);
          this.CommonCountryForm.get('countryName_th').setValue(countryName_th);
          this.loading = false;
        },
        error => {
          this.loading = false;
          this.apiError = true;
          this.alertService.error(error);
        });
  }

  onCancel() {
    this.router.navigate(['admin/common/country/listing']);
  }
}
