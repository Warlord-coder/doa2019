import {Component, OnInit} from '@angular/core';
import {DataTableParams, DataTableResource} from 'ngx-datatable-bootstrap4';
import {NgbModal} from '@ng-bootstrap/ng-bootstrap';
import {ActivatedRoute, Router} from '@angular/router';
import {FormBuilder, FormGroup, Validators} from '@angular/forms';
import {first} from 'rxjs/operators';
import {AlertService, PassportService, VolumeService} from '../../../../services';
import * as moment from 'moment';
import {toLower} from 'lodash';

@Component({
  selector: 'app-listing-common-country-page',
  templateUrl: './listing.component.html',
})
export class NgbdRegisterVolsListComponent implements OnInit {
  resources = new DataTableResource<any>([]);
  items: any[] = [];
  passortId = null;
  count = 0;
  apiError = false;
  loading = false;
  tempItems = [];
  passportInfo = null;
  momentObj = null;
  registerVolumeForm: FormGroup;
  submitted = false;

  constructor(private modalService: NgbModal,
              private formBuilder: FormBuilder,
              private activeRoute: ActivatedRoute,
              private alertService: AlertService,
              private volService: VolumeService,
              private passportService: PassportService,
              private router: Router) {
    this.momentObj = moment;
  }

  ngOnInit() {
    this.registerVolumeForm = this.formBuilder.group({
      form_type: ['Register 1/2', Validators.required]
    });

    this.activeRoute.params.subscribe(params => {
      const {passortId} = params;
      this.passortId = passortId;
      if (passortId) {
        this.onGetPassportListing(passortId);
        this.onGetVolumeService(passortId);
      }
    });
  }

  get f() {
    if (!this.apiError && this.submitted && !this.registerVolumeForm.invalid) {
      this.alertService.error(null);
    }

    return this.registerVolumeForm.controls;
  }

  reloadData(params: DataTableParams) {
    this.resources.query(params).then(vals => {
      this.tempItems = vals;
      this.items = vals;
    });
  }

  onDelete(item: object, deleteContent) {
    this.modalService.open(deleteContent, {centered: true});
  }

  onSearch({target: {value: searchRole = ''} = {}}) {
    this.items = this.tempItems.filter(({volume_no}) => volume_no && toLower(volume_no).includes(toLower(searchRole)));
  }

  onCreate(createContent) {
    this.modalService.open(createContent, {centered: true});
  }

  onSubmit() {
    this.submitted = true;
    if (this.registerVolumeForm.invalid) {
      this.alertService.error('Some fields are required. Please fill those fields');
      return;
    }

    this.loading = true;
    this.apiError = false;

    this.alertService.reset();
    this.loading = true;

    this.modalService.dismissAll();
    this.volService.createOrUpdate({...this.registerVolumeForm.value, passport_no: this.passortId}, null)
      .pipe(first())
      .subscribe(
        ({data = {}}: any) => {
          const {_id, form_type} = data;
          console.log('Form Type', form_type);
          if (form_type === 'Register 4') {
            return this.router.navigate([`/admin/register/unassigned-plant/${_id}/listing`]);
          }
          if (form_type === 'Register 3') {
            return this.router.navigate([`/admin/register/register-3/${_id}/listing`]);
          }
          this.router.navigate([`/admin/register/plant/${_id}/listing`]);
          this.loading = false;
        },
        error => {
          this.loading = false;
          this.apiError = true;
          this.alertService.error(error);
        });
  }

  onGetPassportListing(id) {
    this.loading = true;
    this.passportService.getSpecific(id)
      .pipe(first())
      .subscribe(
        ({data = []}: any) => {
          this.passportInfo = data;
          this.loading = false;
        },
        error => {
          this.loading = false;
          this.alertService.error(error);
        });
  }

  onGetVolumeService(id) {
    this.loading = true;
    this.volService.getAll(id)
      .pipe(first())
      .subscribe(
        ({data = []}: any) => {
          this.resources = new DataTableResource<any>(data);
          this.resources.count().then(count => this.count = count);
          this.reloadData({});
          this.loading = false;
        },
        error => {
          this.loading = false;
          this.alertService.error(error);
        });
  }

  onEdit({_id = '', form_type = ''} = {}) {
    localStorage.setItem('lastUrl', `/admin/register/volume/${this.passortId}`);
    if (form_type === 'Register 3') {
      return this.router.navigate([`/admin/register/register-3/${_id}/listing`]);
    }
    this.router.navigate([`/admin/register/plant/${_id}/listing`]);
  }

  onLockVolume() {
    this.loading = true;
    this.passportService.createOrUpdate({isVolumeLock: true}, this.passortId)
      .pipe(first())
      .subscribe(
        ({data = []}: any) => {
          this.passportInfo = data;
          this.loading = false;
        },
        error => {
          this.loading = false;
          this.alertService.error(error);
        });
  }
}
