import {Component, OnInit} from '@angular/core';
import {ActivatedRoute, Router} from '@angular/router';
import {FormBuilder, FormGroup, Validators, FormArray, FormControl, ValidationErrors} from '@angular/forms';
import {pickBy} from 'lodash';
import {
  AlertService, AttributeListService,
  CommonProvinceService,
  PassportService,
  PlantCategoryService, PlantCharacterService,
  PlantGroupsService,
  PlantGroupTypesService, PlantTypeService,
  VolumeService,
  WarehouseService
} from '../../../../services';
import {getRoleResources} from '../../../../utils';
import {TreeviewItem} from 'ngx-treeview';
import {first, timeout} from 'rxjs/operators';
import {PlantService} from '../../../../services/plant.service';
import {CommonCountryService} from '../../../../services/common-country.service';
import {CommonAumperService} from '../../../../services/common-aumper.service';
import {environment} from '../../../../../environments/environment';

@Component({
  selector: 'app-add-update-plant-register-page',
  templateUrl: './add-update.component.html',
})
export class NgbdAddUpdatePlantDOAComponent implements OnInit {
  plantGroupTypeForm: FormGroup;
  attributeForm: FormGroup;
  submitted = false;
  loading = false;
  file = null;
  id = null;
  categoryList = [];
  groupTypeList = [];
  groupList = [];
  provinceList = [];
  aumperList = [];
  countryList = [];
  volumeInfo = null;
  passportInfo = null;
  labInfo = null;
  apiError = false;
  plantsListing = [];
  reg_seed_no = null;
  attributeList = [];
  plantTypes = [];
  reg_gene_id = null;
  selectedPlant = null;
  descriptorFile = null;
  description_file = null;
  volumeId = null;
  selectedAttribute = null;
  roleResources: TreeviewItem[];
  config = {
    class: 'autocomplete', max: 5, placeholder: 'Type to search',
    sourceField: ['plantTypeName_sci_en']
  };

  selectedAttributeId = "";
  errors = {
    minMaxError: false
  }
  attributeError = false;
  constructor(private router: Router,
              private formBuilder: FormBuilder,
              private activeRoute: ActivatedRoute,
              private plantTypeService: PlantTypeService,
              private plantCategoryService: PlantCategoryService,
              private plantGroupTypesService: PlantGroupTypesService,
              private plantGroupsService: PlantGroupsService,
              private plantCharacterService: PlantCharacterService,
              private volumeService: VolumeService,
              private passportService: PassportService,
              private warehouseService: WarehouseService,
              private countryService: CommonCountryService,
              private attributeListService: AttributeListService,
              private provinceService: CommonProvinceService,
              private aumperService: CommonAumperService,
              private alertService: AlertService) {
    this.onGetListing('plantCharacterService', 'plantsListing');
    this.onGetListing('plantCategoryService', 'categoryList');
    this.onGetListing('plantGroupTypesService', 'groupTypeList');
    this.onGetListing('plantGroupsService', 'groupList');
    this.onGetListing('countryService', 'countryList');
    this.onGetListing('aumperService', 'aumperList');
    this.onGetListing('provinceService', 'provinceList');
    this.onGetListing('attributeListService', 'attributeList');
  }

  ngOnInit() {
    this.roleResources = getRoleResources();
    this.attributeForm = this.formBuilder.group({});
    this.plantGroupTypeForm = this.formBuilder.group({
      plant_gene_name: [''],
      reg_gene_type: [''],
      reg_gene_category: [''],
      reg_plant_alter_name: [[]],
      reg_plant_sci_name: [''],
      reg_seed_collect_date: [''],
      selfing_input: [''],
      selfing_dropdown: [''],
      oop_input: [''],
      oop_dropdown: [''],
      other_input: [''],
      other_dropdown: [''],
      department_type: [''],
      department_sub_type: [''],
      department_name: [''],
      source_country: [''],
      source_country_1: [''],
      source_province: [''],
      source_district: [''],
      source_address: [''],
      lat: [''],
      long: [''],
      source_by_department: [''],
      reg_seed_collect_person: [''],
      reg_seed_owner: [''],
      reg_seed_sender: [''],
      reg_character_brief: [''],
      reg_character_for_record: [''],
      reg_sent_lab_date: [''],
      reg_remark: [''],
      attributeSetId: [''],
      area_information: [''],
      other_information: [''],
      note: [''],
      attributes: this.formBuilder.array([])
    });
    this.activeRoute.params.subscribe(params => {
      const {id, volume} = params;
      this.id = id;
      if (id && id !== 'create') {
        setTimeout(() =>  {
          this.onGetPlantRegisterInfo(id);
       }, 500);
        
      }
    });
  }

  get f() {
    if (!this.apiError && this.submitted && !this.plantGroupTypeForm.invalid) {
      this.alertService.error(null);
    }

    return this.plantGroupTypeForm.controls;
  }

  get g() {
    if (!this.apiError && this.submitted && !this.attributeForm.invalid) {
      this.alertService.error(null);
    }

    return this.attributeForm.controls;
  }
  onSubmit() {
    this.submitted = true;
    if (this.plantGroupTypeForm.invalid) {
      Object.keys(this.attributeForm.controls).forEach(key => {

        const controlErrors: ValidationErrors = this.attributeForm.get(key).errors;
        if (controlErrors != null) {
              Object.keys(controlErrors).forEach(keyError => {
                //console.log('Key control: ' + key + ', keyError: ' + keyError + ', err value: ', controlErrors[keyError]);
              });
            }
          });
      this.alertService.error('Some fields are required. Please fill those fields');
      return;
    }

    const KEYS = ['plant_gene_name', 'reg_gene_type', 'reg_gene_category', 'reg_seed_collect_date',
      'selfing_input', 'selfing_dropdown', 'oop_input', 'oop_dropdown', 'other_input',
      'other_dropdown', 'department_type', 'department_sub_type', 'department_name',
      'source_country', 'source_country_1', 'source_province', 'source_district',
      'source_address', 'lat', 'long', 'source_by_department', 'reg_seed_collect_person',
      'reg_seed_owner', 'reg_seed_sender', 'reg_character_brief', 'reg_character_for_record',
      'reg_plant_sci_name', 'reg_sent_lab_date', 'reg_remarks'];

    const data = pickBy(this.plantGroupTypeForm.value, (obj, key) => {
      if(key == "attributes") return;
      return !KEYS.includes(key);
    });
    
    let attributes_values = [];
    
    let index = 0;
    pickBy(this.plantGroupTypeForm.controls['attributes'].value, (obj, key) => {
      if(key.split("_en").length > 1){
        this.selectedAttribute.attributes.forEach(item => {
          if(item['_id'] ==  key.split("_")[0]) {
            return attributes_values.push({attribute_id:item['_id'], name_en: obj, code: item['code'], name_th: '', type: 'checkbox'});
          }
        })
      } else {
        this.selectedAttribute.attributes.forEach(item => {
          if(item['_id'] ==  key.split("en")[0]) {
            return attributes_values.push({attribute_id:item['_id'], name_en: obj, code: item['code'], name_th: '', type: '-'});
          }
        })
      } 
      if(key.split("_th").length > 1) {
        attributes_values[index]['name_th'] = obj;
        index++;
      } else if(key.split("_th").length > -1 && key.indexOf("th") > -1){
        attributes_values[index]['name_th'] = obj;
        index++;
      }
    })
    if(this.selectedAttribute) data['attribute'] = {attribute_set_id: this.selectedAttribute._id, attribute_value: attributes_values};
   
    this.warehouseService.createOrUpdate({
      ...data,
      description_file: this.description_file,
      file: this.descriptorFile,
      isDescriptorFileUpload: Boolean(this.descriptorFile)
    }, this.id)
      .pipe(first())
      .subscribe(
        () => {
          this.loading = false;
          this.submitted = false;
          this.router.navigate([`/admin/manage-data/plant-doa/listing`]);
          
        },
        error => {
          this.loading = false;
          this.apiError = true;
          this.alertService.error(error);
        });
  }

  onSelectedChange($event) {
    console.log('Select', $event);
  }

  onFilterChange($event) {
    console.log('Filter Change', $event);
  }

  onGetListing(serviceName, key) {
    this.loading = true;
    this[serviceName].getAll()
      .pipe(first())
      .subscribe(
        ({data = []}: any) => {
          this.loading = false;
          this[key] = data;
        },
        error => {
          this.loading = false;
          this.alertService.error(error);
        });
  }

  onGetPlantRegisterInfo(id) {
    this.loading = true;

    this.warehouseService.getSpecific(id)
      .pipe(first())
      .subscribe(
        ({data = []}: any) => {
          const {plant_gene_name, reg_gene_id, reg_seed_no, reg_plant_sci_name, description_file, attribute} = data;
          this.labInfo = data;
          if(plant_gene_name!=undefined && plant_gene_name!= null)
            this['plantTypeService'].getSpecific(plant_gene_name) 
            .pipe(first())
            .subscribe(
              ({data = []}: any) => {
                this.plantTypes.push(data )
                this.loading = false;
              },
              error => {
                this.loading = false;
                this.alertService.error(error);
              });
          Object.keys(data).forEach(key => {
            if(key == "attribute") {
              this.selectedAttribute = null;
              for (const item of  this.attributeList) {
                if (item._id === attribute.attribute_set_id) {
                  this.selectedAttribute = item;
                  this.plantGroupTypeForm.get('attributeSetId').setValue( item._id );
                  this.createOptionValue(item.attributes);
                  let sameIdNo = 0;
                  for(let i = 0; i < attribute.attribute_value.length; i++) {
                    if(attribute.attribute_value[i] == undefined) continue;
                    if(attribute.attribute_value[i].type == '-') {
                      sameIdNo = 0;
                      if(attribute.attribute_value[i].name_en != ""){
                        this.attributeForm.get(attribute.attribute_value[i].attribute_id+"en").setValue(attribute.attribute_value[i].name_en);
                      }
                      if(attribute.attribute_value[i].name_th != ""){ 
                        this.attributeForm.get(attribute.attribute_value[i].attribute_id+"th").setValue(attribute.attribute_value[i].name_th);
                      }
                      
                    } else {
                      if(attribute.attribute_value[i].name_en != ""){
                        this.attributeForm.get(attribute.attribute_value[i].attribute_id+'_'+sameIdNo+"_en").setValue(attribute.attribute_value[i].name_en);
                      }
                      if(attribute.attribute_value[i].name_th != ""){ 
                        this.attributeForm.get(attribute.attribute_value[i].attribute_id+'_'+sameIdNo+"_th").setValue(attribute.attribute_value[i].name_th);
                      }
                      sameIdNo++;
                    }
                    
                  }
                  this.plantGroupTypeForm.setControl('attributes', this.attributeForm);
                }
              }
            } else if (this.plantGroupTypeForm.get(key)) {
              this.plantGroupTypeForm.get(key).setValue(data[key]);
            }
          });
          this.reg_gene_id = reg_gene_id;
          this.reg_seed_no = reg_seed_no;
          this.description_file = description_file;
          this.findPlantSciName(data);
          this.loading = false;
        },
        error => {
          this.loading = false;
          this.alertService.error(error);
        });
  }

  findPlantSciName({reg_plant_sci_name}) {
    if (!reg_plant_sci_name) {
      return;
    }
    for (const plant of  this.plantsListing) {
      if (reg_plant_sci_name === plant._id) {
        this.selectedPlant = plant;
        this.plantGroupTypeForm.get('reg_plant_sci_name').setValue(plant.genus + ' ' + plant.species);
      }
    }

  }

  onSelect(plant) {
    this.selectedPlant = plant;
    this.plantGroupTypeForm.get('reg_plant_sci_name').setValue(plant.genus + ' ' + plant.species);
  }

  onChange({target: {value = ''} = {}}) {
    /*if (this.selectedAttribute) {
      this.removeControl(this.selectedAttribute.attributes);
    }*/
    this.selectedAttribute = null;
    for (const item of  this.attributeList) {
      if (item._id === value) {
        this.selectedAttribute = item;
        this.createOptionValue(item.attributes);
      }
    }

  }

  onGetVolumeInfo(id) {
    this.loading = true;
    this.volumeService.getSpecific(id)
      .pipe(first())
      .subscribe(
        ({data = []}: any) => {
          this.loading = false;
          const {passport_no} = data;
          this.volumeInfo = data;
          this.onGetPassportInfo(passport_no);
        },
        error => {
          this.loading = false;
          this.alertService.error(error);
        });
  }

  onGetPassportInfo(id) {
    this.loading = true;
    this.passportService.getSpecific(id)
      .pipe(first())
      .subscribe(
        ({data = []}: any) => {
          this.loading = false;
          const {passport_no} = data;
          this.passportInfo = data;
        },
        error => {
          this.loading = false;
          this.alertService.error(error);
        });
  }

  getAttachment(basePath, name) {
    if (name) {
      return `${environment.endpoint}/${basePath}/${name}`;
    }

    return null;
  }

  onFileUpload({srcElement: {files = []} = {}}, key) {
    this[key] = files[0];
  }


  findItemPlantListing = (dbVal) => {
    for (const item of this.plantsListing) {
      if (item._id === dbVal) {
        this.selectedPlant = item;
        $('#plantSciName').val(item.plantTypeName_sci_en);
        return;
      }
    }
  }

  onCancel() {
    this.router.navigate(['/admin/manage-data/plant-doa/listing']);
  }

  createOptionValue(data: any) {
    
    this.clearAttributeForm();
    data.forEach(attribute => {
      let {name_en, name_th, _id, code, min, max, fieldInput, require, optionValue} = attribute;
      if(fieldInput == 'checkbox') {
        optionValue.forEach((option, key) => {
          this.attributeForm.addControl(`${_id}_${key}_en`, this.formBuilder.control(''))
          this.attributeForm.addControl(`${_id}_${key}_th`, this.formBuilder.control(''))
        })
      } else {
        this.attributeForm.addControl(`${_id}en`, this.formBuilder.control(''));
        this.attributeForm.addControl(`${_id}th`, this.formBuilder.control(''));
      }
      if(fieldInput == "text" || fieldInput == "textarea") {
        if(min == undefined || min == "") min = 1;
        if(max == undefined || max == "") max = 99999;

        this.attributeForm.controls[`${_id}en`].setValidators([Validators.minLength(min), Validators.maxLength(max)]);
        this.attributeForm.controls[`${_id}th`].setValidators([Validators.minLength(min), Validators.maxLength(max)]);
      }
      if(require === "true") {
        if(fieldInput != 'checkbox'){
          this.attributeForm.controls[`${_id}en`].setValidators([Validators.required]);
          this.attributeForm.controls[`${_id}th`].setValidators([Validators.required]);
        }
        
      }
      
    });
    this.plantGroupTypeForm.setControl('attributes', this.attributeForm);
    
  }

  clearAttributeForm() {
    
    Object.keys(this.attributeForm.controls).forEach(key => {
      this.attributeForm.removeControl(key);
    });
  }
  /*removeControl(data: any) {
    data.forEach(attribute => {
      const {name_en, name_th} = attribute;
      if (this.plantGroupTypeForm.get(name_en)) {
        this.plantGroupTypeForm.removeControl(name_en);
      }
      if (this.plantGroupTypeForm.get(name_th)) {
        this.plantGroupTypeForm.removeControl(name_th);
      }
    });
  }*/

  changeValue(type, lang, id, event, index = null) {
    let attributes = this.selectedAttribute.attributes;
    let optionValues;
    let selectedVal = event.target.value;
    if(type == "dropdown") {
      attributes.forEach(item => {
        if(item['_id'] == id) {
          optionValues = item['optionValue'];
          optionValues.forEach(option => {
            if(option['value_'+lang] == selectedVal) {
              this.attributeForm.get(id+"th").setValue(option['value_th']);
            }
          });
        }
      })
    }
    if(type == "text" || type == "date") {
      attributes.forEach(item => {
        if(item['_id'] == id) {
          this.attributeForm.get(id+"th").setValue(selectedVal);
        }
      })
    }
    if(type == 'checkbox') {
      if(event.target.checked == true) {
        attributes.forEach(item => {
          if(item['_id'] == id) {
            optionValues = item['optionValue'];
            optionValues.forEach(option => {
              if(option['value_'+lang] == selectedVal) {
                this.attributeForm.get(id+'_'+index+"_th").setValue(option['value_th']);
                this.attributeForm.get(id+'_'+index+"_en").setValue(option['value_en']);
              }
            });
          }
        })
      } else  {
        attributes.forEach(item => {
          if(item['_id'] == id) {
            optionValues = item['optionValue'];
            optionValues.forEach(option => {
              if(option['value_'+lang] == selectedVal) {
                this.attributeForm.get(id+'_'+index+"_th").setValue('');
                this.attributeForm.get(id+'_'+index+"_en").setValue('');
              }
            });
          }
        })
      }
      
    }
  }
}
