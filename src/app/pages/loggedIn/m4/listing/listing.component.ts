import {Component, OnInit} from '@angular/core';
import {DataTableParams, DataTableResource} from 'ngx-datatable-bootstrap4';
import {NgbModal} from '@ng-bootstrap/ng-bootstrap';
import {Router, ActivatedRoute} from '@angular/router';
import {toLower} from 'lodash';
import {FormBuilder, FormGroup, Validators} from '@angular/forms';
import {AlertService, RecoveryService} from '../../../../services';
import {first} from 'rxjs/operators';
import * as moment from 'moment';
import {identity, pickBy} from 'lodash';

@Component({
  selector: 'app-listing-common-country-page',
  templateUrl: './listing.component.html',
})
export class NgbdM4CompletedListComponent implements OnInit {
  resources = new DataTableResource<any>([]);
  items: any[] = [];
  count = 0;
  registerPassportForm: FormGroup;
  tempItems: any[] = [];
  submitted = false;
  momentObj: any;
  file = null;
  loading = false;
  apiError = false;
  roles = [];
  constructor(private modalService: NgbModal,
              private formBuilder: FormBuilder,
              private alertService: AlertService,
              private recoveryService: RecoveryService,
              private router: Router,
              private route: ActivatedRoute) {
    this.momentObj = moment;
  }

  ngOnInit() {
    this.route
      .data
      .subscribe(data => {
        this.roles = data['roles'];
      });
    this.onGetRecoveryListing();
  }

  onGetRecoveryListing() {
    this.loading = true;
    this.recoveryService.listCompleteRecoveryRegister()
      .pipe(first())
      .subscribe(
        ({data = []}: any) => {
          this.resources = new DataTableResource<any>(data);
          this.resources.count().then(count => this.count = count);
          this.reloadData({});
          this.loading = false;
        },
        error => {
          this.loading = false;
          this.alertService.error(error);
        });
  }

  reloadData(params: DataTableParams) {
    this.resources.query(params).then(vals => {
      this.items = vals;
      this.tempItems = this.items;
    });
  }


  onSearch({target: {value: searchRole = ''} = {}}) {
    this.items = this.tempItems.filter(({d_no}) => d_no && toLower(d_no).includes(toLower(searchRole)));
  }

  checkRole(index){
    if(localStorage.getItem('superAdmin') === "true") return true;
    if(localStorage.getItem('rolesId').indexOf(this.roles[index]) > -1) return true;
    return false;
  }
}
