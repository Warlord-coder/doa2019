import {Component, AfterViewInit, EventEmitter, Output} from '@angular/core';
import {
  NgbModal,
  ModalDismissReasons,
  NgbPanelChangeEvent,
  NgbCarouselConfig
} from '@ng-bootstrap/ng-bootstrap';
import {PerfectScrollbarConfigInterface} from 'ngx-perfect-scrollbar';
import {AuthService} from '../../services';
import {environment} from '../../../environments/environment';
import {Router} from '@angular/router';

declare var $: any;

@Component({
  selector: 'app-navigation',
  templateUrl: './navigation.component.html'
})
export class NavigationComponent implements AfterViewInit {
  @Output() toggleSidebar = new EventEmitter<void>();

  public config: PerfectScrollbarConfigInterface = {};

  constructor(private modalService: NgbModal, private authService: AuthService, private router: Router) {
  }

  public showSearch = false;

  ngAfterViewInit() {
  }


  getUserImage(imageUrl) {
    if (imageUrl) {
      return `${environment.endpoint}/profile-pictures/${imageUrl}`;
    }

    return 'assets/images/users/profile.png';
  }

  onLogout() {
    localStorage.removeItem('authorization');
    this.router.navigate(['/authentication/login']);
  }

}
