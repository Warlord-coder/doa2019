// Sidebar route metadata
export interface RouteInfo {
  path: string;
  title: string;
  icon: string;
  class: string;
  label: string;
  labelClass: string;
  external?: boolean;
  extralink: boolean;
  submenu: RouteInfo[];
  value: number;
  allSubmenu: any
}
