import {Component, AfterViewInit, OnInit, Input} from '@angular/core';
import {NgbModal} from '@ng-bootstrap/ng-bootstrap';
import {Router, ActivatedRoute} from '@angular/router';
import {includes, forEach, uniq} from 'lodash';
import {WarehouseService} from '../../services/warehouse.service';
import {first} from 'rxjs/operators';
declare var $: any;

@Component({
  selector: 'app-sidebar',
  templateUrl: './sidebar.component.html'
})
export class SidebarComponent implements OnInit {
  showMenu = '';
  menuRoles = [];
  stock_withdraw;
  stock_distribution;
  stock_alert;
  stock_recovery;
  alert_draft;
  draft;
  m4_count;
  @Input('routes') public sidebarnavItems: any = [];
  @Input('roles') public roles: any = [];
  // this is for the open close
  addExpandClass(element: any) {
    if (element === this.showMenu) {
      this.showMenu = '0';
    } else {
      this.showMenu = element;
    }
  }

  constructor(
    private modalService: NgbModal,
    private router: Router,
    private route: ActivatedRoute,
    private warehouseService: WarehouseService,
  ) {
    //
    this.warehouseService.stockWatch.subscribe(data => {
      this.stock_withdraw = data['withdraw'];
      this.stock_distribution = data['distribution'];
      this.stock_alert = data['alert'];
      this.stock_recovery = data['recovery'];
      this.alert_draft = data['alert_draft'];
      this.draft = data['draft'];
      this.m4_count = data['m4_count'];
		})
  }

  // End open close
  ngOnInit() {
    this.sidebarnavItems = this.sidebarnavItems.filter(sidebarnavItem => sidebarnavItem);
    this.menuRoles = [];
    this.menuRoles = this.getRoles(this.roles);
    
  }

  getRoles(roles) {
    let menuRoles = [];
    forEach(roles, val => {
      if(val == 3) menuRoles.push(2);
      if(val == 8) menuRoles.push(7);
      if(val == 13) menuRoles.push(12);
      if(val == 18) menuRoles.push(17);
      if(val == 23) menuRoles.push(22);
      if(val == 28) menuRoles.push(27);
      if(val == 33) menuRoles.push(32);
      if(val == 38) menuRoles.push(37);
      if(val == 43) menuRoles.push(42);
      if(val == 48) menuRoles.push(47);
      if(val == 53) menuRoles.push(52);
      if(val == 58) menuRoles.push(57);
      if(val == 63) menuRoles.push(62);
      if(val == 65) menuRoles.push(64);
      if(val == 67) menuRoles.push(66);
      if(val == 69) menuRoles.push(68);
      if(val == 71) menuRoles.push(70);
      if(val == 73) menuRoles.push(72);
      if(val == 75) menuRoles.push(74);
      if(val == 77) menuRoles.push(76);
      if(val == 79) menuRoles.push(78);
      if(val == 81) menuRoles.push(80);
      if(val == 83) menuRoles.push(82);
      if(val == 85) menuRoles.push(84);
      if(val == 89) menuRoles.push(88);
      if(val == 93) menuRoles.push(92);
      if(val == 97) menuRoles.push(96);
      if(val == 100) menuRoles.push(99);
      if(val == 103) menuRoles.push(102);
      if(val == 105) menuRoles.push(104);
      if(val == 108) menuRoles.push(107);
      if(val == 111) menuRoles.push(110);
      if(val == 114) menuRoles.push(113);
      if(val == 117) menuRoles.push(116);
      if(val == 120) menuRoles.push(119);
      if(val == 122) menuRoles.push(121);
      if(val == 126) menuRoles.push(125);
      if(val == 128) menuRoles.push(127);
      if(val == 132) menuRoles.push(131);
      if(val == 136) menuRoles.push(135);
      if(val == 140) menuRoles.push(139);
      if(val == 143) menuRoles.push(142);
      if(val == 147) menuRoles.push(146);
      if(val == 149) menuRoles.push(148);
      if(val == 154) menuRoles.push(153);
      if(val == 159) menuRoles.push(158);
      if(val == 169) menuRoles.push(168);
      if(val == 174) menuRoles.push(173);
      if(val == 176) menuRoles.push(175);
      if(val == 180) menuRoles.push(179);
      if(val == 184) menuRoles.push(183);
      if(val == 186) menuRoles.push(185);
      if(val == 188) menuRoles.push(187);
    });
    menuRoles = uniq(menuRoles);
    return menuRoles;
  }
  checkRole(value) {

    if(localStorage.getItem('superAdmin') === "true") return true;
    if(value == 999) return false;
    if(value == 0) {
      return true;
    }
    return includes(this.menuRoles, value);
  }
  checkMainRole(values) {
    let rolesId = localStorage.getItem('rolesId').split(',');
    let menuRoles = this.getRoles(rolesId);
    if(localStorage.getItem('superAdmin') === "true") return true;
    for(let i = 0; i < values.length; i++) {
      for(let j = 0; j < menuRoles.length; j++) {
        if(values[i] === menuRoles[j]) return true;
      }
    }
    return false;
  }
}
