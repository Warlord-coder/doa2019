import {
  Component,
  Input,
  OnDestroy,
  Inject,
  ViewEncapsulation
} from '@angular/core';
import {
  Router,
  NavigationStart,
  NavigationEnd,
  NavigationCancel,
  NavigationError
} from '@angular/router';
import {DOCUMENT} from '@angular/common';
import {AuthService} from '../services';
import {first} from 'rxjs/operators';

@Component({
  selector: 'app-spinner',
  template: `
    <div class="preloader  d-flex  align-items-center  justify-content-center" *ngIf="isSpinnerVisible">
      <img src="assets/images/logo-icon.png" alt="homepage" class="spinner-animation"/>
    </div>`,
  encapsulation: ViewEncapsulation.None
})
export class SpinnerComponent implements OnDestroy {
  public isSpinnerVisible = true;

  @Input() public backgroundColor = 'rgba(0, 115, 170, 0.69)';

  constructor(
    private router: Router,
    private authService: AuthService,
    @Inject(DOCUMENT) private document: Document
  ) {

    if (localStorage.getItem('authorization')) {
      this.authService.me()
        .pipe(first())
        .subscribe();
    }
    this.router.events.subscribe(
      event => {
        if (event instanceof NavigationStart) {
          this.isSpinnerVisible = true;
        } else if (
          event instanceof NavigationEnd ||
          event instanceof NavigationCancel ||
          event instanceof NavigationError
        ) {
          this.isSpinnerVisible = false;
        }
      },
      () => {
        this.isSpinnerVisible = false;
      }
    );
  }

  ngOnDestroy(): void {
    this.isSpinnerVisible = false;
  }
}
