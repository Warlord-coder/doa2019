import {Component, Input, OnInit} from '@angular/core';
import {Title} from '@angular/platform-browser';
import {Router, NavigationEnd, ActivatedRoute} from '@angular/router';
import 'rxjs/add/operator/filter';
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/mergeMap';
import {WithdrawService, WarehouseService, DistributionService, LabService, RecoveryService} from '../../services';
import {first, timeout} from 'rxjs/operators';

@Component({
  selector: 'app-breadcrumb',
  templateUrl: './breadcrumb.component.html',
  styleUrls: ['./breadcrumb.component.css']
})
export class BreadcrumbComponent implements OnInit {
  @Input() layout;
  pageInfo;
  breadcrumbs = [];
  constructor(
    private router: Router,
    private activatedRoute: ActivatedRoute,
    private titleService: Title,
    private withDrawService: WithdrawService,
    private distributionService: DistributionService,
    private labService: LabService,
    private recoveryService: RecoveryService,
    private warehouseService: WarehouseService,
  ) {
    this.router.events
      .filter(event => event instanceof NavigationEnd)
      .map(() => this.activatedRoute)
      .map(route => {
        while (route.firstChild) {
          route = route.firstChild;
        }
        return route;
      })
      .filter(route => route.outlet === 'primary')
      .mergeMap(route => route.data)
      .subscribe(event => {
        this.breadcrumbs = event.breadcrumbs;
        if(this.breadcrumbs == undefined) return;
        this.breadcrumbs.forEach(breadcrumb => {
          if(breadcrumb['checkId'] == true) breadcrumb.link = localStorage.getItem('lastUrl');
          if(breadcrumb['checkId1'] == true) breadcrumb.link = localStorage.getItem('lastUrl1');
        });
        this.titleService.setTitle(event.title);
        this.pageInfo = event;
      });
  }
  ngOnInit() {
    this.getStocks();
  }

   getStocks() {
    let withdraw_count = 0;
    let distribution_count = 0;
    let alert_count = 0;
    let recovery_count = 0;
    let alert_draft_number = 0;
    let draft_number = 0;
    let m4_count = 0;
    this.withDrawService.getrequestAll('w9')
      .pipe(first())
      .subscribe(
        ({data = []}: any) => {
          data.forEach(element => {
            withdraw_count += element.pending;
          });
        },
        error => {
        });
    
    this.distributionService.getrequestAll()
      .pipe(first())
      .subscribe(
        ({data = []}: any) => {
          data.forEach(element => {
            distribution_count += element.pending;
          });
        },
        error => {
        });
    
    this.labService.getAlertM9()
      .pipe(first())
      .subscribe(
        ({data = []}: any) => {
          data.forEach(element => {
            alert_count += element.pending;
          });
        },
        error => {
        });
    
    this.recoveryService.getAlertRecoveryM9()
      .pipe(first())
      .subscribe(
        ({data = []}: any) => {
          data.forEach(element => {
            if(element.recovery_status === 'pending'){
              recovery_count++;
            }
          });
        },
        error => {
        });


    this.warehouseService.alertFilterNotDraftList({alert: false})
        .pipe(first())
        .subscribe(
          ({data = []}: any) => {
            data['data'].forEach(async labs => {
              if(labs.length == 0) return;
              let item = {lab_id: '', lab_no: '', total: labs.length, completed: 0, createdAt: '', updatedAt: '', status: (labs.status?labs.status:'Draft')};
              let count = 0;
              labs.forEach(lab => {
                if(lab.wh_status == 'complete') count++;
              });
              item.completed = count;
              
              if(item.completed != item.total) {
                alert_draft_number++;
              }
            });
          },
          error => {
          });
      
    this.warehouseService.alertFilterDraftList({alert: true})
        .pipe(first())
        .subscribe(
          ({data = []}: any) => {
            data['data'].forEach(async labs => {
              if(labs.length == 0) return;
              let item = {lab_id: '', lab_no: '', total: labs.length, completed: 0, createdAt: '', updatedAt: '', status: (labs.status?labs.status:'Draft')};
              let count = 0;
              labs.forEach(lab => {
                if(lab.status == 'complete') count++;
              });
              item.completed = count;
              
              if(item.completed != item.total) {
                draft_number++;
              }
            });
          },
          error => {
          });

    this.recoveryService.listCompleteRecoveryRegister()
          .pipe(first())
          .subscribe(
            ({data = []}: any) => {
              m4_count = data.length;
            },
            error => {
            });    
      setTimeout(() => {
        this.warehouseService.emitStockData({withdraw: withdraw_count, distribution: distribution_count, alert: alert_count, recovery: recovery_count, alert_draft:alert_draft_number, draft: draft_number, m4_count: m4_count});
      }, 6000)
    
    
  }
}
