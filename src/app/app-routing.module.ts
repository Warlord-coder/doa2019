import {Routes} from '@angular/router';
import {NgbModule} from '@ng-bootstrap/ng-bootstrap';

import {FullComponent} from './layouts/full/full.component';

export const Approutes: Routes = [
  {
    path: 'authentication',
    children: [
      {
        path: '',
        loadChildren: () => import('./pages/authentication/authentication.module').then(m => m.AuthenticationModule)
      }
    ]
  },
  {
    path: '',
    component: FullComponent,
    children: [
      {
        path: '',
        loadChildren: () => import('./pages/loggedIn/loggedIn.module').then(m => m.LoggedInModule)
      }
    ]
  }, {
    path: '**',
    redirectTo: '/'
  }
];
