import {Component, OnInit, HostListener} from '@angular/core';
import {Router} from '@angular/router';
import * as moment from 'moment';
import * as momentTimeZone from 'moment-timezone';
import { WarehouseService, DistributionService, WithdrawService, RecoveryService } from '../../services';
import {PerfectScrollbarConfigInterface} from 'ngx-perfect-scrollbar';

import {ROUTES} from '../../shared/sidebar/menu-items';
import {AuthService, RoleService} from '../../services';
import {first} from 'rxjs/operators';

@Component({
  selector: 'app-full-layout',
  templateUrl: './full.component.html',
  styleUrls: ['./full.component.scss']
})
export class FullComponent implements OnInit {
  color = 'defaultdark';
  currentTimeStamp = null;
  showMinisidebar = false;
  userTimezone = momentTimeZone.tz.guess();
  routes = [];
  roles = [];
  roleId = "";
  loading = true;
  public innerWidth: any;

  public config: PerfectScrollbarConfigInterface = {};

  constructor(public router: Router,
              private withDrawService: WithdrawService,
              private recoveryService: RecoveryService,
              private whservice: WarehouseService,
              private des:DistributionService,
              private authService: AuthService,
              private roleService: RoleService) {
  }

  ngOnInit() {
    if (this.router.url === '/') {
      this.router.navigate(['/dashboard/dashboard1']);
    }
    this.handleLayout();
    this.roleId = localStorage.getItem('role');

    this.roleService.getSpecific(this.roleId)
      .pipe(first())
      .subscribe(
        ({data = {}}: any) => {
          const { rolesInfo} = data;
          localStorage.setItem("rolesId", rolesInfo);
          this.roles = rolesInfo;
          this.routes = ROUTES;
        },
        error => {
          this.loading = false;
          // this.apiError = true;
          // this.alertService.error(error);
        });

    this.onGetLabAlertM4();
    this.onGetAlertM5();
    this.onGetDistM5();
    this.onAlertM8();
    setInterval(() => {
      this.currentTimeStamp = moment().format('MMMM DD YYYY, HH:mm:ss');
    }, 1000);
  }

  @HostListener('window:resize', ['$event'])
  onResize(event) {
    this.handleLayout();
  }

  toggleSidebar() {
    this.showMinisidebar = !this.showMinisidebar;
  }

  handleLayout() {
    this.innerWidth = window.innerWidth;
    if (this.innerWidth < 1170) {
      this.showMinisidebar = true;
    } else {
      this.showMinisidebar = false;
    }
  }
  onGetLabAlertM4() {
    this.recoveryService.getAllPendingM4()
      .pipe(first())
      .subscribe(
        ({ data = [] }: any) => {
          let text = document.getElementsByClassName('m4Alert')[0].innerHTML;
          setTimeout(() => {
            document.getElementsByClassName('m4Alert')[0].innerHTML = text+"(" + data.length + ")";
          }, 500);
        },
        error => {

        });
  }
  onGetAlertM5() {
    this.withDrawService.getAllPlantsByStatus('pending')
      .pipe(first())
      .subscribe(
        ({ data = [] }: any) => {
          let text = document.getElementsByClassName('withdrawAlert')[0].innerHTML;
          setTimeout(() => {
            document.getElementsByClassName('withdrawAlert')[0].innerHTML = text+"(" + data.length + ")";
          }, 500);
        },
        error => {
         
        });
  }
  onGetDistM5() {
    this.des.getAllPlantsByStatus('pending')
      .pipe(first())
      .subscribe(
        ({ data = [] }: any) => {
          let text = document.getElementsByClassName('distributionAlert')[0].innerHTML;
          setTimeout(() => {
            document.getElementsByClassName('distributionAlert')[0].innerHTML = text+"(" + data.length + ")";
          }, 500);
        },
        error => {
          
        });
  }
  onAlertM8() {
    this.whservice.getAlertList()
      .pipe(first())
      .subscribe(
        ({data = []}: any) => {
          setTimeout(() => {
            document.getElementsByClassName('m8Alert')[0].innerHTML = "Alert List (รายการแจ้งเตือนฟื้นฟูพืชจากคลัง)(" + data.length + ")";
          }, 500);
        },
        error => {
         
        });
  }
}
