import {Component, EventEmitter, Input, Output} from '@angular/core';

import {AlertService} from '../../services';

@Component({
  selector: 'app-ngbd-popup',
  templateUrl: 'popup.component.html'
})

export class NgbdPopupComponent {
  @Input('title') popoverTitle = 'Approve confirmation';
  @Input('message') popoverMessage = 'Are you sure to approve. Once Approve, it can\'t be undone\n';
  @Input('confirmText') confirmText = 'Confirm';
  @Input('cancelText') cancelText = 'Cancel';
  @Input('placement') placement;
  @Input('icon') icon;
  @Input('btnText') btnText;
  @Input('btnClass') btnClass = 'btn btn-primary btn-sm mt-3';
  @Output() onConfirm = new EventEmitter();
  @Output() onCancel = new EventEmitter();
}

