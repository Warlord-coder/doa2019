import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {FormsModule, ReactiveFormsModule} from '@angular/forms';
import { ConfirmationPopoverModule } from 'angular-confirmation-popover';

import {NgbModule} from '@ng-bootstrap/ng-bootstrap';
import {NgbdAlertBasicComponent} from './alert/alert.component';
import {NgbdPopoverComponent} from './popover/popover.component';
import {NgbdPopupComponent} from './popup/popup.component';


@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    ReactiveFormsModule,
    NgbModule,
    ConfirmationPopoverModule.forRoot({
      confirmButtonType: 'danger' // set defaults here
    })
  ],
  declarations: [
    NgbdAlertBasicComponent,
    NgbdPopoverComponent,
    NgbdPopupComponent
  ],
  exports: [
    NgbdAlertBasicComponent,
    NgbdPopoverComponent,
    NgbdPopupComponent
  ]
})
export class ComponentsModule {}
