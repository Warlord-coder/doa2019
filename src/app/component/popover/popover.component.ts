import {Component, EventEmitter, Input, Output} from '@angular/core';

import {AlertService} from '../../services';

@Component({
  selector: 'app-ngbd-popover',
  templateUrl: 'popover.component.html'
})

export class NgbdPopoverComponent {
  @Input('title') popoverTitle = 'Submit confirmation';
  @Input('message') popoverMessage = 'Are you sure to submit. Once Submitted, it can\'t be undone';
  @Input('confirmText') confirmText = 'Confirm';
  @Input('cancelText') cancelText = 'Cancel';
  @Input('placement') placement;
  @Input('icon') icon;
  @Input('btnText') btnText;
  @Input('btnClass') btnClass = 'btn btn-primary btn-sm mt-3';
  @Output() onConfirm = new EventEmitter();
  @Output() onCancel = new EventEmitter();
}
